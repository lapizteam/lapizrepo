<?php 
include("../parentHeader.php") ;
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

$loginId=$_SESSION['LogID'];
$loginType=$_SESSION['LogType'];

$adNo = $loginId;
			
?>
<script>
function hidesearch()
{
	document.getElementById('from_to').style.display='none';
	document.getElementById('month_year').style.display='block';
}
function showsearch()
{
	document.getElementById('from_to').style.display='block';
	document.getElementById('month_year').style.display='none';
}
</script>

<div class="col-md-10 col-sm-8 rightarea">

	<div class="discussionpage notification_wrap">
		
		<div class=" rightareatop">
            <h2 class="discussion-hd"> DIARY </h2>
        </div>
        <div class="diary_wrap">
        	<div class="diary_search">
        		<form id="diary_search_form" method="post" >
        			<div class="row">
        				
                        <!----------------------------  from to    ------------------------------------------------------->
                        <div id="from_to">
        				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        					<input class="form-control datepicker" name="from_date" placeholder="From" value="<?php echo @$_REQUEST['from_date'];?>" type="text">
        				</div>
        				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        					<input class="form-control datepicker" name="to_date" placeholder="To" value="<?php echo @$_REQUEST['to_date'];?>" type="text">
        				</div>
                        </div>
                        <!--------------------------  month year  -------------------------->
                        <div id="month_year" style="display:none">
        				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        	<select name="month" id="month" class="form-control " >
                            <option value="">Month</option>
                            <?php 
							$selMonth = "select * from ".TABLE_MONTH;
							$selMonthRes = mysql_query($selMonth);
							while($monthRows = mysql_fetch_array($selMonthRes)){
							?>
                             <option value="<?php echo $monthRows['ID']?>" <?php if($monthRows['ID']==@$_POST['month']){echo 'selected';}?>><?php echo $monthRows['monthName'];?></option>
                            <?php }?>
                            </select>
        					
        				</div>
        				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                            <select name="year" id="year" class="form-control " >
                            <option value="">Year</option>
                            <?php 
							$selYear = "select * from ".TABLE_ACADEMICYEAR;
							$selYearRes = mysql_query($selYear);
							while($yearRows = mysql_fetch_array($selYearRes)){
							?>
                             <option value="<?php echo $yearRows['ID']?>" <?php if($yearRows['ID']==@$_POST['year']){echo 'selected';}?>><?php echo $yearRows['fromYear']."-".$yearRows['toYear'];?></option>
                            <?php }?>
                            </select>
        				</div>
                        </div>
        				<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
        					<input name="submit" value="" class="btn btn-default lens" title="Search" type="submit">
        				</div>
        			</div>
        		</form>
        	</div>
            <?php 
				
				
				$cond  = 1;
				$cond1 = 1; //for exam
				$cond2 = 1; //for notifiation
				$cond3 = 1; //for library
				$cond4 = 1; //for attendance
				$cond5 = 1; //for messfee
				$cond6 = 1; //for busfee
				$cond7 = 1; //for fee
				
				if(isset($_POST['submit']))
				{
					$fDate	 = @$_POST['from_date'];
					$tDate 	 = @$_POST['to_date'];
					$sMonth	= @$_POST['month'];
					$sYear	 = @$_POST['year'];
						
					if($fDate && $tDate)
					{
						$cond1 = $cond1." AND ".TABLE_EXAMENTRY.".`examDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."'";
						$cond2 = $cond2." AND ".TABLE_NOTIFICATION.".`addDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."'";
						$cond3 = $cond3." AND ".TABLE_BOOK_ISSUE.".`issueDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."'";
						$cond7 = $cond7." AND ".TABLE_FEEPAYMENT.".`payDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date($tDate)."'";
	
					}
					if($tDate && !$fDate)
					{
						$cond1 = $cond1." AND ".TABLE_EXAMENTRY.".`examDate` BETWEEN '". $App->dbformat_date(date('01/m/Y'))."' AND '". $App->dbformat_date($tDate)."'";
						$cond2 = $cond2." AND ".TABLE_NOTIFICATION.".`addDate` BETWEEN '". $App->dbformat_date(date('01/m/Y'))."' AND '". $App->dbformat_date($tDate)."'";
						$cond3 = $cond3." AND ".TABLE_BOOK_ISSUE.".`issueDate` BETWEEN '". $App->dbformat_date(date('01/m/Y'))."' AND '". $App->dbformat_date($tDate)."'";
						$cond7 = $cond7." AND ".TABLE_FEEPAYMENT.".`payDate` BETWEEN '". $App->dbformat_date(date('01/m/Y'))."' AND '". $App->dbformat_date($tDate)."'";
					}
					if(!$tDate && $fDate)
					{
						$cond1 = $cond1." AND ".TABLE_EXAMENTRY.".`examDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date(date('d/m/Y'))."'";
						$cond2 = $cond2." AND ".TABLE_NOTIFICATION.".`addDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date(date('d/m/Y'))."'";
						$cond3 = $cond3." AND ".TABLE_BOOK_ISSUE.".`issueDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date(date('d/m/Y'))."'";
						$cond7 = $cond7." AND ".TABLE_FEEPAYMENT.".`payDate` BETWEEN '". $App->dbformat_date($fDate)."' AND '". $App->dbformat_date(date('d/m/Y'))."'";
					}
					if($sMonth)
					{
						$cond4 = $cond4." AND ".TABLE_WORKINGDAYS.".acMonth like'%".$sMonth."%'";
						$cond5 = $cond5." AND ".TABLE_MESSFEEPAYMENT.".month like'%".$sMonth."%'";
						$cond6 = $cond6." AND ".TABLE_BUSFEEPAYMENT.".month like'%".$sMonth."%'";
						
					}
					if($sYear)
					{
						$cond4 = $cond4." AND ".TABLE_WORKINGDAYS.".acYear like'%".$sYear."%'";
						$cond5 = $cond5." AND ".TABLE_MESSFEEPAYMENT.".acYear like'%".$sYear."%'";
						$cond6 = $cond6." AND ".TABLE_BUSFEEPAYMENT.".acYear like'%".$sYear."%'";
					}
	
					
				}
				
				//For basic details	
				$basicQuery = "SELECT ".TABLE_STUDENT.".name,".TABLE_STUDENT.".class cl,".TABLE_STUDENT.".division,".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_STUDENT.".photo from ".TABLE_STUDENT.",".TABLE_CLASS.",".TABLE_DIVISION." where ".TABLE_STUDENT.".adNo='$adNo' and ".TABLE_CLASS.".ID = ".TABLE_STUDENT.".class and ".TABLE_DIVISION.".ID=".TABLE_STUDENT.".division";	
				$basicAll= $db->query($basicQuery);	
				$number=mysql_num_rows($basicAll);
				$row1=mysql_fetch_array($basicAll);
				$class = $row1['cl'];

				//Exam details
				$examQuery = "SELECT ".TABLE_MARKENTRY.".mark,".TABLE_MARKENTRY.".percentage,".TABLE_MARKENTRY.".remark,".TABLE_EXAMENTRY.".examDate,".TABLE_EXAMENTRY.".timeFrom,".TABLE_EXAMENTRY.".timeTo,".TABLE_MARKBASIC.".totalMark,".TABLE_MARKBASIC.".minMark,".TABLE_SUBJECT.".subjectName,".TABLE_EXAMTYPES.".examType FROM ".TABLE_MARKENTRY.",".TABLE_MARKBASIC.",".TABLE_EXAMENTRY.",".TABLE_SUBJECT.",".TABLE_EXAMTYPES." WHERE ".TABLE_MARKENTRY.".adNo = '$adNo' AND ".TABLE_MARKBASIC.".ID=".TABLE_MARKENTRY.".markBasicId AND ".TABLE_MARKBASIC.".examName=".TABLE_EXAMTYPES.".ID  AND ".TABLE_MARKBASIC.".examentryId=".TABLE_EXAMENTRY.".ID  AND ".TABLE_MARKBASIC.".subject=".TABLE_SUBJECT.".ID AND $cond1 group by ".TABLE_MARKENTRY.".ID";
				$examAll= $db->query($examQuery);

				//attendance
				$attendanceQuery = "SELECT ".TABLE_ATTENDANCE.".attendance,".TABLE_ATTENDANCE.".remark,".TABLE_WORKINGDAYS.".workingDay,".TABLE_MONTH.".monthName,".TABLE_ACADEMICYEAR.".fromYear,".TABLE_ACADEMICYEAR.".toYear from `".TABLE_ATTENDANCE."`,`".TABLE_WORKINGDAYS."`,`".TABLE_MONTH."`,`".TABLE_ACADEMICYEAR."` where ".TABLE_ATTENDANCE.".workId=".TABLE_WORKINGDAYS.".ID and ".TABLE_WORKINGDAYS.".acMonth =".TABLE_MONTH.".ID and ".TABLE_WORKINGDAYS.".acYear =".TABLE_ACADEMICYEAR.".ID and ".TABLE_ATTENDANCE.".adNo='$adNo' and $cond4 ";
				$attendanceAll = $db->query($attendanceQuery);
				
				// fee details
				$feeQuery = "SELECT payDate,payingAmount FROM ".TABLE_FEEPAYMENT." WHERE admNo='$adNo' and $cond7 order by ".TABLE_FEEPAYMENT.".ID desc";
				
				$feeAll = $db->query($feeQuery);
				
				//mess fee details
				$messFeeQuery = "SELECT ".TABLE_MESSFEEPAYMENT.".payDate ,".TABLE_MESSFEEPAYMENT.".paidAmount,".TABLE_MONTH.".monthName FROM ".TABLE_MESSFEEPAYMENT.",".TABLE_MONTH." WHERE ".TABLE_MESSFEEPAYMENT.".adNo='$adNo' AND ".TABLE_MONTH.".ID=".TABLE_MESSFEEPAYMENT.".month and $cond5 order by ".TABLE_MESSFEEPAYMENT.".ID desc";
				$messFeeAll = $db->query($messFeeQuery);
				
				//busfeedetails
				$busFeeQuery = "SELECT ".TABLE_BUSFEEPAYMENT.".payDate,".TABLE_BUSFEEPAYMENT.".paidAmount,".TABLE_MONTH.".monthName FROM ".TABLE_BUSFEEPAYMENT.",".TABLE_MONTH." WHERE ".TABLE_BUSFEEPAYMENT.".adNo='$adNo' AND ".TABLE_MONTH.".ID=".TABLE_BUSFEEPAYMENT.".month and $cond6 order by ".TABLE_BUSFEEPAYMENT.".ID desc";
				$busFeeAll = $db->query($busFeeQuery);
				
				//Notification
		$notificationQuery = "select ".TABLE_NOTIFICATION.".ID,".TABLE_NOTIFICATION.".event,".TABLE_NOTIFICATION.".discription,".TABLE_NOTIFICATION.".adNo,".TABLE_NOTIFICATION.".addDate
					 from ".TABLE_NOTIFICATION."
					 inner join ".TABLE_NOTIFICATIONCLASS." 
					 on ".TABLE_NOTIFICATIONCLASS.".classId = '$class' where ((".TABLE_NOTIFICATIONCLASS.".eventId = ".TABLE_NOTIFICATION.".ID AND ".TABLE_NOTIFICATION.".showStatus = 'ClassWise') 
					 or ". TABLE_NOTIFICATION.".adNo='$adNo' or ".TABLE_NOTIFICATION.".showStatus ='All') and $cond2  group by ".TABLE_NOTIFICATION.".ID order by  ". TABLE_NOTIFICATION.".ID" ;
					$notificationAll = $db->query($notificationQuery);
					
				//Library details
					$libraryQuery1 = "SELECT ".TABLE_BOOK_REG.".bookName,".TABLE_BOOK_ISSUE.".issueDate,".TABLE_BOOK_ISSUE.".returnedDate FROM ".TABLE_BOOK_ISSUE.",".TABLE_BOOK_REG." WHERE ".TABLE_BOOK_ISSUE.".adNo = '$adNo' and ".TABLE_BOOK_ISSUE.".status = 'delivered' and ".TABLE_BOOK_ISSUE.".bookId=".TABLE_BOOK_REG.".ID and $cond3";
					$libraryAll = $db->query($libraryQuery1);
									
					$libraryQuery2 = "SELECT ".TABLE_BOOK_REG.".bookName,".TABLE_BOOK_ISSUE.".issueDate,".TABLE_BOOK_ISSUE.".returnedDate FROM ".TABLE_BOOK_ISSUE.",".TABLE_BOOK_REG." WHERE ".TABLE_BOOK_ISSUE.".adNo = '$adNo' and ".TABLE_BOOK_ISSUE.".status = 'pending' and ".TABLE_BOOK_ISSUE.".bookId=".TABLE_BOOK_REG.".ID and $cond3 ";
					$libraryPend = $db->query($libraryQuery2);
				
			?>
        	<div class="diary_inner">
        		<div class="diary_nav">
	        		<ul class="diary_navigation">
	        			<li><a href="#basic_det" class="active">basic details</a></li>
	        			<li><a href="#exam_det" onclick="showsearch()">exam details</a></li>
	        			<li><a href="#attendance_det" onclick="hidesearch()">attendance</a></li>
	        			<li><a href="#fee_det" onclick="hidesearch()">fees</a></li>
	        			<li><a href="#notification_det" onclick="showsearch()">notification</a></li>
	        			<li><a href="#library_det" onclick="showsearch()">library</a></li>
	        			<!--<li><a href="#">club activities</a></li>-->
	        		</ul>
	        		<div style="clear: both;"></div>
        		</div>
        		<div class="diary_blocks">
                
                <!----------------------Basic Details---------------->
               		
       			  <div id="basic_det" class="diary_block active">
        				<div class="student_pic">
                        	<?php if($row1['photo']){?>
                        	<img src="../../cpanel/student/<?php echo $row1['photo']; ?>" alt="" width="120" />
                        	<?php }else{?>
                        	<img src="../../img/student.png" alt="" width="120" />
                        	<?php }?>
       				  </div>
        				<div class="student_det">
        					<div class="det_block">
        						<span class="det_head">name</span>
        						:&nbsp;&nbsp;&nbsp;<?php echo $row1['name']; ?>
       					  </div>
        					<div class="det_block">
        						<span class="det_head">class</span>
        						:&nbsp;&nbsp;&nbsp;<?php echo $row1['class']; ?>
       					  </div>
        					<div class="det_block">
        						<span class="det_head">division</span>
        						:&nbsp;&nbsp;&nbsp;<?php echo $row1['division']; ?>
       					  </div>
        				</div>
        				<div style="clear: both;"></div>
        			</div>
                    
                 <!----------------------Exam Details---------------->   
        			<div id="exam_det" class="diary_block">
                    	 <?php	
						 	if(mysql_num_rows($examAll)>0)
							{		
							while($row2=mysql_fetch_array($examAll))
							{
							?>
        				<div class="notify_block">
        					<div class="date_box">
                            <?php 
							
							 $examDate = $row2['examDate'];
							 $examDate2 = date('d/M/y', strtotime($examDate));
							 $examDate1 = explode("/",$examDate2);
							
							?>
        						<?php echo  $examDate1[0];?><br /><?php echo  $examDate1[1]." ".$examDate1[2]?>
        					</div>
        					<div class="notify_det">
   							 <?php echo "Scored ".$row2['mark']." out of ".$row2['totalMark']." for the ". $row2['subjectName']." paper in the ".$row2['examType']." exam."?><br />
        					</div>
        					<div style="clear: both";></div>
        				</div>
                        <?php }}
						else{
						?>
                        
                          <div align="center">  No data</div> 
                            <?php }?>
        			</div>
                    <!----------------------Attendance Details---------------->   
        			<div id="attendance_det" class="diary_block">
                    	 <?php
						 if(mysql_num_rows($attendanceAll)>0)
							{			
							while($row3=mysql_fetch_array($attendanceAll))
							{
							?>
        				<div class="notify_block">
        					<div class="date_box" style="width: 100px;">
                            
        						<?php /*?><?php echo $row3['monthName'];?><br /><?php */?><?php echo  $row3['fromYear']."-".$row3['toYear']?>
        					</div>
        					<div class="notify_det">
   							 <?php echo "On ".$row3['monthName']." out of ".$row3['workingDay']." working days present in ".$row3['attendance']." days"; ?><br />
        					</div>
        					<div style="clear: both";></div>
        				</div>
                        <?php }}else{
						?>
                        
                          <div align="center">  No data</div> 
                            <?php }?>
        			</div>
                    
                    
                    <!----------------------Fee Details---------------->
        			<div id="fee_det" class="diary_block"> 
                    <?php
					if((mysql_num_rows($feeAll)>0) || (mysql_num_rows($busFeeAll)>0) || (mysql_num_rows($messFeeAll)>0)) 
					{
					if(mysql_num_rows($feeAll)>0)
					{
					?>
                    <h4>Fee payment</h4>
                    <?php
					while($row4=mysql_fetch_array($feeAll))
					{
					?>      				
        				<div class="notify_block">
        					<div class="notify_det">
        						<?php echo "Paid Rs.".$row4['payingAmount']." on ".$App->dbformat_date_db($row4['payDate']);?>
        					</div>
        					<div style="clear: both";></div>
        				</div>
                        <?php }}
					
						if(mysql_num_rows($messFeeAll)>0)
						{
						?>
                        <h4>Mess fee payment</h4>
                         <?php
						 
						while($row5=mysql_fetch_array($messFeeAll))
						{
						?>
                        <div class="notify_block">
                        <div class="notify_det">
        						<?php echo "Paid Rs.".$row5['paidAmount']." on ".$App->dbformat_date_db($row5['payDate'])." for the month of ".$row5['monthName'];?>
        					</div>
        					<div style="clear: both";></div>
        				</div>
                        <?php }}
						if(mysql_num_rows($busFeeAll)>0)
					{
					?>
                    <h4>Bus fee payment</h4>
                    <?php
					while($row6=mysql_fetch_array($busFeeAll))
					{
					?>      				
        				<div class="notify_block">
        					<div class="notify_det">
        						<?php echo "Paid Rs.".$row6['paidAmount']." on ".$App->dbformat_date_db($row6['payDate'])." for the month of ".$row6['monthName'];?>
        					</div>
        					<div style="clear: both";></div>
        				</div>
                        <?php }}}else{
						?>
                        
                          <div align="center">  No data</div> 
                            <?php }?>
        			
                    </div>
                    <!----------------------Notifiation Details---------------->
        			<div id="notification_det" class="diary_block"> 
                    <?php
					if(mysql_num_rows($notificationAll)>0)
					{
					while($row9 = mysql_fetch_array($notificationAll))
					{
					 $addDate = $row9['addDate'];
					 $addDate2 = date('d/M/y', strtotime($addDate));
					 $addDate1 = explode("/",$addDate2);
					?>      				
        				<div class="notify_block">
        					<div class="date_box">
        						<?php echo  $addDate1[0];?><br /><?php echo  $addDate1[1]." ". $addDate1[2];?>
        					</div>
        					<div class="notify_det">
        						<?php echo "Notification about '".$row9['event']."' announced on ".$App->dbformat_date_db($row9['addDate']);?>
        					</div>
        					<div style="clear: both";></div>
        				</div>
                        <?php }}else{
						?>
                        
                          <div align="center">  No data</div> 
                            <?php }?>
        			</div>
                     <!----------------------Library Details---------------->
        			<div id="library_det" class="diary_block"> 
                    <?php 
					if((mysql_num_rows($libraryAll)>0) || (mysql_num_rows($libraryPend)>0))
					{
					if(mysql_num_rows($libraryAll)>0)
					{
					?>
                    <h4> Returned books</h4>
                    <?php
					while($row7 = mysql_fetch_array($libraryAll))
					{
					?>      				
        				<div class="notify_block">
        					<div class="notify_det">
        						<?php echo "The book '".$row7['bookName']."' has issued on ".$App->dbformat_date_db($row7['issueDate'])." and returned on ".$App->dbformat_date_db($row7['returnedDate']);?>
        					</div>
        					<div style="clear: both";></div>
        				</div>
                        <?php }}
						if(mysql_num_rows($libraryPend)>0)
						{
						?>
                        <h4>Pending books</h4>
                         <?php
						while($row8 = mysql_fetch_array($libraryPend))
						{
						?>
                        <div class="notify_block">
                        <div class="notify_det">
        						<?php echo "The book '".$row8['bookName']."' has issued on ".$App->dbformat_date_db($row8['issueDate']);?>
        					</div>
        					<div style="clear: both";></div>
        				 </div>
                        <?php }}}else{
						?>
                        
                          <div align="center">  No data</div> 
                            <?php }?>
                       
        			</div>
                    <!----------------------End Library-------------------->
        			</div>
                    
                    
        		</div>
        	</div>
           
        </div>
		
	</div>
     </div>

   <?php include("../parentFooter.php") ?>
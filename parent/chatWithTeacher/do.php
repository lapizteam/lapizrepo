<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['message'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();											
				$success=0;
				$senderType = "parent";
				$recieverType = "teacher";
				
				$data['acYear']		 =	$App->convert($_REQUEST['year']);
				$data['message']		=	$App->convert($_REQUEST['message']);
				$data['description']	=	$App->convert($_REQUEST['description']);
				$data['senderId']	   =	$App->convert($_REQUEST['adNo']);
				$data['senderType']	 =	$App->convert($senderType);
				$data['recieverType']   =	$App->convert($recieverType);
				$data['recieverId']	 =	$App->convert($_REQUEST['teacher']);
				$data['sendDate']	   =    date("Y-m-d");
										
				$success=$db->query_insert(TABLE_TEACHERCHAT, $data);								
				$db->close();
					if($success)
					{
					$_SESSION['msg']="Message Send Successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}
				
			}		
		break;
		
		case 'reply':
		
		if(!$_REQUEST['reply'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();											
				$success=0;
				$senderType = "parent";
				$recieverType = "teacher";
				
				$data['messageId']	  =	$App->convert($_REQUEST['messageId']);
				$data['reply']		  =	$App->convert($_REQUEST['reply']);
				$data['senderId']	   =	$App->convert($_REQUEST['senderId']);
				$data['senderType']	 =	$App->convert($senderType);
				$data['recieverType']   =	$App->convert($recieverType);
				$data['recieverId']	 =	$App->convert($_REQUEST['recieverId']);
				$data['sendDate']	   =    date("Y-m-d");
										
				$success=$db->query_insert(TABLE_REPLY, $data);								
				$db->close();
					if($success)
					{
					$_SESSION['msg']="Message Send Successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}
				
			}		
		break;		
		
		
}
?>
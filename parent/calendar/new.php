<?php

	include("../parentHeader.php") ;
	if($_SESSION['LogID']=="")
	{
	header("location:../../logout.php");
	}
	$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
	$db->connect();
	$loginId=$_SESSION['LogID'];
	$loginType=$_SESSION['LogType'];


$select = mysql_query("select * from ".TABLE_EVENT."");
$results = array();
while($row=mysql_fetch_array($select))
		{
				$results[] = $row;
		}
$leng=mysql_num_rows($select);

?>

<link rel='stylesheet' href='agenda/lib/cupertino/jquery-ui.min.css' />
<link href='agenda/fullcalendar.css' rel='stylesheet' />
<link href='agenda/fullcalendar.print.css' rel='stylesheet' media='print' />
<script src='agenda/lib/moment.min.js'></script>
<script src='agenda/lib/jquery.min.js'></script>
<script src='agenda/fullcalendar.min.js'></script>
<script src='agenda/lang-all.js'></script>
<script>
	$(document).ready(function() {
		
		
		var currentLangCode = 'en';

		// build the language selector's options
		$.each($.fullCalendar.langs, function(langCode) {
			$('#lang-selector').append(
				$('<option/>')
					.attr('value', langCode)
					.prop('selected', langCode == currentLangCode)
					.text(langCode)
			);
		});

		// rerender the calendar when the selected option changes
		$('#lang-selector').on('change', function() {
			if (this.value) {
				currentLangCode = this.value;
				$('#calendar').fullCalendar('destroy');
				renderCalendar();
			}
		});
		
		function renderCalendar() {
		$('#calendar').fullCalendar({
			theme: true,
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
			defaultDate: '<?php echo date("Y-m-d"); ?>',
			editable: false,
			lang: currentLangCode,
			eventLimit: true, // allow "more" link when too many events
			events: [

						
			<?php for($i=0;$i< $leng;$i++)
				{
					$dateFrom	=	$results[$i][3];	
					$dateTo		=	$results[$i][3];
					$dateTo 	= date('Y-m-d', strtotime($dateTo .' +1 day'));	
			
					$begin = new DateTime($dateFrom);
					$end = new DateTime($dateTo);
	
					$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
					foreach($daterange as $date)
					{
						$dt[$i]=$date->format("Y-m-d");
						?>
						{
							title: '<?php echo $results[$i][2]; ?>',
							start: '<?php  echo $results[$i][3]; ?>',
							url: '../eventView/view.php?eid=<?php echo $results[$i][3]; ?>',
							color:'<?php echo '#'.str_pad(dechex(mt_rand(0,0xFFFFFF)),6,'0',STR_PAD_LEFT);?>',
							overlap: false,
							
						},
						<?php
					}
				}
						?>
												
			]
		});
		
		}
		renderCalendar();
		
	});

</script>
<style>
	body {
		
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
	}
	
	#top {
		background: #eee;
		border-bottom: 1px solid #ddd;
		padding: 0 10px;
		line-height: 40px;
		font-size: 13px;
	}

	#calendar {
		max-width: 900px;
		margin: 0 auto;
	}
	.tablearea3 td {
    padding: 18px 15px !important;}

</style>

<div class="col-md-10 col-sm-8 rightarea">
        <div class="student-shedule">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>EVENT CALENDAR</h2>
            </div>
              
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div style="">
        
		
		<!--    Calender   -->
						<div id='calendar'><div id='top'>
					
							Language:
							<select id='lang-selector'></select>
					
						</div></div>
		
			 
              </div>
            </div>
          </div>
        </div>
      </div>
    <?php include("CalenderFooter.php") ?>
     </div>
     </div>
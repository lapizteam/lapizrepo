<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Lapiz</title>

<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/style.css" rel="stylesheet">
<link href="../../css/newstyle.css" rel="stylesheet">
<link href="../../css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="../../css/owl.carousel.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../../font-awesome/css/font-awesome.min.css" />
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bgmain">
<div id="header">


 <div class="container">
   <a href="#" class="logo"></a>
   <div class="navigation">
    <div class="dropdown">
	 <a href="../logout.php">Logout</a>
  <!--  Settings
    <span class="caret"></span>
  </a>

  <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
  <li><a href="#">Change Password</a></li>
     <li><a href="../logout.php">Logout</a></li>
  </ul>
</div>-->
   </div>
   
   </div>
   </div>
   <div class="clearer"></div>
 

<div id="contentarea">
  <div class="container">
     <div class="row">
      <div class="col-md-2 col-sm-4 leftnav">
        <div class="leftmenu">
         <ul  role="menu">
		 <li>
             <a href="../parentDash/parentDash.php">
               <span class="menuicons icn1"></span>Home
              </a>           
          </li>
            <li>
             <a href="../calendar/new.php">
               <span class="menuicons icn6"></span>Calendar
              </a>           
          </li>
          <li>
             <a href="../diary/new.php">
               <span class="menuicons icn8"></span>Student Diary
              </a>           
          </li>
          
          <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn5"></span>Class Teacher</a>
               <ul class="dropdown-menu dropdown2" role="menu">      
			 	<li><a href="../teacherProfile/new.php">Contacts</a></li>
                <li><a href="../chatWithTeacher/new.php">Chat</a></li>
                </ul>
          </li>
		  
		 <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn5"></span>Fee Details</a>
               <ul class="dropdown-menu dropdown2" role="menu">      
			 	<li><a href="../feeDetails/new.php">Class Fee</a></li>
                <li><a href="../feeDetails/messFee.php">Mess Fee</a></li>
                <li><a href="../feeDetails/busFee.php">Bus Fee</a></li>
                </ul>
          </li>
		 <li>
             <a href="../markDetails/new.php">
               <span class="menuicons icn2"></span>Mark Details
              </a>           
          </li>
          <li>
             <a href="../attendanceDetails/new.php">
               <span class="menuicons icn4"></span>Attendance Details
              </a>           
          </li>
         <li>
             <a href="../library/new.php">
               <span class="menuicons icn8"></span>Library
              </a>           
          </li>
		  		                    
         </ul>
        </div>
      </div>
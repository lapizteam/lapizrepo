<?php
 
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['vfw_cpasecpass']=="")
{
header("location:logout.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include('inc_title.php'); ?>
<link href="../css/style2.css" rel="stylesheet" type="text/css" />
<link href="../images/logo.ico" type="image/x-icon"  rel="shortcut icon" />
<!--[if lt IE 7]>
<style type="text/css">
	 img { behavior: url(iepngfix.htc); }
</style>
<![endif]-->
<script language="javascript"  type="text/javascript" src="../../validation/validate.js"></script></head>
<body>
<div id="main">
  <!--Header starts-->
    <?php include('inc_banner.php'); ?>
  <!--Header ends-->
  <!--Content starts-->
  <div id="container">
    <div id="content">
      <div id="page">
        <div id="page_inner">
          <div id="row1">
            <div id="col1">
                      
			   <div style="text-align:center;">
                 <a href="../pages/news/list.php"><img src="../images/news.jpg" alt="News" width="60" height="60" border="0" /></a>
				
				 </div>
				   <div style="text-align:center;"><a href="../pages/news/list.php">News</a></div></br>
        		   <div style="text-align:center;">
                 <a href="../pages/gallery/list.php"><img src="../images/portfolio.jpg" alt="News" width="60" height="60" border="0" /></a></div>       
				 <div style="text-align:center;"><a href="../pages/gallery/list.php">Gallery</a></div></br>
				 	   <div style="text-align:center;">
                 <a href="change_password.php"><img src="../images/change_password.jpg" alt="News" width="60" height="60" border="0" /></a></div>
				 <div style="text-align:center;"><a href="change_password.php">Change Password</a></div></br>
				 
				 
				 				 	   <div style="text-align:center;">
                 <a href="change_username.php"><img src="../images/username.png" alt="News" width="60" height="60" border="0" /></a></div>
				 <div style="text-align:center;"><a href="change_username.php">Change Username</a></div></br>
				 
				 	   <div style="text-align:center;">
                 <a href="logout.php"><img src="../images/logout.jpg" alt="News" width="60" height="60" border="0" /></a></div> 
				  <div style="text-align:center;"><a href="logout.php">Logout</a></div></br>
              
          

            </div>
            <div id="col2">
              <h2>Change Username </h2>
              <div class="divcontent">
                <form action="do.php" method="post" enctype="multipart/form-data" name="chpassword" id="chpassword" onsubmit="return validate('chpassword');">
                  <table width="100%" border="0">
                   <?php 
	  if(isset($_REQUEST['mes']))
	  {
	  ?>
                    <tr>
                      <td colspan="2" align="center"><font color="red"><?php echo $_REQUEST['mes']; ?></font></td>
                    </tr>
                    <?php
		}
		
		?>
			
                    <tr>
                      <td>Current User Name <span class="red">*</span></td>
                      <td><input name="uname" type="text" class="widther2" id="uname" alt="required" title="Current User Name" ></td>
                    </tr>
                    
                    <tr>
                      <td>New User Name <span class="red">*</span></td>
                      <td><input name="nuname" type="text" class="widther2" id="nuname" alt="required" title="New User Name "></td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="26%"> Password <span class="red">*</span> </td>
                      <td width="74%"><input name="cpass" type="password" class="widther2" id="cpass" alt="required" title="Current Password" ></td>
                    </tr>
                    
                    
                    <tr>
                      <td><input type="hidden" name="op" value="usernamechange" /></td>
                      <td><input type="submit" name="Submit" value="CHANGE" class="btn" ></td>
                    </tr>
                  </table>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--Content ends-->
  <!--Footer starts-->
  <div id="footer">
    <?php include('inc_footer.php') ; ?></div>
  </div>
  <!--Footer ends-->
</div>
</body>
</html>

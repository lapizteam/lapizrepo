<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['vfw_cpasecpass']=="")
{
header("location:logout.php");
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php include('inc_title.php'); ?>
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../images/logo.ico" type="image/x-icon"  rel="shortcut icon" />
<!--[if lt IE 7]>
<style type="text/css">
	 img { behavior: url(iepngfix.htc); }
</style>
<![endif]-->
</head>
<body>
<div id="main">
  <!--Header starts-->
  <?php include('inc_banner.php'); ?>
  <!--Header ends-->
  <!--Content starts-->
  <div id="container">
    <div id="content">
      <div id="page">
        <div id="page_inner">
		<div class="iconcontainer">
              <div class="icon"><a href="../pages/news/new.php"><img src="../images/news.jpg" alt="News" width="50" height="50" border="0" /></a></div>
              <div class="iconlink"><a href="../pages/news/new.php">News</a></div>
            </div>
   
			<div class="iconcontainer">
              <div class="icon"><a href="../pages/gallery/new.php"><img src="../images/portfolio.jpg" alt="Portfolio" width="50" height="50" border="0" /></a></div>
              <div class="iconlink"><a href="../pages/gallery/new.php">Gallery</a> </div>
            </div>
			
			<div class="iconcontainer">
              <div class="icon"><a href="change_password.php"><img src="../images/change_password.jpg" alt="Change Password" width="50" height="50" border="0" /></a></div>
              <div class="iconlink"><a href="change_password.php">Change Password </a></div>
            </div>
            <div class="iconcontainer">
              <div class="icon"><a href="logout.php"><img src="../images/logout.jpg" alt="Logout" border="0" /></a></div>
              <div class="iconlink"><a href="logout.php">Logout</a></div>
            </div>
 
		 
        </div>
      </div>
    </div>
  </div>
  <!--Content ends-->
  <!--Footer starts-->
  <div id="footer">
    <?php include('inc_footer.php') ; ?></div>
  </div>
  <!--Footer ends-->
</div>
</body>
</html>
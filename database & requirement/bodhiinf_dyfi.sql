-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: May 29, 2017 at 11:31 AM
-- Server version: 5.5.54-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bodhiinf_dyfi`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `ID` int(11) NOT NULL,
  `schoolName` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `eMail` varchar(50) NOT NULL,
  `website` varchar(50) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`ID`, `schoolName`, `location`, `address`, `phone`, `mobile`, `eMail`, `website`, `loginId`) VALUES
(1, 'BODHI INFO SOLUTION', 'Calicut', 'Kuniyil Kavu Jn\r\nCalicut', '0495-24550551', '+919876655432', 'bodhiinfos@gmail.com', 'www.bodhiinfo.com', 1);

-- --------------------------------------------------------

--
-- Table structure for table `academicyear`
--

CREATE TABLE `academicyear` (
  `ID` int(11) NOT NULL,
  `fromYear` varchar(20) NOT NULL,
  `toYear` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `academicyear`
--

INSERT INTO `academicyear` (`ID`, `fromYear`, `toYear`, `status`) VALUES
(1, '2015', '2016', 'NO'),
(2, '2014', '2015', 'NO'),
(3, '2016', '2017', 'YES');

-- --------------------------------------------------------

--
-- Table structure for table `account_group`
--

CREATE TABLE `account_group` (
  `ID` int(11) NOT NULL,
  `name` text NOT NULL,
  `parent` int(11) NOT NULL,
  `proid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_group`
--

INSERT INTO `account_group` (`ID`, `name`, `parent`, `proid`) VALUES
(1, 'Account', 0, 0),
(2, 'Balance Sheet', 1, 0),
(3, 'Profit & Loss Account', 1, 0),
(4, 'Assets', 2, 0),
(5, 'Liabilities', 2, 0),
(6, 'Current Assets', 4, 0),
(7, 'Fixed Assets', 4, 0),
(8, 'Cash In Hand', 6, 0),
(9, 'Deposits', 6, 0),
(10, 'Investments', 6, 0),
(11, 'Loans & Advances (asset)', 6, 0),
(12, 'Bank A/c', 6, 0),
(13, 'Expenses', 3, 0),
(14, 'Income', 3, 0),
(15, 'Cash', 8, 0),
(16, 'Capital', 5, 0),
(17, 'Current Liabilities', 5, 0),
(18, 'Loans (liability)', 5, 0),
(19, 'Suspense Accounts', 5, 0),
(20, 'Bank OD A/C', 17, 0),
(21, 'Duties & Taxes', 17, 0),
(22, 'Journal Accounts', 17, 0),
(23, 'Provisions', 17, 0),
(24, 'Direct Expense', 13, 0),
(25, 'Indirect Expense', 13, 0),
(26, 'Direct Income', 14, 0),
(27, 'Indirect Income', 14, 0),
(28, 'Fittings', 7, 0),
(29, 'Advertisement', 25, 0),
(30, 'Donation Given', 25, 0),
(31, 'Electricity Bills', 25, 0),
(32, 'Miscellaneous Expense', 25, 0),
(33, 'Office Expense', 25, 0),
(34, 'Other Expenses', 25, 0),
(35, 'Telephone Bills', 25, 0),
(36, 'Other Income', 27, 0);

-- --------------------------------------------------------

--
-- Table structure for table `acmonth`
--

CREATE TABLE `acmonth` (
  `ID` int(11) NOT NULL,
  `monthName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acmonth`
--

INSERT INTO `acmonth` (`ID`, `monthName`) VALUES
(1, 'January'),
(2, 'February'),
(3, 'March'),
(4, 'April'),
(5, 'May'),
(6, 'June'),
(7, 'July'),
(8, 'August'),
(9, 'September'),
(10, 'October'),
(11, 'November'),
(12, 'December');

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `ID` int(11) NOT NULL,
  `workId` int(11) NOT NULL,
  `divisionId` int(11) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `attendance` int(11) NOT NULL,
  `remark` text NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`ID`, `workId`, `divisionId`, `adNo`, `attendance`, `remark`, `loginId`) VALUES
(1, 1, 1, '1001', 26, '', 1),
(2, 1, 1, '1002', 26, '', 1),
(3, 1, 1, '1007', 26, '', 1),
(4, 1, 1, '1008', 26, '', 1),
(5, 1, 1, '1009', 26, '', 1),
(6, 1, 1, '1010', 20, '', 1),
(7, 1, 1, '1011', 26, '', 1),
(8, 1, 1, '1012', 26, '', 1),
(9, 1, 1, '1013', 26, '', 1),
(10, 1, 1, '1014', 25, '', 1),
(11, 1, 1, '1015', 26, '', 1),
(12, 1, 1, '1016', 26, '', 1),
(13, 1, 1, '1017', 26, '', 1),
(14, 1, 1, '1064', 26, '', 1),
(15, 1, 1, '1065', 26, '', 1),
(16, 1, 3, '1006', 26, '', 1),
(17, 1, 3, '1576', 26, '', 1),
(18, 1, 3, '1577', 26, '', 1),
(19, 1, 3, '1578', 26, '', 1),
(20, 1, 3, '1579', 26, '', 1),
(21, 1, 3, '1580', 26, '', 1),
(22, 1, 3, '1581', 26, '', 1),
(23, 1, 3, '1582', 26, '', 1),
(24, 1, 3, '1583', 26, '', 1),
(25, 1, 3, '1584', 26, '', 1),
(26, 1, 3, '1585', 26, '', 1),
(27, 1, 3, '1586', 26, '', 1),
(28, 1, 3, '1587', 26, '', 1),
(29, 1, 3, '1588', 26, '', 1),
(30, 1, 3, '1589', 26, '', 1),
(31, 1, 2, '1003', 26, '', 1),
(32, 1, 2, '1004', 26, '', 1),
(33, 1, 2, '1005', 26, '', 1),
(34, 1, 2, '1066', 26, '', 1),
(35, 1, 2, '1565', 26, '', 1),
(36, 1, 2, '1566', 26, '', 1),
(37, 1, 2, '1567', 26, '', 1),
(38, 1, 2, '1568', 26, '', 1),
(39, 1, 2, '1569', 26, '', 1),
(40, 1, 2, '1570', 26, '', 1),
(41, 1, 2, '1571', 26, '', 1),
(42, 1, 2, '1572', 26, '', 1),
(43, 1, 2, '1573', 26, '', 1),
(44, 1, 2, '1574', 26, '', 1),
(45, 1, 2, '1575', 0, '', 1),
(46, 2, 1, '1001', 25, '', 1),
(47, 2, 1, '1002', 25, '', 1),
(48, 2, 1, '1007', 25, '', 1),
(49, 2, 1, '1008', 25, '', 1),
(50, 2, 1, '1009', 22, '', 1),
(51, 2, 1, '1010', 25, '', 1),
(52, 2, 1, '1011', 25, '', 1),
(53, 2, 1, '1012', 25, '', 1),
(54, 2, 1, '1013', 25, '', 1),
(55, 2, 1, '1014', 25, '', 1),
(56, 2, 1, '1015', 25, '', 1),
(57, 2, 1, '1016', 25, '', 1),
(58, 2, 1, '1017', 25, '', 1),
(59, 2, 1, '1064', 25, '', 1),
(60, 2, 1, '1065', 25, '', 1),
(61, 3, 1, '1001', 15, '', 1),
(62, 3, 1, '1002', 15, '', 1),
(63, 3, 1, '1007', 15, '', 1),
(64, 3, 1, '1008', 15, '', 1),
(65, 3, 1, '1009', 15, '', 1),
(66, 3, 1, '1010', 15, '', 1),
(67, 3, 1, '1011', 15, '', 1),
(68, 3, 1, '1012', 15, '', 1),
(69, 3, 1, '1013', 15, '', 1),
(70, 3, 1, '1014', 15, '', 1),
(71, 3, 1, '1015', 15, '', 1),
(72, 3, 1, '1016', 15, '', 1),
(73, 3, 1, '1017', 15, '', 1),
(74, 3, 1, '1064', 15, '', 1),
(75, 3, 1, '1065', 15, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bookfinepayment`
--

CREATE TABLE `bookfinepayment` (
  `ID` int(11) NOT NULL,
  `bookIssueId` int(11) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `delayDays` int(11) NOT NULL,
  `fineAmount` float NOT NULL,
  `paymentDate` date NOT NULL,
  `remark` text NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookfinepayment`
--

INSERT INTO `bookfinepayment` (`ID`, `bookIssueId`, `adNo`, `delayDays`, `fineAmount`, `paymentDate`, `remark`, `loginId`) VALUES
(1, 2, '1002', 5, 10, '2015-11-12', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `bookreg`
--

CREATE TABLE `bookreg` (
  `ID` int(11) NOT NULL,
  `regDate` date NOT NULL,
  `bookNo` varchar(20) NOT NULL,
  `bookName` varchar(50) NOT NULL,
  `author` varchar(50) NOT NULL,
  `publisher` varchar(50) NOT NULL,
  `category` int(11) NOT NULL,
  `price` float NOT NULL,
  `rack` int(11) NOT NULL,
  `remark` text NOT NULL,
  `availability` varchar(50) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookreg`
--

INSERT INTO `bookreg` (`ID`, `regDate`, `bookNo`, `bookName`, `author`, `publisher`, `category`, `price`, `rack`, `remark`, `availability`, `loginId`) VALUES
(1, '2015-10-28', '1122', 'Wings of Fire', 'A. P. J. Abdul Kalam', '', 1, 285, 2, '', 'issued', 1),
(2, '2015-10-28', '1123', 'Pathummayude aadu', 'Muhammad Basheer', '', 2, 0, 3, '', 'available', 1),
(3, '2015-11-12', '1124', 'Sopanam', 'Balamany Amma', 'abc', 3, 0, 1, '', 'available', 1),
(4, '2015-11-12', '1125', 'Odayil ninn', 'P.Keshavadhev', 'abc', 2, 0, 3, '', 'available', 1),
(5, '2015-11-12', '1126', 'Veenapoovu', 'Kumaranasan', 'xyz', 3, 0, 1, '', 'available', 1),
(6, '2015-11-12', '1127', 'Three days to see', 'Helan', '', 1, 0, 2, '', 'issued', 1),
(7, '2015-11-12', '1128', 'Harry Potter and the Philosopher\'s Stone', 'J K Rowling', '', 2, 0, 3, '', 'available', 1),
(8, '2015-11-12', '1129', 'The Cat in the Hat', 'Dr Seuss', '', 2, 0, 3, '', 'available', 1),
(9, '2015-11-12', '1130', 'The Hunger Games', 'Suzanne Collins', '', 2, 0, 3, '', 'issued', 1),
(10, '2015-11-12', '1131', 'Mr. Wuffles!', 'David Wiesner', 'Roaring Brook, 2014.', 2, 0, 3, '', 'available', 1),
(11, '2015-11-12', '1132', 'Race on the River', 'Scott Nickel', 'Stone Arch Books, 2011', 2, 0, 3, '', 'issued', 1);

-- --------------------------------------------------------

--
-- Table structure for table `book_category`
--

CREATE TABLE `book_category` (
  `ID` int(11) NOT NULL,
  `category` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_category`
--

INSERT INTO `book_category` (`ID`, `category`) VALUES
(1, 'Autobiography'),
(2, 'Story'),
(3, 'Poem');

-- --------------------------------------------------------

--
-- Table structure for table `book_issue`
--

CREATE TABLE `book_issue` (
  `ID` int(11) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `bookId` int(11) NOT NULL,
  `issueDate` date NOT NULL,
  `returnDate` date NOT NULL,
  `status` varchar(15) NOT NULL,
  `returnedDate` date NOT NULL DEFAULT '0000-00-00',
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_issue`
--

INSERT INTO `book_issue` (`ID`, `adNo`, `bookId`, `issueDate`, `returnDate`, `status`, `returnedDate`, `loginId`) VALUES
(1, '1001', 1, '2015-10-28', '2015-11-07', 'delivered', '2015-10-28', 1),
(2, '1002', 2, '2015-10-28', '2015-11-07', 'delivered', '2015-11-12', 1),
(3, '1014', 3, '2015-11-12', '2015-11-22', 'delivered', '2015-11-12', 1),
(4, '1014', 9, '2015-11-12', '2015-11-22', 'pending', '0000-00-00', 1),
(5, '1007', 6, '2015-11-12', '2015-11-22', 'pending', '0000-00-00', 1),
(6, '1008', 4, '2015-11-12', '2015-11-22', 'delivered', '2016-06-29', 1),
(7, '1009', 1, '2015-11-12', '2015-11-22', 'pending', '0000-00-00', 1),
(8, '1011', 11, '2015-11-12', '2015-11-22', 'pending', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `book_return_days`
--

CREATE TABLE `book_return_days` (
  `ID` int(11) NOT NULL,
  `noOfDays` int(11) NOT NULL,
  `noOfBooks` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_return_days`
--

INSERT INTO `book_return_days` (`ID`, `noOfDays`, `noOfBooks`) VALUES
(1, 10, 2);

-- --------------------------------------------------------

--
-- Table structure for table `busallocation`
--

CREATE TABLE `busallocation` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `stageId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `busallocation`
--

INSERT INTO `busallocation` (`ID`, `acYear`, `adNo`, `stageId`, `loginId`) VALUES
(1, 1, '1001', 1, 1),
(2, 1, '1570', 1, 1),
(3, 1, '1014', 1, 1),
(4, 1, '1007', 3, 1),
(5, 1, '1065', 4, 1),
(6, 1, '1582', 3, 1),
(7, 1, '1577', 4, 1),
(8, 1, '1580', 2, 1),
(9, 1, '1616', 1, 1),
(10, 1, '1620', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `busfeepayment`
--

CREATE TABLE `busfeepayment` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `payDate` date NOT NULL,
  `voucherNo` bigint(20) NOT NULL,
  `paidAmount` float NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `busfeepayment`
--

INSERT INTO `busfeepayment` (`ID`, `acYear`, `month`, `adNo`, `payDate`, `voucherNo`, `paidAmount`, `loginId`) VALUES
(1, 1, 1, '1570', '2015-10-28', 1, 200, 1),
(2, 1, 1, '1620', '2015-11-12', 2, 250, 1),
(3, 1, 1, '1616', '2015-11-12', 3, 200, 1),
(4, 1, 1, '1582', '2015-11-12', 4, 250, 1),
(5, 1, 1, '1014', '2015-11-12', 5, 200, 1),
(6, 1, 1, '1065', '2015-11-12', 6, 250, 1),
(7, 1, 1, '1001', '2015-11-21', 7, 200, 1);

-- --------------------------------------------------------

--
-- Table structure for table `class`
--

CREATE TABLE `class` (
  `ID` int(5) NOT NULL,
  `class` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `class`
--

INSERT INTO `class` (`ID`, `class`) VALUES
(1, 'On'),
(2, 'Two'),
(3, 'Three'),
(4, 'Tenth'),
(5, 'Plus One'),
(6, 'Plus Two');

-- --------------------------------------------------------

--
-- Table structure for table `classmessfee`
--

CREATE TABLE `classmessfee` (
  `ID` int(11) NOT NULL,
  `messFeeSetId` int(11) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classmessfee`
--

INSERT INTO `classmessfee` (`ID`, `messFeeSetId`, `adNo`, `loginId`) VALUES
(8, 5, '1006', 1),
(9, 5, '1576', 1),
(10, 5, '1577', 1),
(11, 5, '1578', 1),
(12, 5, '1579', 1),
(13, 6, '1606', 1),
(14, 6, '1607', 1),
(15, 6, '1608', 1),
(16, 6, '1609', 1),
(17, 6, '1611', 1),
(18, 1, '1003', 1),
(19, 1, '1004', 1),
(20, 1, '1005', 1),
(21, 1, '1001', 1),
(22, 1, '1002', 1),
(23, 1, '1007', 1),
(24, 1, '1008', 1),
(25, 1, '1009', 1),
(26, 1, '1014', 1);

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

CREATE TABLE `club` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `clubName` varchar(20) NOT NULL,
  `staffIn` int(11) NOT NULL,
  `description` text NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `club`
--

INSERT INTO `club` (`ID`, `acYear`, `clubName`, `staffIn`, `description`, `loginId`) VALUES
(1, 1, 'Science', 1, '', 1),
(2, 1, 'Maths', 2, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `club_students`
--

CREATE TABLE `club_students` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `clubId` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `division` int(11) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `club_students`
--

INSERT INTO `club_students` (`ID`, `acYear`, `clubId`, `class`, `division`, `adNo`, `loginId`) VALUES
(3, 1, 2, 1, 1, '1001', 1),
(4, 1, 2, 1, 1, '1002', 1),
(5, 1, 2, 1, 1, '1007', 1),
(6, 1, 2, 1, 1, '1008', 1),
(7, 1, 2, 1, 1, '1009', 1),
(8, 1, 1, 1, 1, '1001', 1),
(9, 1, 1, 1, 1, '1002', 1),
(10, 1, 1, 1, 1, '1007', 1),
(11, 1, 1, 1, 1, '1008', 1),
(12, 1, 1, 1, 2, '1003', 1),
(13, 1, 1, 1, 2, '1004', 1),
(14, 1, 1, 1, 2, '1005', 1),
(15, 1, 1, 1, 2, '1066', 1),
(16, 1, 2, 1, 2, '1003', 1),
(17, 1, 2, 1, 2, '1004', 1),
(18, 1, 2, 1, 2, '1005', 1),
(19, 1, 2, 1, 2, '1066', 1),
(20, 1, 1, 2, 3, '1006', 8),
(21, 1, 1, 2, 3, '1576', 8),
(22, 1, 1, 2, 3, '1577', 8);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `ID` int(11) NOT NULL,
  `districtName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`ID`, `districtName`) VALUES
(1, 'Alappuzha'),
(2, 'Ernakulam'),
(3, 'Idukki'),
(4, 'Kannur'),
(5, 'Kasaragod'),
(6, 'Kollam'),
(7, 'Kottayam'),
(8, 'Kozhikode'),
(9, 'Malappuram'),
(10, 'Palakkad'),
(11, 'Pathanamthitta'),
(12, 'Thrissur'),
(13, 'Trivandrum'),
(14, 'Wayanad');

-- --------------------------------------------------------

--
-- Table structure for table `division`
--

CREATE TABLE `division` (
  `ID` int(5) NOT NULL,
  `classId` int(5) NOT NULL,
  `division` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `division`
--

INSERT INTO `division` (`ID`, `classId`, `division`) VALUES
(1, 1, 'A'),
(2, 1, 'B'),
(3, 2, 'A'),
(4, 2, 'B'),
(5, 3, 'A'),
(6, 3, 'B'),
(7, 4, 'A'),
(8, 4, 'B'),
(9, 5, 'SCIENCE - '),
(10, 5, 'SCIENCE - '),
(11, 6, 'SCIENCE - '),
(12, 6, 'COMMERCE');

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

CREATE TABLE `event` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `event` text NOT NULL,
  `eventDate` date NOT NULL,
  `discription` text NOT NULL,
  `addDate` date NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`ID`, `acYear`, `event`, `eventDate`, `discription`, `addDate`, `loginId`) VALUES
(1, 1, 'annual day celebration', '2016-01-20', '2016  January 20 Wednesday. Programs starts at 10 AM.', '2015-10-28', 1),
(2, 1, 'Diwali celebration', '2015-11-22', '10 AM - 12 PM', '2015-10-28', 1),
(3, 1, 'Science club inauguration', '2015-11-22', '1 pm - 2 pm', '2015-10-28', 1),
(4, 1, 'Half Yearly exam starting', '2015-08-03', 'For all classes except 1st std', '2015-11-12', 1),
(5, 1, 'Onam Celebration', '2015-08-14', '16 th Holiday starting', '2015-11-12', 1),
(6, 1, 'Annual Exam starting', '2016-03-15', 'for all classes on  same date', '2015-11-12', 1),
(7, 1, 'X mas celebration', '2015-12-21', 'Half day', '2015-11-12', 1),
(8, 1, 'christmas', '2016-12-25', 'holiday', '2016-12-15', 1),
(9, 1, 'annual day', '2016-12-02', 'annual day', '2016-12-15', 1),
(10, 1, 'PTA meeting', '2016-12-07', 'PTA meeting', '2016-12-15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `examentry`
--

CREATE TABLE `examentry` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `examName` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `examDate` date NOT NULL,
  `timeFrom` varchar(20) NOT NULL,
  `timeTo` varchar(20) NOT NULL,
  `discription` text NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examentry`
--

INSERT INTO `examentry` (`ID`, `acYear`, `examName`, `subject`, `examDate`, `timeFrom`, `timeTo`, `discription`, `loginId`) VALUES
(1, 1, 1, 1, '2016-03-15', '10:00:AM', '12:00:PM', '', 1),
(2, 1, 1, 2, '2016-03-16', '10:00:AM', '12:00:PM', '', 1),
(3, 1, 1, 3, '2016-03-18', '10:00:AM', '12:00:PM', '', 1),
(4, 1, 1, 4, '2016-03-21', '10:00:AM', '12:00:PM', '', 1),
(5, 1, 1, 5, '2016-03-23', '10:00:AM', '12:00:PM', '', 1),
(6, 1, 2, 1, '2015-08-05', '10:00:AM', '12:00:PM', '', 1),
(7, 1, 2, 2, '2015-08-03', '10:00:AM', '12:00:PM', '', 1),
(8, 1, 2, 3, '2015-08-07', '10:00:AM', '12:00:PM', '', 1),
(9, 1, 2, 4, '2015-08-10', '10:00:AM', '12:00:PM', '', 1),
(10, 1, 2, 5, '2015-08-12', '2:00:PM', '3:00:PM', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `examtypes`
--

CREATE TABLE `examtypes` (
  `ID` int(11) NOT NULL,
  `examType` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `examtypes`
--

INSERT INTO `examtypes` (`ID`, `examType`) VALUES
(1, 'Annual'),
(2, 'Half yearly'),
(3, 'D'),
(4, 'Class Test'),
(5, 'Onam'),
(6, 'Christmas');

-- --------------------------------------------------------

--
-- Table structure for table `feepayment`
--

CREATE TABLE `feepayment` (
  `ID` int(10) NOT NULL,
  `payDate` date NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `admNo` varchar(20) NOT NULL,
  `class` int(11) NOT NULL,
  `division` int(11) NOT NULL,
  `regFee` float NOT NULL,
  `admFee` float NOT NULL,
  `specialFee` float NOT NULL,
  `tuitionFee` float NOT NULL,
  `payingAmount` float NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feepayment`
--

INSERT INTO `feepayment` (`ID`, `payDate`, `voucherNo`, `admNo`, `class`, `division`, `regFee`, `admFee`, `specialFee`, `tuitionFee`, `payingAmount`, `loginId`) VALUES
(1, '2015-10-28', 1, '1001', 1, 1, 1000, 0, 0, 1000, 2000, 1),
(2, '2015-11-12', 2, '1007', 1, 1, 500, 0, 500, 1000, 2000, 1),
(4, '2015-11-12', 3, '1065', 1, 1, 500, 0, 0, 1500, 2000, 1),
(5, '2015-11-12', 4, '1008', 1, 1, 0, 0, 500, 1500, 2000, 1),
(6, '2015-11-12', 5, '1570', 1, 2, 500, 0, 0, 1500, 2000, 1),
(7, '2015-11-12', 6, '1006', 2, 3, 500, 0, 0, 1500, 2000, 1),
(8, '2015-11-12', 7, '1577', 2, 3, 500, 0, 0, 1500, 2000, 1),
(9, '2015-11-12', 8, '1580', 2, 3, 0, 0, 500, 1500, 2000, 1),
(10, '2015-11-12', 9, '1614', 3, 5, 0, 0, 0, 2000, 2000, 1),
(11, '2015-11-13', 10, '1014', 1, 1, 0, 0, 0, 2000, 2000, 1),
(12, '2016-01-25', 11, '11', 2, 3, 200, 77, 0, 0, 277, 1),
(13, '2016-01-25', 12, '11', 2, 3, 500, 0, 0, 0, 500, 1);

-- --------------------------------------------------------

--
-- Table structure for table `feesetting`
--

CREATE TABLE `feesetting` (
  `ID` int(5) NOT NULL,
  `class` int(11) NOT NULL,
  `registrationFee` float NOT NULL,
  `admissionFee` float NOT NULL,
  `specialFee` float NOT NULL,
  `tuitionFee` float NOT NULL,
  `total` float NOT NULL,
  `installmentNo` int(2) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feesetting`
--

INSERT INTO `feesetting` (`ID`, `class`, `registrationFee`, `admissionFee`, `specialFee`, `tuitionFee`, `total`, `installmentNo`, `loginId`) VALUES
(1, 1, 1000, 500, 500, 1000, 3000, 2, 1),
(2, 2, 1000, 500, 500, 2000, 4000, 2, 1),
(3, 3, 1000, 500, 500, 3000, 5000, 2, 1),
(4, 4, 250, 2000, 1200, 3000, 6450, 1, 1),
(5, 5, 250, 2000, 1200, 5000, 8450, 1, 1),
(6, 6, 250, 2000, 1200, 5000, 8450, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `ledger`
--

CREATE TABLE `ledger` (
  `ID` int(11) NOT NULL,
  `account_group` int(11) NOT NULL,
  `name` text NOT NULL,
  `details` text NOT NULL,
  `opening_balance` double NOT NULL,
  `pay_type` varchar(6) NOT NULL,
  `master_ledger` int(11) DEFAULT NULL,
  `cdate` date NOT NULL,
  `uid` int(11) NOT NULL,
  `proid` int(11) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ledger`
--

INSERT INTO `ledger` (`ID`, `account_group`, `name`, `details`, `opening_balance`, `pay_type`, `master_ledger`, `cdate`, `uid`, `proid`, `loginId`) VALUES
(1, 19, 'Suspense', 'Suspense Account', 0, 'Credit', 1, '2013-10-10', 1, 3, 0),
(2, 8, 'Cash Account', '', 0, 'Credit', 1, '2013-03-26', 1, 3, 0),
(3, 24, 'Salary Payments', '', 0, 'Credit', 1, '0000-00-00', 1, 3, 0),
(4, 26, 'Fee', 'Fee', 0, 'Credit', 1, '2013-08-25', 1, 3, 0),
(5, 26, 'Fee Payment', '1', 22212, 'Debit', 1, '2015-09-28', 1, 3, 0),
(6, 33, 'Library book', '', 0, 'Debit', 0, '2015-10-28', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `ID` int(11) NOT NULL,
  `staffId` int(11) NOT NULL,
  `userName` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`ID`, `staffId`, `userName`, `password`, `type`) VALUES
(1, 0, 'admin', 'a44a0ecb3de8bb0b65cce231826d8720', 'Admin'),
(6, 1, 'rashid', '7d0ba610dea3dbcc848a97d8dfd767ae', 'Teacher'),
(8, 3, 'sruthi', '98506495661c8c6c17af958c5d8b1a29', 'FrontOffice'),
(9, 2, 'anoop', 'd6615aeac1752bfc4ae2a5e07287f9f1', 'Teacher'),
(10, 4, 'lijina', 'e0c40bdf476062cd5e4ea1389f037308', 'Librarian'),
(11, 5, 'nidhin', '163db25b1b1da065ad36c2d70b008f67', 'Teacher'),
(12, 6, 'basheer', '91ec06327fa7109a83b3f9cae87071b3', 'Teacher'),
(13, 7, 'geetha', '984064710a8a5eb516c6bb47652968e7', 'Teacher'),
(14, 8, 'linta', 'b8508a5b281a20899c06397462aca71d', 'Teacher'),
(15, 9, 'anvar', '872de53a900f3250ae5649ea19e5c381', 'Teacher');

-- --------------------------------------------------------

--
-- Table structure for table `markbasic`
--

CREATE TABLE `markbasic` (
  `ID` int(11) NOT NULL,
  `examentryId` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `examName` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `division` int(11) NOT NULL,
  `totalMark` int(11) NOT NULL,
  `minMark` int(11) NOT NULL,
  `markDate` date NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `markbasic`
--

INSERT INTO `markbasic` (`ID`, `examentryId`, `year`, `examName`, `subject`, `class`, `division`, `totalMark`, `minMark`, `markDate`, `loginId`) VALUES
(1, 1, 1, 1, 1, 1, 1, 50, 15, '2015-10-28', 1),
(2, 6, 1, 2, 1, 1, 1, 50, 10, '2015-11-12', 1),
(3, 7, 1, 2, 2, 1, 1, 50, 50, '2015-11-12', 1),
(4, 8, 1, 2, 3, 1, 1, 50, 18, '2015-11-12', 1),
(5, 9, 1, 2, 4, 1, 1, 50, 18, '2015-11-12', 1),
(6, 10, 1, 2, 5, 1, 1, 50, 18, '2015-11-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `markentry`
--

CREATE TABLE `markentry` (
  `ID` int(11) NOT NULL,
  `markBasicId` int(11) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `mark` int(11) NOT NULL,
  `percentage` float NOT NULL,
  `grade` varchar(11) NOT NULL,
  `remark` text NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `markentry`
--

INSERT INTO `markentry` (`ID`, `markBasicId`, `adNo`, `mark`, `percentage`, `grade`, `remark`, `loginId`) VALUES
(1, 1, '1001', 40, 80, 'A', '', 1),
(2, 1, '1002', 36, 72, 'B+', '', 1),
(3, 2, '1001', 45, 90, 'A+', '', 1),
(4, 2, '1002', 46, 92, 'A+', '', 1),
(5, 2, '1007', 14, 28, 'D', '', 1),
(6, 2, '1008', 25, 50, 'C+', '', 1),
(7, 2, '1009', 36, 72, 'B+', '', 1),
(8, 2, '1010', 12, 24, 'D', '', 1),
(9, 2, '1011', 36, 72, 'B+', '', 1),
(10, 2, '1012', 25, 50, 'C+', '', 1),
(11, 2, '1013', 49, 98, 'A+', '', 1),
(12, 2, '1014', 50, 100, 'A+', '', 1),
(13, 2, '1015', 50, 100, 'A+', '', 1),
(14, 2, '1016', 44, 88, 'A', '', 1),
(15, 2, '1017', 40, 80, 'A', '', 1),
(16, 2, '1064', 30, 60, 'B', '', 1),
(17, 2, '1065', 20, 40, 'C', '', 1),
(18, 3, '1001', 30, 60, 'B', '', 1),
(19, 3, '1002', 50, 100, 'A+', 'good', 1),
(20, 3, '1007', 6, 12, 'FAIL', '', 1),
(21, 3, '1008', 25, 50, 'C+', '', 1),
(22, 3, '1009', 48, 96, 'A+', '', 1),
(23, 3, '1010', 42, 84, 'A', '', 1),
(24, 3, '1011', 16, 32, 'D+', '', 1),
(25, 3, '1012', 42, 84, 'A', '', 1),
(26, 3, '1013', 26, 52, 'C+', '', 1),
(27, 3, '1014', 36, 72, 'B+', '', 1),
(28, 3, '1015', 16, 32, 'D+', '', 1),
(29, 3, '1016', 29, 58, 'C+', '', 1),
(30, 3, '1017', 13, 26, 'D', '', 1),
(31, 3, '1064', 47, 94, 'A+', '', 1),
(32, 3, '1065', 32, 64, 'B', '', 1),
(33, 4, '1001', 50, 100, 'A+', 'good', 1),
(34, 4, '1002', 46, 92, 'A+', '', 1),
(35, 4, '1007', 41, 82, 'A', '', 1),
(36, 4, '1008', 42, 84, 'A', '', 1),
(37, 4, '1009', 46, 92, 'A+', '', 1),
(38, 4, '1010', 47, 94, 'A+', '', 1),
(39, 4, '1011', 32, 64, 'B', '', 1),
(40, 4, '1012', 30, 60, 'B', '', 1),
(41, 4, '1013', 38, 76, 'B+', '', 1),
(42, 4, '1014', 31, 62, 'B', '', 1),
(43, 4, '1015', 20, 40, 'C', '', 1),
(44, 4, '1016', 25, 50, 'C+', '', 1),
(45, 4, '1017', 29, 58, 'C+', '', 1),
(46, 4, '1064', 27, 54, 'C+', '', 1),
(47, 4, '1065', 12, 24, 'D', '', 1),
(48, 5, '1001', 20, 40, 'C', '', 1),
(49, 5, '1002', 35, 70, 'B+', '', 1),
(50, 5, '1007', 38, 76, 'B+', '', 1),
(51, 5, '1008', 31, 62, 'B', '', 1),
(52, 5, '1009', 50, 100, 'A+', 'good', 1),
(53, 5, '1010', 49, 98, 'A+', '', 1),
(54, 5, '1011', 48, 96, 'A+', '', 1),
(55, 5, '1012', 47, 94, 'A+', '', 1),
(56, 5, '1013', 12, 24, 'D', '', 1),
(57, 5, '1014', 42, 84, 'A', '', 1),
(58, 5, '1015', 30, 60, 'B', '', 1),
(59, 5, '1016', 29, 58, 'C+', '', 1),
(60, 5, '1017', 21, 42, 'C', '', 1),
(61, 5, '1064', 18, 36, 'D+', '', 1),
(62, 5, '1065', 11, 22, 'D', '', 1),
(63, 6, '1001', 30, 60, 'B', '', 1),
(64, 6, '1002', 15, 30, 'D+', '', 1),
(65, 6, '1007', 13, 26, 'D', '', 1),
(66, 6, '1008', 50, 100, 'A+', 'good', 1),
(67, 6, '1009', 25, 50, 'C+', '', 1),
(68, 6, '1010', 32, 64, 'B', '', 1),
(69, 6, '1011', 36, 72, 'B+', '', 1),
(70, 6, '1012', 49, 98, 'A+', '', 1),
(71, 6, '1013', 41, 82, 'A', '', 1),
(72, 6, '1014', 35, 70, 'B+', '', 1),
(73, 6, '1015', 25, 50, 'C+', '', 1),
(74, 6, '1016', 21, 42, 'C', '', 1),
(75, 6, '1017', 27, 54, 'C+', '', 1),
(76, 6, '1064', 26, 52, 'C+', '', 1),
(77, 6, '1065', 41, 82, 'A', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `messfeepayment`
--

CREATE TABLE `messfeepayment` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `noOfDays` int(11) NOT NULL,
  `amountPerDay` int(11) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `payDate` date NOT NULL,
  `voucherNo` int(20) NOT NULL,
  `paidAmount` float NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messfeepayment`
--

INSERT INTO `messfeepayment` (`ID`, `acYear`, `month`, `noOfDays`, `amountPerDay`, `adNo`, `payDate`, `voucherNo`, `paidAmount`, `loginId`) VALUES
(1, 1, 1, 7, 15, '1001', '2015-10-28', 1, 105, 1),
(2, 1, 1, 26, 15, '1007', '2015-11-12', 2, 390, 1),
(3, 1, 1, 26, 15, '1009', '2015-11-12', 3, 390, 1),
(4, 1, 1, 26, 15, '1008', '2015-11-12', 4, 390, 1),
(5, 1, 1, 26, 15, '1004', '2015-11-12', 5, 390, 1),
(6, 1, 1, 26, 15, '1006', '2015-11-12', 6, 390, 1),
(7, 1, 1, 20, 15, '1577', '2015-11-12', 7, 300, 1),
(8, 1, 1, 26, 15, '1606', '2015-11-12', 8, 390, 1),
(9, 1, 1, 20, 15, '1611', '2015-11-12', 9, 300, 1),
(10, 1, 1, 23, 15, '1014', '2015-11-13', 10, 345, 1);

-- --------------------------------------------------------

--
-- Table structure for table `messfeesetting`
--

CREATE TABLE `messfeesetting` (
  `ID` int(11) NOT NULL,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `fee` float NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messfeesetting`
--

INSERT INTO `messfeesetting` (`ID`, `year`, `month`, `class`, `fee`, `loginId`) VALUES
(1, 1, 1, 1, 15, 1),
(2, 1, 12, 1, 15, 1),
(3, 1, 12, 2, 15, 1),
(4, 1, 12, 3, 15, 1),
(5, 1, 1, 2, 15, 1),
(6, 1, 1, 3, 15, 1),
(7, 3, 1, 4, 58, 1),
(8, 3, 1, 5, 67, 1),
(9, 3, 1, 6, 67, 1);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `event` text NOT NULL,
  `discription` text NOT NULL,
  `showStatus` varchar(20) NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `addDate` date NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`ID`, `acYear`, `event`, `discription`, `showStatus`, `adNo`, `addDate`, `loginId`) VALUES
(1, 1, 'PTA meeting', '30/10/2015  11 Am to 1 PM', 'All', '', '2015-10-28', 1),
(2, 1, 'Parent meeting', '2/11/2015  at 10 AM', 'Individual', '1001', '2015-10-28', 1),
(3, 1, 'Study tour', '4/11/2015 starts at 9 Am', 'All', '', '2016-07-05', 1),
(4, 1, 'Half Yearly exam starting', 'August 5 Th on wards', 'ClassWise', '', '2015-11-12', 1),
(5, 1, 'Annual Exam', '2015 Annual Exam will be starts on 15 march', 'All', '', '2016-05-23', 1),
(6, 1, 'PTA meeting', 'December 12', 'ClassWise', '', '2015-11-12', 1),
(7, 1, 'Parent meeting', 'December 1 st', 'Individual', '1014', '2015-11-12', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notificationclass`
--

CREATE TABLE `notificationclass` (
  `ID` int(11) NOT NULL,
  `classId` int(11) NOT NULL,
  `eventId` int(11) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notificationclass`
--

INSERT INTO `notificationclass` (`ID`, `classId`, `eventId`, `loginId`) VALUES
(2, 2, 4, 1),
(3, 3, 4, 1),
(4, 2, 6, 1);

-- --------------------------------------------------------

--
-- Table structure for table `printsettings`
--

CREATE TABLE `printsettings` (
  `ID` int(11) NOT NULL,
  `all` varchar(10) NOT NULL,
  `feePayment` varchar(10) NOT NULL,
  `messFeePayment` varchar(10) NOT NULL,
  `busFeePayment` varchar(10) NOT NULL,
  `staffPayment` varchar(10) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `printsettings`
--

INSERT INTO `printsettings` (`ID`, `all`, `feePayment`, `messFeePayment`, `busFeePayment`, `staffPayment`, `loginId`) VALUES
(1, 'off', 'on', 'on', 'on', 'on', 1);

-- --------------------------------------------------------

--
-- Table structure for table `promotion`
--

CREATE TABLE `promotion` (
  `ID` int(11) NOT NULL,
  `promoteDate` date NOT NULL,
  `promoteTime` time NOT NULL,
  `existedAcYear` int(11) NOT NULL,
  `existedClass` int(11) NOT NULL,
  `existedDivision` int(11) NOT NULL,
  `promotedAcYear` int(11) NOT NULL,
  `promotedClass` int(11) NOT NULL,
  `promotedDivision` int(11) NOT NULL,
  `adNo` varchar(10) NOT NULL,
  `discription` text NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rack_reg`
--

CREATE TABLE `rack_reg` (
  `ID` int(11) NOT NULL,
  `rackNo` varchar(20) NOT NULL,
  `discription` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rack_reg`
--

INSERT INTO `rack_reg` (`ID`, `rackNo`, `discription`) VALUES
(1, 'p1', 'Poem section'),
(2, 'A1', 'Autobiography section'),
(3, 's1', 'story section');

-- --------------------------------------------------------

--
-- Table structure for table `reply`
--

CREATE TABLE `reply` (
  `ID` int(11) NOT NULL,
  `messageId` int(11) NOT NULL,
  `reply` text NOT NULL,
  `senderType` varchar(20) NOT NULL,
  `senderId` int(11) NOT NULL,
  `recieverType` varchar(20) NOT NULL,
  `recieverId` int(11) NOT NULL,
  `sendDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `salarysetting`
--

CREATE TABLE `salarysetting` (
  `ID` int(5) NOT NULL,
  `da` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salarysetting`
--

INSERT INTO `salarysetting` (`ID`, `da`) VALUES
(1, 10);

-- --------------------------------------------------------

--
-- Table structure for table `setinstalment`
--

CREATE TABLE `setinstalment` (
  `ID` int(5) NOT NULL,
  `class` int(11) NOT NULL,
  `amount` float NOT NULL,
  `paymentDate` date NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `setinstalment`
--

INSERT INTO `setinstalment` (`ID`, `class`, `amount`, `paymentDate`, `loginId`) VALUES
(1, 1, 2000, '2015-10-31', 1),
(2, 1, 1000, '2015-12-30', 1),
(3, 2, 2000, '2015-06-30', 1),
(4, 2, 2000, '2015-11-30', 1),
(5, 3, 2000, '2015-07-15', 1),
(6, 3, 3000, '2015-12-31', 1),
(7, 4, 6450, '2017-01-05', 1),
(8, 5, 8450, '2017-01-06', 1),
(9, 6, 8450, '2017-01-06', 1);

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `ID` int(11) NOT NULL,
  `staffId` int(5) NOT NULL,
  `name` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `age` int(11) NOT NULL,
  `address` varchar(50) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL,
  `dateOfJoin` date NOT NULL,
  `gender` varchar(20) NOT NULL,
  `type` varchar(20) NOT NULL,
  `designation` varchar(20) NOT NULL,
  `basicPay` float NOT NULL DEFAULT '0',
  `hra` float NOT NULL DEFAULT '0',
  `cca` float NOT NULL DEFAULT '0',
  `pf` float NOT NULL DEFAULT '0',
  `sli` float NOT NULL DEFAULT '0',
  `gli` float NOT NULL DEFAULT '0',
  `accountNo` varchar(20) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`ID`, `staffId`, `name`, `dob`, `age`, `address`, `phone`, `email`, `dateOfJoin`, `gender`, `type`, `designation`, `basicPay`, `hra`, `cca`, `pf`, `sli`, `gli`, `accountNo`, `loginId`) VALUES
(1, 222, 'Rashid', '1987-10-02', 28, 'Malappuram', '9936256958', 'rashid@gmail.com', '2015-10-28', 'Male', 'teaching', 'Teacher', 15000, 0, 0, 0, 0, 0, '0', 1),
(2, 223, 'Anoop', '1985-07-10', 30, 'Vadakara', '9936256958', '123@gmail.com', '2015-10-28', 'Male', 'teaching', 'Teacher', 12000, 0, 0, 0, 0, 0, '0', 1),
(3, 224, 'Sruthi', '1991-03-06', 25, '', '', '', '2015-10-28', 'Female', 'nonteaching', 'front office', 10000, 0, 0, 0, 0, 0, '0', 1),
(4, 225, 'Lijina', '1985-03-06', 31, '', '', '', '2015-11-12', 'Female', 'nonteaching', 'Librarian', 15000, 0, 0, 0, 0, 0, '0', 1),
(5, 226, 'Nidhin', '1986-07-16', 29, 'Kakkodi', '9936256958', 'nidhin@gmail.com', '2015-11-12', 'Male', 'teaching', 'Teacher', 15000, 500, 0, 300, 0, 0, '123456789', 1),
(6, 227, 'Basheer', '1984-01-31', 32, 'Kallay', '9936256958', 'basheer@gmail.com', '2015-11-12', 'Male', 'teaching', 'Teacher', 20000, 0, 0, 0, 0, 0, '0', 1),
(7, 228, 'Geetha', '1978-12-31', 37, 'areecode', '9936256958', 'geethA@gmail.com', '2015-11-12', 'Female', 'teaching', 'Teacher', 16000, 0, 0, 0, 0, 0, '0', 1),
(8, 229, 'Linta', '1986-08-08', 29, 'Kottayam', '9625252525', 'liNta@gmail.com', '2015-11-12', 'Female', 'teaching', 'Teacher', 15000, 0, 0, 0, 0, 0, '0', 1),
(9, 333, 'Anvar Sadath', '1989-07-05', 27, 'Manjeri', '9847232233', '', '2016-12-22', 'Male', 'teaching', 'Teacher', 15000, 2000, 1000, 5000, 0, 0, '4564838299292', 1),
(10, 334, 'Abbas Ali', '1990-07-16', 26, 'Kondotty', '9048392019', '', '2016-12-22', 'Male', 'teaching', 'Teacher', 12000, 2000, 1000, 4000, 0, 0, '0', 1);

-- --------------------------------------------------------

--
-- Table structure for table `staffnotification`
--

CREATE TABLE `staffnotification` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `notification` text NOT NULL,
  `discription` text NOT NULL,
  `addDate` date NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staffnotification`
--

INSERT INTO `staffnotification` (`ID`, `acYear`, `notification`, `discription`, `addDate`, `loginId`) VALUES
(1, 1, 'PTA meeting', '30/11/2015  10 am to 12 pm', '2015-10-28', 1),
(2, 1, 'Annual Exam Schedule', 'Annual Exam Schedule\r\nAnnual Exam Schedule\r\nAnnual Exam Schedule', '2016-05-23', 1),
(3, 1, 'School Events', 'School EventsSchool EventsSchool EventsSchool EventsSchool Events', '2016-05-23', 1),
(4, 1, 'Study tour', '4/11/2015 starts at 9 Am', '2016-07-05', 1),
(5, 1, 'PTA meeting', '30/10/2015 11 Am to 1 PM', '2016-07-05', 1);

-- --------------------------------------------------------

--
-- Table structure for table `staffpayment`
--

CREATE TABLE `staffpayment` (
  `ID` int(5) NOT NULL,
  `staffId` int(11) NOT NULL,
  `paymentDate` date NOT NULL,
  `voucherNo` int(11) NOT NULL,
  `basicSalary` float NOT NULL,
  `da` float NOT NULL,
  `hra` float NOT NULL,
  `cca` float NOT NULL,
  `otherAllowance` float NOT NULL DEFAULT '0',
  `pf` float NOT NULL,
  `sli` float NOT NULL,
  `gli` float NOT NULL,
  `otherDeduction` float NOT NULL DEFAULT '0',
  `total` float NOT NULL,
  `ledgerID` int(10) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staffpayment`
--

INSERT INTO `staffpayment` (`ID`, `staffId`, `paymentDate`, `voucherNo`, `basicSalary`, `da`, `hra`, `cca`, `otherAllowance`, `pf`, `sli`, `gli`, `otherDeduction`, `total`, `ledgerID`, `loginId`) VALUES
(1, 1, '2015-10-28', 1, 15000, 1500, 0, 0, 0, 0, 0, 0, 0, 16500, 2, 1),
(2, 2, '2015-11-12', 2, 12000, 1200, 0, 0, 800, 0, 0, 0, 0, 14000, 2, 1),
(3, 3, '2015-11-12', 3, 10000, 1000, 0, 0, 0, 0, 0, 0, 0, 11000, 2, 1),
(4, 5, '2015-11-12', 4, 15000, 1500, 500, 0, 300, 300, 0, 0, 0, 17000, 2, 1),
(5, 7, '2015-11-12', 5, 16000, 1600, 0, 0, 0, 0, 0, 0, 0, 17600, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `stagedetails`
--

CREATE TABLE `stagedetails` (
  `ID` int(11) NOT NULL,
  `vehicleId` int(11) NOT NULL,
  `stageName` varchar(20) NOT NULL,
  `amount` float NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stagedetails`
--

INSERT INTO `stagedetails` (`ID`, `vehicleId`, `stageName`, `amount`, `loginId`) VALUES
(1, 1, 'pottammal', 200, 1),
(2, 1, 'paimbra', 250, 1),
(3, 2, 'Palath', 250, 1),
(4, 2, 'Chelannur', 250, 1);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `ID` int(5) NOT NULL,
  `adNo` varchar(10) NOT NULL,
  `adDate` date NOT NULL,
  `name` varchar(50) NOT NULL,
  `class` int(11) NOT NULL,
  `division` int(11) NOT NULL,
  `address` varchar(50) NOT NULL,
  `district` varchar(20) NOT NULL,
  `pin` bigint(10) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `dob` date NOT NULL,
  `age` int(3) NOT NULL,
  `placeOfBirth` varchar(20) NOT NULL,
  `motherTongue` varchar(20) NOT NULL,
  `nationality` varchar(20) DEFAULT NULL,
  `religion` varchar(20) NOT NULL,
  `caste` varchar(20) NOT NULL,
  `bloodGroup` varchar(5) DEFAULT NULL,
  `idMark` varchar(50) DEFAULT NULL,
  `father` varchar(20) DEFAULT NULL,
  `fOccup` varchar(20) DEFAULT NULL,
  `fQuali` varchar(50) DEFAULT NULL,
  `fOfficeAddr` varchar(50) DEFAULT NULL,
  `fDistrict` varchar(20) DEFAULT NULL,
  `fPin` bigint(10) DEFAULT NULL,
  `fPhone` varchar(15) DEFAULT NULL,
  `fMobile` varchar(15) DEFAULT NULL,
  `fEmail` varchar(50) DEFAULT NULL,
  `mother` varchar(20) DEFAULT NULL,
  `mOccup` varchar(20) DEFAULT NULL,
  `mQuali` varchar(50) DEFAULT NULL,
  `mOfficeAddr` varchar(50) DEFAULT NULL,
  `mDistrict` varchar(20) DEFAULT NULL,
  `mPhone` varchar(15) DEFAULT NULL,
  `mMobile` varchar(20) DEFAULT NULL,
  `mEmail` varchar(50) DEFAULT NULL,
  `sibling` varchar(20) DEFAULT NULL,
  `guardian` varchar(20) DEFAULT NULL,
  `gAddress` varchar(50) DEFAULT NULL,
  `gDistrict` varchar(20) DEFAULT NULL,
  `gMobile` varchar(20) DEFAULT NULL,
  `gEmail` varchar(50) DEFAULT NULL,
  `boardingPoint` varchar(20) DEFAULT NULL,
  `studentTcStatus` varchar(20) NOT NULL DEFAULT 'no',
  `acYear` int(11) NOT NULL,
  `photo` text NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`ID`, `adNo`, `adDate`, `name`, `class`, `division`, `address`, `district`, `pin`, `phone`, `mobile`, `sex`, `dob`, `age`, `placeOfBirth`, `motherTongue`, `nationality`, `religion`, `caste`, `bloodGroup`, `idMark`, `father`, `fOccup`, `fQuali`, `fOfficeAddr`, `fDistrict`, `fPin`, `fPhone`, `fMobile`, `fEmail`, `mother`, `mOccup`, `mQuali`, `mOfficeAddr`, `mDistrict`, `mPhone`, `mMobile`, `mEmail`, `sibling`, `guardian`, `gAddress`, `gDistrict`, `gMobile`, `gEmail`, `boardingPoint`, `studentTcStatus`, `acYear`, `photo`, `loginId`) VALUES
(1, '1001', '2015-10-28', 'Sindhuja R', 1, 1, 'Pottammal', '8', 0, '', '', 'Female', '1993-10-21', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '9745950753', 'radhaKrishnan@gmail.com', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1001_ldy.jpg', 1),
(2, '1002', '2015-10-28', 'Rishad', 1, 1, 'Malappuram', '9', 0, '', '', 'Male', '2010-02-02', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1002_download (1).jpg', 1),
(3, '1003', '2015-10-28', 'Farhana', 1, 2, 'Payyoli', '', 0, '', '', 'Female', '1992-03-04', 24, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1003_download (2).jpg', 1),
(4, '1004', '2015-10-28', 'Asnad', 1, 2, 'Kannur', '', 0, '', '', 'Male', '2010-02-02', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, '', 1),
(5, '1005', '2015-10-29', 'Vishnu', 1, 2, 'Kunnamangalam', '', 0, '', '', 'Male', '2010-06-15', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1005_student1.jpg', 1),
(6, '1006', '2015-10-29', 'Akhila', 2, 3, 'Atholi', '', 0, '', '', 'Female', '2010-02-02', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1006_download.jpg', 1),
(7, '1007', '2015-11-11', 'Arun T', 1, 1, 'Kakkodi', '', 0, '', '', 'Male', '2010-06-15', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1007_images (4).jpg', 1),
(8, '1008', '2015-11-11', 'Avani', 1, 1, 'Nanmanda', '8', 0, '', '', 'Female', '2010-02-02', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1008_images (3).jpg', 1),
(9, '1009', '2015-11-11', 'Manju', 1, 1, 'Nallalam', '', 0, '', '', 'Female', '2010-06-08', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1009_images.jpg', 1),
(10, '1010', '2015-11-11', 'Nirmal', 1, 1, 'Narikkuni', '', 0, '', '', 'Male', '2010-02-09', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1010_images (5).jpg', 1),
(11, '1011', '2015-11-11', 'Nayana', 1, 1, 'Mavoor', '', 0, '', '', 'Female', '2010-02-02', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1011_download.jpg', 1),
(12, '1012', '2015-11-11', 'Sooraj', 1, 1, 'Palath', '', 0, '', '', 'Male', '2010-06-15', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1012_images (2).jpg', 1),
(13, '1013', '2015-11-11', 'Manu  M', 1, 1, 'Vadakara', '', 0, '', '', 'Male', '2010-06-15', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1013_student1.jpg', 1),
(14, '1014', '2015-11-11', 'Anwar', 1, 1, 'Kondotti', '9', 0, '0495245505', '9876655432', 'Male', '2010-06-16', 5, 'malappuram', 'malayalam', 'Indian', 'Muslim', '', 'o +ve', '', 'Muhammed', 'Bussiness', '', '', '9', 0, '', '9567129796', 'Muhammed@gmail.com', 'Ayisha', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1014_images (11).jpg', 1),
(15, '1015', '2015-11-11', 'Hibna', 1, 1, 'Atholi', '', 0, '', '', 'Female', '2010-02-10', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1015_abcd3.jpg', 1),
(16, '1016', '2015-11-11', 'Lakshmi', 1, 1, 'Vadakara', '', 0, '', '', 'Female', '2010-02-09', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1016_abcd10.jpg', 1),
(17, '1017', '2015-11-11', 'Neenu', 1, 1, 'Vengara', '', 0, '', '', 'Female', '2010-02-02', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1017_abcd5.jpg', 1),
(18, '1064', '2015-11-11', 'Shafeeq', 1, 1, 'Malappuram', '', 0, '', '', 'Male', '2010-06-15', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1064_abcd8.jpg', 1),
(19, '1065', '2015-11-11', 'Asnad', 1, 1, 'Narikkuni', '', 0, '', '', 'Male', '2010-02-02', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1065_abcd9.jpg', 1),
(20, '1066', '2015-11-11', 'Fathima', 1, 2, 'Narikkuni', '', 0, '', '', 'Female', '2010-02-10', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1066_abcd12.jpg', 1),
(21, '1565', '2015-11-11', 'Anvar', 1, 2, 'Calicut', '', 0, '', '', 'Male', '2010-02-09', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1565_abcd13.jpg', 1),
(22, '1566', '2015-11-11', 'Tintu', 1, 2, 'Koyilandi', '7', 0, '', '', 'Female', '2010-06-15', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1566_abcd17.jpg', 1),
(23, '1567', '2015-11-11', 'Athira', 1, 2, 'Pottammal', '', 0, '', '', 'Female', '2010-02-16', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1567_abcd18.jpg', 1),
(24, '1568', '2015-11-11', 'Aswathi', 1, 2, 'Pottammal', '', 0, '', '', 'Female', '2010-03-04', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1568_abcd19.jpg', 1),
(25, '1569', '2015-11-11', 'Shameer', 1, 2, 'Kannur', '', 0, '', '', 'Male', '2010-07-21', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1569_abcd25.jpg', 1),
(26, '1570', '2015-11-11', 'Rishad', 1, 2, 'Cheruvadi', '', 0, '', '', 'Male', '2010-03-10', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1570_abcd27.jpg', 1),
(27, '1571', '2015-11-11', 'Abbas', 1, 2, 'Kondotti', '', 0, '', '', 'Male', '2010-07-07', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1571_abcd14.jpg', 1),
(28, '1572', '2015-11-11', 'Sinan', 1, 2, 'Malappuram', '', 0, '', '', 'Male', '2010-10-19', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1572_abcd120.jpg', 1),
(29, '1573', '2015-11-11', 'Ayisha', 1, 2, 'Payyoli', '', 0, '', '', 'Female', '2010-07-14', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1573_abcd26.jpg', 1),
(30, '1574', '2015-11-11', 'Aiswarya', 1, 2, 'Mahi', '', 0, '', '', 'Male', '2010-12-24', 5, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1574_abcd24.jpg', 1),
(31, '1575', '2015-11-12', 'Sindhuja P', 1, 2, 'Kunnamangalam', '8', 0, '', '', 'Female', '2010-02-10', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1575_images (1).jpg', 1),
(32, '1576', '2015-11-12', 'Krishna', 2, 3, 'Farook', '', 0, '', '', 'Female', '2008-07-16', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1576_images.jpg', 1),
(33, '1577', '2015-11-12', 'Arif', 2, 3, 'Meenchantha', '', 0, '', '', 'Male', '2008-06-17', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1577_download.jpg', 1),
(34, '1578', '2015-11-12', 'Sharanya', 2, 3, 'Malaparamb', '', 0, '', '', 'Female', '2008-10-28', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1578_images (5).jpg', 1),
(35, '1579', '2015-11-12', 'Binfas', 2, 3, 'Mukkam', '8', 0, '', '', 'Male', '2008-07-24', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1579_images (2).jpg', 1),
(36, '1580', '2015-11-12', 'Hafiz', 2, 3, 'Mukkam', '', 0, '', '', 'Male', '2008-06-17', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1580_images (7).jpg', 1),
(37, '1581', '2015-11-12', 'Arjun', 2, 3, 'Areekkad', '', 0, '', '', 'Male', '2008-06-18', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1581_images (8).jpg', 1),
(38, '1582', '2015-11-12', 'Abaya', 2, 3, 'Koyilandi', '', 0, '', '', 'Female', '2008-07-23', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1582_images (4).jpg', 1),
(39, '1583', '2015-11-12', 'Sajila', 2, 3, 'Vadakara', '', 0, '', '', 'Female', '2008-06-17', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1583_images (9).jpg', 1),
(40, '1584', '2015-11-12', 'Anjana', 2, 3, 'Vellimadukunnu', '', 0, '', '', 'Female', '2008-10-28', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1584_images (6).jpg', 1),
(41, '1585', '2015-11-12', 'Anagha', 2, 3, 'Perambra', '', 0, '', '', 'Female', '2008-10-28', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1585_images (12).jpg', 1),
(42, '1586', '2015-11-12', 'Jipson', 2, 3, 'Westhill', '8', 0, '', '', 'Male', '2008-03-05', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1586_images (11).jpg', 1),
(43, '1587', '2015-11-12', 'Neena', 2, 3, 'Kappad', '', 0, '', '', 'Female', '2008-10-28', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1587_images (13).jpg', 1),
(44, '1588', '2015-11-12', 'Neethu', 2, 3, 'Poonoor', '', 0, '', '', 'Female', '2008-06-15', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1588_images (14).jpg', 1),
(45, '1589', '2015-11-12', 'Nitha', 2, 3, 'Pullaloor', '', 0, '', '', 'Female', '2009-01-01', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1589_images (10).jpg', 1),
(46, '1590', '2015-11-12', 'Jobin', 2, 4, 'Kottayam', '', 0, '', '', 'Male', '2008-12-17', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1590_images (21).jpg', 1),
(47, '1591', '2015-11-12', 'Marvan', 2, 4, 'Palazhi', '', 0, '', '', 'Male', '2009-01-05', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1591_images (17).jpg', 1),
(48, '1592', '2015-11-12', 'Rameez', 2, 4, 'Palazhi', '8', 0, '', '', 'Male', '2008-11-20', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1592_images (20).jpg', 1),
(49, '1593', '2015-11-12', 'Nishana', 2, 4, 'vengeri', '', 0, '', '', 'Female', '2008-12-01', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1593_abcd4.jpg', 1),
(50, '1595', '2015-11-12', 'Najidha', 2, 4, 'Balussery', '', 0, '', '', 'Female', '2008-10-21', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1595_abcd22.jpg', 1),
(51, '1596', '2015-11-12', 'Perly', 2, 4, 'Thondayadu', '', 0, '', '', 'Female', '2009-01-13', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1596_abcd26.jpg', 1),
(52, '1597', '2015-11-12', 'Aleena', 2, 4, 'Mayanad', '', 0, '', '', 'Female', '2009-02-03', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1597_images (18).jpg', 1),
(53, '1598', '2015-11-12', 'Amana', 2, 4, 'Mootoly', '', 0, '', '', 'Female', '2009-03-04', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1598_images (27).jpg', 1),
(54, '1599', '2015-11-12', 'Nikil', 2, 4, 'Puramery', '', 0, '', '', 'Male', '2009-04-22', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1599_images (26).jpg', 1),
(55, '1610', '2015-11-12', 'Shahsad', 2, 4, 'Vattoly', '', 0, '', '', 'Male', '2009-01-29', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1610_download (4).jpg', 1),
(56, '1601', '2015-11-12', 'Amrutha', 2, 4, 'Vadakara', '', 0, '', '', 'Female', '2009-02-02', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1601_abcd24.jpg', 1),
(57, '1602', '2015-11-12', 'Jumana', 2, 4, 'Karikkamkulam', '', 0, '', '', 'Female', '2009-02-10', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1602_images (29).jpg', 1),
(58, '1603', '2015-11-12', 'vishak', 2, 4, 'Areecode', '', 0, '', '', 'Male', '2009-06-16', 6, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1603_images (34).jpg', 1),
(59, '1604', '2015-11-12', 'Aswathi V', 2, 4, 'Pottamal', '', 0, '', '', 'Female', '2008-12-29', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1604_images (32).jpg', 1),
(60, '1605', '2015-11-12', 'Theertha', 2, 4, 'Karaparamb', '8', 0, '', '', 'Female', '2008-12-28', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1605_images (15).jpg', 1),
(61, '1606', '2015-11-12', 'Munna', 3, 5, 'Mukkam', '9', 0, '', '', 'Female', '2009-04-27', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1606_images (19).jpg', 1),
(62, '1607', '2015-11-12', 'Meera', 3, 5, 'Kondotti', '', 0, '', '', 'Female', '2008-06-10', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1607_images (38).jpg', 1),
(63, '1608', '2015-11-12', 'Deepthi', 3, 5, 'Vengery', '', 0, '', '', 'Female', '2008-12-28', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1608_images (30).jpg', 1),
(64, '1609', '2015-11-12', 'Diya', 3, 5, 'Kakkodi', '', 0, '', '', 'Female', '2008-06-03', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1609_images (41).jpg', 1),
(65, '1611', '2015-11-12', 'Reshma', 3, 5, 'kallery', '', 0, '', '', 'Female', '2009-01-05', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1611_images (36).jpg', 1),
(66, '1612', '2015-11-12', 'Sanjay', 3, 5, 'Palath', '', 0, '', '', 'Male', '2008-01-01', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1612_images (42).jpg', 1),
(67, '1613', '2015-11-12', 'Nithya', 3, 5, 'kotooli', '', 0, '', '', 'Female', '2008-01-07', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1613_images (40).jpg', 1),
(68, '1614', '2015-11-12', 'Durga', 3, 5, 'kallayi', '', 0, '', '', 'Female', '2007-10-23', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1614_images (3).jpg', 1),
(69, '1615', '2015-11-12', 'Sreevidya', 3, 5, 'Ramanattukara', '', 0, '', '', 'Female', '2008-10-14', 7, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1615_abcd2.jpg', 1),
(70, '1616', '2015-11-12', 'Deepak', 3, 5, 'Mahi', '', 0, '', '', 'Male', '2006-08-18', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1616_abcd16.jpg', 1),
(71, '1617', '2015-11-12', 'thamanna', 3, 5, 'kakkodi', '', 0, '', '', 'Female', '2008-01-29', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1617_images (5).jpg', 1),
(72, '1618', '2015-11-12', 'Subina', 3, 5, 'Chungam', '', 0, '', '', 'Female', '2007-06-19', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1618_images (39).jpg', 1),
(73, '1619', '2015-11-12', 'Renu', 3, 5, 'Chelanur', '', 0, '', '', 'Female', '2007-06-05', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1619_images (4).jpg', 1),
(74, '1620', '2015-11-12', 'Jithin', 3, 5, 'Koyilandi', '', 0, '', '', 'Male', '2007-01-02', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1620_abcd14.jpg', 1),
(75, '1621', '2015-11-12', 'Rohith', 3, 5, 'vadakara', '', 0, '', '', 'Male', '2007-06-05', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1621_images (42).jpg', 1),
(76, '1622', '2015-11-12', 'Josmi', 3, 6, 'wayanad', '', 0, '', '', 'Female', '2007-12-30', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1622_download (1).jpg', 1),
(77, '1623', '2015-11-12', 'Josmi', 3, 6, 'Wayanad', '', 0, '', '', 'Female', '2007-01-01', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1623_download (1).jpg', 1),
(78, '1624', '2015-11-12', 'veena', 3, 6, 'Nadakkavu', '', 0, '', '', 'Female', '2006-06-14', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1624_images (31).jpg', 1),
(79, '1625', '2015-11-12', 'Neerav', 3, 6, 'Payyoli', '', 0, '', '', 'Male', '2007-06-05', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1625_download (4).jpg', 1),
(80, '1626', '2015-11-12', 'Sibin', 3, 6, 'vadakara', '', 0, '', '', 'Male', '2007-01-08', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1626_download (5).jpg', 1),
(81, '1627', '2015-11-12', 'Sreeshma', 3, 6, 'Kovoor', '', 0, '', '', 'Female', '2006-01-02', 10, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1627_images (33).jpg', 1),
(82, '1628', '2015-11-12', 'Teena', 3, 6, 'Kuttikattoor', '', 0, '', '', 'Female', '2007-01-10', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1628_images (19).jpg', 1),
(83, '1629', '2015-11-12', 'Ananya', 3, 6, 'Varkkala', '', 0, '', '', 'Female', '2006-02-07', 10, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1629_images (28).jpg', 1),
(84, '1630', '2015-11-12', 'Aruna', 3, 6, 'EastHill', '8', 0, '', '', 'Female', '2006-07-05', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1630_abcd19.jpg', 1),
(85, '1631', '2015-11-12', 'Parvathi', 3, 6, 'Puthiyara', '', 0, '', '', 'Female', '2006-02-07', 10, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1631_images (25).jpg', 1),
(86, '1632', '2015-11-12', 'Aswin', 3, 6, 'Balussery', '', 0, '', '', 'Male', '2007-06-12', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1632_images (23).jpg', 1),
(87, '1634', '2015-11-12', 'Sarath', 3, 6, 'Kappad', '', 0, '', '', 'Male', '2007-06-20', 8, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1634_images (21).jpg', 1),
(88, '1635', '2015-11-12', 'Amar', 3, 6, 'Nanmanda', '', 0, '', '', 'Male', '2006-12-31', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1635_abcd8.jpg', 1),
(89, '1636', '2015-11-12', 'Siva', 3, 6, 'Vadakara', '', 0, '', '', 'Male', '2007-01-10', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1636_images (17).jpg', 1),
(90, '1637', '2015-11-12', 'Gokul', 3, 6, 'Westhill', '', 0, '', '', 'Male', '2007-01-01', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1637_images (24).jpg', 1),
(91, '1638', '2015-11-12', 'Keerthana', 3, 6, 'Kumarawsami', '', 0, '', '', 'Female', '2007-05-14', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1638_images (38).jpg', 1),
(92, '1639', '2015-11-12', 'Jyothi', 3, 6, 'Areecode', '', 0, '', '', 'Female', '2006-02-07', 10, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1639_images (1).jpg', 1),
(93, '1640', '2015-11-12', 'Nandhana', 3, 6, 'Narikkuni', '', 0, '', '', 'Female', '2006-06-06', 9, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, 'uploads/1640_images (31).jpg', 1),
(94, '11', '2016-01-25', 's', 2, 3, 's', '', 0, '443', '9846759100', 'Male', '2016-01-12', 0, '', '', 'Indian', '', '', '', '', '', '', '', '', '', 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 'no', 1, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE `subject` (
  `ID` int(11) NOT NULL,
  `subjectName` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`ID`, `subjectName`) VALUES
(1, 'Malayalam'),
(2, 'English'),
(3, 'Maths'),
(4, 'Hindi'),
(5, 'IT'),
(6, 'Computer Science'),
(7, 'Zoology'),
(8, 'Botany'),
(9, 'Physics'),
(10, 'Economics'),
(11, 'History'),
(12, 'Science');

-- --------------------------------------------------------

--
-- Table structure for table `tblad`
--

CREATE TABLE `tblad` (
  `ID` int(11) NOT NULL,
  `ad` varchar(250) NOT NULL,
  `endDate` date NOT NULL,
  `pic1` text NOT NULL,
  `pic2` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblad`
--

INSERT INTO `tblad` (`ID`, `ad`, `endDate`, `pic1`, `pic2`) VALUES
(6, 'Ad1', '2015-09-30', 'uploads/20150908133029_0.jpg', 'uploads/20150908133029_1.jpg'),
(7, 'ad2', '2015-09-29', 'uploads/20150908133416_0.jpg', 'uploads/20150908133416_1.jpg'),
(8, 'ad3', '2015-09-28', 'uploads/20150908133526_0.jpg', 'uploads/20150908133526_1.jpg'),
(9, 'ad4', '2015-09-27', 'uploads/20150908133620_0.jpg', 'uploads/20150908133620_1.jpg'),
(11, 'testing', '2015-09-10', 'uploads/20150911105833_0.jpg', 'uploads/20150911105833_1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tblbloodbank`
--

CREATE TABLE `tblbloodbank` (
  `ID` int(11) NOT NULL,
  `bloodGroup` varchar(4) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbloodbank`
--

INSERT INTO `tblbloodbank` (`ID`, `bloodGroup`, `name`, `phone`) VALUES
(1, 'A+', 'Rashid', '989578855'),
(2, 'B+', 'Shahsad', '996'),
(3, 'A+', 'à´±à´¾à´·à´¿à´¦àµâ€Œ', '989578855'),
(4, 'O+', 'aasnad khan', '+97143519759'),
(5, 'AB+', 'à´±à´¿à´·à´¾à´¦àµ à´…à´²à´¿', '9633122237'),
(6, 'O+', 'zsnad khan', '9746410718');

-- --------------------------------------------------------

--
-- Table structure for table `tblbusplace`
--

CREATE TABLE `tblbusplace` (
  `ID` int(11) NOT NULL,
  `place` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbusplace`
--

INSERT INTO `tblbusplace` (`ID`, `place`) VALUES
(5, 'aa'),
(2, 'Calicut'),
(4, 'Ramanattukara'),
(1, 'Valluvambram'),
(6, 'zz');

-- --------------------------------------------------------

--
-- Table structure for table `tblbustime`
--

CREATE TABLE `tblbustime` (
  `ID` int(11) NOT NULL,
  `fromPlaceID` int(11) NOT NULL,
  `toPlaceID` int(11) NOT NULL,
  `fromTime` varchar(15) NOT NULL,
  `toTime` varchar(15) NOT NULL,
  `busName` varchar(200) NOT NULL,
  `via` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblbustime`
--

INSERT INTO `tblbustime` (`ID`, `fromPlaceID`, `toPlaceID`, `fromTime`, `toTime`, `busName`, `via`) VALUES
(2, 2, 1, '07.00 PM', '08.00 PM', 'FATHIMA', 'Ramanattukara'),
(3, 1, 2, '08.35 AM', '09.40 AM', 'FATHIMA', 'Ramanattukara'),
(4, 1, 4, '07.00 PM', '07.40 PM', 'sss', 'kondotty'),
(5, 5, 6, '10:00', '11:00', 'asnad', 'nothing'),
(6, 5, 6, '10', '11', 'asss', 'uui');

-- --------------------------------------------------------

--
-- Table structure for table `tblcareer`
--

CREATE TABLE `tblcareer` (
  `ID` int(11) NOT NULL,
  `type` varchar(15) NOT NULL,
  `message` text NOT NULL,
  `contactNo` varchar(15) NOT NULL,
  `postDate` varchar(20) NOT NULL,
  `heading` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcareer`
--

INSERT INTO `tblcareer` (`ID`, `type`, `message`, `contactNo`, `postDate`, `heading`) VALUES
(1, 'Employee', 'I Need A Job', '343452', '11/08/2015', ''),
(2, 'Employee', 'dgdgsgsgdf  \r\n\r\nsdgsdsdgsdgsdg d dggd dfh', '343452', '10/08/2015', ''),
(3, 'Employer', 'need some one\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.\r\n@10:20 AMà´•àµ†.à´Žà´‚. à´®à´¾à´£à´¿à´¯àµ† à´®àµà´–àµà´¯à´®à´¨àµà´¤àµà´°à´¿à´¯à´¾à´•àµà´•à´¾à´¨àµâ€ à´†à´²àµ‹à´šà´¿à´šàµà´šà´¿à´Ÿàµà´Ÿà´¿à´²àµà´²àµ†à´¨àµà´¨àµ à´•à´¾à´¨à´‚ à´°à´¾à´œàµ‡à´¨àµà´¦àµà´°à´¨àµâ€\r\n@7:30 AMà´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ; à´›àµ‹à´Ÿàµà´Ÿà´¾ à´°à´¾à´œà´¨àµ† à´‡à´¨àµà´¨àµ à´‡à´¨àµà´¤àµà´¯à´¯à´¿à´²àµ‡à´•àµà´•àµ à´•àµŠà´£àµà´Ÿàµà´µà´¨àµà´¨àµ‡à´•àµà´•à´¿à´²àµà´²\r\n@onlinedeskà´…à´—àµâ€Œà´¨à´¿à´ªà´°àµâ€à´µà´¤à´‚ à´ªàµŠà´Ÿàµà´Ÿà´¿à´¤àµà´¤àµ†à´±à´¿à´šàµà´šàµ; à´¬à´¾à´²à´¿ à´µà´¿à´®à´¾à´¨à´¤àµà´¤à´¾à´µà´³à´‚ à´…à´Ÿà´šàµà´šàµ\r\n@7:29 AMà´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡àµà´°àµˆà´µà´°àµâ€ à´…à´¨à´¿à´²àµâ€ à´•àµà´®à´¾à´±à´¿à´¨àµ† à´ªàµ‹à´²àµ€à´¸àµ à´®à´°àµâ€à´¦à´¿à´šàµà´šàµ†à´¨àµà´¨à´¾à´°àµ‹à´ªà´¿à´šàµà´šà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ.', '343452', '27/08/2015', ''),
(4, 'Employer', 'calicut', '2222', '04/11/2015', 'it job');

-- --------------------------------------------------------

--
-- Table structure for table `tblemergency`
--

CREATE TABLE `tblemergency` (
  `ID` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `place` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblemergency`
--

INSERT INTO `tblemergency` (`ID`, `name`, `place`, `phone`) VALUES
(2, 'Ambulance', 'Ramanattukara', '989578855');

-- --------------------------------------------------------

--
-- Table structure for table `tblfeedback`
--

CREATE TABLE `tblfeedback` (
  `ID` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `email` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblfeedback`
--

INSERT INTO `tblfeedback` (`ID`, `name`, `message`, `email`) VALUES
(45, 'asnadbodhi@gmail.com', 'hi', ''),
(46, 'asnadbodhi@gmail.com', 'hi how r u', ''),
(47, 'asnadbodhi@gmail.com', 'hi how r u @ ; -/]} m 34%^&**@', ''),
(48, 'anoopbodhi@gmail.com', 'asnad khan', ''),
(51, 'asnad.bodhi@gmail.com', 'kii', ''),
(52, '', '', ''),
(53, 'asnad.bodhi@gmail.com', 'hello', ''),
(54, 'asnad.bodhi@gmail.com', 'replay', '');

-- --------------------------------------------------------

--
-- Table structure for table `tblhealth`
--

CREATE TABLE `tblhealth` (
  `ID` int(11) NOT NULL,
  `typeID` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `place` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblhealth`
--

INSERT INTO `tblhealth` (`ID`, `typeID`, `name`, `place`, `phone`) VALUES
(2, 3, 'VVM Medical', 'VVM', '989578855');

-- --------------------------------------------------------

--
-- Table structure for table `tblhealthmessage`
--

CREATE TABLE `tblhealthmessage` (
  `ID` int(11) NOT NULL,
  `healthID` int(11) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblhealthmessage`
--

INSERT INTO `tblhealthmessage` (`ID`, `healthID`, `message`) VALUES
(2, 2, 'Skin Specialist  on Sunday'),
(3, 2, 'sunday holidy');

-- --------------------------------------------------------

--
-- Table structure for table `tblhealthtype`
--

CREATE TABLE `tblhealthtype` (
  `ID` int(11) NOT NULL,
  `type` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblhealthtype`
--

INSERT INTO `tblhealthtype` (`ID`, `type`) VALUES
(1, 'Pvt. Hospital'),
(2, 'Clinic'),
(3, 'Medical Shops'),
(4, 'Gov. Hospital');

-- --------------------------------------------------------

--
-- Table structure for table `tblinstitute`
--

CREATE TABLE `tblinstitute` (
  `ID` int(11) NOT NULL,
  `type` varchar(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblinstitute`
--

INSERT INTO `tblinstitute` (`ID`, `type`, `name`, `address`, `phone`) VALUES
(3, 'School', 'vadi husna', 'vatolyt', '254'),
(6, 'College', 'RGIT', '', '989578855');

-- --------------------------------------------------------

--
-- Table structure for table `tblinstitutedetails`
--

CREATE TABLE `tblinstitutedetails` (
  `ID` int(11) NOT NULL,
  `instituteID` int(11) NOT NULL,
  `branch` varchar(200) NOT NULL,
  `totalSeat` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblinstitutedetails`
--

INSERT INTO `tblinstitutedetails` (`ID`, `instituteID`, `branch`, `totalSeat`) VALUES
(2, 6, 'IS', '50'),
(3, 6, 'CS', '50'),
(4, 6, 'BM', '50');

-- --------------------------------------------------------

--
-- Table structure for table `tbljob`
--

CREATE TABLE `tbljob` (
  `ID` int(11) NOT NULL,
  `typeID` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbljob`
--

INSERT INTO `tbljob` (`ID`, `typeID`, `name`, `phone`) VALUES
(1, 1, 'à´±à´¾à´·à´¿à´¦àµâ€Œ', '15878525');

-- --------------------------------------------------------

--
-- Table structure for table `tbljobtype`
--

CREATE TABLE `tbljobtype` (
  `ID` int(11) NOT NULL,
  `jobType` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbljobtype`
--

INSERT INTO `tbljobtype` (`ID`, `jobType`) VALUES
(1, 'IT'),
(2, 'DRIVER'),
(3, 'COOLI');

-- --------------------------------------------------------

--
-- Table structure for table `tbllogin`
--

CREATE TABLE `tbllogin` (
  `ID` int(11) NOT NULL,
  `type` varchar(30) NOT NULL,
  `userName` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbllogin`
--

INSERT INTO `tbllogin` (`ID`, `type`, `userName`, `password`) VALUES
(1, 'admin', 'admin', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `tblmarket`
--

CREATE TABLE `tblmarket` (
  `ID` int(11) NOT NULL,
  `heading` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `contactNo` varchar(15) NOT NULL,
  `postDate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblmarket`
--

INSERT INTO `tblmarket` (`ID`, `heading`, `description`, `contactNo`, `postDate`) VALUES
(2, 'Car For Sale', 'Alto 2007', '989578855', '19/08/2015'),
(3, 'bus for sale', 'jj', '9746410718', '26/08/2015'),
(4, 'chakka for sale', 'bdhj', '9746410718', '19/08/2015'),
(5, 'manga for sale', 'jj', '9746410718', '05/08/2015'),
(6, 'sindhuja', 'testing testing', '998', '25/08/2015'),
(17, '800 for sale', 'vilkanam', '9746410718', '18/09/2015'),
(18, 'new i10 for sale', 'it0', '989758684898', '04/09/2015'),
(19, '800 vilkanam', 'vilkanam', '9746410718', '19/09/2015'),
(26, 'new i10 for sale', 'new i10 for sale with 480000/-', '9895578388', '12/09/2015'),
(27, 'CAR', 'CHUMMA', '9999', '15/09/2015'),
(33, 'new i10 for sale', 'new i10 for salenew i10 for sale', '9895578388', '12/09/2015'),
(34, 'new i10 for sale', 'marketData marketData', '9895578388', '12/09/2015'),
(35, 'check', 'check', '6', '18/09/2015');

-- --------------------------------------------------------

--
-- Table structure for table `tblnotification`
--

CREATE TABLE `tblnotification` (
  `ID` int(11) NOT NULL,
  `heading` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `postDate` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblnotification`
--

INSERT INTO `tblnotification` (`ID`, `heading`, `description`, `postDate`) VALUES
(1, 'New Offers in amazon', 'Check mail', '27/08/2015'),
(2, 'à´Žà´•àµà´¸àµà´Ÿàµà´°à´¾ à´Ÿàµˆà´®à´¿à´²àµâ€ ', 'à´—àµ‡à´¾à´³àµâ€à´®à´´ à´¤àµ€à´°àµâ€à´¤àµà´¤ à´ªàµ‡à´¾à´°à´¾à´Ÿàµà´Ÿà´¤àµà´¤à´¿à´²àµâ€ à´¸àµ†à´µà´¿à´¯àµà´¯à´¯àµ† à´¤à´•à´°àµâ€à´¤àµà´¤àµ à´¬à´¾à´´àµà´¸à´²àµ‡à´¾à´£ à´¯àµà´µàµ‡à´« à´¸àµ‚à´ªàµà´ªà´°àµâ€ à´•à´ªàµà´ªàµ à´•à´¿à´°àµ€à´Ÿà´‚ à´¨àµ‡à´Ÿà´¿', '13/08/2015'),
(3, 'à´•à´¥à´¾à´ªàµà´°à´¸à´‚à´—à´•à´²à´¯à´¿à´²àµ†', 'à´•àµ‡à´°à´³à´¤àµà´¤à´¿à´²àµ† à´¸à´¹àµƒà´•àµ‡à´°à´³à´¤àµà´¤à´¿à´²àµ† à´¸à´¹àµƒà´¦à´¯ à´¸à´¦à´¸àµà´¸àµà´•à´³àµâ€ à´¹àµƒà´¦à´¯à´¤àµà´¤àµ‡à´¾à´Ÿàµ à´•àµ‡à´°à´³à´¤àµà´¤à´¿à´²àµ† à´¸à´¹àµƒà´¦à´¯ à´¸à´¦à´¸àµà´¸àµà´•à´³àµâ€ à´¹àµƒà´¦à´¯à´¤àµà´¤àµ‡à´¾à´Ÿàµ à´šàµ‡à´°àµâ€à´¤àµà´¤àµà´µàµ†à´šàµà´š à´’à´°àµ à´¨à´¾à´¦à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´¨à´¿à´²à´¾à´µàµ à´ªà´°à´¨àµà´¨ à´†à´•à´¾à´¶à´¤àµà´¤à´¿à´¨àµ à´¤à´¾à´´àµ† à´‡à´¤à´³àµâ€à´µà´¿à´°à´¿à´¯àµà´¨àµà´¨ à´‡à´¶à´²à´¿à´²àµâ€ à´šà´¾à´²à´¿à´šàµà´š à´•à´¥à´•à´³àµâ€ à´•àµ‡à´³àµâ€à´•àµà´•à´¾à´¨àµâ€ à´ªàµ†à´£àµà´£àµà´™àµà´™à´³à´Ÿà´•àµà´•à´‚ à´•à´¾à´¤àµà´¤à´¿à´°àµà´¨àµà´¨àµ†à´¾à´°àµ à´•à´¾à´²à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´Ÿàµ‡à´ªàµà´±àµ†à´•àµà´•à´¾à´°àµâ€à´¡à´±àµà´•à´³àµâ€ à´ªàµ‡à´¾à´²àµà´‚ à´…à´ªàµ‚à´°àµâ€à´µà´®à´¾à´¯à´¿à´°àµà´¨àµà´¨ à´…à´¨àµà´¨àµ à´…à´µà´°àµ† à´•à´¿à´¸àµà´¸à´•à´³àµâ€ à´ªà´±à´žàµà´žàµ à´ªà´¾à´Ÿà´¿à´¯àµà´£à´°àµâ€à´¤àµà´¤à´¿ à´’à´°àµ à´ªàµ†à´£àµâ€à´•àµà´Ÿàµà´Ÿà´¿. à´’à´°à´¿à´•àµà´•à´²àµâ€ à´•àµ‡à´Ÿàµà´Ÿà´¾à´²àµâ€ à´®à´¨à´¸à´¿à´²àµ†à´¨àµà´¨àµà´‚ à´¤à´™àµà´™à´¿à´¨à´¿à´²àµà´•àµà´•àµà´¨àµà´¨ à´’à´°àµ à´—à´¾à´¨à´‚ à´ªàµ‡à´¾à´²àµ†à´¯à´¾à´¯à´¿à´°àµà´¨àµà´¨àµ  à´•à´¾à´¥à´¿à´• à´†à´¯à´¿à´· à´¬àµ€à´—à´‚. \r\n\r\nà´šàµ‡à´°àµâ€à´¤àµà´¤àµà´µàµ†à´šàµà´š à´’à´°àµ à´¨à´¾à´¦à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´¨à´¿à´²à´¾à´µàµ à´ªà´°à´¨àµà´¨ à´†à´•à´¾à´¶à´¤àµà´¤à´¿à´¨àµ à´¤à´¾à´´àµ† à´‡à´¤à´³àµâ€à´µà´¿à´°à´¿à´¯àµà´¨àµà´¨ à´‡à´¶à´²à´¿à´²àµâ€ à´šà´¾à´²à´¿à´šàµà´š à´•à´¥à´•à´³àµâ€ à´•àµ‡à´³àµâ€à´•àµà´•à´¾à´¨àµâ€ à´ªàµ†à´£àµà´£àµà´™àµà´™à´³à´Ÿà´•àµà´•à´‚ à´•à´¾à´¤àµà´¤à´¿à´°àµà´¨àµà´¨àµ†à´¾à´°àµ à´•à´¾à´²à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´Ÿàµ‡à´ªàµà´±àµ†à´•àµà´•à´¾à´°àµâ€à´¡à´±àµà´•à´³àµâ€ à´ªàµ‡à´¾à´²àµà´‚ à´…à´ªàµ‚à´°àµâ€à´µà´®à´¾à´¯à´¿à´°àµà´¨àµà´¨ à´…à´¨àµà´¨àµ à´…à´µà´°àµ† à´•à´¿à´¸àµà´¸à´•à´³àµâ€ à´ªà´±à´žàµà´žàµ à´ªà´¾à´Ÿà´¿à´¯àµà´£à´°àµâ€à´¤àµà´¤à´¿ à´’à´°àµ à´ªàµ†à´£àµâ€à´•àµà´Ÿàµà´Ÿà´¿. à´’à´°à´¿à´•àµà´•à´²àµâ€ à´•àµ‡à´Ÿàµà´Ÿà´¾à´²àµâ€ à´®à´¨à´¸à´¿à´²àµ†à´¨àµà´¨àµà´‚ à´¤à´™àµà´™à´¿à´¨à´¿à´²àµà´•àµà´•àµà´¨àµà´¨ à´’à´°àµ à´—à´¾à´¨à´‚ à´ªàµ‡à´¾à´²àµ†à´¯à´¾à´¯à´¿à´°àµà´¨àµà´¨àµ  à´•à´¾à´¥à´¿à´• à´†à´¯à´¿à´· à´¬àµ€à´—à´‚. à´šàµ‡à´°àµâ€à´¤àµà´¤àµà´µàµ†à´šàµà´š à´’à´°àµ à´¨à´¾à´¦à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´¨à´¿à´²à´¾à´µàµ à´ªà´°à´¨àµà´¨ à´†à´•à´¾à´¶à´¤àµà´¤à´¿à´¨àµ à´¤à´¾à´´àµ† à´‡à´¤à´³àµâ€à´µà´¿à´°à´¿à´¯àµà´¨àµà´¨ à´‡à´¶à´²à´¿à´²àµâ€ à´šà´¾à´²à´¿à´šàµà´š à´•à´¥à´•à´³àµâ€ à´•àµ‡à´³àµâ€à´•àµà´•à´¾à´¨àµâ€ à´ªàµ†à´£àµà´£àµà´™àµà´™à´³à´Ÿà´•àµà´•à´‚ à´•à´¾à´¤àµà´¤à´¿à´°àµà´¨àµà´¨àµ†à´¾à´°àµ à´•à´¾à´²à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´Ÿàµ‡à´ªàµà´±àµ†à´•àµà´•à´¾à´°àµâ€à´¡à´±àµà´•à´³àµâ€ à´ªàµ‡à´¾à´²àµà´‚ à´…à´ªàµ‚à´°àµâ€à´µà´®à´¾à´¯à´¿à´°àµà´¨àµà´¨ à´…à´¨àµà´¨àµ à´…à´µà´°àµ† à´•à´¿à´¸àµà´¸à´•à´³àµâ€ à´ªà´±à´žàµà´žàµ à´ªà´¾à´Ÿà´¿à´¯àµà´£à´°àµâ€à´¤àµà´¤à´¿ à´’à´°àµ à´ªàµ†à´£àµâ€à´•àµà´Ÿàµà´Ÿà´¿. à´’à´°à´¿à´•àµà´•à´²àµâ€ à´•àµ‡à´Ÿàµà´Ÿà´¾à´²àµâ€ à´®à´¨à´¸à´¿à´²àµ†à´¨àµà´¨àµà´‚ à´¤à´™àµà´™à´¿à´¨à´¿à´²àµà´•àµà´•àµà´¨àµà´¨ à´’à´°àµ à´—à´¾à´¨à´‚ à´ªàµ‡à´¾à´²àµ†à´¯à´¾à´¯à´¿à´°àµà´¨àµà´¨àµ  à´•à´¾à´¥à´¿à´• à´†à´¯à´¿à´· à´¬àµ€à´—à´‚. \r\n\r\nà´šàµ‡à´°àµâ€à´¤àµà´¤àµà´µàµ†à´šàµà´š à´’à´°àµ à´¨à´¾à´¦à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´¨à´¿à´²à´¾à´µàµ à´ªà´°à´¨àµà´¨ à´†à´•à´¾à´¶à´¤àµà´¤à´¿à´¨àµ à´¤à´¾à´´àµ† à´‡à´¤à´³àµâ€à´µà´¿à´°à´¿à´¯àµà´¨àµà´¨ à´‡à´¶à´²à´¿à´²àµâ€ à´šà´¾à´²à´¿à´šàµà´š à´•à´¥à´•à´³àµâ€ à´•àµ‡à´³àµâ€à´•àµà´•à´¾à´¨àµâ€ à´ªàµ†à´£àµà´£àµà´™àµà´™à´³à´Ÿà´•àµà´•à´‚ à´•à´¾à´¤àµà´¤à´¿à´°àµà´¨àµà´¨àµ†à´¾à´°àµ à´•à´¾à´²à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´Ÿàµ‡à´ªàµà´±àµ†à´•àµà´•à´¾à´°àµâ€à´¡à´±àµà´•à´³àµâ€ à´ªàµ‡à´¾à´²àµà´‚ à´…à´ªàµ‚à´°àµâ€à´µà´®à´¾à´¯à´¿à´°àµà´¨àµà´¨ à´…à´¨àµà´¨àµ à´…à´µà´°àµ† à´•à´¿à´¸àµà´¸à´•à´³àµâ€ à´ªà´±à´žàµà´žàµ à´ªà´¾à´Ÿà´¿à´¯àµà´£à´°àµâ€à´¤àµà´¤à´¿ à´’à´°àµ à´ªàµ†à´£àµâ€à´•àµà´Ÿàµà´Ÿà´¿. à´’à´°à´¿à´•àµà´•à´²àµâ€ à´•àµ‡à´Ÿàµà´Ÿà´¾à´²àµâ€ à´®à´¨à´¸à´¿à´²àµ†à´¨àµà´¨àµà´‚ à´¤à´™àµà´™à´¿à´¨à´¿à´²àµà´•àµà´•àµà´¨àµà´¨ à´’à´°àµ à´—à´¾à´¨à´‚ à´ªàµ‡à´¾à´²àµ†à´¯à´¾à´¯à´¿à´°àµà´¨àµà´¨àµ  à´•à´¾à´¥à´¿à´• à´†à´¯à´¿à´· à´¬àµ€à´—à´‚. à´¦à´¯ à´¸à´¦à´¸àµà´¸àµà´•à´³àµâ€ à´¹àµƒà´¦à´¯à´¤àµà´¤àµ‡à´¾à´Ÿàµ à´•àµ‡à´°à´³à´¤àµà´¤à´¿à´²àµ† à´¸à´¹àµƒà´¦à´¯ à´¸à´¦à´¸àµà´¸àµà´•à´³àµâ€ à´¹àµƒà´¦à´¯à´¤àµà´¤àµ‡à´¾à´Ÿàµ à´šàµ‡à´°àµâ€à´¤àµà´¤àµà´µàµ†à´šàµà´š à´’à´°àµ à´¨à´¾à´¦à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´¨à´¿à´²à´¾à´µàµ à´ªà´°à´¨àµà´¨ à´†à´•à´¾à´¶à´¤àµà´¤à´¿à´¨àµ à´¤à´¾à´´àµ† à´‡à´¤à´³àµâ€à´µà´¿à´°à´¿à´¯àµà´¨àµà´¨ à´‡à´¶à´²à´¿à´²àµâ€ à´šà´¾à´²à´¿à´šàµà´š à´•à´¥à´•à´³àµâ€ à´•àµ‡à´³àµâ€à´•àµà´•à´¾à´¨àµâ€ à´ªàµ†à´£àµà´£àµà´™àµà´™à´³à´Ÿà´•àµà´•à´‚ à´•à´¾à´¤àµà´¤à´¿à´°àµà´¨àµà´¨àµ†à´¾à´°àµ à´•à´¾à´²à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´Ÿàµ‡à´ªàµà´±àµ†à´•àµà´•à´¾à´°àµâ€à´¡à´±àµà´•à´³àµâ€ à´ªàµ‡à´¾à´²àµà´‚ à´…à´ªàµ‚à´°àµâ€à´µà´®à´¾à´¯à´¿à´°àµà´¨àµà´¨ à´…à´¨àµà´¨àµ à´…à´µà´°àµ† à´•à´¿à´¸àµà´¸à´•à´³àµâ€ à´ªà´±à´žàµà´žàµ à´ªà´¾à´Ÿà´¿à´¯àµà´£à´°àµâ€à´¤àµà´¤à´¿ à´’à´°àµ à´ªàµ†à´£àµâ€à´•àµà´Ÿàµà´Ÿà´¿. à´’à´°à´¿à´•àµà´•à´²àµâ€ à´•àµ‡à´Ÿàµà´Ÿà´¾à´²àµâ€ à´®à´¨à´¸à´¿à´²àµ†à´¨àµà´¨àµà´‚ à´¤à´™àµà´™à´¿à´¨à´¿à´²àµà´•àµà´•àµà´¨àµà´¨ à´’à´°àµ à´—à´¾à´¨à´‚ à´ªàµ‡à´¾à´²àµ†à´¯à´¾à´¯à´¿à´°àµà´¨àµà´¨àµ  à´•à´¾à´¥à´¿à´• à´†à´¯à´¿à´· à´¬àµ€à´—à´‚. \r\n\r\nà´šàµ‡à´°àµâ€à´¤àµà´¤àµà´µàµ†à´šàµà´š à´’à´°àµ à´¨à´¾à´¦à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´¨à´¿à´²à´¾à´µàµ à´ªà´°à´¨àµà´¨ à´†à´•à´¾à´¶à´¤àµà´¤à´¿à´¨àµ à´¤à´¾à´´àµ† à´‡à´¤à´³àµâ€à´µà´¿à´°à´¿à´¯àµà´¨àµà´¨ à´‡à´¶à´²à´¿à´²àµâ€ à´šà´¾à´²à´¿à´šàµà´š à´•à´¥à´•à´³àµâ€ à´•àµ‡à´³àµâ€à´•àµà´•à´¾à´¨àµâ€ à´ªàµ†à´£àµà´£àµà´™àµà´™à´³à´Ÿà´•àµà´•à´‚ à´•à´¾à´¤àµà´¤à´¿à´°àµà´¨àµà´¨àµ†à´¾à´°àµ à´•à´¾à´²à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´Ÿàµ‡à´ªàµà´±àµ†à´•àµà´•à´¾à´°àµâ€à´¡à´±àµà´•à´³àµâ€ à´ªàµ‡à´¾à´²àµà´‚ à´…à´ªàµ‚à´°àµâ€à´µà´®à´¾à´¯à´¿à´°àµà´¨àµà´¨ à´…à´¨àµà´¨àµ à´…à´µà´°àµ† à´•à´¿à´¸àµà´¸à´•à´³àµâ€ à´ªà´±à´žàµà´žàµ à´ªà´¾à´Ÿà´¿à´¯àµà´£à´°àµâ€à´¤àµà´¤à´¿ à´’à´°àµ à´ªàµ†à´£àµâ€à´•àµà´Ÿàµà´Ÿà´¿. à´’à´°à´¿à´•àµà´•à´²àµâ€ à´•àµ‡à´Ÿàµà´Ÿà´¾à´²àµâ€ à´®à´¨à´¸à´¿à´²àµ†à´¨àµà´¨àµà´‚ à´¤à´™àµà´™à´¿à´¨à´¿à´²àµà´•àµà´•àµà´¨àµà´¨ à´’à´°àµ à´—à´¾à´¨à´‚ à´ªàµ‡à´¾à´²àµ†à´¯à´¾à´¯à´¿à´°àµà´¨àµà´¨àµ  à´•à´¾à´¥à´¿à´• à´†à´¯à´¿à´· à´¬àµ€à´—à´‚. à´šàµ‡à´°àµâ€à´¤àµà´¤àµà´µàµ†à´šàµà´š à´’à´°àµ à´¨à´¾à´¦à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´¨à´¿à´²à´¾à´µàµ à´ªà´°à´¨àµà´¨ à´†à´•à´¾à´¶à´¤àµà´¤à´¿à´¨àµ à´¤à´¾à´´àµ† à´‡à´¤à´³àµâ€à´µà´¿à´°à´¿à´¯àµà´¨àµà´¨ à´‡à´¶à´²à´¿à´²àµâ€ à´šà´¾à´²à´¿à´šàµà´š à´•à´¥à´•à´³àµâ€ à´•àµ‡à´³àµâ€à´•àµà´•à´¾à´¨àµâ€ à´ªàµ†à´£àµà´£àµà´™àµà´™à´³à´Ÿà´•àµà´•à´‚ à´•à´¾à´¤àµà´¤à´¿à´°àµà´¨àµà´¨àµ†à´¾à´°àµ à´•à´¾à´²à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´Ÿàµ‡à´ªàµà´±àµ†à´•àµà´•à´¾à´°àµâ€à´¡à´±àµà´•à´³àµâ€ à´ªàµ‡à´¾à´²àµà´‚ à´…à´ªàµ‚à´°àµâ€à´µà´®à´¾à´¯à´¿à´°àµà´¨àµà´¨ à´…à´¨àµà´¨àµ à´…à´µà´°àµ† à´•à´¿à´¸àµà´¸à´•à´³àµâ€ à´ªà´±à´žàµà´žàµ à´ªà´¾à´Ÿà´¿à´¯àµà´£à´°àµâ€à´¤àµà´¤à´¿ à´’à´°àµ à´ªàµ†à´£àµâ€à´•àµà´Ÿàµà´Ÿà´¿. à´’à´°à´¿à´•àµà´•à´²àµâ€ à´•àµ‡à´Ÿàµà´Ÿà´¾à´²àµâ€ à´®à´¨à´¸à´¿à´²àµ†à´¨àµà´¨àµà´‚ à´¤à´™àµà´™à´¿à´¨à´¿à´²àµà´•àµà´•àµà´¨àµà´¨ à´’à´°àµ à´—à´¾à´¨à´‚ à´ªàµ‡à´¾à´²àµ†à´¯à´¾à´¯à´¿à´°àµà´¨àµà´¨àµ  à´•à´¾à´¥à´¿à´• à´†à´¯à´¿à´· à´¬àµ€à´—à´‚. \r\n\r\nà´šàµ‡à´°àµâ€à´¤àµà´¤àµà´µàµ†à´šàµà´š à´’à´°àµ à´¨à´¾à´¦à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´¨à´¿à´²à´¾à´µàµ à´ªà´°à´¨àµà´¨ à´†à´•à´¾à´¶à´¤àµà´¤à´¿à´¨àµ à´¤à´¾à´´àµ† à´‡à´¤à´³àµâ€à´µà´¿à´°à´¿à´¯àµà´¨àµà´¨ à´‡à´¶à´²à´¿à´²àµâ€ à´šà´¾à´²à´¿à´šàµà´š à´•à´¥à´•à´³àµâ€ à´•àµ‡à´³àµâ€à´•àµà´•à´¾à´¨àµâ€ à´ªàµ†à´£àµà´£àµà´™àµà´™à´³à´Ÿà´•àµà´•à´‚ à´•à´¾à´¤àµà´¤à´¿à´°àµà´¨àµà´¨àµ†à´¾à´°àµ à´•à´¾à´²à´®àµà´£àµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´Ÿàµ‡à´ªàµà´±àµ†à´•àµà´•à´¾à´°àµâ€à´¡à´±àµà´•à´³àµâ€ à´ªàµ‡à´¾à´²àµà´‚ à´…à´ªàµ‚à´°àµâ€à´µà´®à´¾à´¯à´¿à´°àµà´¨àµà´¨ à´…à´¨àµà´¨àµ à´…à´µà´°àµ† à´•à´¿à´¸àµà´¸à´•à´³àµâ€ à´ªà´±à´žàµà´žàµ à´ªà´¾à´Ÿà´¿à´¯àµà´£à´°àµâ€à´¤àµà´¤à´¿ à´’à´°àµ à´ªàµ†à´£àµâ€à´•àµà´Ÿàµà´Ÿà´¿. à´’à´°à´¿à´•àµà´•à´²àµâ€ à´•àµ‡à´Ÿàµà´Ÿà´¾à´²àµâ€ à´®à´¨à´¸à´¿à´²àµ†à´¨àµà´¨àµà´‚ à´¤à´™àµà´™à´¿à´¨à´¿à´²àµà´•àµà´•àµà´¨àµà´¨ à´’à´°àµ à´—à´¾à´¨à´‚ à´ªàµ‡à´¾à´²àµ†à´¯à´¾à´¯à´¿à´°àµà´¨àµà´¨àµ  à´•à´¾à´¥à´¿à´• à´†à´¯à´¿à´· à´¬àµ€à´—à´‚. ', '10/08/2015'),
(4, 'à´°àµ‚à´ªà´¯àµà´Ÿàµ† à´®àµ‚à´²àµà´¯à´‚ à´‡à´Ÿà´¿', 'à´°àµ‚à´ªà´¯àµà´Ÿàµ† à´®àµ‚à´²àµà´¯à´¤àµà´¤à´¿à´²àµâ€ à´µà´¨àµâ€ à´‡à´Ÿà´¿à´µàµ. à´¡àµ‡à´¾à´³à´±à´¿à´¨àµ†à´¤à´¿à´°àµ† à´°àµ‚à´ªà´¯àµà´Ÿàµ† à´®àµ‚à´²àµà´¯à´‚ 57 à´ªàµˆà´¸à´¯à´¾à´£àµ à´‡à´Ÿà´¿à´žàµà´žà´¤àµ. 64.76 à´°àµ‚à´ªà´¯à´¾à´£àµ à´¨à´¿à´²à´µà´¿à´²àµâ€ à´¡àµ‡à´¾à´³à´±à´¿à´¨àµ†à´¤à´¿à´°àµ† à´°àµ‚à´ªà´¯àµà´Ÿàµ† à´®àµ‚à´²àµà´¯à´‚. 2013 à´¸àµ†à´ªàµà´±àµà´±à´‚à´¬à´±à´¿à´¨àµ à´¶àµ‡à´·à´‚ à´‡à´¤àµà´°à´¯àµà´‚ à´‡à´Ÿà´¿à´µàµà´£àµà´Ÿà´¾à´•àµà´¨àµà´¨à´¤àµ à´†à´¦àµà´¯à´®à´¾à´¯à´¾à´£àµ.', '13/08/2015'),
(15, 'à´•àµ‡à´¾à´´à´¿à´•àµà´•àµ‡à´¾à´Ÿàµà´Ÿàµ à´•àµ†.', '\r\nà´•àµ‡à´¾à´´à´¿à´•àµà´•àµ‡à´¾à´Ÿàµà´Ÿàµ à´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´œàµ€à´µà´¨à´•àµà´•à´¾à´°àµâ€ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµà´¨àµà´¨àµ\r\nPublished on Tue, 09/01/2015 - 09:11 ( 2 hours 36 min ago)\r\n(+)(-) Font Size\r\n   ShareThis\r\nà´•àµ‡à´¾à´´à´¿à´•àµà´•àµ‡à´¾à´Ÿàµà´Ÿàµ à´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´œàµ€à´µà´¨à´•àµà´•à´¾à´°àµâ€ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµà´¨àµà´¨àµ\r\n\r\nà´•àµ‡à´¾à´´à´¿à´•àµà´•àµ‡à´¾à´Ÿàµ: à´ªà´¾à´µà´™àµà´™à´¾à´Ÿàµ à´•àµ†.à´Žà´¸àµ.à´†à´°àµâ€.à´Ÿà´¿.à´¸à´¿ à´¡à´¿à´ªàµà´ªàµ‡à´¾ à´œàµ€à´µà´¨à´•àµà´•à´¾à´°àµâ€ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµà´¨àµà´¨àµ. à´ªà´¾à´µà´™àµà´™à´¾à´Ÿàµ à´¡à´¿à´ªàµà´ªàµ‡à´¾ à´ªàµ‚à´°àµâ€à´£à´®à´¾à´¯àµà´‚ à´•àµ‡à´¾à´´à´¿à´•àµà´•àµ‡à´¾à´Ÿàµ à´®à´¾à´µàµ‚à´°àµâ€ à´±àµ‡à´¾à´¡à´¿à´²àµâ€ à´ªàµà´¤àµà´¤à´¾à´¯à´¿ à´ªà´£à´¿ à´•à´´à´¿à´ªàµà´ªà´¿à´šàµà´š à´¡à´¿à´ªàµà´ªàµ‡à´¾à´¯à´¿à´²àµ‡à´•àµà´•àµ à´®à´¾à´±àµà´±à´£à´®àµ†à´¨àµà´¨à´¾à´µà´¶àµà´¯à´ªàµà´ªàµ†à´Ÿàµà´Ÿà´¾à´£àµ à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•àµ. à´œàµ€à´µà´¨à´•àµà´•à´¾à´°àµà´Ÿàµ† à´ªà´£à´¿à´®àµà´Ÿà´•àµà´•à´¿à´¨àµ† à´¤àµà´Ÿà´°àµâ€à´¨àµà´¨àµ à´ªà´¾à´µà´™àµà´™à´¾à´Ÿàµ à´¡à´¿à´ªàµà´ªàµ‡à´¾à´¯à´¿à´²àµâ€ à´¨à´¿à´¨àµà´¨àµà´³àµà´³ à´¸à´°àµâ€à´µàµ€à´¸àµà´•à´³àµâ€ à´ªàµ‚à´°àµâ€à´£à´®à´¾à´¯àµà´‚ à´®àµà´Ÿà´™àµà´™à´¿.', '01/09/2015'),
(16, 'shahsad adipoliyanu', 'kidilana\r\nAtrak pora', '01/09/2015'),
(17, 'à´‡à´¨àµà´¤àµà´¯à´•àµà´•àµ à´œà´¯à´‚ à´à´´àµ', 'à´•àµ†à´¾à´³à´‚à´¬àµ‡à´¾: à´’à´°àµ à´¦à´¿à´µà´¸à´µàµà´‚ à´à´´àµ à´µà´¿à´•àµà´•à´±àµà´±àµà´‚ à´…à´ªàµà´ªàµà´±à´¤àµà´¤àµ à´‡à´¨àµà´¤àµà´¯à´¯àµ† à´•à´¾à´¤àµà´¤à´¿à´°à´¿à´•àµà´•àµà´¨àµà´¨à´¤àµ 23 à´µà´°àµâ€à´·à´‚ à´¨àµ€à´£àµà´Ÿ à´µà´°à´³àµâ€à´šàµà´šà´¯àµà´Ÿàµ† à´…à´±àµà´¤à´¿à´¯à´¾à´£àµ. à´…à´¤à´¿à´¶à´¯à´™àµà´™à´³àµâ€ à´¸à´‚à´­à´µà´¿à´šàµ ...', '01/09/2015'),
(18, 'à´²àµ‡à´¾à´•à´•à´ªàµà´ªàµ à´¯àµ‡à´¾à´—àµà´¯à´¤', 'à´²àµ‡à´¾à´•à´•à´ªàµà´ªàµ à´¯àµ‡à´¾à´—àµà´¯à´¤à´¾à´®à´¤àµà´¸à´°à´¤àµà´¤à´¿à´²àµâ€ à´‡à´±à´¾à´¨àµ† à´¨àµ‡à´°à´¿à´Ÿà´¾à´¨àµ†à´¾à´°àµà´™àµà´™àµà´¨àµà´¨ à´‡à´¨àµà´¤àµà´¯à´•àµà´•àµ à´¸àµ—à´¹àµƒà´¦ à´ªàµ‡à´¾à´°à´¾à´Ÿàµà´Ÿà´¤àµà´¤à´¿à´²àµâ€ à´¨àµ‡à´ªàµà´ªà´¾à´³à´¿à´¨àµ à´®àµà´¨àµà´¨à´¿à´²àµâ€ à´¸à´®à´¨à´¿à´². à´ªàµà´£àµ† à´¬à´¾à´²àµ†à´µà´¾à´¡à´¿ à´¸àµà´±àµà´±àµ‡à´¡à´¿à´¯à´¤àµà´¤à´¿à´²àµâ€ à´¨à´Ÿà´¨àµà´¨ à´®à´¤àµà´¸à´°à´¤àµà´¤à´¿à´²àµâ€ à´’à´°àµ à´—àµ‡à´¾à´³àµâ€à´ªàµ‡à´¾à´²àµà´®à´Ÿà´¿à´•àµà´•à´¾à´¤àµ†à´¯à´¾à´£àµ à´¸àµà´¨à´¿à´²àµâ€ à´›àµ‡à´¤àµà´°à´¿à´¯àµà´‚ à´¸à´‚à´˜à´µàµà´‚ à´¨à´¿à´°à´¾à´¶à´°à´¾à´¯à´¤àµ. à´ªà´°à´¿à´·àµà´•à´°à´¿à´šàµà´š à´œà´´àµà´¸à´¿à´¯à´¿à´²à´¿à´±à´™àµà´™à´¿à´¯ à´‡à´¨àµà´¤àµà´¯ à´•àµ‡à´¾à´šàµà´šàµ à´¸àµà´±àµà´±àµ€à´«à´¨àµâ€ à´•àµ‡à´¾à´£àµâ€à´¸àµà´±àµà´±à´¨àµâ€àµˆà´±à´¨àµâ€ à´ªà´•à´°àµâ€à´¨àµà´¨àµà´¨à´²àµâ€à´•à´¿à´¯ à´ªàµà´¤à´¿à´¯ à´ªà´¾à´ à´™àµà´™à´³àµà´‚ à´—àµà´°àµ—à´£àµà´Ÿà´¿à´²àµâ€ à´ªàµà´°à´¾à´µà´°àµâ€à´¤àµà´¤à´¿à´•à´®à´¾à´•àµà´•à´¿. à´ªàµà´°à´¤à´¿à´°àµ‡à´¾à´§à´¤àµà´¤à´¿à´²àµâ€ à´§à´¨àµâ€à´ªà´¾à´²àµâ€ à´—à´£àµ‡à´·àµà´‚ à´¸à´¨àµà´¦àµ‡à´¶àµ à´œà´¿à´™àµà´•à´¾à´¨àµà´‚ à´’à´¨àµà´¨à´¿à´šàµà´šàµ à´¨à´Ÿà´¤àµà´¤à´¿à´¯ à´¨àµ€à´•àµà´•à´™àµà´™à´³àµâ€ à´µà´°à´¾à´¨à´¿à´°à´¿à´•àµà´•àµà´¨àµà´¨ à´®à´¤àµà´¸à´°à´™àµà´™à´³àµâ€à´•àµà´•àµà´³àµà´³ à´¸à´¨àµà´¦àµ‡à´¶à´‚à´•àµ‚à´Ÿà´¿à´¯à´¾à´¯à´¿à´°àµà´¨àµà´¨àµ. à´Žà´¨àµà´¨à´¾à´²àµâ€, à´›àµ‡à´¤àµà´°à´¿à´¯àµà´‚ à´œàµ†à´œàµ† à´²à´¾à´²àµâ€à´ªàµ†à´•àµà´²àµà´µà´¯àµà´‚ à´±àµ‡à´¾à´¬à´¿à´¨àµâ€ à´¸à´¿à´™àµà´™àµà´‚ à´¨à´¯à´¿à´šàµà´š à´®àµà´¨àµà´¨àµ‡à´±àµà´±à´¤àµà´¤à´¿à´¨àµ à´Žà´¤à´¿à´°àµâ€à´µà´² à´•àµà´²àµà´•àµà´•à´¾à´¨à´¾à´¯à´¿à´²àµà´³àµ†à´¨àµà´¨à´¤àµ à´¨à´¿à´°à´¾à´¶à´ªàµà´ªàµ†à´Ÿàµà´¤àµà´¤à´¿.\r\nà´®à´²à´¯à´¾à´³à´¿ à´¤à´¾à´°à´‚ à´¸à´¿.à´•àµ†. à´µà´¿à´¨àµ€à´¤àµ 66à´¾à´‚ à´®à´¿à´¨à´¿à´±àµà´±à´¿à´²àµâ€ à´ªà´•à´°à´•àµà´•à´¾à´°à´¨à´¾à´¯à´¿ à´‡à´±à´™àµà´™à´¿à´¯à´¿à´°àµà´¨àµà´¨àµ.\r\nà´†à´¦àµà´¯ à´ªà´•àµà´¤à´¿à´¯à´¿à´²àµâ€ à´¶àµà´°à´¦àµà´§àµ‡à´¯à´®à´¾à´¯ à´¨àµ€à´•àµà´•à´™àµà´™à´³àµâ€ à´¨à´Ÿà´¤àµà´¤à´¿à´¯àµ†à´™àµà´•à´¿à´²àµà´‚ à´¨à´¿à´°àµâ€à´­à´¾à´—àµà´¯à´‚ à´¨àµ€à´²à´ªàµà´ªà´Ÿà´•àµà´•àµ à´µà´¿à´¨à´¯à´¾à´¯à´¿. 44à´¾à´‚ à´®à´¿à´¨à´¿à´±àµà´±à´¿à´²àµâ€ à´¯àµ‚à´œà´¿à´¨àµâ€à´¸à´£àµâ€ à´²à´¿à´™àµà´¦àµ‡à´¾à´¯àµà´Ÿàµ† à´®à´¿à´•à´šàµà´šàµ†à´¾à´°àµ à´·àµ‡à´¾à´Ÿàµà´Ÿàµ à´à´±àµ† à´ªàµà´°à´¯à´¾à´¸à´ªàµà´ªàµ†à´Ÿàµà´Ÿà´¾à´¯à´¿à´°àµà´¨àµà´¨àµ à´Žà´¤à´¿à´°àµâ€à´ªàµà´°à´¤à´¿à´°àµ‡à´¾à´§à´‚ à´¤à´Ÿàµà´Ÿà´¿à´¯à´•à´±àµà´±à´¿à´¯à´¤àµ. à´°à´£àµà´Ÿà´¾à´‚ à´ªà´•àµà´¤à´¿à´¯àµà´Ÿàµ† à´…à´µà´¸à´¾à´¨à´‚ à´µà´°àµ† à´‡à´¤àµ à´¤àµà´Ÿà´°àµâ€à´¨àµà´¨àµ†à´™àµà´•à´¿à´²àµà´‚ à´—àµ‡à´¾à´³àµâ€à´®à´¾à´¤àµà´°à´‚ à´ªà´¿à´±à´¨àµà´¨à´¿à´²àµà´²', '01/09/2015'),
(40, 'Farahana Bday', 'Today is Farahana Bday', '10/09/2015'),
(41, 'treat is ready', 'farhana treat is ready in bodhi', '10/09/2015'),
(43, 'Asnad', 'beegaran', '17/09/2015'),
(44, 'Asnad', 'kodum beegaran', '17/09/2015'),
(45, 'Asnad', 'puli', '17/09/2015'),
(46, 'Asnad', 'midukan', '18/09/2015'),
(49, 'new i10 for sale', 'new i10 for salenew i10 for salenew i10 for sale', '12/09/2015'),
(50, 'Happy b day', 'Anwar bday is 2day', '14/09/2015'),
(51, 'Shahsad resign cheukayaaanu', 'Shahsad resign  and going to dubai', '25/09/2015'),
(52, 'test', 'scs', '12/11/2015');

-- --------------------------------------------------------

--
-- Table structure for table `tblresource`
--

CREATE TABLE `tblresource` (
  `ID` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `position` varchar(200) NOT NULL,
  `contactNo` varchar(15) NOT NULL,
  `type` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresource`
--

INSERT INTO `tblresource` (`ID`, `name`, `position`, `contactNo`, `type`) VALUES
(1, 'Rashid', 'Test', '989578855', 0),
(3, 'à´±à´¾à´·à´¿à´¦àµâ€Œ', '', '+97143519759', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblshop`
--

CREATE TABLE `tblshop` (
  `ID` int(11) NOT NULL,
  `typeID` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblshop`
--

INSERT INTO `tblshop` (`ID`, `typeID`, `name`, `phone`) VALUES
(1, 1, 'abcs', '+97143519759'),
(2, 1, 'Rashid', '23323'),
(3, 1, 'Test', '2222222'),
(4, 2, 'hotel', '98456');

-- --------------------------------------------------------

--
-- Table structure for table `tblshoptype`
--

CREATE TABLE `tblshoptype` (
  `ID` int(11) NOT NULL,
  `shopType` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblshoptype`
--

INSERT INTO `tblshoptype` (`ID`, `shopType`) VALUES
(1, 'Bakery'),
(2, 'Hotel');

-- --------------------------------------------------------

--
-- Table structure for table `tbltaxi`
--

CREATE TABLE `tbltaxi` (
  `ID` int(11) NOT NULL,
  `vehicleTypeId` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltaxi`
--

INSERT INTO `tbltaxi` (`ID`, `vehicleTypeId`, `name`, `phone`) VALUES
(1, 1, 'Rashid', '9895578855'),
(2, 2, 'ads', '23323'),
(3, 1, 'abc', '322');

-- --------------------------------------------------------

--
-- Table structure for table `tbltaxitype`
--

CREATE TABLE `tbltaxitype` (
  `ID` int(11) NOT NULL,
  `type` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbltaxitype`
--

INSERT INTO `tbltaxitype` (`ID`, `type`) VALUES
(1, 'Auto'),
(2, 'Car'),
(3, 'Jeep'),
(4, 'Auto-Taxi');

-- --------------------------------------------------------

--
-- Table structure for table `tblusers`
--

CREATE TABLE `tblusers` (
  `ID` int(11) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblusers`
--

INSERT INTO `tblusers` (`ID`, `mobile`, `email`) VALUES
(1, '', ''),
(2, '', ''),
(3, '', ''),
(4, '', ''),
(5, '', ''),
(6, '', ''),
(7, '328', 'asnad.bodhi@gmail.com'),
(8, '', ''),
(9, '1572', 'asnad.bodhi@gmail.com'),
(10, '333333', 'asnad.bodhi@gmail.com'),
(11, '255522', 'asnad.bodhi@gmail.com'),
(12, '964855', 'asnad.bodhi@gmail.com'),
(13, '', ''),
(14, '532', 'asnad.bodhi@gmail.com'),
(15, '532', 'asnad.bodhi@gmail.com'),
(16, '532', 'asnad.bodhi@gmail.com'),
(17, '52858', 'asnad.bodhi@gmail.com'),
(18, '2576', 'asnad.bodhi@gmail.com'),
(19, '888', 'asnad.bodhi@gmail.com'),
(20, '9746410718', 'cksindhuja007@gmail.com'),
(21, '9746410718', 'cksindhuja007@gmail.com'),
(22, '9746410718', 'cksindhuja007@gmail.com'),
(23, '535', 'cksindhuja007@gmail.com'),
(24, '5974', 'asnad.bodhi@gmail.com'),
(25, '654328', 'asnad.bodhi@gmail.com'),
(26, '9605134048', 'shafeequemat@gmail.com'),
(27, '9605134048', 'shafeequemat@gmail.com'),
(28, '64343434', 'cksindhuja007@gmail.com'),
(29, '64343434', 'cksindhuja007@gmail.com'),
(30, '567526', 'shafeequemat@gmail.com'),
(31, '958662', 'asnad.bodhi@gmail.com'),
(32, '456', 'asnad.bodhi@gmail.com'),
(33, '456', 'asnad.bodhi@gmail.com'),
(34, '', ''),
(35, '', ''),
(36, '', ''),
(37, '', ''),
(38, '', ''),
(39, '', ''),
(40, '', ''),
(41, '9605134048', 'shafeequemat@gmail.com'),
(42, '', ''),
(43, '98955783888', ''),
(44, '9847843177', 'anwarsadthz@gmail.com'),
(45, '9847843177', 'anwarsadthz@gmail.com'),
(46, '9567129796', ''),
(47, '8086834568', ''),
(48, '9072559277', 'shamsup1965@gmail.com'),
(49, '9072559277', 'shamsup1965@gmail.com'),
(50, '9567129796', ''),
(51, '9746410718', 'asnad0006@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `tcissue`
--

CREATE TABLE `tcissue` (
  `ID` int(11) NOT NULL,
  `tcNo` varchar(20) NOT NULL,
  `dateOfTc` date NOT NULL,
  `adNo` varchar(20) NOT NULL,
  `adDate` date NOT NULL,
  `name` varchar(20) NOT NULL,
  `class` varchar(20) NOT NULL,
  `division` varchar(20) NOT NULL,
  `dob` date NOT NULL,
  `qualiStatus` varchar(50) NOT NULL,
  `reason` text NOT NULL,
  `placeToGo` text NOT NULL,
  `tcIssueStatus` varchar(20) NOT NULL,
  `printcount` int(11) NOT NULL DEFAULT '0',
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teacherallocation`
--

CREATE TABLE `teacherallocation` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `division` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacherallocation`
--

INSERT INTO `teacherallocation` (`ID`, `acYear`, `class`, `division`, `teacher`, `loginId`) VALUES
(1, 1, 1, 1, 2, 1),
(2, 1, 1, 2, 1, 1),
(3, 1, 2, 3, 5, 1),
(4, 1, 2, 4, 6, 1),
(5, 1, 3, 5, 7, 1),
(6, 1, 3, 6, 8, 1);

-- --------------------------------------------------------

--
-- Table structure for table `teacherchat`
--

CREATE TABLE `teacherchat` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `message` text NOT NULL,
  `description` text NOT NULL,
  `senderId` int(11) NOT NULL,
  `senderType` varchar(20) NOT NULL,
  `recieverType` varchar(20) NOT NULL,
  `recieverId` int(11) NOT NULL,
  `sendDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teacherchat`
--

INSERT INTO `teacherchat` (`ID`, `acYear`, `message`, `description`, `senderId`, `senderType`, `recieverType`, `recieverId`, `sendDate`) VALUES
(1, 1, 'HJBSDJFh kj:doij', 'iuhikjfshigjlorjiawEUWIFJVO NKFLNBHSV', 1, 'teacher', 'parent', 1574, '2016-06-10');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `ID` int(11) NOT NULL,
  `from_ledger` int(11) NOT NULL,
  `to_ledger` int(11) NOT NULL,
  `voucher_no` int(11) NOT NULL,
  `voucher_type` text NOT NULL,
  `credit` double NOT NULL,
  `debit` double DEFAULT NULL,
  `remark` text,
  `cdate` date NOT NULL,
  `transaction_date` date NOT NULL,
  `uid` int(11) NOT NULL,
  `b_id` int(11) DEFAULT NULL,
  `proid` int(11) NOT NULL DEFAULT '0',
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`ID`, `from_ledger`, `to_ledger`, `voucher_no`, `voucher_type`, `credit`, `debit`, `remark`, `cdate`, `transaction_date`, `uid`, `b_id`, `proid`, `loginId`) VALUES
(1, 2, 5, 1, 'Mess Fee Payments', 105, 0, 'Mess - Admission No: 1001', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(2, 5, 2, 1, 'Mess Fee Payments', 0, 105, 'Mess - Admission No: 1001', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(5, 2, 5, 1, 'Fee Payments', 2000, 0, 'Admission No: 1001', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(6, 5, 2, 1, 'Fee Payments', 0, 2000, 'Admission No: 1001', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(7, 2, 3, 1, 'Salary Payments', 0, 16500, ' Staff Id : 1', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(8, 3, 2, 1, 'Salary Payments', 16500, 0, 'Staff Id : 1', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(9, 2, 3, 1, 'Account Transfer', 0, 1000, '', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(10, 3, 2, 1, 'Account Transfer', 1000, 0, '', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(13, 2, 6, 1, 'Other Payments', 0, 500, '', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(14, 6, 2, 1, 'Other Payments', 500, 0, '', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(15, 2, 6, 2, 'Other Payments', 0, 500, '', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(16, 6, 2, 2, 'Other Payments', 500, 0, '', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(17, 2, 4, 1, 'Other Receipts', 1000, 0, '', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(18, 4, 2, 1, 'Other Receipts', 0, 1000, '', '2015-10-28', '2015-10-28', 1, NULL, 0, 1),
(37, 2, 5, 8, 'Fee Payments', 2000, 0, 'Admission No: 1580', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(38, 5, 2, 8, 'Fee Payments', 0, 2000, 'Admission No: 1580', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(39, 2, 5, 7, 'Fee Payments', 2000, 0, 'Admission No: 1577', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(40, 5, 2, 7, 'Fee Payments', 0, 2000, 'Admission No: 1577', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(41, 2, 5, 6, 'Fee Payments', 2000, 0, 'Admission No: 1006', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(42, 5, 2, 6, 'Fee Payments', 0, 2000, 'Admission No: 1006', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(43, 2, 5, 5, 'Fee Payments', 2000, 0, 'Admission No: 1570', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(44, 5, 2, 5, 'Fee Payments', 0, 2000, 'Admission No: 1570', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(47, 2, 5, 2, 'Fee Payments', 2000, 0, 'Admission No: 1007', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(48, 5, 2, 2, 'Fee Payments', 0, 2000, 'Admission No: 1007', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(49, 2, 5, 3, 'Fee Payments', 2000, 0, 'Admission No: 1065', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(50, 5, 2, 3, 'Fee Payments', 0, 2000, 'Admission No: 1065', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(51, 2, 5, 4, 'Fee Payments', 2000, 0, 'Admission No: 1008', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(52, 5, 2, 4, 'Fee Payments', 0, 2000, 'Admission No: 1008', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(53, 2, 5, 2, 'Mess Fee Payments', 390, 0, 'Mess - Admission No: 1007', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(54, 5, 2, 2, 'Mess Fee Payments', 0, 390, 'Mess - Admission No: 1007', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(55, 2, 5, 3, 'Mess Fee Payments', 390, 0, 'Mess - Admission No: 1009', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(56, 5, 2, 3, 'Mess Fee Payments', 0, 390, 'Mess - Admission No: 1009', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(57, 2, 5, 4, 'Mess Fee Payments', 390, 0, 'Mess - Admission No: 1008', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(58, 5, 2, 4, 'Mess Fee Payments', 0, 390, 'Mess - Admission No: 1008', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(59, 2, 5, 5, 'Mess Fee Payments', 390, 0, 'Mess - Admission No: 1004', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(60, 5, 2, 5, 'Mess Fee Payments', 0, 390, 'Mess - Admission No: 1004', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(61, 2, 5, 6, 'Mess Fee Payments', 390, 0, 'Mess - Admission No: 1006', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(62, 5, 2, 6, 'Mess Fee Payments', 0, 390, 'Mess - Admission No: 1006', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(63, 2, 5, 7, 'Mess Fee Payments', 300, 0, 'Mess - Admission No: 1577', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(64, 5, 2, 7, 'Mess Fee Payments', 0, 300, 'Mess - Admission No: 1577', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(65, 2, 5, 8, 'Mess Fee Payments', 390, 0, 'Mess - Admission No: 1606', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(66, 5, 2, 8, 'Mess Fee Payments', 0, 390, 'Mess - Admission No: 1606', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(67, 2, 5, 9, 'Mess Fee Payments', 300, 0, 'Mess - Admission No: 1611', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(68, 5, 2, 9, 'Mess Fee Payments', 0, 300, 'Mess - Admission No: 1611', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(69, 2, 5, 2, 'Bus Fee Payments', 250, 0, 'Bus - Admission No: 1620', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(70, 5, 2, 2, 'Bus Fee Payments', 0, 250, 'Bus - Admission No: 1620', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(71, 2, 5, 3, 'Bus Fee Payments', 200, 0, 'Bus - Admission No: 1616', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(72, 5, 2, 3, 'Bus Fee Payments', 0, 200, 'Bus - Admission No: 1616', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(73, 2, 5, 4, 'Bus Fee Payments', 250, 0, 'Bus - Admission No: 1582', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(74, 5, 2, 4, 'Bus Fee Payments', 0, 250, 'Bus - Admission No: 1582', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(75, 2, 5, 5, 'Bus Fee Payments', 200, 0, 'Bus - Admission No: 1014', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(76, 5, 2, 5, 'Bus Fee Payments', 0, 200, 'Bus - Admission No: 1014', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(77, 2, 5, 6, 'Bus Fee Payments', 250, 0, 'Bus - Admission No: 1065', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(78, 5, 2, 6, 'Bus Fee Payments', 0, 250, 'Bus - Admission No: 1065', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(81, 2, 3, 3, 'Salary Payments', 0, 11000, ' Staff Id : 3', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(82, 3, 2, 3, 'Salary Payments', 11000, 0, 'Staff Id : 3', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(83, 2, 3, 2, 'Salary Payments', 0, 14000, 'Staff Id : 2', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(84, 3, 2, 2, 'Salary Payments', 14000, 0, 'Staff Id : 2', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(85, 2, 3, 4, 'Salary Payments', 0, 17000, ' Staff Id : 5', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(86, 3, 2, 4, 'Salary Payments', 17000, 0, 'Staff Id : 5', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(87, 2, 3, 5, 'Salary Payments', 0, 17600, ' Staff Id : 7', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(88, 3, 2, 5, 'Salary Payments', 17600, 0, 'Staff Id : 7', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(89, 2, 5, 2, 'Other Receipts', 50000, 0, '', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(90, 5, 2, 2, 'Other Receipts', 0, 50000, '', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(91, 2, 6, 2, 'Account Transfer', 0, 60000, '', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(92, 6, 2, 2, 'Account Transfer', 60000, 0, '', '2015-11-12', '2015-11-12', 1, NULL, 0, 1),
(93, 2, 5, 10, 'Mess Fee Payments', 345, 0, 'Mess - Admission No: 1014', '2015-11-13', '2015-11-13', 1, NULL, 0, 1),
(94, 5, 2, 10, 'Mess Fee Payments', 0, 345, 'Mess - Admission No: 1014', '2015-11-13', '2015-11-13', 1, NULL, 0, 1),
(95, 2, 5, 10, 'Fee Payments', 2000, 0, 'Admission No: 1014', '2015-11-13', '2015-11-13', 1, NULL, 0, 1),
(96, 5, 2, 10, 'Fee Payments', 0, 2000, 'Admission No: 1014', '2015-11-13', '2015-11-13', 1, NULL, 0, 1),
(97, 2, 5, 1, 'Bus Fee Payments', 200, 0, 'Bus - Admission No: 1570', '2015-11-21', '2015-10-28', 1, NULL, 0, 1),
(98, 5, 2, 1, 'Bus Fee Payments', 0, 200, 'Bus - Admission No: 1570', '2015-11-21', '2015-10-28', 1, NULL, 0, 1),
(99, 2, 5, 7, 'Bus Fee Payments', 200, 0, 'Bus - Admission No: 1001', '2015-11-21', '2015-11-21', 1, NULL, 0, 1),
(100, 5, 2, 7, 'Bus Fee Payments', 0, 200, 'Bus - Admission No: 1001', '2015-11-21', '2015-11-21', 1, NULL, 0, 1),
(101, 2, 5, 9, 'Fee Payments', 2000, 0, 'Admission No: 1614', '2016-01-25', '2015-11-12', 1, NULL, 0, 1),
(102, 5, 2, 9, 'Fee Payments', 0, 2000, 'Admission No: 1614', '2016-01-25', '2015-11-12', 1, NULL, 0, 1),
(103, 2, 5, 11, 'Fee Payments', 277, 0, 'Admission No: 11', '2016-01-25', '2016-01-25', 1, NULL, 0, 1),
(104, 5, 2, 11, 'Fee Payments', 0, 277, 'Admission No: 11', '2016-01-25', '2016-01-25', 1, NULL, 0, 1),
(105, 2, 5, 12, 'Fee Payments', 500, 0, 'Admission No: 11', '2016-01-25', '2016-01-25', 1, NULL, 0, 1),
(106, 5, 2, 12, 'Fee Payments', 0, 500, 'Admission No: 11', '2016-01-25', '2016-01-25', 1, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `userpermission`
--

CREATE TABLE `userpermission` (
  `ID` int(11) NOT NULL,
  `staffId` int(11) NOT NULL,
  `basicSetting` varchar(20) NOT NULL DEFAULT '0',
  `basicSettingEdit` varchar(20) NOT NULL DEFAULT '0',
  `studAttendance` varchar(20) NOT NULL DEFAULT '0',
  `busSettings` varchar(20) NOT NULL DEFAULT '0',
  `busSettingEdit` varchar(20) NOT NULL DEFAULT '0',
  `examType` varchar(20) NOT NULL DEFAULT '0',
  `examTypeEdit` varchar(20) NOT NULL DEFAULT '0',
  `examEntry` varchar(20) NOT NULL DEFAULT '0',
  `examEntryEdit` varchar(20) NOT NULL DEFAULT '0',
  `feePayment` varchar(20) NOT NULL DEFAULT '0',
  `feePaymentEdit` varchar(20) NOT NULL DEFAULT '0',
  `markEntry` varchar(20) NOT NULL DEFAULT '0',
  `accountReport` varchar(20) NOT NULL DEFAULT '0',
  `accountTransfer` varchar(20) NOT NULL DEFAULT '0',
  `accountTransferEdit` varchar(20) NOT NULL DEFAULT '0',
  `messFee` varchar(20) NOT NULL DEFAULT '0',
  `messFeeEdit` varchar(20) NOT NULL DEFAULT '0',
  `notification` varchar(20) NOT NULL DEFAULT '0',
  `notificationEdit` varchar(20) NOT NULL DEFAULT '0',
  `printSetting` varchar(20) NOT NULL DEFAULT '0',
  `promotion` varchar(20) NOT NULL DEFAULT '0',
  `reportView` varchar(20) NOT NULL DEFAULT '0',
  `staffReg` varchar(20) NOT NULL DEFAULT '0',
  `staffEdit` varchar(20) NOT NULL DEFAULT '0',
  `studentReg` varchar(20) NOT NULL DEFAULT '0',
  `studentEdit` varchar(20) NOT NULL DEFAULT '0',
  `library` varchar(20) NOT NULL DEFAULT '0',
  `libraryEdit` varchar(20) NOT NULL DEFAULT '0',
  `mail` varchar(10) NOT NULL DEFAULT '0',
  `club` varchar(20) NOT NULL DEFAULT '0',
  `clubEdit` varchar(20) NOT NULL DEFAULT '0',
  `classTeacher` varchar(20) NOT NULL DEFAULT '0',
  `classTeacherEdit` varchar(20) NOT NULL DEFAULT '0',
  `diary` varchar(20) NOT NULL DEFAULT '0',
  `calendar` varchar(20) NOT NULL DEFAULT '0',
  `chat` varchar(20) NOT NULL DEFAULT '0',
  `loginId` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userpermission`
--

INSERT INTO `userpermission` (`ID`, `staffId`, `basicSetting`, `basicSettingEdit`, `studAttendance`, `busSettings`, `busSettingEdit`, `examType`, `examTypeEdit`, `examEntry`, `examEntryEdit`, `feePayment`, `feePaymentEdit`, `markEntry`, `accountReport`, `accountTransfer`, `accountTransferEdit`, `messFee`, `messFeeEdit`, `notification`, `notificationEdit`, `printSetting`, `promotion`, `reportView`, `staffReg`, `staffEdit`, `studentReg`, `studentEdit`, `library`, `libraryEdit`, `mail`, `club`, `clubEdit`, `classTeacher`, `classTeacherEdit`, `diary`, `calendar`, `chat`, `loginId`) VALUES
(1, 1, '0', '0', '1', '0', '0', '1', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '', '1'),
(3, 3, '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '1', '1', '0', '0', '1', '1', '1', '1', '1', '1', '1', '', '1'),
(4, 2, '0', '0', '1', '0', '0', '1', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '1', '1'),
(5, 4, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '0', '0', '0', '1', '1', '0', '1'),
(6, 5, '0', '0', '1', '0', '0', '1', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '1', '1'),
(7, 6, '0', '0', '1', '0', '0', '1', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '1', '1'),
(8, 7, '0', '0', '1', '0', '0', '1', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '1', '1'),
(9, 8, '0', '0', '1', '0', '0', '1', '1', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '1', '1', '1', '1'),
(10, 9, '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '0', '0', '1', '0', '0', '1', '1', '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `vehiclereg`
--

CREATE TABLE `vehiclereg` (
  `ID` int(11) NOT NULL,
  `routeNo` int(11) NOT NULL,
  `routeName` varchar(20) NOT NULL,
  `vehicle` varchar(20) NOT NULL,
  `stageNo` int(11) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vehiclereg`
--

INSERT INTO `vehiclereg` (`ID`, `routeNo`, `routeName`, `vehicle`, `stageNo`, `loginId`) VALUES
(1, 1, 'Kunnamangalam', 'KL 11 1212', 2, 1),
(2, 2, 'Narikkuni', 'KL 11 1213', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `workingdays`
--

CREATE TABLE `workingdays` (
  `ID` int(11) NOT NULL,
  `acYear` int(11) NOT NULL,
  `acMonth` int(11) NOT NULL,
  `workingDay` int(11) NOT NULL,
  `loginId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workingdays`
--

INSERT INTO `workingdays` (`ID`, `acYear`, `acMonth`, `workingDay`, `loginId`) VALUES
(1, 1, 1, 26, 1),
(2, 1, 2, 25, 1),
(3, 1, 3, 15, 1),
(4, 1, 6, 26, 1),
(5, 1, 7, 26, 1),
(6, 1, 8, 16, 1),
(7, 1, 9, 26, 1),
(8, 1, 10, 26, 1),
(9, 1, 11, 26, 1),
(10, 1, 12, 16, 1),
(11, 3, 1, 22, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `academicyear`
--
ALTER TABLE `academicyear`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `account_group`
--
ALTER TABLE `account_group`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `acmonth`
--
ALTER TABLE `acmonth`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `workId` (`workId`),
  ADD KEY `divisionId` (`divisionId`),
  ADD KEY `adNo` (`adNo`);

--
-- Indexes for table `bookfinepayment`
--
ALTER TABLE `bookfinepayment`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD KEY `bookIssueId` (`bookIssueId`),
  ADD KEY `adNo` (`adNo`);

--
-- Indexes for table `bookreg`
--
ALTER TABLE `bookreg`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `category` (`category`),
  ADD KEY `rack` (`rack`);

--
-- Indexes for table `book_category`
--
ALTER TABLE `book_category`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `book_issue`
--
ALTER TABLE `book_issue`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `adNo` (`adNo`),
  ADD KEY `bookId` (`bookId`);

--
-- Indexes for table `book_return_days`
--
ALTER TABLE `book_return_days`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `busallocation`
--
ALTER TABLE `busallocation`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`),
  ADD KEY `adNo` (`adNo`),
  ADD KEY `stageId` (`stageId`);

--
-- Indexes for table `busfeepayment`
--
ALTER TABLE `busfeepayment`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `month` (`month`),
  ADD KEY `adNo` (`adNo`),
  ADD KEY `acYear` (`acYear`);

--
-- Indexes for table `class`
--
ALTER TABLE `class`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `classmessfee`
--
ALTER TABLE `classmessfee`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `messFeeSetId` (`messFeeSetId`),
  ADD KEY `adNo` (`adNo`);

--
-- Indexes for table `club`
--
ALTER TABLE `club`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`),
  ADD KEY `staffIn` (`staffIn`),
  ADD KEY `clubName` (`clubName`);

--
-- Indexes for table `club_students`
--
ALTER TABLE `club_students`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`),
  ADD KEY `clubId` (`clubId`),
  ADD KEY `class` (`class`),
  ADD KEY `division` (`division`),
  ADD KEY `adNo` (`adNo`);

--
-- Indexes for table `division`
--
ALTER TABLE `division`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `classId` (`classId`);

--
-- Indexes for table `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`);

--
-- Indexes for table `examentry`
--
ALTER TABLE `examentry`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`),
  ADD KEY `examName` (`examName`),
  ADD KEY `subject` (`subject`);

--
-- Indexes for table `examtypes`
--
ALTER TABLE `examtypes`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `feepayment`
--
ALTER TABLE `feepayment`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `admNo` (`admNo`),
  ADD KEY `class` (`class`),
  ADD KEY `division` (`division`),
  ADD KEY `class_2` (`class`);

--
-- Indexes for table `feesetting`
--
ALTER TABLE `feesetting`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `class` (`class`);

--
-- Indexes for table `ledger`
--
ALTER TABLE `ledger`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `account_group` (`account_group`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `markbasic`
--
ALTER TABLE `markbasic`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`),
  ADD KEY `year` (`year`),
  ADD KEY `examName` (`examName`),
  ADD KEY `subject` (`subject`),
  ADD KEY `class` (`class`),
  ADD KEY `division` (`division`);

--
-- Indexes for table `markentry`
--
ALTER TABLE `markentry`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `markBasicId` (`markBasicId`),
  ADD KEY `adNo` (`adNo`);

--
-- Indexes for table `messfeepayment`
--
ALTER TABLE `messfeepayment`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`),
  ADD KEY `month` (`month`),
  ADD KEY `adNo` (`adNo`);

--
-- Indexes for table `messfeesetting`
--
ALTER TABLE `messfeesetting`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `year` (`year`),
  ADD KEY `month` (`month`),
  ADD KEY `class` (`class`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`);

--
-- Indexes for table `notificationclass`
--
ALTER TABLE `notificationclass`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `classId` (`classId`),
  ADD KEY `eventId` (`eventId`);

--
-- Indexes for table `printsettings`
--
ALTER TABLE `printsettings`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `promotion`
--
ALTER TABLE `promotion`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `existedAcYear` (`existedAcYear`),
  ADD KEY `existedDivision` (`existedDivision`),
  ADD KEY `existedClass` (`existedClass`),
  ADD KEY `promotedAcYear` (`promotedAcYear`),
  ADD KEY `promotedClass` (`promotedClass`),
  ADD KEY `promotedDivision` (`promotedDivision`);

--
-- Indexes for table `rack_reg`
--
ALTER TABLE `rack_reg`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `reply`
--
ALTER TABLE `reply`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `setinstalment`
--
ALTER TABLE `setinstalment`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `class` (`class`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `staffnotification`
--
ALTER TABLE `staffnotification`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`);

--
-- Indexes for table `staffpayment`
--
ALTER TABLE `staffpayment`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `staffId` (`staffId`),
  ADD KEY `staffId_2` (`staffId`);

--
-- Indexes for table `stagedetails`
--
ALTER TABLE `stagedetails`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `vehicleId` (`vehicleId`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `adNo` (`adNo`),
  ADD KEY `class` (`class`),
  ADD KEY `division` (`division`),
  ADD KEY `acYear` (`acYear`),
  ADD KEY `acYear_2` (`acYear`),
  ADD KEY `acYear_3` (`acYear`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblad`
--
ALTER TABLE `tblad`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblbloodbank`
--
ALTER TABLE `tblbloodbank`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblbusplace`
--
ALTER TABLE `tblbusplace`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `place` (`place`);

--
-- Indexes for table `tblbustime`
--
ALTER TABLE `tblbustime`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblcareer`
--
ALTER TABLE `tblcareer`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblemergency`
--
ALTER TABLE `tblemergency`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblfeedback`
--
ALTER TABLE `tblfeedback`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblhealth`
--
ALTER TABLE `tblhealth`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblhealthmessage`
--
ALTER TABLE `tblhealthmessage`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblhealthtype`
--
ALTER TABLE `tblhealthtype`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblinstitute`
--
ALTER TABLE `tblinstitute`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblinstitutedetails`
--
ALTER TABLE `tblinstitutedetails`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbljob`
--
ALTER TABLE `tbljob`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbljobtype`
--
ALTER TABLE `tbljobtype`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbllogin`
--
ALTER TABLE `tbllogin`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblmarket`
--
ALTER TABLE `tblmarket`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblnotification`
--
ALTER TABLE `tblnotification`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblresource`
--
ALTER TABLE `tblresource`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblshop`
--
ALTER TABLE `tblshop`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblshoptype`
--
ALTER TABLE `tblshoptype`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbltaxi`
--
ALTER TABLE `tbltaxi`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tbltaxitype`
--
ALTER TABLE `tbltaxitype`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tblusers`
--
ALTER TABLE `tblusers`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `tcissue`
--
ALTER TABLE `tcissue`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `adNo` (`adNo`),
  ADD KEY `adNo_2` (`adNo`);

--
-- Indexes for table `teacherallocation`
--
ALTER TABLE `teacherallocation`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`),
  ADD KEY `class` (`class`),
  ADD KEY `division` (`division`),
  ADD KEY `teacher` (`teacher`);

--
-- Indexes for table `teacherchat`
--
ALTER TABLE `teacherchat`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `from_ledger` (`from_ledger`),
  ADD KEY `to_ledger` (`to_ledger`);

--
-- Indexes for table `userpermission`
--
ALTER TABLE `userpermission`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `staffId` (`staffId`),
  ADD KEY `staffId_2` (`staffId`);

--
-- Indexes for table `vehiclereg`
--
ALTER TABLE `vehiclereg`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `ID` (`ID`);

--
-- Indexes for table `workingdays`
--
ALTER TABLE `workingdays`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `acYear` (`acYear`),
  ADD KEY `acMonth` (`acMonth`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `academicyear`
--
ALTER TABLE `academicyear`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `account_group`
--
ALTER TABLE `account_group`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `acmonth`
--
ALTER TABLE `acmonth`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;
--
-- AUTO_INCREMENT for table `bookfinepayment`
--
ALTER TABLE `bookfinepayment`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `bookreg`
--
ALTER TABLE `bookreg`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `book_category`
--
ALTER TABLE `book_category`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `book_issue`
--
ALTER TABLE `book_issue`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `book_return_days`
--
ALTER TABLE `book_return_days`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `busallocation`
--
ALTER TABLE `busallocation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `busfeepayment`
--
ALTER TABLE `busfeepayment`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `class`
--
ALTER TABLE `class`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `classmessfee`
--
ALTER TABLE `classmessfee`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `club`
--
ALTER TABLE `club`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `club_students`
--
ALTER TABLE `club_students`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `division`
--
ALTER TABLE `division`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `event`
--
ALTER TABLE `event`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `examentry`
--
ALTER TABLE `examentry`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `examtypes`
--
ALTER TABLE `examtypes`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `feepayment`
--
ALTER TABLE `feepayment`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `feesetting`
--
ALTER TABLE `feesetting`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ledger`
--
ALTER TABLE `ledger`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `markbasic`
--
ALTER TABLE `markbasic`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `markentry`
--
ALTER TABLE `markentry`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;
--
-- AUTO_INCREMENT for table `messfeepayment`
--
ALTER TABLE `messfeepayment`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `messfeesetting`
--
ALTER TABLE `messfeesetting`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `notificationclass`
--
ALTER TABLE `notificationclass`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `printsettings`
--
ALTER TABLE `printsettings`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `promotion`
--
ALTER TABLE `promotion`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rack_reg`
--
ALTER TABLE `rack_reg`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `reply`
--
ALTER TABLE `reply`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `setinstalment`
--
ALTER TABLE `setinstalment`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `staffnotification`
--
ALTER TABLE `staffnotification`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `staffpayment`
--
ALTER TABLE `staffpayment`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `stagedetails`
--
ALTER TABLE `stagedetails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tblad`
--
ALTER TABLE `tblad`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tblbloodbank`
--
ALTER TABLE `tblbloodbank`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tblbusplace`
--
ALTER TABLE `tblbusplace`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tblbustime`
--
ALTER TABLE `tblbustime`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tblcareer`
--
ALTER TABLE `tblcareer`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tblemergency`
--
ALTER TABLE `tblemergency`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tblfeedback`
--
ALTER TABLE `tblfeedback`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `tblhealth`
--
ALTER TABLE `tblhealth`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tblhealthmessage`
--
ALTER TABLE `tblhealthmessage`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblhealthtype`
--
ALTER TABLE `tblhealthtype`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tblinstitute`
--
ALTER TABLE `tblinstitute`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tblinstitutedetails`
--
ALTER TABLE `tblinstitutedetails`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbljob`
--
ALTER TABLE `tbljob`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tbljobtype`
--
ALTER TABLE `tbljobtype`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbllogin`
--
ALTER TABLE `tbllogin`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tblmarket`
--
ALTER TABLE `tblmarket`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `tblnotification`
--
ALTER TABLE `tblnotification`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `tblresource`
--
ALTER TABLE `tblresource`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tblshop`
--
ALTER TABLE `tblshop`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tblshoptype`
--
ALTER TABLE `tblshoptype`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbltaxi`
--
ALTER TABLE `tbltaxi`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbltaxitype`
--
ALTER TABLE `tbltaxitype`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tblusers`
--
ALTER TABLE `tblusers`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `tcissue`
--
ALTER TABLE `tcissue`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `teacherallocation`
--
ALTER TABLE `teacherallocation`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `teacherchat`
--
ALTER TABLE `teacherchat`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;
--
-- AUTO_INCREMENT for table `userpermission`
--
ALTER TABLE `userpermission`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `vehiclereg`
--
ALTER TABLE `vehiclereg`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `workingdays`
--
ALTER TABLE `workingdays`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `attendance_ibfk_1` FOREIGN KEY (`workId`) REFERENCES `workingdays` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `attendance_ibfk_2` FOREIGN KEY (`divisionId`) REFERENCES `division` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `attendance_ibfk_3` FOREIGN KEY (`adNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION;

--
-- Constraints for table `bookfinepayment`
--
ALTER TABLE `bookfinepayment`
  ADD CONSTRAINT `bookfinepayment_ibfk_1` FOREIGN KEY (`bookIssueId`) REFERENCES `book_issue` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `bookfinepayment_ibfk_2` FOREIGN KEY (`adNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION;

--
-- Constraints for table `bookreg`
--
ALTER TABLE `bookreg`
  ADD CONSTRAINT `bookreg_ibfk_1` FOREIGN KEY (`category`) REFERENCES `book_category` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `bookreg_ibfk_2` FOREIGN KEY (`rack`) REFERENCES `rack_reg` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `book_issue`
--
ALTER TABLE `book_issue`
  ADD CONSTRAINT `book_issue_ibfk_1` FOREIGN KEY (`adNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `book_issue_ibfk_2` FOREIGN KEY (`bookId`) REFERENCES `bookreg` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `busallocation`
--
ALTER TABLE `busallocation`
  ADD CONSTRAINT `busallocation_ibfk_1` FOREIGN KEY (`adNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `busallocation_ibfk_2` FOREIGN KEY (`stageId`) REFERENCES `stagedetails` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `busallocation_ibfk_3` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `busfeepayment`
--
ALTER TABLE `busfeepayment`
  ADD CONSTRAINT `busfeepayment_ibfk_1` FOREIGN KEY (`month`) REFERENCES `acmonth` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `busfeepayment_ibfk_3` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `busfeepayment_ibfk_4` FOREIGN KEY (`adNo`) REFERENCES `busallocation` (`adNo`) ON UPDATE NO ACTION;

--
-- Constraints for table `classmessfee`
--
ALTER TABLE `classmessfee`
  ADD CONSTRAINT `classmessfee_ibfk_1` FOREIGN KEY (`messFeeSetId`) REFERENCES `messfeesetting` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `classmessfee_ibfk_2` FOREIGN KEY (`adNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION;

--
-- Constraints for table `club`
--
ALTER TABLE `club`
  ADD CONSTRAINT `club_ibfk_1` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `club_ibfk_2` FOREIGN KEY (`staffIn`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `club_students`
--
ALTER TABLE `club_students`
  ADD CONSTRAINT `club_students_ibfk_1` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `club_students_ibfk_2` FOREIGN KEY (`clubId`) REFERENCES `club` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `club_students_ibfk_3` FOREIGN KEY (`class`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `club_students_ibfk_4` FOREIGN KEY (`division`) REFERENCES `division` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `club_students_ibfk_5` FOREIGN KEY (`adNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION;

--
-- Constraints for table `division`
--
ALTER TABLE `division`
  ADD CONSTRAINT `division_ibfk_1` FOREIGN KEY (`classId`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `examentry`
--
ALTER TABLE `examentry`
  ADD CONSTRAINT `examentry_ibfk_1` FOREIGN KEY (`examName`) REFERENCES `examtypes` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `examentry_ibfk_2` FOREIGN KEY (`subject`) REFERENCES `subject` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `examentry_ibfk_3` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `examentry_ibfk_4` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `feepayment`
--
ALTER TABLE `feepayment`
  ADD CONSTRAINT `feepayment_ibfk_1` FOREIGN KEY (`admNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `feepayment_ibfk_3` FOREIGN KEY (`division`) REFERENCES `division` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `feepayment_ibfk_4` FOREIGN KEY (`class`) REFERENCES `feesetting` (`class`) ON UPDATE NO ACTION;

--
-- Constraints for table `feesetting`
--
ALTER TABLE `feesetting`
  ADD CONSTRAINT `feesetting_ibfk_1` FOREIGN KEY (`class`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `ledger`
--
ALTER TABLE `ledger`
  ADD CONSTRAINT `ledger_ibfk_1` FOREIGN KEY (`account_group`) REFERENCES `account_group` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `markbasic`
--
ALTER TABLE `markbasic`
  ADD CONSTRAINT `markbasic_ibfk_4` FOREIGN KEY (`class`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `markbasic_ibfk_5` FOREIGN KEY (`division`) REFERENCES `division` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `markbasic_ibfk_6` FOREIGN KEY (`year`) REFERENCES `examentry` (`acYear`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `markbasic_ibfk_7` FOREIGN KEY (`examName`) REFERENCES `examentry` (`examName`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `markbasic_ibfk_8` FOREIGN KEY (`subject`) REFERENCES `examentry` (`subject`) ON UPDATE NO ACTION;

--
-- Constraints for table `markentry`
--
ALTER TABLE `markentry`
  ADD CONSTRAINT `markentry_ibfk_1` FOREIGN KEY (`markBasicId`) REFERENCES `markbasic` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `markentry_ibfk_2` FOREIGN KEY (`adNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION;

--
-- Constraints for table `messfeepayment`
--
ALTER TABLE `messfeepayment`
  ADD CONSTRAINT `messfeepayment_ibfk_1` FOREIGN KEY (`month`) REFERENCES `acmonth` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `messfeepayment_ibfk_2` FOREIGN KEY (`adNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `messfeepayment_ibfk_3` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `messfeesetting`
--
ALTER TABLE `messfeesetting`
  ADD CONSTRAINT `messfeesetting_ibfk_1` FOREIGN KEY (`year`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `messfeesetting_ibfk_2` FOREIGN KEY (`month`) REFERENCES `acmonth` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `messfeesetting_ibfk_3` FOREIGN KEY (`class`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `notification_ibfk_1` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `notificationclass`
--
ALTER TABLE `notificationclass`
  ADD CONSTRAINT `notificationclass_ibfk_1` FOREIGN KEY (`classId`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `notificationclass_ibfk_2` FOREIGN KEY (`eventId`) REFERENCES `notification` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `notificationclass_ibfk_3` FOREIGN KEY (`eventId`) REFERENCES `notification` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `promotion`
--
ALTER TABLE `promotion`
  ADD CONSTRAINT `promotion_ibfk_4` FOREIGN KEY (`promotedAcYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `promotion_ibfk_5` FOREIGN KEY (`promotedClass`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `promotion_ibfk_6` FOREIGN KEY (`promotedDivision`) REFERENCES `division` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `setinstalment`
--
ALTER TABLE `setinstalment`
  ADD CONSTRAINT `setinstalment_ibfk_1` FOREIGN KEY (`class`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `staffnotification`
--
ALTER TABLE `staffnotification`
  ADD CONSTRAINT `staffnotification_ibfk_1` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `staffpayment`
--
ALTER TABLE `staffpayment`
  ADD CONSTRAINT `staffpayment_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `stagedetails`
--
ALTER TABLE `stagedetails`
  ADD CONSTRAINT `stagedetails_ibfk_1` FOREIGN KEY (`vehicleId`) REFERENCES `vehiclereg` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `student_ibfk_1` FOREIGN KEY (`class`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_ibfk_2` FOREIGN KEY (`division`) REFERENCES `division` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `student_ibfk_3` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `tcissue`
--
ALTER TABLE `tcissue`
  ADD CONSTRAINT `tcissue_ibfk_1` FOREIGN KEY (`adNo`) REFERENCES `student` (`adNo`) ON UPDATE NO ACTION;

--
-- Constraints for table `teacherallocation`
--
ALTER TABLE `teacherallocation`
  ADD CONSTRAINT `teacherallocation_ibfk_1` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `teacherallocation_ibfk_2` FOREIGN KEY (`class`) REFERENCES `class` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `teacherallocation_ibfk_3` FOREIGN KEY (`division`) REFERENCES `division` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `teacherallocation_ibfk_4` FOREIGN KEY (`teacher`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `transaction`
--
ALTER TABLE `transaction`
  ADD CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`from_ledger`) REFERENCES `ledger` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `transaction_ibfk_2` FOREIGN KEY (`to_ledger`) REFERENCES `ledger` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `userpermission`
--
ALTER TABLE `userpermission`
  ADD CONSTRAINT `userpermission_ibfk_1` FOREIGN KEY (`staffId`) REFERENCES `staff` (`ID`) ON UPDATE NO ACTION;

--
-- Constraints for table `workingdays`
--
ALTER TABLE `workingdays`
  ADD CONSTRAINT `workingdays_ibfk_1` FOREIGN KEY (`acMonth`) REFERENCES `acmonth` (`ID`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `workingdays_ibfk_2` FOREIGN KEY (`acYear`) REFERENCES `academicyear` (`ID`) ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid=$_SESSION['LogID'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['cName'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['acYear']		   =	$App->convert($_REQUEST['year']);
				$data['clubName']		 =	$App->convert($_REQUEST['cName']);
				$data['staffIn']		  =	$App->convert($_REQUEST['staffIn']);
				$data['description']	  =	$App->convert($_REQUEST['description']);
				$data['loginId']		  =	$App->convert($uid);
						
				$eventId=$db->query_insert(TABLE_CLUB, $data);
				//echo TABLE_CLUB, $data;die;
				$db->close();
				
				$_SESSION['msg']="Club details added successfully";					
				header("location:new.php");
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['cName'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['acYear']		   =	$App->convert($_REQUEST['year']);
				$data['clubName']		 =	$App->convert($_REQUEST['cName']);
				$data['staffIn']		  =	$App->convert($_REQUEST['staffIn']);
				$data['description']	  =	$App->convert($_REQUEST['description']);
				$data['loginId']		  =	$App->convert($uid);
						
				$db->query_update(TABLE_CLUB, $data,"ID='{$fid}'");
				//echo TABLE_CLUB, $data,"ID='{$fid}'";die;
				$db->close();				
				$_SESSION['msg']="Club details updated successfully";					
				header("location:new.php");							
			}
		break;	
		
	// DELETE SECTION
	//-
	case 'delete':		
				$id		=	$_REQUEST['id'];
				//$page	=	$_REQUEST['page'];
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				//$db->query("DELETE FROM `".TABLE_EVENT."` WHERE ID='{$id}'");		
				//echo "DELETE FROM `".TABLE_CLUB."` WHERE ID='{$id}'";die;
				try
				{
					$success= @mysql_query("DELETE FROM `".TABLE_CLUB."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}							
				$db->close();	
				if($success)
				{						
				$_SESSION['msg']="Event deleted successfully";
				}
				else
				{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";		
				}
				header("location:new.php");				
		break;		
}
?>
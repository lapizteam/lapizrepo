<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

  <!-- Modal1 -->
     <div class="col-md-10 col-sm-8 rightarea">
        <div class="student-report">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>FEE DETAILS</h2>
            </div>
            <div class="col-sm-8 text-right">
              <form class="form-inline form3" method="post">
               
			    	<div class="form-group">
					Class Name</br>  
                  <input type="text"  name="search_class" id="search_class" class="form-control namebox"   placeholder="Class Name"  value="<?php echo @$_REQUEST['search_class'] ?>">                   
                </div>
               
              <div class="form-group">
					</br>
                   <input type="submit" name="submit" value=""class="btn btn-default lens" title="Search"/>
				   
                </div>
              </form>
            </div>
          </div>
<?php	
$cond="1";
if(@$_REQUEST['search_class'])
{
	$cond=" ".TABLE_CLASS.".class like'%".$_POST['search_class']."%'";
}
?>
          <div class="row">
            <div class="col-sm-12">
              <div class="tablearea3 table-responsive">
                <table class="table view_limitter pagination_table" id="headerTable" >
                  <thead>
                    <tr>
                      <td>SLNO</td>
						<td>Class</td>
						<td>Registration Fee</td>
						<td>Admission Fee</td>
						<td>Special Fee</td>
                        <td>Tution Fee</td>
						<td>Total</td>
                        <td>View</td>
                    </tr>
                  </thead>
				  
                  <tbody>
				  <?php					
					$selAllQuery="SELECT ".TABLE_FEESETTING.".ID,".TABLE_FEESETTING.".registrationFee,".TABLE_FEESETTING.".class as classID, ".TABLE_FEESETTING.".admissionFee,".TABLE_FEESETTING.".specialFee,".TABLE_FEESETTING.".tuitionFee,".TABLE_FEESETTING.".installmentNo,".TABLE_FEESETTING.".total,".TABLE_CLASS.".class from `".TABLE_FEESETTING."`,`".TABLE_CLASS."` where ".TABLE_FEESETTING.".class=".TABLE_CLASS.".ID AND $cond ";
					
					$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="8">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
					while($row=mysql_fetch_array($selectAll))
					{
					$tableId=$row['ID'];
					$class_id=$row['classID'];
					?>
					<tr>
						<td><?php echo $i;$i++; ?></td>
						<td><?php echo $row['class'] ?></td>
						<td><?php echo $row['registrationFee'] ?></td>
						<td><?php echo $row['admissionFee'] ?></td>
						<td><?php echo $row['specialFee'] ?></td>
						<td><?php echo $row['tuitionFee']; ?> </td>
						<td><?php echo $row['total']; ?> </td>
						<td><a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>" class="viewbtn">View</a>
						<!-- Modal3 -->
						  <div class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
							<div class="modal-dialog">
							  <div class="modal-content"> 										
								<div role="tabpanel" class="tabarea2"> 
								  
								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">
									<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Fee Details</a> </li>
									
								  </ul>
								  
								  <!-- Tab panes -->
								  <div class="tab-content">
									<div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
									  <table class="table nobg" id="headerTable2<?php echo $i; ?>" >
										<tbody style="background-color:#FFFFFF">
										  <tr>
										  	<td>Class</td>
											<td> : </td>
											<td><?php echo $row['class']; ?></td>
										  </tr>
										  <tr>
											<td>Registration Fee</td>
											<td> : </td>
											<td><?php echo $row['registrationFee']; ?></td>
										  </tr>
										 <tr>
											<td>Admission Fee</td>
											<td> : </td>
											<td><?php echo $row['admissionFee']; ?></td>
										 </tr>
										 <tr>
										 	<td>Special Fee</td>
											<td> : </td>
											<td><?php echo $row['specialFee']; ?></td>
										 </tr>
										 <tr>
										  	<td>Tuition Fee</td>
											<td> : </td>
											<td><?php echo $row['tuitionFee']; ?></td>
										  </tr>
										  <tr>
											<td>Total</td>
											<td> : </td>
											<td><?php echo $row['total']; ?></td>
										  </tr>
										 <tr>
											<td>Number of instalment</td>
											<td> : </td>
											<td><?php echo $row['installmentNo']; ?></td>
										 </tr>
										 <?php 
										$instal_sel="SELECT * FROM ".TABLE_SETINSTALMENT." where class='$class_id'";
										$j=1;
										$res2=$db->query($instal_sel);
										while($row2=mysql_fetch_array($res2))
										{
										?>
										<tr>
											<td >Instalment <?php echo $j?></td>
											<td> : </td>
											<td><?php  echo $row2['amount'] ?></td>
											<td >Payment Date</td>
											<td> : </td>
											<td><?php  echo $App->dbformat_date_db($row2['paymentDate']); ?></td>		
										</tr>
									<?php
									 $j=$j+1;
									 } 
									 ?>
								</tbody>
							  </table>
									</div>
								  </div>
								</div>
							  </div>
							</div>
						  </div>
						  <!-- Modal3 cls --> 
						
						
						
						
						</td>
					 </tr>
					<?php
					}
			}
					?>
                  </tbody>
                </table>
				
              </div>
			  <!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
            </div>
          </div>
        </div>
      </div>
      
     
      
      
      
  <?php include("../adminFooter.php") ?>
<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{

	// NEW SECTION
	//-
	case 'new':
		
		if(!$_POST['name'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{							
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$staffId=$App->convert($_REQUEST['staffId']);
				$existId=$db->existValuesId(TABLE_STAFF," staffId='$staffId'");

				if($existId>0)
				{					
					$_SESSION['msg']="Staff Number already Exist !!";					
					header("location:new.php");				
				}
				else
				{		
				
				$data['staffId']				=	$App->convert($_REQUEST['staffId']);
				$data['name']					=	$App->convert($_REQUEST['name']);
				$data['dob']					=	$App->dbFormat_date($_REQUEST['dob']);
				$data['age']					=	$App->convert($_REQUEST['age']);
				$data['address']				=	$App->convert($_REQUEST['address']);
				$data['phone']					=	$App->convert($_REQUEST['phone']);
				$data['email']					=	$App->convert($_REQUEST['email']);
				$data['dateOfJoin']				=	$App->dbFormat_date($_REQUEST['dateOfJoin']);
				$data['gender']					=	$App->convert($_REQUEST['gender']);
				$data['type']					=	$App->convert($_REQUEST['type']);
				$data['designation']			=	$App->convert($_REQUEST['designation']);
 				$data['basicPay']				=	$App->convert($_REQUEST['basic']);
				$data['hra']					=	$App->convert($_REQUEST['hra']);
				$data['cca']					=	$App->convert($_REQUEST['cca']);
				$data['pf']						=	$App->convert($_REQUEST['pf']);
				$data['sli']					=	$App->convert($_REQUEST['sli']);
				$data['gli']					=	$App->convert($_REQUEST['gli']);
				$data['accountNo']				=	$App->convert($_REQUEST['accountNo']);	
				$data['loginId']				=	$App->convert($uid);
							
				
				$success=$db->query_insert(TABLE_STAFF, $data);
				$db->close();				
				
				if($success)
					{
					$_SESSION['msg']="Staff Added Successfully";										
					}
				else
					{
					$_SESSION['msg']="Failed";							
					}	
					header("location:new.php");		
				}
			}
		
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];
	
       
		if(!$_POST['name'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
					
				$staffId=$App->convert($_REQUEST['staffId']);
				$existId=$db->existValuesId(TABLE_STAFF," staffId='$staffId' AND ID!='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Staff Number already Exist !!";					
					header("location:edit.php?id=$fid");						
					break ;
				}
				else
				{	
				
				$data['staffId']				=	$App->convert($_REQUEST['staffId']);
				$data['name']					=	$App->convert($_REQUEST['name']);
				$data['dob']					=	$App->dbFormat_date($_REQUEST['dob']);
				$data['age']					=	$App->convert($_REQUEST['age']);
				$data['address']				=	$App->convert($_REQUEST['address']);
				$data['phone']					=	$App->convert($_REQUEST['phone']);
				$data['email']					=	$App->convert($_REQUEST['email']);
				$data['dateOfJoin']				=	$App->dbFormat_date($_REQUEST['dateOfJoin']);
				$data['gender']					=	$App->convert($_REQUEST['gender']);
				$data['type']					=	$App->convert($_REQUEST['type']);
				$data['designation']			=	$App->convert($_REQUEST['designation']);
 				$data['basicPay']				=	$App->convert($_REQUEST['basic']);
				$data['hra']					=	$App->convert($_REQUEST['hra']);
				$data['cca']					=	$App->convert($_REQUEST['cca']);
				$data['pf']						=	$App->convert($_REQUEST['pf']);
				$data['sli']					=	$App->convert($_REQUEST['sli']);
				$data['gli']					=	$App->convert($_REQUEST['gli']);
				$data['accountNo']				=	$App->convert($_REQUEST['accountNo']);
				$data['loginId']				=	$App->convert($uid);	
				
					$success=$db->query_update(TABLE_STAFF, $data, "ID='{$fid}'");					 
					$db->close(); 				
				
				if($success)
					{
					$_SESSION['msg']="Staff updated successfully";																	
					}
					else
					{
					$_SESSION['msg']="Failed";									
					}	
					header("location:new.php");	
				}
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$sql2 = "DELETE FROM `".TABLE_USERPERMISSION."` WHERE staffId='{$id}'";
				$db->query($sql2);
				$sql3 = "DELETE FROM `".TABLE_ADMIN."` WHERE staffId='{$id}'";
				$db->query($sql3);
				try
				{			
				$success= @mysql_query("DELETE FROM `".TABLE_STAFF."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}	
				/*$sql = "DELETE FROM `".TABLE_STAFF."` WHERE ID='{$id}'";
				$success=$db->query($sql);*/
				
				$db->close(); 
				
				
				if($success)
					{
					$_SESSION['msg']="Staff deleted successfully";									
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
					}	
				header("location:new.php");	
		break;
		

}
?>
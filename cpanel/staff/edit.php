<?php include("../adminHeader.php"); 
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}


$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function getAge()
{

var dob=document.getElementById('dob').value;

//today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = dd+'-'+mm+'-'+yyyy;
//today end
var dobSplit= dob.split('/');
var todaySplit= today.split('-');

var dobLast = new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
var todayLast = new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
var dateDiff=(todayLast.getTime() - dobLast.getTime()) / (1000*60*60*24);
var age=Math.round(dateDiff/365);

document.getElementById('age').value=age;
}
//staff Id check-exist or not

$(document).ready(function() {
$("#staffId").keyup(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var staffId = $(this).val(); //get the string typed by user
   $("#user-result").html('<img src="ajax-loader.gif" />');
   $.post('check_staffId.php', {'staff':staffId}, function(data) { //make ajax call to check_username.php
   $("#user-result").html(data); //dump the data received from PHP page
   });
});
});


</script>
<script>
function valid()
{
flag=false;
var reguler1 =/^[a-zA-Z ]*$/;
var rmobile =/^(?:(?:\+|0{0,2})91(\s\s*)?|[0]?)?[789]\d{9}$/; 
var remail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;

jName = document.getElementById('name').value;
	if(!jName.match(reguler1))
 	{		 
	document.getElementById('namediv').innerHTML="Enter valid name";
	flag=true;
	}
	
Jdob=document.getElementById('dob').value;		
	if(Jdob=='')
	{
	document.getElementById('dobdiv').innerHTML="You can't leave this empty.";
	flag=true;
	}
	
	  var date  = Jdob.substring(0,2);
	  var month = Jdob.substring(3,5);
	  var year  = Jdob.substring(6,10);
	
	var myDate= new Date(year,month-1,date);		
	var today = new Date();	
	
	if (myDate>today)
	  {
	 document.getElementById('dobdiv').innerHTML="Enter valid date of birth";
		flag=true;
	  }	

jMobile=document.getElementById('phone').value;
	if(!jMobile.match(rmobile) && jMobile!='')
	{	
	document.getElementById('phonediv').innerHTML="Enter valid Mobile number.";
	flag=true;
	}
jEmail=document.getElementById('email').value;
	if(!jEmail.match(remail)&& jEmail!='')
	{	
	document.getElementById('emaildiv').innerHTML="Enter valid Email Id.";
	flag=true;
	}
Jdoj=document.getElementById('dateOfJoin').value;		
	if(Jdoj=='')
	{
	document.getElementById('dateOfJoindiv').innerHTML="You can't leave this empty.";
	flag=true;
	}	
	
jgli=document.getElementById('gli').value;	
	if(isNaN(jgli))
	{		
	document.getElementById('glidiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jsli=document.getElementById('sli').value;	
	if(isNaN(jsli))
	{		
	document.getElementById('slidiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jpf=document.getElementById('pf').value;	
	if(isNaN(jpf))
	{		
	document.getElementById('pfdiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jcca=document.getElementById('cca').value;	
	if(isNaN(jcca))
	{		
	document.getElementById('ccadiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jhra=document.getElementById('hra').value;	
	if(isNaN(jhra))
	{		
	document.getElementById('hradiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jbasic=document.getElementById('basic').value;	
	if(isNaN(jbasic))
	{		
	document.getElementById('basicdiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
		
if(flag==true)
	{
	return false;
	}
}

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>


<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];

        $editId = mysql_real_escape_string($editId);

	$tableEdit="SELECT * FROM ".TABLE_STAFF." WHERE ID='$editId'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);

?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">STAFF REGISTRATION </h4>
            </div>
            <div class="modal-body clearfix">
			<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
			<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 <div class="col-sm-6">
                    <div class="form-group">
                      <label for="staffId">Staff ID:</label>
                      <input type="text" id="staffId" name="staffId" class="form-control2" value="<?php echo $editRow['staffId'];?>" readonly="readonly" placeholder="Staff ID" title="Staff Id" />				
					  <span id="user-result"></span>
                    </div>
					
                    <div class="form-group">
                      <label for="name">Name:<span class="star">*</span> </label>
                      <input type="text" id="name" name="name" class="form-control2" value="<?php echo $editRow['name'];?>" required placeholder="Name" title="Staff Name" onfocus="clearbox('namediv')"/>
					  <div id="namediv" class="valid" style="color:#FF6600;"></div>					
                    </div>
                   
                    <div class="form-group">
                      <label for="dob">Date Of Birth:<span class="star">*</span></label>                    
					   <input type="text" id="dob" name="dob" class="form-control2 datepicker"  value="<?php echo $App->dbformat_date_db($editRow['dob'])?>" required placeholder="Date Of Birth" readonly="readonly" onchange="getAge()"/>
					   <div id="dobdiv" class="valid" style="color:#FF6600;"></div>
                    </div>					                   								 
                  
                    <div class="form-group">
                      <label for="age">Age:<span class="star">*</span></label>
                     <input type="text" id="age" name="age" class="form-control2" value="<?php echo $editRow['age'];?>"  placeholder="Age" title="Age" onmouseover="getAge()" readonly />
                    </div>									  	
					
					<div class="form-group">
                      <label for="address">Residential Address:</label>                      
						 <textarea style="width:265px" name="address"  id="address" class="form-control2" placeholder="Address" ><?php echo $editRow['address'];?></textarea>				   
                    </div>									
					
					<div class="form-group">
                      <label for="phone">Mobile Number:</label>
					   <input type="text" id="phone" name="phone" value="<?php echo $editRow['phone'];?>"  title="Mobile Number"  placeholder="Mobile No" class="form-control2" onfocus="clearbox('phonediv')"/>
					    <div id="phonediv" class="valid" style="color:#FF6600;"></div> 	  
                    </div>
					<div class="form-group">
                      <label for="phone">Email Id:</label>
					   <input type="text" id="email" name="email" value="<?php echo $editRow['email'];?>"  title="Email Id"  placeholder="Email Id" class="form-control2" onfocus="clearbox('emaildiv')"/>	 
					   <div id="emaildiv" class="valid" style="color:#FF6600;"></div> 
                    </div>
										
					<div class="form-groupy">
                      <label for="dateOfJoin">Date Of Join:<span class="star">*</span></label>
                      <input type="text" id="dateOfJoin" name="dateOfJoin" value="<?php echo $App->dbformat_date_db($editRow['dateOfJoin']);?>"  title="Date Of Join"  class="form-control2 datepicker" readonly="readonly"/>					  
                    </div>					
					
					<div class="form-groupy">
                      <label for="gender">Gender: <span class="star">*</span></label>
						 <select name="gender" id="gender" class="form-control2" title="Select Type" required>
							<option value="Male" <?php if($editRow['gender']=='Male'){?> selected<?php } ?>> Male </option>
                    		<option value="Female" <?php if($editRow['gender']=='Female'){?> selected<?php } ?>> Female </option>
						</select>		  
                    </div>								
                    <div class="form-group">
                      <label for="type">Type:<span class="star">*</span></label>
                        <select name="type" id="type" class="form-control2" title="Select Type">
							<option value="nonteaching" <?php if($editRow['type']=='nonteaching'){?> selected<?php } ?>> Non Teaching </option>
                    		<option value="teaching" <?php if($editRow['type']=='teaching'){?> selected<?php } ?>> Teaching </option>
                    	</select>
                    </div>
				</div>
				 <div class="col-sm-6">
					 <div class="form-group">
                      <label for="designation">Designation:<span class="star">*</span></label>
                      <input type="text" id="designation" name="designation" value="<?php echo $editRow['designation'];?>" title="Designation" placeholder="Designation" class="form-control2" required />
                    </div>
					 <div class="form-group">
                      <label for="basic">Basic Pay:<span class="star">*</span></label><BR />
                      <input type="text" id="basic" name="basic" value="<?php echo $editRow['basicPay'];?>" title="Basic Pay" placeholder="Basic Pay" class="form-control2" required onfocus="clearbox('basicdiv')"/>
					  <div id="basicdiv" class="valid" style="color:#FF6600;"></div>
                    </div>			  
                    <div class="form-groupy">
                      <label for="hra">House Rent Allowance [HRA] :<span class="star">*</span></label>
                       <input type="text" id="hra" name="hra" value="<?php echo $editRow['hra'];?>"  placeholder="HRA" class="form-control2" required onfocus="clearbox('hradiv')"/>	
					    <div id="hradiv" class="valid" style="color:#FF6600;"></div>					  
                    </div>
					<div class="form-group">
                      <label for="cca">City Compansatory Allowance [CCA] :<span class="star">*</span></label>
                      <input type="text" id="cca" name="cca" placeholder="CCA" value="<?php echo $editRow['cca'];?>" class="form-control2" required onfocus="clearbox('ccadiv')"/>
					  <div id="ccadiv" class="valid" style="color:#FF6600;"></div> 				  
                    </div>
					<div class="form-group">
                      <label for="pf">Provident Fund [PF] :<span class="star">*</span></label>
                      <input type="text" id="pf" name="pf" value="<?php echo $editRow['pf'];?>" placeholder="PF" class="form-control2" required onfocus="clearbox('pfdiv')"/> 
					  <div id="pfdiv" class="valid" style="color:#FF6600;"></div>						  		  
                    </div>
					<div class="form-group">
                      <label for="sli">State Life Insurance [SLI] :<span class="star">*</span></label>
                      <input type="text" id="sli" name="sli" value="<?php echo $editRow['sli'];?>" placeholder="SLI" class="form-control2" required onfocus="clearbox('slidiv')"/>	
					  <div id="slidiv" class="valid" style="color:#FF6600;"></div>			  
                    </div>					
					<div class="form-group">
                      <label for="gli">Group Life Insurance [GLI] :<span class="star">*</span></label>
                      <input type="text" id="gli" name="gli" value="<?php echo $editRow['gli'];?>"  placeholder="GLI" class="form-control2" required onfocus="clearbox('glidiv')" />
					  <div id="glidiv" class="valid" style="color:#FF6600;"></div>			  
                    </div>
					<div class="form-group">
                      <label for="accountNo">Account Number :<span class="star">*</span></label>
                      <input type="text" id="accountNo" name="accountNo" value="<?php echo $editRow['accountNo'];?>" placeholder="Account Number" class="form-control2" required/> 			  			
                    </div>					
				</div>					 																								
                </div>	
		</div>
	 <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>

<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$type		=	$_SESSION['LogType'];
$staffEdit	=	$_SESSION['staffEdit'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<script>
function getAge()
{

var dob=document.getElementById('dob').value;

//today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = dd+'-'+mm+'-'+yyyy;
//today end
var dobSplit= dob.split('/');
var todaySplit= today.split('-');

var dobLast = new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
var todayLast = new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
var dateDiff=(todayLast.getTime() - dobLast.getTime()) / (1000*60*60*24);
var age=Math.round(dateDiff/365);

document.getElementById('age').value=age;
}
//staff Id check-exist or not

$(document).ready(function() {
$("#staffId").keyup(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var staffId = $(this).val(); //get the string typed by user
   $("#user-result").html('<img src="ajax-loader.gif" />');
   $.post('check_staffId.php', {'staff':staffId}, function(data) { //make ajax call to check_username.php
   $("#user-result").html(data); //dump the data received from PHP page
   });
});
});
</script>

<script>
function valid()
{
flag=false;
var reguler1 =/^[a-zA-Z ]*$/;
var rmobile =/^(?:(?:\+|0{0,2})91(\s\s*)?|[0]?)?[789]\d{9}$/; 
var remail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;

jName = document.getElementById('name').value;
	if(!jName.match(reguler1))
 	{		 
	document.getElementById('namediv').innerHTML="Enter valid name";
	flag=true;
	}
	
Jdob=document.getElementById('dob').value;		
	if(Jdob=='')
	{
	document.getElementById('dobdiv').innerHTML="You can't leave this empty.";
	flag=true;
	}
	
	  var date  = Jdob.substring(0,2);
	  var month = Jdob.substring(3,5);
	  var year  = Jdob.substring(6,10);
	
	var myDate= new Date(year,month-1,date);		
	var today = new Date();	
	
	if (myDate>today)
	  {
	 document.getElementById('dobdiv').innerHTML="Enter valid date of birth";
		flag=true;
	  }	

jMobile=document.getElementById('phone').value;
	if(!jMobile.match(rmobile) && jMobile!='')
	{	
	document.getElementById('phonediv').innerHTML="Enter valid Mobile number.";
	flag=true;
	}
jEmail=document.getElementById('email').value;
	if(!jEmail.match(remail) && jEmail!='' )
	{	
	document.getElementById('emaildiv').innerHTML="Enter valid Email Id.";
	flag=true;
	}
	
Jdoj=document.getElementById('dateOfJoin').value;		
	if(Jdoj=='')
	{
	document.getElementById('dateOfJoindiv').innerHTML="You can't leave this empty.";
	flag=true;
	}	
	
jgli=document.getElementById('gli').value;	
	if(isNaN(jgli))
	{		
	document.getElementById('glidiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jsli=document.getElementById('sli').value;	
	if(isNaN(jsli))
	{		
	document.getElementById('slidiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jpf=document.getElementById('pf').value;	
	if(isNaN(jpf))
	{		
	document.getElementById('pfdiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jcca=document.getElementById('cca').value;	
	if(isNaN(jcca))
	{		
	document.getElementById('ccadiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jhra=document.getElementById('hra').value;	
	if(isNaN(jhra))
	{		
	document.getElementById('hradiv').innerHTML="Enter valid amount";
	flag=true;
	}
	
jbasic=document.getElementById('basic').value;	
	if(isNaN(jbasic))
	{		
	document.getElementById('basicdiv').innerHTML="Enter valid amount";
	flag=true;
	}
		
if(flag==true)
	{
	return false;
	}
}

function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">STAFF REGISTRATION</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4">
            <form method="post">
              <div class="input-group">
			  <input type="text" class="form-control"  name="sname" placeholder="Staff Name" value="<?php echo @$_REQUEST['sname'] ?>">             
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['sname'])
{
	$cond=$cond." and ".TABLE_STAFF.".name like'%".$_POST['sname']."%'";
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table view_limitter pagination_table" >
                <thead>
                  <tr >
				    <th>Sl No</th>               
					<th>Staff ID</th>
					<th>Name</th>
					<th>Designation</th>					
										
                  </tr>
                </thead>
                <tbody>
				<?php 															
						$selAllQuery="SELECT * FROM ".TABLE_STAFF." WHERE $cond ORDER BY ID ASC";
												
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="4">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
							while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
					   <td><?php echo $i++;?>
						  <div class="adno-dtls"> <?php if($staffEdit || $type=="Admin"){?> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  |<?php }?> <a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a> </div>
						  
							<!-- Modal3 -->
							  <div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
								<div class="modal-dialog">
								  <div class="modal-content"> 
									<!-- <div class="modal-body clearfix">
								fddhdhdhddhdh
								  </div>-->	 		
									<div role="tabpanel" class="tabarea2"> 
									  
									  <!-- Nav tabs -->
									 <ul class="nav nav-tabs" role="tablist">									  
										<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Personal</a> </li>
										<li role="presentation"> <a href="#payment<?php echo $tableId; ?>" aria-controls="payment" role="tab" data-toggle="tab">Payment</a> </li>										
									</ul>
									  
									  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
										  <table class="table nobg">
											<tbody style="background-color:#FFFFFF;">
						
											   <tr>
												<td>Staff ID</td>
												<td> : </td>
												<td><?php echo $row['staffId']; ?></td>
											  </tr>
											  <tr>
												<td>Staff Name</td>
												<td> : </td>
												<td><?php echo $row['name']; ?></td>
											  </tr>
											  <tr>
												<td>Date of Birth</td>
												<td> : </td>
												<td><?php echo $App->dbformat_date_db($row['dob']);  ?></td>
											  </tr>
											  <tr>
												<td>Age</td>
												<td> : </td>
												<td><?php echo $row['age']; ?></td>
											  </tr>
											  <tr>
												<td>Address</td>
												<td> : </td>
												<td><?php echo $row['address']; ?></td>
											  </tr>
											  <tr>
												<td>Mobile Number</td>
												<td> : </td>
												<td><?php echo $row['phone']; ?></td>
											  </tr>
											  <tr>
												<td>Email Id</td>
												<td> : </td>
												<td style="text-transform: none;"><?php echo $row['email']; ?></td>
											  </tr>
											   <tr>
												<td>Date Of Join</td>
												<td> : </td>
												<td><?php echo $App->dbformat_date_db($row['dateOfJoin']); ?></td>
											  </tr>	
											   <tr>
												<td>Gender</td>
												<td> : </td>
												<td><?php echo $row['gender']; ?></td>
											  </tr>
                                             <tr>
												<td>Type</td>
												<td> : </td>
												<td><?php echo $row['type']; ?></td>
											  </tr>
											   <tr>
												<td>Designation</td>
												<td> : </td>
												<td><?php echo $row['designation']; ?></td>
											  </tr>												   								  
											</tbody>
										  </table>
										</div>
										<div role="tabpanel" class="tab-pane" id="payment<?php echo $tableId; ?>">
										  <table class="table nobg" >
											<tbody style="background-color:#FFFFFF">
                                           
											  <tr>
												<td>Basic Pay</td>
												<td> : </td>
												<td><?php echo $row['basicPay']; ?></td>
											  </tr>
											  <tr>
												<td>HRA</td>
												<td> : </td>
												<td><?php echo $row['hra']; ?></td>
											  </tr>
											  <tr>
												<td>CCA</td>
												<td> : </td>
												<td><?php echo $row['cca']; ?></td>
											  </tr>
											  <tr>
												<td>PF</td>
												<td> : </td>
												<td><?php echo $row['pf']; ?></td>
											  </tr>
											  <tr>
												<td>SLI</td>
												<td> : </td>
												<td><?php echo $row['sli']; ?></td>
											  </tr>
                                              <tr>
												<td>GLI</td>
												<td> : </td>
												<td><?php echo $row['gli']; ?></td>
											  </tr>
											  <tr>
												<td>Account Number</td>
												<td> : </td>											
												<td><?php echo  $row['accountNo']; ?></td>
											  </tr>											 
											</tbody>
										  </table>
										</div>										
									  </div>
									</div>
								  </div>
								</div>
							  </div>
							  <!-- Modal3 cls --> 
						  
						  
						  </td>
						<td><?php echo $row['staffId']; ?></td>
						<td><?php echo $row['name']; ?></td>						
						<td><?php echo $row['designation']; ?> </td>                   	 	
					  </tr>
					  <?php }
				}?>                  
                </tbody>
              </table>
			  	
            </div>
			<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">STAFF REGISTRATION </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="staffId">Staff ID:<span class="star">*</span></label>
                      <input type="text" id="staffId" name="staffId" class="form-control2" value=""  required placeholder="Staff ID" title="Staff Id" />				
					  <span id="user-result"></span>
                    </div>
					
                    <div class="form-group">
                      <label for="name">Name: <span class="star">*</span></label>
                      <input type="text" id="name" name="name" class="form-control2" value="" required placeholder="Name" title="Staff Name" onfocus="clearbox('namediv')"/>
					  <div id="namediv" class="valid" style="color:#FF6600;"></div>					
                    </div>
                   
                    <div class="form-group">
                      <label for="dob">Date Of Birth:<span class="star">*</span></label>                    
					   <input type="text" id="dob" name="dob" class="form-control2 datepicker"  value=""  placeholder="Date Of Birth" readonly="readonly" onchange="getAge()" onfocus="clearbox('dobdiv')"/>
					   <div id="dobdiv" class="valid" style="color:#FF6600;"></div>
                    </div>					                   								 
                  
                    <div class="form-group">
                      <label for="age">Age:<span class="star">*</span></label>
                     <input type="text" id="age" name="age" class="form-control2" value=""  placeholder="Age" title="Age" onmouseover="getAge()" readonly />
					
                    </div>									  	
					
					<div class="form-group">
                      <label for="address">Residential Address:</label>                      
						 <textarea style="width:265px" name="address"  id="address" class="form-control2"  placeholder="Address"></textarea>				   
                    </div>									
					
					<div class="form-group">
                      <label for="phone">Mobile Number:</label>
					   <input type="text" id="phone" name="phone" value=""  title="Mobile Number"  placeholder="Mobile No" class="form-control2" onfocus="clearbox('phonediv')"/>	 
					   <div id="phonediv" class="valid" style="color:#FF6600;"></div> 
                    </div>
					
					<div class="form-group">
                      <label for="email">Email Id:</label>
					   <input type="text" id="email" name="email" value=""  title="Email Id"  placeholder="Email Id" class="form-control2" onfocus="clearbox('emaildiv')"/>	 
					   <div id="emaildiv" class="valid" style="color:#FF6600;"></div> 
                    </div>
										
					<div class="form-groupy">
                      <label for="dateOfJoin">Date Of Join:<span class="star">*</span></label>
                      <input type="text" id="dateOfJoin" name="dateOfJoin" value="<?php echo date("d/m/Y");?>" required title="Date Of Join"  class="form-control2 datepicker" readonly="readonly" onfocus="clearbox('dateOfJoindiv')"/>	
					  <div id="dateOfJoindiv" class="valid" style="color:#FF6600;"></div>				  
                    </div>					
					
					<div class="form-groupy">
                      <label for="gender">Gender:<span class="star">*</span> </label>
						 <select name="gender" id="gender" class="form-control2" title="Select Type" required>
							<option value="Male"> Male </option>
							<option value="Female"> Female </option>
						</select>		  
                    </div>								
				</div>
				 <div class="col-sm-6">
				 <div class="form-group">
                      <label for="type">Type:<span class="star">*</span></label>
                        <select name="type" id="type" class="form-control2" title="Select Type" required>
							<option value="nonteaching"> Non Teaching </option>
							<option value="teaching"> Teaching </option>
                    	</select>
                    </div>
					 <div class="form-group">
                      <label for="designation">Designation:<span class="star">*</span></label>
                      <input type="text" id="designation" name="designation" required value="" title="Designation" placeholder="Designation" class="form-control2" />
                    </div>
					 <div class="form-group">
                      <label for="basic">Basic Pay:<span class="star">*</span></label><BR />
                      <input type="text" id="basic" name="basic" value="" required title="Basic Pay" placeholder="Basic Pay" class="form-control2" onfocus="clearbox('basicdiv')" />
					  <div id="basicdiv" class="valid" style="color:#FF6600;"></div>
                    </div>			  
                    <div class="form-groupy">
                      <label for="hra">House Rent Allowance [HRA] :<span class="star">*</span></label>
                       <input type="text" id="hra" name="hra" value="0" required  placeholder="HRA" class="form-control2" onfocus="clearbox('hradiv')"/>
					   <div id="hradiv" class="valid" style="color:#FF6600;"></div>					  
                    </div>
					<div class="form-group">
                      <label for="cca">City Compansatory Allowance [CCA] :<span class="star">*</span></label>
                      <input type="text" id="cca" name="cca" placeholder="CCA"  required value="0" class="form-control2" onfocus="clearbox('ccadiv')" /> 	
					  <div id="ccadiv" class="valid" style="color:#FF6600;"></div>			  
                    </div>
					<div class="form-group">
                      <label for="pf">Provident Fund [PF] :<span class="star">*</span></label>
                      <input type="text" id="pf" name="pf" value="0" placeholder="PF" required class="form-control2" onfocus="clearbox('pfdiv')" /> 
					  <div id="pfdiv" class="valid" style="color:#FF6600;"></div>						  		  
                    </div>
					<div class="form-group">
                      <label for="sli">State Life Insurance [SLI] :<span class="star">*</span></label>
                      <input type="text" id="sli" name="sli" value="0" placeholder="SLI" required class="form-control2" onfocus="clearbox('slidiv')"/>
					  <div id="slidiv" class="valid" style="color:#FF6600;"></div>				  
                    </div>					
					<div class="form-group">
                      <label for="gli">Group Life Insurance [GLI] :<span class="star">*</span></label>
                      <input type="text" id="gli" name="gli" value="0"   placeholder="GLI" required class="form-control2" onfocus="clearbox('glidiv')" />	
					  <div id="glidiv" class="valid" style="color:#FF6600;"></div>		  
                    </div>
					<div class="form-group">
                      <label for="accountNo">Account Number :<span class="star">*</span></label>
                      <input type="text" id="accountNo" name="accountNo" value="0" required placeholder="Account Number" class="form-control2" onfocus="clearbox('accountNodiv')"/> 
					 		  
                    </div>
					
				</div>
			</div>
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
	  </div>
      <!-- Modal1 cls --> 
     
	 
	 
	  
      </div>
  </div>
<?php include("../adminFooter.php") ?>

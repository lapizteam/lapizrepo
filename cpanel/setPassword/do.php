<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['user'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$user		=	$App->convert($_REQUEST['user']);
				$pwd		=	$App->convert($_REQUEST['pwd']);
				$cPwd		=	$App->convert($_REQUEST['confirm']);
				
				$staffId	=	$App->convert($_REQUEST['hiddenStaffId']);
				$type		=	$App->convert($_REQUEST['type']);				
					
				$existId=$db->existValuesId(TABLE_ADMIN,"staffId='$staffId'");				
				if($existId>0)
				{					
					$_SESSION['msg']="This Staff Has been Already Registered";					
					header("location:new.php");								
				}
				else
				{
				
					if($pwd==$cPwd)
					{	
						
						$username1	=	mysql_real_escape_string(htmlentities($user));
						$password1	=	mysql_real_escape_string(htmlentities($pwd));
						
						$pstring1	 =	md5($password1); // Encrepted Password
						$pstring2	 =	mysql_real_escape_string(htmlentities($pstring1));  // Password to store the database
						$data['staffId']		=	$App->convert($_REQUEST['hiddenStaffId']);							
						$data['userName']		=	$user;
						$data['password']		=	$pstring2;
						$data['type']			=	$App->convert($_REQUEST['type']);
						
						$permission['staffId']	=	$App->convert($_REQUEST['hiddenStaffId']);
						$permission['loginId']	=	$App->convert($uid);
						if($type=="FrontOffice")
						{
							$permission['basicSetting']			=1;		
							$permission['basicSettingEdit']		=1;											
							$permission['studAttendance']		=1;			
							$permission['feePayment']			=1;		
							$permission['feePaymentEdit']		=1;		
							$permission['busSettings']			=1;		
							$permission['busSettingEdit']		=1;	
							$permission['messFee']				=1;		
							$permission['messFeeEdit']			=1;			
							$permission['accountReport']		=1;			
							$permission['accountTransfer']		=1;			
							$permission['accountTransferEdit']	=1;	
							$permission['examType']				=1;
							$permission['examTypeEdit']			=1;	
							$permission['examEntry']			=1;		
							$permission['examEntryEdit']		=1;		
							$permission['markEntry']			=1;		
							$permission['notification']			=1;		
							$permission['notificationEdit']		=1;			
							$permission['printSetting']			=1;
							$permission['reportView']			=1;
							$permission['promotion']			=1;			
							$permission['studentReg']			=1;		
							$permission['studentEdit']			=1;		
							$permission['staffReg']				=1;		
							$permission['staffEdit']			=1;	
							$permission['mail']					=1;
							
							$permission['club']					=1;
							$permission['clubEdit']				=1;
							
							$permission['library']				=0;	
							$permission['libraryEdit']			=0;
							$permission['diary']				=1;
							$permission['calendar']				=1;
							$permission['classTeacher']			=1;
							$permission['classTeacherEdit']		=1;	
							$permission['chat']					=0;
													
																				
						}
						else if($type=="Librarian")
						{
							$permission['library']				=1;	
							$permission['libraryEdit']			=1;
							
							$permission['basicSetting']			=0;		
							$permission['basicSettingEdit']		=0;											
							$permission['studAttendance']		=0;			
							$permission['feePayment']			=0;		
							$permission['feePaymentEdit']		=0;		
							$permission['busSettings']			=0;		
							$permission['busSettingEdit']		=0;	
							$permission['messFee']				=0;		
							$permission['messFeeEdit']			=0;			
							$permission['accountReport']		=0;			
							$permission['accountTransfer']		=0;			
							$permission['accountTransferEdit']	=0;
							$permission['examType']				=0;
							$permission['examTypeEdit']			=0;			
							$permission['examEntry']			=0;		
							$permission['examEntryEdit']		=0;		
							$permission['markEntry']			=0;		
							$permission['notification']			=0;		
							$permission['notificationEdit']		=0;			
							$permission['printSetting']			=0;	
							$permission['reportView']			=0;		
							$permission['promotion']			=0;			
							$permission['studentReg']			=0;		
							$permission['studentEdit']			=0;		
							$permission['staffReg']				=0;		
							$permission['staffEdit']			=0;	
							$permission['club']					=0;
							$permission['clubEdit']				=0;
							$permission['mail']					=0;
							$permission['diary']				=1;
							$permission['calendar']				=1;
							$permission['classTeacher']			=0;
							$permission['classTeacherEdit']		=0;
							$permission['chat']					=0;	

						}
						else if($type=="Teacher")
						{
							$permission['studAttendance']		=1;
							$permission['examType']				=1;
							$permission['examTypeEdit']			=1;
							$permission['examEntry']			=1;		
							$permission['examEntryEdit']		=1;
							$permission['markEntry']			=1;
							$permission['mail']					=1;
							
							$permission['library']				=0;	
							$permission['libraryEdit']			=0;
							
							$permission['basicSetting']			=0;		
							$permission['basicSettingEdit']		=0;																			
							$permission['feePayment']			=0;		
							$permission['feePaymentEdit']		=0;		
							$permission['busSettings']			=0;		
							$permission['busSettingEdit']		=0;	
							$permission['messFee']				=0;		
							$permission['messFeeEdit']			=0;			
							$permission['accountReport']		=0;			
							$permission['accountTransfer']		=0;			
							$permission['accountTransferEdit']	=0;																					
							$permission['notification']			=0;		
							$permission['notificationEdit']		=0;			
							$permission['printSetting']			=0;	
							$permission['reportView']			=0;		
							$permission['promotion']			=0;			
							$permission['studentReg']			=0;		
							$permission['studentEdit']			=0;		
							$permission['staffReg']				=0;		
							$permission['staffEdit']			=0;
							$permission['club']					=0;
							$permission['clubEdit']				=0;
							$permission['diary']					=1;
							$permission['calendar']				=1;
							$permission['classTeacher']			=0;
							$permission['classTeacherEdit']		=0;
							$permission['chat']					=1;	
		
						}
					
					
							$selectRes=mysql_query("SELECT * FROM ".TABLE_ADMIN." WHERE userName='$user'");
							$selectRow=mysql_fetch_array($selectRes);
							if($selectRow==0)
							{
								$success1 = $db->query_insert(TABLE_ADMIN, $data);									
								$success2 = $db->query_insert(TABLE_USERPERMISSION, $permission);	
								
								//sending username and password to staff's mail
								if($success1!='' && $success2!='')
								{
									$qry = "select eMail from ".TABLE_ABOUT;
									$schoolEmail = mysql_query($qry);
									$schoolRows = mysql_fetch_array($schoolEmail);
									
									$staffId =	$App->convert($_REQUEST['hiddenStaffId']);
									$qry1 = "select email from ".TABLE_STAFF." where ID=$staffId";
									$staffEmail = mysql_query($qry1);
									$staffRows = mysql_fetch_array($staffEmail);
									
									$from = $schoolRows['eMail'];
									$to = $staffRows['email'];
									$subject = "Notification from admin";
									$message = "You can login to Lapiz using username : ".$username1." and password : ".$password1;
									$headers = "From:" . $from . "\r\n" .
												   "Reply-To:" . $from;
									$success=mail($to,$subject,$message,$headers);
									$db->close();
										if($success)
										{	
										$_SESSION['msg']="Password Saved and Mail Send Successfully..";															
										}
										else
										{
										$_SESSION['msg']="Failed To Send Mail..";															
										}										
								}//if($success1!='' && $success2!='')
								else
								{
								$_SESSION['msg'] = "Insertion Failed";							
								}								
							}//if($selectRow==0)
							else
							{								
								$_SESSION['msg']="Entered UserName Is Already Exist";													
							}
						
					}//if($pwd==$cPwd)
					else
					{					
						$_SESSION['msg']="Password is Mismatches with Confirm Password";					
						
					}
				}//else
					header("location:new.php");
			}	//else	
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];
		//echo $fid;die;  		    
		if(!$_REQUEST['user'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
								
				$user=$App->convert($_REQUEST['user']);
				$pwd=$App->convert($_REQUEST['pwd']);
				$cPwd=$App->convert($_REQUEST['confirm']);
				
				$staffId	=	$App->convert($_REQUEST['hiddenStaffId']);
				$type		=	$App->convert($_REQUEST['type']);				
					
				$existId=$db->existValuesId(TABLE_ADMIN,"staffId='$staffId' AND type='$type' AND ID!='$fid'");				
				if($existId>0)
				{						
					$_SESSION['msg']="This Staff Has been Already Registered With This Type";															
				}
				else
				{
					
					if($pwd==$cPwd)
					{	
					
					$username1	=	mysql_real_escape_string(htmlentities($user));
					$password1	=	mysql_real_escape_string(htmlentities($pwd));
					
					$pstring1	 =	md5($password1); // Encrepted Password
					$pstring2	 =	mysql_real_escape_string(htmlentities($pstring1));  // Password to store the database
					
					$data['staffId']		=	$App->convert($_REQUEST['hiddenStaffId']);		
					$data['userName']		=	$user;
					$data['password']		=	$pstring2;
					$data['type']			=	$App->convert($_REQUEST['type']);
					
					$permission['staffId']	=	$App->convert($_REQUEST['hiddenStaffId']);
					$permission['loginId']	=	$App->convert($uid);
						if($type=="FrontOffice")
						{
							$permission['basicSetting']			=1;		
							$permission['basicSettingEdit']		=1;											
							$permission['studAttendance']		=1;			
							$permission['feePayment']			=1;		
							$permission['feePaymentEdit']		=1;		
							$permission['busSettings']			=1;		
							$permission['busSettingEdit']		=1;	
							$permission['messFee']				=1;		
							$permission['messFeeEdit']			=1;			
							$permission['accountReport']		=1;			
							$permission['accountTransfer']		=1;			
							$permission['accountTransferEdit']	=1;	
							$permission['examType']				=1;
							$permission['examTypeEdit']			=1;	
							$permission['examEntry']			=1;		
							$permission['examEntryEdit']		=1;									
							$permission['markEntry']			=1;		
							$permission['notification']			=1;		
							$permission['notificationEdit']		=1;			
							$permission['printSetting']			=1;
							$permission['reportView']			=1;
							$permission['promotion']			=1;			
							$permission['studentReg']			=1;		
							$permission['studentEdit']			=1;		
							$permission['staffReg']				=1;		
							$permission['staffEdit']			=1;	
							$permission['mail']					=1;
							
							$permission['library']				=0;	
							$permission['libraryEdit']			=0;
							$permission['diary']				=1;
							$permission['calendar']			=1;	
							$permission['classTeacher']			=1;
							$permission['classTeacherEdit']		=1;
							$permission['chat']					=0;	
						
																				
						}
						else if($type=="Librarian")
						{
							$permission['library']				=1;	
							$permission['libraryEdit']			=1;
							
							$permission['basicSetting']			=0;		
							$permission['basicSettingEdit']		=0;											
							$permission['studAttendance']		=0;			
							$permission['feePayment']			=0;		
							$permission['feePaymentEdit']		=0;		
							$permission['busSettings']			=0;		
							$permission['busSettingEdit']		=0;	
							$permission['messFee']				=0;		
							$permission['messFeeEdit']			=0;			
							$permission['accountReport']		=0;			
							$permission['accountTransfer']		=0;			
							$permission['accountTransferEdit']	=0;	
							$permission['examType']				=0;
							$permission['examTypeEdit']			=0;	
							$permission['examEntry']			=0;		
							$permission['examEntryEdit']		=0;		
							$permission['markEntry']			=0;		
							$permission['notification']			=0;		
							$permission['notificationEdit']		=0;			
							$permission['printSetting']			=0;	
							$permission['reportView']			=0;		
							$permission['promotion']			=0;			
							$permission['studentReg']			=0;		
							$permission['studentEdit']			=0;		
							$permission['staffReg']				=0;		
							$permission['staffEdit']			=0;	
							$permission['mail']					=0;
							$permission['diary']				=1;
							$permission['calendar']			=1;
							$permission['classTeacher']			=0;
							$permission['classTeacherEdit']		=0;
							$permission['chat']					=0;	

						}
						else if($type=="Teacher")
						{
							$permission['studAttendance']		=1;
							$permission['examType']				=1;
							$permission['examTypeEdit']			=1;
							$permission['examEntry']			=1;		
							$permission['examEntryEdit']		=1;
							$permission['markEntry']			=1;
							$permission['mail']					=1;
							
							$permission['library']				=0;	
							$permission['libraryEdit']			=0;
							
							$permission['basicSetting']			=0;		
							$permission['basicSettingEdit']		=0;																			
							$permission['feePayment']			=0;		
							$permission['feePaymentEdit']		=0;		
							$permission['busSettings']			=0;		
							$permission['busSettingEdit']		=0;	
							$permission['messFee']				=0;		
							$permission['messFeeEdit']			=0;			
							$permission['accountReport']		=0;			
							$permission['accountTransfer']		=0;			
							$permission['accountTransferEdit']	=0;																					
							$permission['notification']			=0;		
							$permission['notificationEdit']		=0;			
							$permission['printSetting']			=0;	
							$permission['reportView']			=0;		
							$permission['promotion']			=0;			
							$permission['studentReg']			=0;		
							$permission['studentEdit']			=0;		
							$permission['staffReg']				=0;		
							$permission['staffEdit']			=0;
							$permission['diary']				=1;
							$permission['calendar']			=1;	
							$permission['classTeacher']			=0;
							$permission['classTeacherEdit']		=0;
							$permission['chat']					=1;	
	
						}
					
							$selectRes1=mysql_query("SELECT * FROM ".TABLE_ADMIN." WHERE ID='$fid'");
							$selectRow1=mysql_fetch_array($selectRes1);
							$staff=$selectRow1['staffId'];
							
							$selectRes=mysql_query("SELECT * FROM ".TABLE_ADMIN." WHERE userName='$user' AND ID!='$fid'");
							$selectRow=mysql_fetch_array($selectRes);
							
							if($selectRow==0)
							{
								$success1 = $db->query_update(TABLE_ADMIN, $data,"ID='{$fid}'");
								$success2 = $db->query_update(TABLE_USERPERMISSION, $permission,"staffId='{$staff}'");	
								
								//sending username and password to staff's mail
								if($success1!='' && $success2!='')
								{
									$qry = "select eMail from ".TABLE_ABOUT;
									$schoolEmail = mysql_query($qry);
									$schoolRows = mysql_fetch_array($schoolEmail);
									
									$staffId =	$App->convert($_REQUEST['hiddenStaffId']);
									$qry1 = "select email from ".TABLE_STAFF." where ID=$staffId";
									$staffEmail = mysql_query($qry1);
									$staffRows = mysql_fetch_array($staffEmail);
									
									$from = $schoolRows['eMail'];
									$to = $staffRows['email'];
									$subject = "Notification from admin";
									$message = "You can login to Lapiz using username : ".$username1."  and password : ".$password1;
									$headers = "From:" . $from . "\r\n" .
												   "Reply-To:" . $from;
									//echo $message;die;
									$success=mail($to,$subject,$message,$headers);
									$db->close();
									//echo $success;die;
										if($success)
										{	
										$_SESSION['msg']="Password Saved and Mail Send Successfully..";															
										}
										else
										{
										$_SESSION['msg']="Failed To Send Mail..";					
										}
								}//if($success1!='' && $success2!='')
								else
								{
								$_SESSION['msg'] = "Insertion Failed";
								}
								
							}//if($selectRow==0)
							else
							{
								$_SESSION['msg']="Entered User Name Is Already Exist";					
								header("location:edit.php?id=$fid");
							}
					}//if($pwd==$cPwd)
					else
					{				
						$_SESSION['msg']="Password is Mismatches with Confirm Password";					
						header("location:new.php");
					}
				}//else
				header("location:new.php");	
			}//else		
		break;
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];
				$success=0;
				$success2=0;
				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$selectRes1=mysql_query("SELECT * FROM ".TABLE_ADMIN." WHERE ID='$id'");
				$selectRow1=mysql_fetch_array($selectRes1);
				$staff=$selectRow1['staffId'];
				
				$success=$db->query("DELETE FROM `".TABLE_ADMIN."` WHERE ID='{$id}'");	
				$success2=$db->query("DELETE FROM `".TABLE_USERPERMISSION."` WHERE staffId='{$staff}'");								
				$db->close(); 	
				
				if($success && $success2)
				{
				$_SESSION['msg']="Password deleted successfully";								
				}
				else
				{
				$_SESSION['msg']="Failed";								
				}
				header("location:new.php");					
		break;		
}
?>
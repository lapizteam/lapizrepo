<?php include("../adminHeader.php"); 
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}


$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function valid()
{
flag=false;
maxLength=9;
	opening_balance=document.getElementById('opening_balance').value;		
		if(isNaN(opening_balance))
		{																			
		document.getElementById('opening_balanceDiv').innerHTML="Enter valid number";
		flag=true;
		}
		if(opening_balance<0)
		{																			///for age
		document.getElementById('opening_balanceDiv').innerHTML="Enter valid number";
		flag=true;
		}
		if(opening_balance.length>maxLength)
		{																			///for age
		document.getElementById('opening_balanceDiv').innerHTML="Enter valid number";
		flag=true;
		}
	
	if(flag==true)
	{
	return false;
	}	
}
//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
function ledgerTypeCheck()
{
masterLedger=document.getElementById('masterLedger').value;		
		if(masterLedger=='1')
		{																			
		alert("You can't edit this account group & ledger name. Because it is a permanent ledger");
		}
}
</script>


<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 
$fid=$_REQUEST['id'];
$fid = mysql_real_escape_string($fid);

$sql ="SELECT * FROM `".TABLE_LEDGER."` WHERE id='$fid'";
$record = $db->query_first($sql);		
?>
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">LEDGER</h4>
            </div>
            <div class="modal-body clearfix">
			<form method="post" action="do.php?op=edit" class="form1"  onsubmit="return valid()">
			<input type="hidden" name="ledgerid" value="<?php echo $record['ID']; ?>" />
			             
                <div class="row">
                 <div class="col-sm-6">
                    <div class="form-group">
                      <label for="name">Account Group:<span class="star">*</span></label>
						 <?php  
						 $group_qry= $db->query("SELECT * FROM `". TABLE_ACCOUNT_GROUP."` WHERE `parent`>'3'"); 
						 ?>
						  <select name="account_group" id="account_group" class="form-control2"   title="Account Group"  required>						   
							  <?php  
							   while($group_qry_row=mysql_fetch_array($group_qry))
								 {						 ?>  
								 <option value="<?php echo $group_qry_row['ID']; ?>"  <?php if($record['account_group']==$group_qry_row['ID']) { ?> 				selected="selected" <?php } ?>><?php echo $group_qry_row['name']; ?></option>
							  <?php  
							  	 }  
								?>
						  </select>									
                    </div>
					
                    <div class="form-group">
                      <label for="father">Ledger Name:<span class="star">*</span></label>
                      <input name="name" id="name" type="text" class="form-control2" required title="Ledger Name" value="<?php echo $record['name'];?>" onclick="ledgerTypeCheck()">					
					  <input type="hidden" id="masterLedger" value="<?php echo $record['master_ledger'];?>">
                    </div>
                   
                    <div class="form-group">
                      <label for="dob">Opening Balance:<span class="star">*</span></label>                    
					 <input name="opening_balance" id="opening_balance" type="text" class="form-control2"  required title="Opening Balance" value="<?php echo $record['opening_balance'];?>" onfocus="clearbox('opening_balanceDiv')">
					 <div id="opening_balanceDiv" class="valid" style="color:#FF6600"></div>  
                    </div>					                   								 
                  
                    <div class="form-group">
                      <label for="nationality">Credit/Debit:<span class="star">*</span></label>
                     <select name="pay_type" id="pay_type"  class="form-control2"  required title="Credit/Debit"  >					 	
						<option value="Credit" <?php if($record['pay_type']=="Credit") { ?> selected="selected" <?php } ?>>Credit</option>
						<option value="Debit" <?php if($record['pay_type']=="Debit") { ?> selected="selected" <?php } ?>>Debit</option>
					  </select>
                    </div>									  	
					
					<div class="form-group">
                      <label for="mobile">Ledger Details:</label>                      
						 <textarea name="details" class="form-control2" id="details" rows="3" cols="180"><?php echo $record['details'];?></textarea>				   
                    </div>																			
				</div>				 																								
                </div>	
		</div>
	 <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>

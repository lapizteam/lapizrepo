<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];


$uid = $_SESSION['LogID'];

switch($optype)
{
		
	case 'new':
		
		if(!$_POST['account_group'] || !$_POST['name'] || !$_POST['pay_type'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{
			
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$existId=0;
				$name	=	$App->convert($_POST['name']);
				$existId=$db->checkValueExist(TABLE_LEDGER,"name='$name'");
				if($existId)
				{		
				$_SESSION['msg']="Ledger name is already exist";				
				header("location:new.php");
				}				
				else
				{
				/* ------------------------------------------ */
					$data['account_group'] 			=	$App->convert($_POST['account_group']);
					$data['name']					=	$App->convert($_POST['name']);
					$data['details']				= 	$App->convert($_POST['details']);
					$data['opening_balance']		=	$App->convert($_POST['opening_balance']);
					$data['pay_type']		    	=   $App->convert($_POST['pay_type']);
					$data['master_ledger']		    =   "0";			
					$data['cdate']					=	"NOW()";
					$data['uid']					=	$_SESSION['LogID'];
					$data['loginId']				=	$_SESSION['LogID'];

					
				//$uid = $_SESSION['PA_UID'];		
				$ledger_id = $db->query_insert(TABLE_LEDGER, $data);
				//echo TABLE_LEDGER, $data;die;
				/* ------------------------------------------ */
				
				$vou_sql1 ="SELECT id FROM `".TABLE_LEDGER."` WHERE `name`='Suspense' ";
				$vou_record1 = $db->query_first($vou_sql1);
				$cash=$vou_record1['id'];
				/* ------------------------------------------ */
				$vou_sql ="SELECT MAX(voucher_no) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Opening Balance'";
				$vou_record = $db->query_first($vou_sql);
				/* ------------------------------------------ */
				
				/* ------------------------------------------ */
				if($_POST['opening_balance']!=0)
				{
				$tfdata['from_ledger'] 		=  $ledger_id;
				$tfdata['to_ledger'] 		=  $ledger_id;
				$tfdata['voucher_no']		=  $vou_record['vn']+1;
				$tfdata['voucher_type']		=  'Opening Balance';
				
				if($_POST['pay_type']=='Credit')
					{
						$tfdata['credit'] = $App->convert($_POST['opening_balance']);
					}
				else if($_POST['pay_type']=='Debit')
					{
						$tfdata['debit'] = $App->convert($_POST['opening_balance']);
					}	
				$tfdata['remark']				=  'Opening Balance';
				$tfdata['cdate']				=	"NOW()";
				$tfdata['transaction_date']		=	"NOW()";
				$tfdata['uid']					=	$_SESSION['LogID'];

				$db->query_insert(TABLE_TRANSACTION, $tfdata);
				
				$tsdata['to_ledger'] 		=  $cash;
				$tsdata['from_ledger'] 		=  $ledger_id;
				$tsdata['voucher_no']		=  $vou_record['vn']+1;
				$tsdata['voucher_type']		=  'Opening Balance';
				
				if($_POST['pay_type']=='Credit')
					{
						$tsdata['debit'] = $App->convert($_POST['opening_balance']);
					}
				else if($_POST['pay_type']=='Debit')
					{
						$tsdata['credit'] = $App->convert($_POST['opening_balance']);
					}	
				$tsdata['remark']				=  'Opening Balance';
				$tsdata['cdate']				=	"NOW()";
				$tsdata['transaction_date']		=	"NOW()";
				$tsdata['uid']					=	$_SESSION['LogID'];
				 
				$db->query_insert(TABLE_TRANSACTION, $tsdata);
				}
				/* ------------------------------------------ */
				if($ledger_id)
					{
					$_SESSION['msg']="Ledger stored successfully";																				
					}
					else
					{
					$_SESSION['msg']="Failed";									
					}	
					header("location:new.php");	
				}									
			}
		
		break;
	
	// EDIT SECTION
	//-
	case 'edit':
		
		$uid = $_SESSION['LogID'];
		$lid			=	$_REQUEST['ledgerid'];
		
		if(!$_POST['account_group'] || !$_POST['name'] || !$_POST['pay_type'])
			{					
				$_SESSION['msg']="Error, Invalid Details ";					
				header("location:edit.php?id=$lid");	
			}
		else
			{
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				/* ------------------------------------------ */
					
					
					/* ---------************---------------if main ledger,cant update name and ac group */
					$select=mysql_query("select master_ledger,name from ".TABLE_LEDGER." where id='$lid'");
					$res=mysql_fetch_array($select);
					$mainLedger=0;
					if($res['master_ledger']=='1')
					{
					$mainLedger						=	$res['name'];
					$data['details']				= 	$App->convert($_POST['details']);
					$data['opening_balance']		=	$App->convert($_POST['opening_balance']);
					$data['pay_type']		    	=   $App->convert($_POST['pay_type']);	
					$data['cdate']					=	"NOW()";				
					}
					/* ---------************--------------- */
					else
					{
					$data['account_group'] 			=	$App->convert($_POST['account_group']);
					$data['name']					=	$App->convert($_POST['name']);
					$data['details']				= 	$App->convert($_POST['details']);
					$data['cdate']					=	"NOW()";
					$data['opening_balance']		=	$App->convert($_POST['opening_balance']);
					$data['pay_type']		    	=   $App->convert($_POST['pay_type']);					
					}
					
				
				$success=$db->query_update(TABLE_LEDGER, $data, "id='{$lid}'");
				/* ------------------------------------------ */
				
				
				/* ------------------------------------------ */
				$vou_sql ="SELECT * FROM `".TABLE_TRANSACTION."` WHERE `from_ledger`='$lid' AND `to_ledger`='$lid' AND `voucher_type`='Opening Balance' ";
				$vou_record = $db->query_first($vou_sql);
				$voucher_no = $vou_record['voucher_no'];
				
				$del_vou_sql = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `from_ledger`='$lid' AND `voucher_type`='Opening Balance'";
				$db->query($del_vou_sql);
				/* ------------------------------------------ */
				$vou_sql1 ="SELECT id FROM `".TABLE_LEDGER."` WHERE `name`='Suspense' ";
				$vou_record1 = $db->query_first($vou_sql1);
				$cash=$vou_record1['id'];
				/* ------------------------------------------ */
				
				
				/* ------------------------------------------ */
				if($_POST['opening_balance']!=0)
				{
				$tfdata['from_ledger'] 		=  $lid;
				$tfdata['to_ledger'] 		=  $lid;
				$tfdata['voucher_no']		=  $voucher_no;
				$tfdata['voucher_type']		=  'Opening Balance';
				
				if($_POST['pay_type']=='Credit')
					{
						$tfdata['credit'] = $App->convert($_POST['opening_balance']);
					}
				else if($_POST['pay_type']=='Debit')
					{
						$tfdata['debit'] = $App->convert($_POST['opening_balance']);
					}	
				$tfdata['remark']				=  'Opening Balance';
				$tfdata['cdate']				=	"NOW()";
				$tfdata['transaction_date']		=	"NOW()";
				$tfdata['uid']					=	$_SESSION['LogID'];

				$db->query_insert(TABLE_TRANSACTION, $tfdata);
				
				$tsdata['to_ledger'] 		=  $cash;
				$tsdata['from_ledger'] 		=  $lid;;
				$tsdata['voucher_no']		=  $voucher_no;
				$tsdata['voucher_type']		=  'Opening Balance';
				
				if($_POST['pay_type']=='Credit')
					{
						$tsdata['debit'] = $App->convert($_POST['opening_balance']);
					}
				else if($_POST['pay_type']=='Debit')
					{
						$tsdata['credit'] = $App->convert($_POST['opening_balance']);
					}	
				$tsdata['remark']				=  'Opening Balance';
				$tsdata['cdate']				=	"NOW()";
				$tsdata['transaction_date']		=	"NOW()";
				$tsdata['uid']					=	$_SESSION['LogID'];

				$db->query_insert(TABLE_TRANSACTION, $tsdata);
				}
				/* ------------------------------------------ */
				
				$db->close();
				
				
				if($success && $mainLedger=='0')
				{
					$_SESSION['msg']="Ledger updated successfully";	
				}													
				else if($success && $mainLedger)
				{
					$_SESSION['msg']="Ledger updated successfully. But ledger name can't change";														
				}
				else
				{
					$_SESSION['msg']="Failed";
				}
					header("location:new.php");	
			}
		
		break;
	
	// DELETE SECTION
	//-
	case 'delete':
		
				$lid	=	$_REQUEST['id'];
				$uid 	=   $_SESSION['LogID'];
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;				
				try
				{			
				mysql_query("DELETE FROM `".TABLE_LEDGER."` WHERE id='{$lid}'  AND master_ledger='0'");				
				$success=mysql_affected_rows();	
				mysql_query("DELETE FROM `".TABLE_TRANSACTION."` WHERE `from_ledger`='$lid' AND `voucher_type`='Opening Balance'");														
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";	
					 header("location:new.php");				            
				}	
								
				/*$sql = "DELETE FROM `".TABLE_LEDGER."` WHERE id='{$lid}'  AND master_ledger='0'";				
				$success=$db->query($sql);
			
				$del_vou_sql = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `from_ledger`='$lid' AND `voucher_type`='Opening Balance'";
				$db->query($del_vou_sql);*/
				
				$db->close(); 			
				
				if($success>0)
					{
					$_SESSION['msg']="Ledger deleted successfully";														
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else or it is a permanent ledger";									
					}	
					header("location:new.php");		

		break;

}
?>
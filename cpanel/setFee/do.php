<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['class'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$success2=0;
				$class		=	$App->convert($_REQUEST['class']);
				$existId=$db->existValuesId(TABLE_FEESETTING,"class='$class'");
					if($existId>0)
					{
						$_SESSION['msg']="Fee Is Already Exist";					
						header("location:new.php");					
					}
					else
					{				
				
						$data['class']				=	$App->convert($class);
						$data['registrationFee']	=	$App->convert($_REQUEST['registration']);
						$data['admissionFee']		=	$App->convert($_REQUEST['admission']);
						$data['specialFee']			=	$App->convert($_REQUEST['special']);
						$data['tuitionFee']			=	$App->convert($_REQUEST['tution']);
						$data['total']				=	$App->convert($_REQUEST['total']);
						$data['installmentNo']		=	$App->convert($_REQUEST['installmentNo']);
						$data['loginId']			=	$App->convert($uid);										
														
						$success = $db->query_insert(TABLE_FEESETTING, $data);
						
						if($App->convert($_REQUEST['installment1'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment1']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment1Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment2'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment2']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment2Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment3'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment3']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment3Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment4'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment4']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment4Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment5'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment5']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment5Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment6'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment6']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment6Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment7'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment7']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment7Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment8'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment8']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment8Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment9'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment9']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment9Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment10'])>0)
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment10']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment10Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}					
						$db->close();
						
						if(($success) && ($success2))
						{
							$_SESSION['msg']="Fee Details added successfully";					
							header("location:new.php");		
						}
						else
						{
							$_SESSION['msg']="Failed";					
							header("location:new.php");
						}
					}
				}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid		=	$_REQUEST['fid'];   
		$classId	=	$_REQUEST['classId']; 
		 
		if(!$_POST['class'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$success2=0;
				
				$class		=	$App->convert($_REQUEST['class']);				
				$existId=$db->existValuesId(TABLE_FEESETTING,"class='$class' AND ID!='$fid'");
					if($existId>0)
					{
						$_SESSION['msg']="Fee Is Already Exist";					
						header("location:new.php");					
					}
					else
					{				
				
						$data['class']				=	$App->convert($class);
						$data['registrationFee']	=	$App->convert($_REQUEST['registration']);
						$data['admissionFee']		=	$App->convert($_REQUEST['admission']);
						$data['specialFee']			=	$App->convert($_REQUEST['special']);
						$data['tuitionFee']			=	$App->convert($_REQUEST['tution']);
						$data['total']				=	$App->convert($_REQUEST['total']);
						$data['installmentNo']		=	$App->convert($_REQUEST['installmentNo']);
						$data['loginId']			=	$App->convert($uid);				
														
						$success = $db->query_update(TABLE_FEESETTING, $data, "ID='{$fid}'");
						$class=$App->convert($_REQUEST['class']);
						$sql = "DELETE FROM `".TABLE_SETINSTALMENT."` WHERE class='$class'";
						$db->query($sql);
						
						if($App->convert($_REQUEST['installment1'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment1']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment1Date']);
							$data2['loginId']		=	$App->convert($uid);					
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment2'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment2']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment2Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment3'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment3']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment3Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment4'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment4']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment4Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment5'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment5']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment5Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment6'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment6']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment6Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment7'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment7']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment7Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment8'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment8']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment8Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment9'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment9']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment9Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}
						if($App->convert($_REQUEST['installment10'])!='')
						{
							$data2['class']			=	$App->convert($_REQUEST['class']);
							$data2['amount']		=	$App->convert($_REQUEST['installment10']);
							$data2['paymentDate']	=	$App->dbFormat_date($_REQUEST['installment10Date']);
							$data2['loginId']		=	$App->convert($uid);
							$success2 = $db->query_insert(TABLE_SETINSTALMENT, $data2);
						}					
						$db->close();
										
						if(($success) && ($success2))
						{
							$_SESSION['msg']="Fee Details updated successfully";					
							header("location:new.php");		
						}
						else
						{
							$_SESSION['msg']="Failed";					
							header("location:new.php");
						}														
					}
				}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];
				$page	=	$_REQUEST['page'];
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success = 0;
				$success2 = 0;
				
				$vou_sql1 ="SELECT class FROM `".TABLE_FEESETTING."` WHERE ID='{$id}'";
					$vou_record1 = $db->query_first($vou_sql1);
					$class=$vou_record1['class'];
					
				
				/*$sql = "DELETE FROM `".TABLE_FEESETTING."` WHERE ID='{$id}'";
				$success = $db->query($sql);
				
				$sql = "DELETE FROM `".TABLE_SETINSTALMENT."` WHERE class='$class'";
				$success2 = $db->query($sql);*/
				
				try
				{			
				$success= @mysql_query("DELETE FROM `".TABLE_FEESETTING."` WHERE ID='{$id}'");		
				$success2= @mysql_query("DELETE FROM `".TABLE_SETINSTALMENT."` WHERE class='$class'");			      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}	
				
				$db->close(); 				
				
				if(($success) && ($success2))
				{
					$_SESSION['msg']="Fee Details deleted successfully";										
				}
				else
				{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
				}	
					header("location:new.php");						
		break;		
}
?>
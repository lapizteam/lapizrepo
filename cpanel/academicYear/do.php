<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['fromYear'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();											
				$success=0;
				$fromYear=$_REQUEST['fromYear'];	
				$toYear=$_REQUEST['toYear'];
				
				$existId=$db->checkValueExist(TABLE_ACADEMICYEAR,"fromYear='$fromYear' AND toYear='$toYear'");
				if($existId>0)
				{				
				$_SESSION['msg']="Academic Year Is Already Exist!!";					
				header("location:new.php");
				}
				else
				{
				
				$data['fromYear']		=	$App->convert($_REQUEST['fromYear']);
				$data['toYear']			=	$App->convert($_REQUEST['toYear']);
				$data['status']			=	'NO';
										
				$success=$db->query_insert(TABLE_ACADEMICYEAR, $data);								
				$db->close();
					if($success)
					{
					$_SESSION['msg']="Academic Year Added Successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}
				}
			}		
		break;		
		
		case 'status':
					$id	=	$_REQUEST['id'];
					$success=0;				
		
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$select=mysql_query("SELECT * FROM ".TABLE_ACADEMICYEAR." WHERE status='Yes'");
				$row=mysql_fetch_array($select);
				
				$old_id=$row['ID'];
				
				$data['status']		=	'NO';
				
				$db->query_update(TABLE_ACADEMICYEAR, $data, " ID='{$old_id}'");									
				
				$data['status']		=	'YES';
				
				$success=$db->query_update(TABLE_ACADEMICYEAR, $data, " ID='{$id}'");									
				$db->close();
				
				
				if($success)
					{
					$_SESSION['msg']="Current Academic Year Updated Successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}
				
		break;		
		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_REQUEST['fromYear'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$fromYear=$_REQUEST['fromYear'];	
				$toYear=$_REQUEST['toYear'];
				
				$existId=$db->existValuesId(TABLE_ACADEMICYEAR,"fromYear='$fromYear' AND toYear='$toYear' AND ID!='{$fid}'");
				if($existId>0)
				{				
				$_SESSION['msg']="Academic Year Is Already Exist!!";					
				header("location:edit.php?id=$fid");
				}
				else
				{
				
				$data['fromYear']		=	$App->convert($_REQUEST['fromYear']);
				$data['toYear']			=	$App->convert($_REQUEST['toYear']);
				$data['status']			=	'NO';
						
				$success=$db->query_update(TABLE_ACADEMICYEAR, $data ,"ID='{$fid}'");								
				$db->close();
				
				
				if($success)
					{
					$_SESSION['msg']="Academic Year Updated Successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}
				}
			}		
				
		break;			
	// DELETE SECTION
	//-
	case 'delete':		
				$id		=	$_REQUEST['id'];				
				$success=1;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				//$success=$db->query("DELETE FROM `".TABLE_ACADEMICYEAR."` WHERE ID='{$id}'");				
			
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_ACADEMICYEAR."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}																
				$db->close(); 					                
        
        
				if($success)
				{
				     $_SESSION['msg']="Academic Year Deleted Successfully";					
					
				}
				else
				{
				$_SESSION['msg']="You can't edit. Because this data is used some where else";	
								
				}
				header("location:new.php");			
		break;		
}
?>
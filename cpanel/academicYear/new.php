<?php include("../adminHeader.php") ;
$type		=	$_SESSION['LogType'];
$basicSettingEdit	=	$_SESSION['basicSettingEdit'];

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<script>
function nextYear(str)
{
str=parseInt(str)+1;
document.getElementById('toYear').value=str;
}


function status_type()
{
var status=confirm("Do you Want to Edit The Current Academic Year ??");
	if(status==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">ACADEMIC YEAR REGISTRATION</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          
        </div>
		
			<div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table view_limitter pagination_table" >
                <thead>
                  <tr>
                    <th>SlNo</th>
					<th>From Year</th>
					<th>To Year</th>
					<th>CurrentYear</th>
					<th>Status</th>												
                  </tr>
                </thead>
                <tbody>
						<?php 
						$i=1;
						$selAllQuery = "SELECT * FROM ".TABLE_ACADEMICYEAR." ORDER BY ID DESC";
						$selectAll=mysql_query($selAllQuery);
						
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="5">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{						
							$i=1;
							while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
						<td><?php echo $i;$i++;?>						 
						  <div class="adno-dtls"> <?php if($basicSettingEdit || $type=="Admin"){?> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> | <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a><?php }?></div></td>						
						<td><?php echo $row['fromYear']; ?></td>
						<td><?php echo $row['toYear']; ?></td>
						<td><?php echo $row['status']; ?></td>	
						<td><a href="do.php?id=<?php echo $tableId; ?>&op=status"onClick="return status_type()" <?php if($row['status']=='YES'){?> style="color:#006600" <?php }?>>Set As Current</a></td>											
					  </tr>
					  <?php }
					  }
					  ?>                  
                </tbody>
              </table>
			  
            </div>
				<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">ACADEMIC YEAR REGISTRATION</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" >
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="fromYear">From Year:<span class="star">*</span></label>
                     <select name="fromYear" id="fromYear" onchange="nextYear(this.value)" required title="From Year"  class="form-control2">
                    	<option value="">Select</option>
						<?php 
						for($i=2010;$i<2025;$i++)
						{
						?>
                    	<option value="<?php echo $i;?>"><?php echo $i;?></option>
						<?php 
						}
						?>
                    </select>
                  	</div>
				  	<div class="form-group">
                     <label for="toYear">To Year:</label>
                       <input type="text" name="toYear" id="toYear" title="To Year" placeholder="To Year" readonly  class="form-control2">
                  	</div>               
             	</div>
			</div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>

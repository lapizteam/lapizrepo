<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//		
		
		// Printing  ========================================================================
		case 'print':
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');
				
				// Including all required classes for creating barcode
				require_once('barcodeLib/class/BCGFontFile.php');
				require_once('barcodeLib/class/BCGColor.php');
				require_once('barcodeLib/class/BCGDrawing.php');
				require_once('barcodeLib/class/BCGcode39.barcode.php');

				
				/******************************* pdf page setting *************************************/
				
				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				
				$pdf->SetCreator(PDF_CREATOR);
				// set default header data
				$pdf->SetPrintHeader(false);
				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				//$pdf->SetMargins(0,PDF_MARGIN_TOP, 0);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				$pdf->SetFont('times', '', 10, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				/******************************* pdf page setting *************************************/
				
				// Set some content to print
				$fid	=	$_REQUEST['id'];								
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				
				$sql2 = "SELECT bookNo,bookName FROM ".TABLE_BOOK_REG." WHERE ID = '$fid'";
				$bookRes = $db->query_first($sql2);
				$bookName = $bookRes['bookName'];
				$bookNo = $bookRes['bookNo'];
				
				// writing bookname to pdf
				$html = $bookName;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				// Loading Font for writing a label under the barcode. 
				$font = new BCGFontFile('./font/Arial.ttf', 11);
				
				//text to be converted
				$text = $bookNo;
				
				// The arguments are R, G, B for color.
				$color_black = new BCGColor(0, 0, 0);
				$color_white = new BCGColor(255, 255, 255);
				
				$drawException = null;
				try {
					$code = new BCGcode39();
					$code->setScale(2); // Resolution
					$code->setThickness(30); // Thickness
					$code->setForegroundColor($color_black); // Color of bars
					$code->setBackgroundColor($color_white); // Color of spaces
					$code->setFont($font); // Font (or 0)
					$code->parse($text); // Text
				} 
				catch(Exception $exception) {
				$drawException = $exception;
				}
				
				/* Here is the list of the arguments
				1 - Filename (empty : display on screen)
				2 - Background color */
				$drawing = new BCGDrawing('', $color_white);
				
				//saving generated barcode to a folder 'barcode'
				$drawing->setFilename('barcode/'.$bookNo.'barcode.png');
				
				if($drawException) 
				{
					$drawing->drawException($drawException);
				} 
				else 
				{
					$drawing->setBarcode($code);
					$drawing->draw();
				}
				header('Content-Type: image/png');
				header('Content-Disposition: inline; filename="barcode.png"');
				
				// Draw (or save) the image into PNG format.
				$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);

				$html ='barcode/'.$bookNo.'barcode.png';
				$pdf->Image($html, 16, 20, 27,13, 'PNG');
				//$pdf->Image($html);
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'bookReport'.$bookNo.pdf", 'I');
				
				
				
				
				
				
					
				
		break;
		
}

?>
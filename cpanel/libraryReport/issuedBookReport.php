<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function fnExcelReport()
    {
	
	
             var tab_text="<table border='2px'><tr> <td colspan='10' style='text-align:center; font-size:11px;'><h3> BOOK ISSUE REPORT</h3></td> </tr>";
             var textRange; var j=0;
          tab = document.getElementById('headerTable'); // id of table1	
		   // id of table1	
		  
try
{
	tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>SL No</td><td bgcolor='#87AFC6'>Admission No</td><td bgcolor='#87AFC6'>Student Name</td><td bgcolor='#87AFC6'>Class</td><td bgcolor='#87AFC6'>Division</td><td bgcolor='#87AFC6'>Book Name</td><td bgcolor='#87AFC6'>Book Number</td><td bgcolor='#87AFC6'>Return Date</td><td bgcolor='#87AFC6'>Delay</td><td bgcolor='#87AFC6'>Issued Date</td></tr>";

x=0;
          for(j = 1 ; j < tab.rows.length ; j=j+1) 
          {     
			
			  tab_text = tab_text + "<tr>";
			 dd=0;
		  	 for(k = 0; k < tab.rows[j].cells.length -1; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab.rows[j].cells[k].innerHTML+"</td>";
						dd=1;
						
			 }
			 
			 if(dd==1)
			 {
			 	x++;
		 		tabID="headerTable2"+(x+1);			 		
			 	tab2 = document.getElementById(tabID);
				tab_text=tab_text+"<td>"+tab2.rows[6].cells[2].innerHTML+"</td>";
								
				
			}
			tab_text = tab_text + "</tr>";
			
          }

	  
}
catch(err)
{
 alert(err);
}
          tab_text=tab_text+"</table>";
		  tab_text= tab_text.replace("<tr></tr>", "");
		  tab_text= tab_text.replace("<tr>  </tr>", "");
	
          tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
          tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
                      tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

               var ua = window.navigator.userAgent;
              var msie = ua.indexOf("MSIE "); 

                 if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                    {
                           txtArea1.document.open("txt/html","replace");
                           txtArea1.document.write(tab_text);
                           txtArea1.document.close();
                           txtArea1.focus(); 
                           sa=txtArea1.document.execCommand("SaveAs",true,"");
                     }  
                  else                 //other browser not tested on IE 11
                      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  


                      return (sa);

	
	}

</script>

<script>
//validation msg
function valid()
{
flag=false;

jadNo = document.getElementById('adNo').value;
jbookName = document.getElementById('bookName').value;
jbookNo = document.getElementById('bookNo').value;
	
	if((jadNo == "") && (jbookName == "") && (jbookNo == ""))
	{		
	document.getElementById('searchdiv').innerHTML="At least one field must be filled.";
	flag=true;
	}
		
if(flag==true)
	{
	return false;
	}
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>

      <div class="col-md-10 col-sm-8 rightarea">
        <div class="student-report">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>BOOK ISSUE REPORT</h2>
            </div>
            <div class="col-sm-8 text-right">
              <form class="form-inline form3" method="post" >
         
                <div class="form-group">
				
                  <input type="text" class="form-control .namebox" value="<?php echo @$_REQUEST['searchAdNo'];?>" id="searchAdNo" name="searchAdNo" placeholder="Admission Number" onfocus="clearbox('searchdiv')" >
				  </div>
				  <div class="form-group">			
				<input id="bookNo" name="searchBookNo" value="<?php echo @$_REQUEST['searchBookNo'];?>" type="text" class="form-control .namebox" placeholder="Book Number" onfocus="return clearbox('searchdiv')" /> 
                </div>
                <div class="form-group">
			
				<input id="bookName" name="searchBookName" value="<?php echo @$_REQUEST['searchBookName'];?>" type="text" class="form-control .namebox" placeholder="Book Name" onfocus="return clearbox('searchdiv')"/>
                </div>
                	
				 <div class="form-group">
                 </br>
				<input id="bookDate" name="searchBookDate" value="<?php echo @$_REQUEST['searchBookDate'];?>" type="text" class="form-control datepicker" placeholder="Return Date" onfocus="return clearbox('searchdiv')"/>
                </div>
                 
               <div class="form-group">
					</br>
                   <input type="submit" name="submit" value="" class="btn btn-default lens" title="Search" onsubmit="return valid()"/>
				   <button onclick="fnExcelReport();"  class="btn btn-default export" title="Export To Excel"></button>
                </div>	
				<div id="searchdiv" class="valid" style="color:#FF6600;margin-left:-358px;"></div>			 
              </form>
            </div>
          </div>
<?php	
$cond="1";
if(@$_REQUEST['searchAdNo'])
{	
$cond=$cond." and ".TABLE_BOOK_ISSUE.".adNo='".$_POST['searchAdNo']."'";
}
if(@$_REQUEST['searchBookName'])
{
$cond=$cond." and ".TABLE_BOOK_REG.".bookName like'%".$_POST['searchBookName']."%'";
}
if(@$_REQUEST['searchBookNo'])
{
$cond=$cond." and ".TABLE_BOOK_REG.".bookNo='".$_POST['searchBookNo']."'";
}
if(@$_REQUEST['searchBookDate'])
{
$cond=$cond." and ".TABLE_BOOK_ISSUE.".returnDate <= '".$App->dbFormat_date($_REQUEST['searchBookDate'])."'";
}


?>
          <div class="row">
            <div class="col-sm-12">
              <div class="tablearea3 table-responsive">
                <table class="table view_limitter pagination_table" id="headerTable" >
                  <thead>
                    <tr>
                      	<td>SLNO</td>
					  	<td>AdNo</td>
						<td>Student Name</td>
						<td>Class</td>
						<td>Division</td>						
                        <td>Book Name</td>
						<td>Book Number</td>
						<td>Return Date</td>
						<td>Delay</td>
						<td>View</td>
                    </tr>
                  </thead>
				  
                  <tbody>
				  <?php					
					$selAllQuery = "SELECT ".TABLE_BOOK_ISSUE.".adNo,".TABLE_STUDENT.".name,".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_BOOK_ISSUE.".ID,".TABLE_BOOK_ISSUE.".bookId,".TABLE_BOOK_REG.".bookNo,".TABLE_BOOK_REG.".bookName,".TABLE_BOOK_ISSUE.".issueDate,".TABLE_BOOK_ISSUE.".returnDate FROM ".TABLE_STUDENT.",".TABLE_CLASS.",".TABLE_DIVISION.",".TABLE_BOOK_ISSUE.",".TABLE_BOOK_REG.",".TABLE_RETURN_DAYS." WHERE ".TABLE_BOOK_ISSUE.".adNo=".TABLE_STUDENT.".adNo and ".TABLE_STUDENT.".class=".TABLE_CLASS.".ID and ".TABLE_STUDENT.".division=".TABLE_DIVISION.".ID and ".TABLE_BOOK_ISSUE.".bookId=".TABLE_BOOK_REG.".ID and ".TABLE_BOOK_ISSUE.".status='pending' and $cond ORDER BY ID desc ";
					//echo $selAllQuery;
					$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="10">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
					
					while($row=mysql_fetch_array($selectAll))
					{
					$tableId=$row['ID'];
					
					$day=date("Y-m-d");						 		
					$returnDate=$row['returnDate'];					
					$today1=date_create($day);
					$retday1=date_create($returnDate);						
					$diff=date_diff($retday1,$today1);									
					$delay=$diff->format("%R%a");	
					$delayDays=$diff->format("%a days");
					?>
					<tr>
						<td><?php echo $i;$i++; ?></td>
						<td><?php echo $row['adNo']; ?> </td>
						<td><?php echo $row['name']; ?> </td>
						<td><?php echo $row['class']; ?> </td>
						<td><?php echo $row['division']?></td>
						<td><?php echo $row['bookName']; ?> </td>	
						<td><?php echo $row['bookNo']; ?> </td>					
						<td><?php echo $App->dbformat_date_db($row['returnDate']); ?> </td>
						<td><?php if($delay>0){ echo $delayDays;} else echo '0'." days"; ?> </td>	
						
						<td><a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>" class="viewbtn">View</a>
						<!-- Modal3 -->
						  <div class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
							<div class="modal-dialog">
							  <div class="modal-content"> 										
								<div role="tabpanel" class="tabarea2"> 
								  
								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">									 
										<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">BOOK ISSUED DETAILS</a> </li>	
									</ul>
								  
								  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
										  <table class="table nobg" id="headerTable2<?php echo $i; ?>">
											<tbody style="background-color:#FFFFFF">
											  <tr> 
												  <td >Admission Number</td>
												  <td>:</td>
												  <td><?php  echo $row['adNo']; ?></td>                    	  
											  </tr>
											  
											  <tr> 
												  <td >Name</td>
												  <td>:</td>
												  <td><?php  echo $row['name']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Class</td>
												  <td>:</td>
												  <td><?php  echo $row['class'];  ?></td>                    	  
											  </tr>
											  
											  <tr> 
												  <td >Division</td>
												  <td>:</td>
												  <td><?php  echo $row['division']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Book Number</td>
												  <td>:</td>
												  <td><?php  echo $row['bookNo']; ?></td>                    	  
											  </tr>
											  
											  <tr> 
												  <td >Book Name</td>
												  <td>:</td>
												  <td><?php  echo $row['bookName'];  ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Issued Date</td>
												  <td>:</td>
												  <td><?php  echo $App->dbFormat_date_db($row['issueDate']); ?></td>                    	  
											  </tr>
											  <tr> 
												  <td >Return Date</td>
												  <td>:</td>
												  <td><?php  echo $App->dbFormat_date_db($row['returnDate']); ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Delay Days</td>
												  <td>:</td>
												  <td><?php if($delay>0){ echo $delayDays;} else echo '0'." days"; ?></td>                    	  
											  </tr>	  
											</tbody>
										  </table>
										</div>
									  </div>
								</div>
							  </div>
							</div>
						  </div>
						  <!-- Modal3 cls --> 
						
						
						
						
						</td>
					 </tr>
					<?php
					}
			}
					?>
                  </tbody>
                </table>
				
              </div>
			  	<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->

            </div>
          </div>
        </div>
      </div>
      
     
      
      
      
  <?php include("../adminFooter.php") ?>
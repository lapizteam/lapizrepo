<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//		
		
		// Printing  ========================================================================
		case 'print':
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');
				//require_once('../../printing/tcpdf_barcodes_1d.php');// include 1D barcode class (search for installation path)
				

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				
				// set default header data
				//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
				$fid	=	$_REQUEST['id'];								
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				
				
				$sql2 = "SELECT bookNo,bookName FROM ".TABLE_BOOK_REG." WHERE ID = '$fid'";
				$bookRes = $db->query_first($sql2);
				$bookName = $bookRes['bookName'];
				$bookNo = $bookRes['bookNo'];
				
				// include 1D barcode class (search for installation path)
				
				
				// set the barcode content and type
				//$barcodeobj = new TCPDFBarcode('4554', 'C128');
				
				// output the barcode as HTML object
				//echo $barcodeobj->getBarcodeHTML(1, 40, 'black');
				//$barcodeimg = $barcodeobj->getBarcodeHTML(1, 40, 'black');
				
								
				
				$html = $bookName;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				//$html=$barcodeobj->setBarcode('$bookNo', 'C128');
				//$html = $barcodeobj->getBarcodePngData(1, 40, array(0,0,0));
				//move_uploaded_file($html,'barcode/'.$html);
				//$html = $barcodeobj->getBarcodeSVG(1, 40, 'black');
				//$html = $barcodeobj->getBarcodePng(1, 40, array(0,0,0));
				//$html = $barcodeobj->getBarcodeHTML(1, 40, 'black');
				
				
				// Including all required classes
				require_once('class/BCGFontFile.php');
				require_once('class/BCGColor.php');
				require_once('class/BCGDrawing.php');
				require_once('class/BCGcode39.barcode.php');
				// Loading Font
				$font = new BCGFontFile('./font/Arial.ttf', 10);
				
				// Don't forget to sanitize user inputs
				//$text = isset($_GET['text']) ? $_GET['text'] : $fid;
				$text = $bookNo;
				// The arguments are R, G, B for color.
				$color_black = new BCGColor(0, 0, 0);
				$color_white = new BCGColor(255, 255, 255);
				
				$drawException = null;
				try {
					$code = new BCGcode39();
					$code->setScale(2); // Resolution
					$code->setThickness(20); // Thickness
					$code->setForegroundColor($color_black); // Color of bars
					$code->setBackgroundColor($color_white); // Color of spaces
					$code->setFont($font); // Font (or 0)
					$code->parse($text); // Text
				} catch(Exception $exception) {
					$drawException = $exception;
				}
				
				/* Here is the list of the arguments
				1 - Filename (empty : display on screen)
				2 - Background color */
				$drawing = new BCGDrawing('', $color_white);
				//$drawing = new BCGDrawing('../barcode/barcode.png', $color_white);
				$drawing->setFilename('barcode/'.$bookNo.'barcode.png');
				
				if($drawException) {
					$drawing->drawException($drawException);
				} else {
					$drawing->setBarcode($code);
					$drawing->draw();
				}
				header('Content-Type: image/png');
				header('Content-Disposition: inline; filename="barcode.png"');
				//move_uploaded_file("barcode.png","/barcode/barcode.png");
				// Draw (or save) the image into PNG format.
				
				$drawing->finish(BCGDrawing::IMG_FORMAT_PNG);

				//move_uploaded_file($html,'barcode/'.$html);
				$html ='barcode/'.$bookNo.'barcode.png';
				//$pdf->Image('2barcode.png', 0, 0, 210, 297, 'PNG');
				$pdf->Image($html);
				//$pdf->Image($html, 50, 140, 100, '', '', '', '', false, 300, '', true);
				//$pdf->WriteHTML($barcodeobj->barcode_codabar('312312'),true,0,true,true);
				
				//$pdf->write1DBarcode($html,'C128','','',90,10,0.4,'','N');	
				//$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				/*$html = $bookNo;						
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);*/	
												
				//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'bookReport'.$bookNo.pdf", 'I');
				
				
				
				
				
				
					
				
		break;
		
}

?>
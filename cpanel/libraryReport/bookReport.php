<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function fnExcelReport()
    {
	
	
             var tab_text="<table border='2px'><tr> <td colspan='11' style='text-align:center; font-size:11px;'><h3>BOOK REPORT</h3></td> </tr>";
             var textRange; var j=0;
          tab = document.getElementById('headerTable'); // id of table1	
		   // id of table1	
		  
try
{
	tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>SL No</td><td bgcolor='#87AFC6'>Book No</td><td bgcolor='#87AFC6'>Book Name</td><td bgcolor='#87AFC6'>Author</td><td bgcolor='#87AFC6'>Category</td><td bgcolor='#87AFC6'>Rack</td><td bgcolor='#87AFC6'>Book Register Date</td><td bgcolor='#87AFC6'>Publisher</td><td bgcolor='#87AFC6'>Price</td><td bgcolor='#87AFC6'>Availability</td><td bgcolor='#87AFC6'>Remark</td></tr>";

x=0;
          for(j = 1 ; j < tab.rows.length ; j=j+1) 
          {     
			
			  tab_text = tab_text + "<tr>";
			 dd=0;
		  	 for(k = 0; k < tab.rows[j].cells.length -1; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab.rows[j].cells[k].innerHTML+"</td>";
						dd=1;
						
			 }
			 
			 if(dd==1)
			 {
			 	x++;
		 		tabID="headerTable2"+(x+1);			 		
			 	tab2 = document.getElementById(tabID);
				tab_text=tab_text+"<td>"+tab2.rows[0].cells[2].innerHTML+"</td><td>"+tab2.rows[4].cells[2].innerHTML+"</td>"+"<td>"+tab2.rows[6].cells[2].innerHTML+"</td><td>"+tab2.rows[8].cells[2].innerHTML+"</td><td>"+tab2.rows[9].cells[2].innerHTML+"</td>";
								
				
			}
			tab_text = tab_text + "</tr>";
			
          }

	  
}
catch(err)
{
 alert(err);
}
          tab_text=tab_text+"</table>";
		  tab_text= tab_text.replace("<tr></tr>", "");
		  tab_text= tab_text.replace("<tr>  </tr>", "");
	
          tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
          tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
                      tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

               var ua = window.navigator.userAgent;
              var msie = ua.indexOf("MSIE "); 

                 if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                    {
                           txtArea1.document.open("txt/html","replace");
                           txtArea1.document.write(tab_text);
                           txtArea1.document.close();
                           txtArea1.focus(); 
                           sa=txtArea1.document.execCommand("SaveAs",true,"");
                     }  
                  else                 //other browser not tested on IE 11
                      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  


                      return (sa);

	
	}

</script>

<script>
//validation msg
function valid()
{
flag=false;

jbookNo = document.getElementById('bookNo').value;
jbookName = document.getElementById('bookName').value;

	if(jbookNo == "" && jbookName == "")
	{		
	document.getElementById('searchdiv').innerHTML="At least one field must be filled.";
	flag=true;
	}
		
if(flag==true)
	{
	return false;
	}
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>



      <div class="col-md-10 col-sm-8 rightarea">
        <div class="student-report">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>BOOK REPORT</h2>
            </div>
            <div class="col-sm-8 text-right">
              <form class="form-inline form3" method="post" >
                
                <div class="form-group">
				</br>
				<input id="bookNo" name="searchBookNo" value="<?php echo @$_POST['searchBookNo'] ?>" type="text" class="form-control .namebox" placeholder="Book Number" onfocus="return clearbox('searchdiv')" /> 
                </div>
                <div class="form-group">
				</br> 
				<input id="bookName" name="searchBookName" value="<?php echo @$_POST['searchBookName'] ?>" type="text" class="form-control .namebox" placeholder="Book Name" onfocus="return clearbox('searchdiv')"/>
                </div>
				 <div class="form-group">
				</br> 
				<input id="author" name="searchAuthor" value="<?php echo @$_POST['searchAuthor'] ?>" type="text" class="form-control .namebox" placeholder="Author" onfocus="return clearbox('searchdiv')"/>
                </div>
                <div class="form-group">
				</br> 
				<input id="category" name="searchCategory" value="<?php echo @$_POST['searchCategory'] ?>" type="text" class="form-control .namebox" placeholder="Category" onfocus="return clearbox('searchdiv')"/>
                </div>
               
               <div class="form-group">
					</br>
                   <input type="submit" name="submit" value=""class="btn btn-default lens" title="Search" onsubmit="return valid()"/>
				   <button onclick="fnExcelReport();"  class="btn btn-default export" title="Export To Excel"></button>
				   
                </div>	
				<br />
				<div id="searchdiv" class="valid" style="color:#FF6600;margin-left:-199px;"></div>				 
              </form>
            </div>
          </div>
<?php	

$cond="1";
if(@$_REQUEST['searchBookNo'])
{
$cond=$cond." and ".TABLE_BOOK_REG.".bookNo like'%".$_POST['searchBookNo']."%'";
}
if(@$_REQUEST['searchBookName'])
{
$cond=$cond." and ".TABLE_BOOK_REG.".bookName like'%".$_POST['searchBookName']."%'";
}
if(@$_REQUEST['searchAuthor'])
{
$cond=$cond." and ".TABLE_BOOK_REG.".author like'%".$_POST['searchAuthor']."%'";
}
if(@$_REQUEST['searchCategory'])
{
$cond=$cond." and ".TABLE_BOOK_CATEGORY.".category like'%".$_POST['searchCategory']."%'";
}



?>
   
          <div class="row">
            <div class="col-sm-12">
              <div class="tablearea3 table-responsive">
                <table class="table view_limitter pagination_table" id="headerTable" >
                  <thead>
                    <tr>
                      <td>SLNO</td>
					  	<td>Book No</td>
						<td>Book Name</td>
						<td>Author</td>
						<td>Category</td>
						<td>Rack</td>						
                        <td>View</td>
                    </tr>
                  </thead>
				  
                  <tbody>
				  <?php					
					$selAllQuery="SELECT  ".TABLE_BOOK_REG.".ID,".TABLE_BOOK_REG.".regDate,".TABLE_BOOK_REG.".bookNo,".TABLE_BOOK_REG.".bookName,".TABLE_BOOK_REG.".author,".TABLE_BOOK_REG.".publisher,".TABLE_BOOK_CATEGORY.".category,".TABLE_BOOK_REG.".price,".TABLE_RACK_REG.".rackNo,".TABLE_BOOK_REG.".remark,".TABLE_BOOK_REG.".availability  FROM ".TABLE_BOOK_REG.",".TABLE_BOOK_CATEGORY.",".TABLE_RACK_REG." WHERE ".TABLE_BOOK_CATEGORY.".ID=".TABLE_BOOK_REG.".category and ".TABLE_RACK_REG.".ID=".TABLE_BOOK_REG.".rack AND $cond ORDER BY ID desc";
					//echo $selAllQuery;
					$selectAll= $db->query($selAllQuery);
					$number=mysql_num_rows($selectAll);
					
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="7">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
					while($row=mysql_fetch_array($selectAll))
					{
					$tableId=$row['ID'];
					
					?>
					<tr>
						<td><?php echo $i;$i++; ?>
                        <div class="adno-dtls"> <a target="_blank" href="do.php?id=<?php echo $tableId; ?>&op=print"  >PRINT BARCODE</a></div>
                        </td>
						<td><?php echo $row['bookNo']; ?></td>
						<td><?php echo $row['bookName']; ?></td>						
						<td><?php echo $row['author']; ?> </td>
                   	 	<td><?php echo $row['category']; ?> </td>
						<td><?php echo $row['rackNo']; ?> </td>
						
						<td><a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>" class="viewbtn">View</a>
						<!-- Modal3 -->
						  <div class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
							<div class="modal-dialog">
							  <div class="modal-content"> 										
								<div role="tabpanel" class="tabarea2"> 
								  
								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">									 
										<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Book Details</a> </li>	
										 									
									</ul>
								  
								  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
										  <table class="table nobg" id="headerTable2<?php echo $i; ?>">
											<tbody style="background-color:#FFFFFF">
											  <tr> 
												  <td >Book Register Date</td>
												  <td>:</td>
												  <td><?php  echo $App->dbFormat_date_db($row['regDate']); ?></td>                    	  
											  </tr>
											  
											  <tr> 
												  <td >Book No</td>
												   <td>:</td>
												  <td><?php  echo $row['bookNo']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Book Name</td>
												   <td>:</td>
												  <td><?php  echo $row['bookName'];  ?></td>                    	  
											  </tr>
											  
											  <tr> 
												  <td >Author</td>
												   <td>:</td>
												  <td><?php  echo $row['author']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Publisher</td>
												   <td>:</td>
												  <td><?php  echo $row['publisher']; ?></td>                    	  
											  </tr>
											  
											  <tr> 
												  <td >Category</td>
												   <td>:</td>
												  <td><?php  echo $row['category'];  ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Price</td>
												   <td>:</td>
												  <td><?php  echo $row['price']; ?></td>                    	  
											  </tr>
											  <tr> 
												  <td >Rack Number</td>
												   <td>:</td>
												  <td><?php  echo $row['rackNo']; ?></td>                    	  
											  </tr>
											  <tr> 
												  <td >Availability</td>
												   <td>:</td>
												  <td><?php  echo $row['availability']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Remark</td>
												   <td>:</td>
												  <td><?php  echo $row['remark']; ?></td>                    	  
											  </tr>	  
											</tbody>
										  </table>
										</div>
										
									  </div>
								</div>
							  </div>
							</div>
						  </div>
						  <!-- Modal3 cls --> 
						
						
						
						
						</td>
					 </tr>
					<?php
					}
			}
					?>
                  </tbody>
                </table>
				
              </div>
			  	<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->

            </div>
          </div>
        </div>
      </div>
      
     
      
      
      
  <?php include("../adminFooter.php") ?>
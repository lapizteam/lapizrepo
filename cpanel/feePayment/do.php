<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['class'])
			{					
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$data['payDate']				=	$App->dbFormat_date($_REQUEST['payDate']);
				$data['voucherNo']				=	$App->convert($_REQUEST['voucher']);
				$data['admNo']					=	$App->convert($_REQUEST['adNo']);
				$data['class']					=	$App->convert($_REQUEST['classHidden']);
				$data['division']				=	$App->convert($_REQUEST['divisionHidden']);											
				$data['regFee']					=	$App->convert($_REQUEST['registration']);
				$data['admFee']					=	$App->convert($_REQUEST['admission']);
				$data['specialFee']				=	$App->convert($_REQUEST['special']);
				$data['tuitionFee']				=	$App->convert($_REQUEST['tution']);	
				$data['payingAmount']			=	$App->convert($_REQUEST['payingAmount']);	
				$data['loginId']				=	$App->convert($uid);													
												
				$success=$db->query_insert(TABLE_FEEPAYMENT, $data);
				
				$vou_sql2 ="SELECT ID FROM `".TABLE_LEDGER."` WHERE `name`='Fee Payment' ";
				$vou_record2 = $db->query_first($vou_sql2);
				$Fee=$vou_record2['ID'];
				
//				$vou_sql ="SELECT MAX(voucher_no) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Fee Payments' ";
				$vou_record = $App->convert($_REQUEST['voucher']);
				/* ------------------------------------------ */																
				/* ------------------------------------------ */
				$tsdata['from_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tsdata['to_ledger'] 		=  	$Fee;
				$tsdata['voucher_no']		= 	$vou_record;
				$tsdata['voucher_type']		=  	'Fee Payments';
				$tsdata['credit']			= 	$App->convert($_POST['payingAmount']);
				$tsdata['debit']			=   0;
				$tsdata['cdate']			=	"NOW()";
				$tsdata['transaction_date']	=	$App->dbFormat_date($_POST['payDate']);
				$tsdata['remark']			=  	'Admission No: '.$App->convert($_REQUEST['adNo']);
				$tsdata['loginId']			=	$_SESSION['LogID'];
				$tsdata['uid']				=	$_SESSION['LogID'];

				$db->query_insert(TABLE_TRANSACTION, $tsdata);
				//echo TABLE_TRANSACTION, $tsdata;die;
				/* ------------------------------------------ */
				/* ------------------------------------------ */
				$tfdata['from_ledger'] 		=  	$Fee;
				$tfdata['to_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tfdata['voucher_no']		= 	 $vou_record;
				$tfdata['voucher_type']		=  	'Fee Payments';
				$tfdata['credit']			=   0;
				$tfdata['debit']			= 	$App->convert($_POST['payingAmount']);
				$tfdata['cdate']			=	"NOW()";
				$tfdata['transaction_date']	=	$App->dbFormat_date($_POST['payDate']);
				$tfdata['remark']			=  	'Admission No: '.$App->convert($_REQUEST['adNo']);
				$tfdata['loginId']			=	$_SESSION['LogID'];
				$tfdata['uid']				=	$_SESSION['LogID'];
			
				$db->query_insert(TABLE_TRANSACTION, $tfdata);	
				//echo TABLE_TRANSACTION, $tsdata;die;																			
				$db->close();
								
				
				if($success)
					{
					$_SESSION['msg']="Fee Paid successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}	
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid		=	$_REQUEST['fid'];   
		 
		if(!$_POST['class'])
			{			
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				
				$data['payDate']				=	$App->dbFormat_date($_REQUEST['payDate']);
				$data['voucherNo']				=	$App->convert($_REQUEST['voucher']);
				$data['admNo']					=	$App->convert($_REQUEST['adNo']);
				$data['class']					=	$App->convert($_REQUEST['classHidden']);
				$data['division']				=	$App->convert($_REQUEST['divisionHidden']);											
				$data['regFee']					=	$App->convert($_REQUEST['registration']);
				$data['admFee']					=	$App->convert($_REQUEST['admission']);
				$data['specialFee']				=	$App->convert($_REQUEST['special']);
				$data['tuitionFee']				=	$App->convert($_REQUEST['tution']);	
				$data['payingAmount']			=	$App->convert($_REQUEST['payingAmount']);
				$data['loginId']				=	$App->convert($uid);
				
				$success=$db->query_update(TABLE_FEEPAYMENT, $data,"ID='{$fid}'");
				
				$sql2 = "SELECT voucherNo FROM `".TABLE_FEEPAYMENT."` WHERE ID='$fid'";
				$vou_record = 	$db->query_first($sql2);
				$voucher_no = 	$vou_record['voucherNo'];
				
				$vou_sql2 ="SELECT ID FROM `".TABLE_LEDGER."` WHERE `name`='Fee Payment' ";
				$vou_record2 = $db->query_first($vou_sql2);
				$Fee=$vou_record2['ID'];
				
				$vou_sql	="DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_no`='$voucher_no' AND `voucher_type`='Fee Payments'";
				$db->query($vou_sql);

//				$vou_sql ="SELECT MAX(voucher_no) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Fee Payments' ";
				//$vou_record = $App->convert($_REQUEST['voucherNo']);
				/* ------------------------------------------ */																
				/* ------------------------------------------ */
				$tsdata['from_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tsdata['to_ledger'] 		=  	$Fee;
				$tsdata['voucher_no']		= 	$voucher_no;
				$tsdata['voucher_type']		=  	'Fee Payments';
				$tsdata['credit']			= 	$App->convert($_POST['payingAmount']);
				$tsdata['debit']			=   0;
				$tsdata['cdate']			=	"NOW()";
				$tsdata['transaction_date']	=	$App->dbFormat_date($_POST['payDate']);
				$tsdata['remark']			=  	'Admission No: '.$App->convert($_REQUEST['adNo']);
				$tsdata['uid']				=	$_SESSION['LogID'];
				$tsdata['loginId']			=	$_SESSION['LogID'];

				$db->query_insert(TABLE_TRANSACTION, $tsdata);
				//echo TABLE_TRANSACTION, $tsdata;die;
				/* ------------------------------------------ */
				/* ------------------------------------------ */
				$tfdata['from_ledger'] 		=  	$Fee;
				$tfdata['to_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tfdata['voucher_no']		= 	 $voucher_no;
				$tfdata['voucher_type']		=  	'Fee Payments';
				$tfdata['credit']			=   0;
				$tfdata['debit']			= 	$App->convert($_POST['payingAmount']);
				$tfdata['cdate']			=	"NOW()";
				$tfdata['transaction_date']	=	$App->dbFormat_date($_POST['payDate']);
				$tfdata['remark']			=  	'Admission No: '.$App->convert($_REQUEST['adNo']);
				$tfdata['uid']				=	$_SESSION['LogID'];
				$tfdata['loginId']			=	$_SESSION['LogID'];
			
				$db->query_insert(TABLE_TRANSACTION, $tfdata);
				//echo TABLE_TRANSACTION, $tfdata;die;			
				$db->close();				
				
				if($success)
					{
					$_SESSION['msg']="Fee Payment Details updated successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}														
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();								
				
				$sql2 = "SELECT voucherNo FROM `".TABLE_FEEPAYMENT."` WHERE ID='$id'";
				$vou_record = 	$db->query_first($sql2);
				$voucher_no = 	$vou_record['voucherNo'];
				
				/*$vou_sql	="DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_no`='$voucher_no' AND `voucher_type`='Fee Payments'";
				$db->query($vou_sql);
				
				$sql = "DELETE FROM `".TABLE_FEEPAYMENT."` WHERE ID='$id'";
				$success=$db->query($sql);*/
				try
				{
				$success1= @mysql_query("DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_no`='$voucher_no' AND `voucher_type`='Fee Payments'");	
				$success2= @mysql_query("DELETE FROM `".TABLE_FEEPAYMENT."` WHERE ID='$id'");		      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}
				$db->close(); 									
				
				if($success1 && $success2)
					{
					$_SESSION['msg']="Fee Details deleted successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";
					header("location:new.php");					
					}					
		break;		
		
		// Printing  ========================================================================
		case 'print':
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				
				// set default header data
				$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
				$fid	=	$_REQUEST['id'];								
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				$sql2 = "SELECT ".TABLE_FEEPAYMENT.".ID,".TABLE_FEEPAYMENT.".payDate,".TABLE_FEEPAYMENT.".voucherNo,".TABLE_FEEPAYMENT.".admNo,".TABLE_FEEPAYMENT.".regFee,".TABLE_FEEPAYMENT.".admFee,".TABLE_FEEPAYMENT.".specialFee,".TABLE_FEEPAYMENT.".tuitionFee,".TABLE_FEEPAYMENT.".payingAmount,".TABLE_FEESETTING.".total,".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_STUDENT.".name FROM `".TABLE_FEEPAYMENT."`,`".TABLE_FEESETTING."`,`".TABLE_STUDENT."`,`".TABLE_CLASS."`,`".TABLE_DIVISION."` WHERE ".TABLE_FEEPAYMENT.".class=".TABLE_CLASS.".ID and ".TABLE_STUDENT.".adNo=".TABLE_FEEPAYMENT.".admNo and ".TABLE_FEEPAYMENT.".class=".TABLE_FEESETTING.".class and ".TABLE_DIVISION.".ID=".TABLE_FEEPAYMENT.".division and ".TABLE_FEEPAYMENT.".ID='$fid' AND ".TABLE_STUDENT.".studentTcStatus='no'";
				$vou_record 	= 	$db->query_first($sql2);
								
				$payDate 		= 	$vou_record['payDate'];
				$voucher_no 	= 	$vou_record['voucherNo'];
				$admNo 			= 	$vou_record['admNo'];
				$class 			= 	$vou_record['class'];
				$division 		= 	$vou_record['division'];
				$total 			= 	$vou_record['payingAmount'];
				$name 			= 	$vou_record['name'];
				$regFee 		= 	$vou_record['regFee'];
				$admFee 		= 	$vou_record['admFee'];
				$specialFee 	= 	$vou_record['specialFee'];
				$tuitionFee 	= 	$vou_record['tuitionFee'];
								
				
				$html = 'Date        :'.$App->dbFormat_date_db($payDate);		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				$html = 'Voucher No  :'.$voucher_no;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
							
				$html = 'Name        :'.$name.'     Adm No  :'.$admNo;						
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
												
				$html = 'Class       :'.$class.' - '.$division;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);				
				$pdf->Write(0, '', '', 0, '', true, 0, false, false, 0);
								
				if($regFee!=0)
				{
				$html = 'Registration Fee :'.$regFee;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				}
				if($admFee!=0)
				{
				$html = 'Admission Fee :'.$admFee;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				}
				if($specialFee!=0)
				{
				$html = 'Special Fee :'.$specialFee;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				}
				if($tuitionFee!=0)
				{
				$html = 'Tuition Fee :'.$tuitionFee;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				}
				$pdf->Write(0, '', '', 0, '', true, 0, false, false, 0);
				
				if($total!=0)
				{
				$html = 'Paid Amount :'.$total;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				}
				else
				{
				$html = 'No Fee Paid';		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				}
				
				//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'FeePayment'.$admNo.pdf", 'I');		
		break;
		
}

?>
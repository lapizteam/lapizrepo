<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['subject'])
			{					
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$subject=ucfirst($_REQUEST['subject']);
				$flag=0;
				
				$res=mysql_query("SELECT * FROM ".TABLE_SUBJECT."");
				while($row=mysql_fetch_array($res))
				{
					$subjectInTable=$row['subjectName'];
					if(strcasecmp($subject,$subjectInTable)==0)
					{
					$flag=1;
					break;
					}
				}
				
				if($flag==1)
				{				
				$_SESSION['msg']="Subject Is Already Exist!";					
				header("location:new.php");	
				}
				
				else
				{				
				$data['subjectName']		=	$App->convert($subject);			
							
				$success=$db->query_insert(TABLE_SUBJECT, $data);								
				$db->close();
								
				if($success)
					{
					$_SESSION['msg']="Subject Details added successfully";					
					header("location:new.php");
					}
				else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}
				}
				
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['subject'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$subject=ucfirst($_REQUEST['subject']);
				$flag=0;
				
				$res=mysql_query("SELECT * FROM ".TABLE_SUBJECT." WHERE ID!='$fid'");
				while($row=mysql_fetch_array($res))
				{
					$subjectInTable=$row['subjectName'];
					if(strcasecmp($subject,$subjectInTable)==0)
					{
					$flag=1;
					break;
					}
				}
				
				if($flag==1)
				{				
				$_SESSION['msg']="Subject Is Already Exist!";					
				header("location:edit.php?id=$fid");
				}
				
				else
				{				
				$data['subjectName']		=	$App->convert($subject);			
							
				$success=$db->query_update(TABLE_SUBJECT, $data,"ID='{$fid}'");								
				$db->close();
								
				if($success)
					{
					$_SESSION['msg']="Subject Details Updated successfully";					
					header("location:new.php");
					}
				else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}
				}
				
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];
				$success	=	0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				try
				{			
				$success= @mysql_query("DELETE FROM `".TABLE_SUBJECT."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}	
				//$success=$db->query("DELETE FROM `".TABLE_SUBJECT."` WHERE ID='{$id}'");								
				$db->close(); 	
				if($success)
				{
				$_SESSION['msg']="Subject Details deleted successfully";									
				}
				else
				{
				$_SESSION['msg']="Can't delete. Because the data is used some where else";									
				}	
				header("location:new.php");		
		break;		
}
?>
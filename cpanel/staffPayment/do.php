<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_POST['name'])
			{
				$_SESSION['msg']="Error,Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$data['paymentDate']		=	$App->dbFormat_date($_REQUEST['payDate']);
				$data['voucherNo']			=	$App->convert($_REQUEST['voucher']);				
				$data['staffId']			=	$App->convert($_REQUEST['name']);				
				$data['basicSalary']		=	$App->convert($_REQUEST['salary']);
				$data['da']					=	$App->convert($_REQUEST['da']);
				$data['hra']				=	$App->convert($_REQUEST['hra']);				
				$data['cca']				=	$App->convert($_REQUEST['cca']);
				$data['otherAllowance']		=	$App->convert($_REQUEST['otherAllowance']);
				$data['pf']					=	$App->convert($_REQUEST['pf']);
				$data['sli']				=	$App->convert($_REQUEST['sli']);
				$data['gli']			    =	$App->convert($_REQUEST['gli']);
				$data['otherDeduction']		=	$App->convert($_REQUEST['otherDeductions']);
				$data['total']				=	$App->convert($_REQUEST['total']);
				$data['ledgerID']			=	$App->convert($_REQUEST['from_ledger']);
				$data['loginId']			=	$App->convert($uid);
				
				
				$success=$db->query_insert(TABLE_STAFFPAYMENT,$data);
				
				$vou_sql2 ="SELECT ID FROM `".TABLE_LEDGER."` WHERE `name`='Salary Payments' ";
				$vou_record2 = $db->query_first($vou_sql2);
				$Fee=$vou_record2['ID'];
				
//				$vou_sql ="SELECT MAX(voucher_no) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Fee Payments' ";
				$vou_record = $App->convert($_REQUEST['voucher']);
				/* ------------------------------------------ */																
				/* ------------------------------------------ */
				$tsdata['from_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tsdata['to_ledger'] 		=  	$Fee;
				$tsdata['voucher_no']		= 	$vou_record;
				$tsdata['voucher_type']		=  	'Salary Payments';
				$tsdata['credit']			= 	0;
				$tsdata['debit']			=   $App->convert($_POST['total']);
				$tsdata['cdate']			=	"NOW()";
				$tsdata['transaction_date']	=	$App->dbformat_date($_POST['payDate']);
				$tsdata['remark']			=  	' Staff Id : '.$App->convert($_REQUEST['name']);
				$tsdata['uid']				=	$_SESSION['LogID'];
				$tsdata['loginId']			=	$_SESSION['LogID'];

				$db->query_insert(TABLE_TRANSACTION, $tsdata);
				
				/* ------------------------------------------ */
				/* ------------------------------------------ */
				$tfdata['from_ledger'] 		=  	$Fee;
				$tfdata['to_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tfdata['voucher_no']		= 	 $vou_record;
				$tfdata['voucher_type']		=  	'Salary Payments';
				$tfdata['credit']			=   $App->convert($_POST['total']);
				$tfdata['debit']			= 	0;
				$tfdata['cdate']			=	"NOW()";
				$tfdata['transaction_date']	=	$App->dbformat_date($_POST['payDate']);
				$tfdata['remark']			=  	'Staff Id : '.$App->convert($_REQUEST['name']);
				$tfdata['uid']				=	$_SESSION['LogID'];
				$tfdata['loginId']			=	$_SESSION['LogID'];
			
				$db->query_insert(TABLE_TRANSACTION, $tfdata);							
				$db->close();			
				
				if($success)
					{
					$_SESSION['msg']="Payment Details added successfully";					
					header("location:new.php");	
					}
				else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}				
			}
		
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];		
       
		if(!$_POST['name'])
			{			
				$_SESSION['msg']="Error,Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				
				$data['paymentDate']		=	$App->dbFormat_date($_REQUEST['payDate']);
				$data['voucherNo']			=	$App->convert($_REQUEST['voucher']);				
				$data['staffId']			=	$App->convert($_REQUEST['name']);				
				$data['basicSalary']		=	$App->convert($_REQUEST['salary']);
				$data['da']					=	$App->convert($_REQUEST['da']);
				$data['hra']				=	$App->convert($_REQUEST['hra']);				
				$data['cca']				=	$App->convert($_REQUEST['cca']);
				$data['otherAllowance']		=	$App->convert($_REQUEST['otherAllowance']);
				$data['pf']					=	$App->convert($_REQUEST['pf']);
				$data['sli']				=	$App->convert($_REQUEST['sli']);
				$data['gli']			    =	$App->convert($_REQUEST['gli']);
				$data['otherDeduction']		=	$App->convert($_REQUEST['otherDeductions']);
				$data['total']				=	$App->convert($_REQUEST['total']);
				$data['ledgerID']			=	$App->convert($_REQUEST['from_ledger']);
				$data['loginId']			=	$App->convert($uid);
				
				
					$success=$db->query_update(TABLE_STAFFPAYMENT,$data,"ID='{$fid}'");
					
				$sql2 = "SELECT voucherNo FROM `".TABLE_STAFFPAYMENT."` WHERE ID='$fid'";
				$vou_record = 	$db->query_first($sql2);
				$voucher_no = 	$vou_record['voucherNo'];
				
				$vou_sql2 ="SELECT ID FROM `".TABLE_LEDGER."` WHERE `name`='Salary Payments' ";
				$vou_record2 = $db->query_first($vou_sql2);
				$Fee=$vou_record2['ID'];
				
				$vou_sql	="DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_no`='$voucher_no' AND `voucher_type`='Salary Payments'";
				$db->query($vou_sql);

//				$vou_sql ="SELECT MAX(voucher_no) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Fee Payments' ";
				//$vou_record = $App->convert($_REQUEST['voucherNo']);
				/* ------------------------------------------ */																
				/* ------------------------------------------ */
				$tsdata['from_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tsdata['to_ledger'] 		=  	$Fee;
				$tsdata['voucher_no']		= 	$voucher_no;
				$tsdata['voucher_type']		=  	'Salary Payments';
				$tsdata['credit']			= 	0;
				$tsdata['debit']			=   $App->convert($_POST['total']);
				$tsdata['cdate']			=	"NOW()";
				$tsdata['transaction_date']	=	$App->dbformat_date($_POST['payDate']);
				$tsdata['remark']			=  	'Staff Id : '.$App->convert($_REQUEST['name']);
				$tsdata['uid']				=	$_SESSION['LogID'];
				$tsdata['loginId']			=	$_SESSION['LogID'];

				$db->query_insert(TABLE_TRANSACTION, $tsdata);
				//echo TABLE_TRANSACTION, $tsdata;die;
				/* ------------------------------------------ */
				/* ------------------------------------------ */
				$tfdata['from_ledger'] 		=  	$Fee;
				$tfdata['to_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tfdata['voucher_no']		= 	 $voucher_no;
				$tfdata['voucher_type']		=  	'Salary Payments';
				$tfdata['credit']			=   $App->convert($_POST['total']);
				$tfdata['debit']			= 	0;
				$tfdata['cdate']			=	"NOW()";
				$tfdata['transaction_date']	=	$App->dbformat_date($_POST['payDate']);
				$tfdata['remark']			=  	'Staff Id : '.$App->convert($_REQUEST['name']);
				$tfdata['uid']				=	$_SESSION['LogID'];
				$tfdata['loginId']			=	$_SESSION['LogID'];
			
				$db->query_insert(TABLE_TRANSACTION, $tfdata);
				//echo TABLE_TRANSACTION, $tfdata;die;					 
				$db->close(); 								
				if($success)
					{
					$_SESSION['msg']="Payment Details updated successfully";					
					header("location:new.php");	
					}
				else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}										
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$sql2 = "SELECT voucherNo FROM `".TABLE_STAFFPAYMENT."` WHERE ID='$id'";
				$vou_record = 	$db->query_first($sql2);
				$voucher_no = 	$vou_record['voucherNo'];
				
				$vou_sql	="DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_no`='$voucher_no' AND `voucher_type`='Salary Payments'";
				$db->query($vou_sql);
				
				$sql = "DELETE FROM `".TABLE_STAFFPAYMENT."` WHERE ID='{$id}'";
				$success=$db->query($sql);
				$db->close(); 
								
				if($success)
					{
					$_SESSION['msg']="Payment Details deleted successfully";					
					header("location:new.php");	
					}
				else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}	
					
		
		break;
		
	
		// Printing  ========================================================================
	case 'print':
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				
				// set default header data
				$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));
				
				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
				$fid	=	$_REQUEST['id'];								
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				$sql2 = "SELECT ".TABLE_STAFF.".name as sname,".TABLE_STAFFPAYMENT.".ID,".TABLE_STAFF.".staffId,".TABLE_STAFF.".designation,".TABLE_STAFFPAYMENT.".paymentDate,".TABLE_STAFFPAYMENT.".voucherNo,".TABLE_STAFFPAYMENT.".basicSalary,".TABLE_STAFFPAYMENT.".da,".TABLE_STAFFPAYMENT.".hra,".TABLE_STAFFPAYMENT.".cca,".TABLE_STAFFPAYMENT.".otherAllowance,".TABLE_STAFFPAYMENT.".pf,".TABLE_STAFFPAYMENT.".sli,".TABLE_STAFFPAYMENT.".gli,".TABLE_STAFFPAYMENT.".otherDeduction,".TABLE_STAFFPAYMENT.".total,".TABLE_LEDGER.".name FROM `".TABLE_STAFF."`,`".TABLE_STAFFPAYMENT."`,`".TABLE_LEDGER."` WHERE ".TABLE_STAFF.".ID=".TABLE_STAFFPAYMENT.".staffId and ".TABLE_STAFFPAYMENT.".ledgerID=".TABLE_LEDGER.".ID AND ".TABLE_STAFFPAYMENT.".ID='$fid'";
				$vou_record = 	$db->query_first($sql2);
				
				$name 			= 	$vou_record['sname'];
				$staffId 		= 	$vou_record['staffId'];
				$paymentDate 	= 	$vou_record['paymentDate'];
				$voucherNo 		= 	$vou_record['voucherNo'];
				$basicSalary 	= 	$vou_record['basicSalary'];
				$da 			= 	$vou_record['da'];
				$hra 			= 	$vou_record['hra'];
				$cca 			= 	$vou_record['cca'];
				$otherAllowance = 	$vou_record['otherAllowance'];
				$pf 			= 	$vou_record['pf'];
				$sli 			= 	$vou_record['sli'];
				$gli 			= 	$vou_record['gli'];
				$otherDeduction = 	$vou_record['otherDeduction'];
				$total 			= 	$vou_record['total'];	
				$allowance		=	$basicSalary+$da+$hra+$cca+$otherAllowance;
				$deduction		=	$pf+$sli+$gli+$otherDeduction;	
				
				$pdf->Write(0, '', '', 0, '', true, 0, false, false, 0);
				$html = '                         SALARY SLIP';		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				$pdf->Write(0, '', '', 0, '', true, 0, false, false, 0);			
								
				$html = 'Date        :'.$App->dbFormat_date_db($paymentDate);		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				$html = 'Voucher No  :'.$voucherNo;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
							
				$html = 'Staff Name  :'.$name.'     Staff ID :'.$staffId;						
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
				$pdf->Write(0, '', '', 0, '', true, 0, false, false, 0);
				
			

				$html1 = 'Basic Salary';
				$html2 = ':'.$basicSalary;
				$html3 = 'Provident Fund(PF)';
				$html4 = ':'.$pf;
				//$pdf->writeHTMLCell($w=0, $h=0, $x=0, $y=0, $htmlpdf, $border=0, $ln=1, $fill=0, $reseth=false, $align='', $autopadding=true);
		
				$pdf->writeHTMLCell(0, 0, 15, 50, $html1, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 90, 50, $html2, 0, 1, 0,false, '', true);
				$pdf->writeHTMLCell(0, 0, 115, 50, $html3, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 175, 50, $html4, 0, 1, 0,false, '', true);
							
				
				$html5 = 'DA';
				$html6 = ':'.$da;
				$html7 = 'State Life Insurance(SLI)';
				$html8 = ':'.$sli;
				
				$pdf->writeHTMLCell(0, 0, 15, 55, $html5, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 90, 55, $html6, 0, 1, 0,false, '', true);
				$pdf->writeHTMLCell(0, 0, 115, 55, $html7, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 175, 55, $html8, 0, 1, 0,false, '', true);
					
				$html9 = 'House Rent Allowance(HRA)';
				$html10 = ':'.$hra;
				$html11 = 'Group Life Insurance(GLI)';
				$html12 = ':'.$gli;
				
				$pdf->writeHTMLCell(0, 0, 15, 60, $html9, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 90, 60, $html10, 0, 1, 0,false, '', true);
				$pdf->writeHTMLCell(0, 0, 115, 60, $html11, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 175, 60, $html12, 0, 1, 0,false, '', true);					
				
				$html13 = 'Compensatory City Allowance(CCA)';
				$html14 = ':'.$cca;
				$html15 = 'Other Deductions';
				$html16 = ':'.$otherDeduction;
				
				$pdf->writeHTMLCell(0, 0, 15, 65, $html13, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 90, 65, $html14, 0, 1, 0,false, '', true);
				$pdf->writeHTMLCell(0, 0, 115, 65, $html15, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 175, 65, $html16, 0, 1, 0,false, '', true);	
				
				$html17 = 'Other Allowance';
				$html18 = ':'.$otherAllowance;
				
				
				$pdf->writeHTMLCell(0, 0, 15, 70, $html17, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 90, 70, $html18, 0, 1, 0,false, '', true);
				$pdf->writeHTMLCell(0, 0, 115, 70, '', 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 175, 70, '', 0, 1, 0,false, '', true);								
								
				
				$html = '------------------------------------------------------------------------';			
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				$html19 = 'Total';
				$html20 = ':'.$allowance;
				$html21 = ':'.$deduction;
				
				
				$pdf->writeHTMLCell(0, 0, 15, 80, $html19, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 90, 80, $html20, 0, 1, 0,false, '', true);
				$pdf->writeHTMLCell(0, 0, 115, 80, '', 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 175, 80, $html21, 0, 1, 0,false, '', true);			
				
				
				$html = '------------------------------------------------------------------------';			
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				$html22 = 'Grand Total';
				$html23 = ':'.$total;
				
				
				
				$pdf->writeHTMLCell(0, 0, 15, 90, $html22, 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 90, 90, $html23, 0, 1, 0,false, '', true);
				$pdf->writeHTMLCell(0, 0, 115, 90, '', 0, 1, 0, false, '', true);
				$pdf->writeHTMLCell(0, 0, 175, 90, '', 0, 1, 0,false, '', true);			
				
				
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'FeePayment'$staffId.pdf", 'I');		
		break;
		

}
?>
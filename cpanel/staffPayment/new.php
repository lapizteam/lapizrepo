<?php include("../adminHeader.php") ;

$type				=	$_SESSION['LogType'];
$staffEdit		=	$_SESSION['staffEdit'];

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<script type="text/javascript">
$(document).ready(function() {
 $("#name").change(function() {    
	var value=$('#name').val();            
      $.ajax({    //create an ajax request to load_page.php
	  
        type: "post",
        url: "designation.php", 
		
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#designation").val(response); 
            //alert(response);
        }

    });
	

  $.ajax({    //create an ajax request to load_page.php
	  
        type: "post",
        url: "da.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#da").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "basicSal.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#salary").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "find_da.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#da").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "hra.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#hra").val(response); 
            //alert(response);
        }

    });
	
		 $.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "cca.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#cca").val(response); 
            //alert(response);
        }

    });
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "pf.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#pf").val(response); 
            //alert(response);
        }

    });
	
 	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "sli.php", 
	
		data:{ value: value},   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#sli").val(response); 
            //alert(response);
        }

    });
	 	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "gli.php ", 
	
		data:{ value: value},   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#gli").val(response); 
            //alert(response);
        }

    });
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "total.php ", 
	
		data:{ value: value},   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#total").val(response); 
            //alert(response);
        }

    });
	
	
});


});
</script>
<script>
	function total_amt()
	{
	var bs=document.getElementById('salary').value;
	if(bs==''){alert("Set Basic Salary In Staff Registration Form");}
	var da=document.getElementById('da').value;
	if(da==''){da=0;}
	var hra=document.getElementById('hra').value;
	if(hra==''){hra=0;}
	var cca=document.getElementById('cca').value;
	if(cca==''){cca=0;}
	var oa=document.getElementById('otherAllowance').value;
	if(oa==''){oa=0;}
	var pf=document.getElementById('pf').value;
	if(pf==''){pf=0;}
	var sli=document.getElementById('sli').value;
	if(sli==''){sli=0;}
	var gli=document.getElementById('gli').value;
	if(gli==''){gli=0;}
	var od=document.getElementById('otherDeductions').value;
	if(od==''){od=0;}

	var d=parseInt(pf)+parseInt(sli)+parseInt(gli)+parseInt(od);
	var tot=parseInt(bs)+parseInt(da)+parseInt(hra)+parseInt(cca)+parseInt(oa)-parseInt(d);
	document.getElementById('total').value=tot;
	
	}
</script>

<script>
//validation msg
function validate()
{
maxLength=9;
flag=false;

jOtherAllowance=document.getElementById('otherAllowance').value;	
	if(isNaN(jOtherAllowance))
	{		
	document.getElementById('otherAllowancediv').innerHTML="Enter valid amount";
	flag=true;
	}
	if(jOtherAllowance<0)
	{																			///for age
	document.getElementById('otherAllowancediv').innerHTML="Enter valid amount";
	flag=true;
	}
		if(jOtherAllowance.length>maxLength)
		{																			
		document.getElementById('otherAllowancediv').innerHTML="Enter valid amount";
		flag=true;
		}
jOtherDeductions=document.getElementById('otherDeductions').value;	
	if(isNaN(jOtherDeductions))
	{		
	document.getElementById('otherDeductionsdiv').innerHTML="Enter valid amount";
	flag=true;
	}
	if(jOtherDeductions<0)
	{																			///for age
	document.getElementById('otherDeductionsdiv').innerHTML="Enter valid amount";
	flag=true;
	}
		if(jOtherDeductions.length>maxLength)
		{																			
		document.getElementById('otherDeductionsdiv').innerHTML="Enter valid amount";
		flag=true;
		}
jTotal=document.getElementById('total').value;
	if(jTotal=="" || jTotal<=0)
	{
	document.getElementById('totaldiv').innerHTML ="Enter valid amount";
	flag=true;
	}
		if(jTotal.length>maxLength)
		{																			
		document.getElementById('totaldiv').innerHTML="Enter valid amount";
		flag=true;
		}
		
if(flag==true)
	{
	return false;
	}
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>
<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 ?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">STAFF PAYMENT </h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
           <div class="col-sm-4">
            <form method="post">
              <div class="input-group" style="float: left; margin-right: 10px;margin-left:10%">
			 <input id="search_staff" name="search_staff" type="text" placeholder="Staff name" class="form-control" value="<?php echo @$_REQUEST['search_staff'] ?>" /> 
			  </div>
			  
			  <div class="form-group">              
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> 
			</div>
            </form>
          </div>
        </div>
<?php	
$cond="1";
if(@$_REQUEST['search_staff'])
{
	//$cond=" name like '%$st%'  ";
	$cond=$cond." and ".TABLE_STAFF.".name like'%".$_POST['search_staff']."%'";
}

?>
			    <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table view_limitter pagination_table" >
                <thead>
                  <tr>
				    <th>Sl No</th>               
					<th>Staff Id</th>
					<th>Name</th>
					<th>Designation</th>					
					<th>Total Amount</th>
					<th>Payment Date</th>	
					<th>Voucher No</th>					
                  </tr>
                </thead>
                <tbody>
				<?php 															
						$selAllQuery="SELECT ".TABLE_STAFF.".name as sname,".TABLE_STAFFPAYMENT.".ID,".TABLE_STAFF.".staffId,".TABLE_STAFF.".designation,".TABLE_STAFFPAYMENT.".paymentDate,".TABLE_STAFFPAYMENT.".voucherNo,".TABLE_STAFFPAYMENT.".basicSalary,".TABLE_STAFFPAYMENT.".da,".TABLE_STAFFPAYMENT.".hra,".TABLE_STAFFPAYMENT.".cca,".TABLE_STAFFPAYMENT.".otherAllowance,".TABLE_STAFFPAYMENT.".pf,".TABLE_STAFFPAYMENT.".sli,".TABLE_STAFFPAYMENT.".gli,".TABLE_STAFFPAYMENT.".otherDeduction,".TABLE_STAFFPAYMENT.".total,".TABLE_LEDGER.".name FROM `".TABLE_STAFF."`,`".TABLE_STAFFPAYMENT."`,`".TABLE_LEDGER."` WHERE ".TABLE_STAFF.".ID=".TABLE_STAFFPAYMENT.".staffId and ".TABLE_STAFFPAYMENT.".ledgerID=".TABLE_LEDGER.".ID AND $cond ORDER BY ".TABLE_STAFFPAYMENT.".ID DESC";						
												
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="7">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
					
							while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
					   <td><?php echo $i++;?>
						  <div class="adno-dtls"> <?php if($staffEdit || $type=="Admin"){?> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> | <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  |<?php }?> <a target="_blank" href="do.php?id=<?php echo $tableId; ?>&op=print"  >PRINT</a>  | <a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a> </div>
						  
							<!-- Modal3 -->
							  <div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
								<div class="modal-dialog">
								  <div class="modal-content"> 
									<!-- <div class="modal-body clearfix">
								fddhdhdhddhdh
								  </div>-->	 		
									<div role="tabpanel" class="tabarea2"> 
									  
									  <!-- Nav tabs -->
									 <ul class="nav nav-tabs" role="tablist">									  
										<li role="presentation" class="active"> <a href="#details<?php echo $tableId; ?>" aria-controls="details" role="tab" data-toggle="tab">Staff Payment Details</a> </li>
																		
									</ul>
									  
									  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="details<?php echo $tableId; ?>">
										  <table class="table nobg">
											<tbody style="background-color:#FFFFFF">
											  
											  <tr>
												  <td >Payment Date</td>
												  <td>:</td>
												  <td><?php  echo $App->dbformat_date_db($row['paymentDate']);?></td>
											  </tr>
											  
											  <tr>
												  <td >Voucher Number</td>
												  <td>:</td>
												  <td> : <?php  echo $row['voucherNo']; ?></td>  
											  </tr>
											  
											  <tr>
												  <td >Name</td>
												  <td>:</td>
												  <td><?php  echo $row['sname']; ?></td>
											  </tr>
											  <tr>
												  <td >Designation</td>
												  <td>:</td>
												  <td><?php  echo $row['designation']; ?></td>  
											  </tr>
											  <tr>
												  <td >Basic Salary</td>
												  <td>:</td>
												  <td><?php echo $row['basicSalary']; ?>	</td>			                                           
											  </tr>
												
											  <tr>
												  <td >DA</td>
												  <td>:</td>
												  <td><?php  echo $row['da']; ?></td>
											  </tr>
											  <tr>
												  <td >HRA</td>
												  <td>:</td>
												  <td><?php echo $row['hra']; ?></td>
											  </tr>
											   
											  <tr>
												  <td >CCA</td>
												  <td>:</td>
												  <td><?php echo $row['cca']; ?></td>
											  </tr>                                            			 		
											
											<tr>
												  <td>Other Allowance</td>
												  <td>:</td>
												  <td><?php echo $row['otherAllowance']; ?></td>
											</tr>
											   
											<tr>
												  <td >PF</td>
												  <td>:</td>
												  <td><?php echo $row['pf']; ?></td>
											</tr>
						
											<tr>                      
												  <td >SLI</td>
												  <td>:</td>
												  <td><?php echo $row['sli']; ?></td>                     
											</tr>
											
											 <tr>
												   <td >GLI</td>
												   <td>:</td>
												   <td><?php echo $row['gli']; ?></td>					   
											</tr>
											 
											<tr>
												  <td >Other Deductions </td>
												  <td>:</td>
												  <td><?php echo $row['otherDeduction']; ?></td>
											</tr>
											
											<tr>
												  <td>Total </td>
												  <td>:</td>
												  <td><?php echo $row['total']; ?></td>
											</tr>
											<tr>
												  <td>Account </td>
												  <td>:</td>
												  <td><?php echo $row['name']; ?></td>
											</tr>						 						
										</tbody>
									  </table>
										</div>
										
									  </div>
									</div>
								  </div>
								</div>
							  </div>
							  <!-- Modal3 cls --> 
						  
						  
						  </td>
						<td><?php echo $row['staffId']; ?></td>
						<td><?php echo $row['sname']; ?></td>
						<td><?php echo $row['designation']; ?></td>												
						<td><?php echo $row['total']; ?> </td> 
						<td><?php echo $App->dbformat_date_db($row['paymentDate']); ?>  </td>
						<td><?php echo $row['voucherNo']; ?> </td>                   	 	
					  </tr>
					  <?php }
				}?>                  
                </tbody>
              </table>
			  	
            </div>
			<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">STAFF PAYMENT</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return validate()">
                <div class="row">
                  <div class="col-sm-6">
				                       
					<div class="form-group">
                      <label for="name">Name:<span class="star">*</span></label>
                      <select name="name" id="name" class="form-control2" required title="Name">
						<option value="">Select</option>
                    	
						<?php 	
			 			 $res=mysql_query("select ID,name from ".TABLE_STAFF."");
			  				while($row=mysql_fetch_array($res))
			  				{
			  
			  			?>
							<option value="<?php echo $row['ID'];?>"><?php echo $row['name'];?></option>
						<?php 
			  				}
			  			?>
					</select>	
                    </div>				                   								 
                  
                    <div class="form-group">
                      <label for="designation">Designation:</label>
                   <input type="text" id="designation" name="designation" value="" placeholder="Designation" class="form-control2" readonly/>
                    </div>	
					 <div class="form-group">
                      <label for="salary">Basic Salary:</label>
                    <input type="text" id="salary" name="salary" value=""  title="Basic Salary" placeholder="Basic Salary" class="form-control2" readonly/>							  
                    </div>										                     
					
                    <div class="form-group">
                      <label for="da">DA:</label>
                     <input type="text" id="da" name="da" value=""  title="DA" placeholder="DA" class="form-control2" readonly />	
                    </div>
                   
                    <div class="form-group">
                      <label for="hra">House Rent Allowance(HRA):</label>                    
					   <input type="text" id="hra" name="hra" value="<?php echo $row['hra'];?>"  title="HRA" placeholder="HRA" class="form-control2" readonly />
                    </div>
					<div class="form-group">
                      <label for="cca">Compensatory City Allowance(CCA):</label>                    
					   <input type="text" id="cca" name="cca" value="<?php echo $row['cca'];?>"  title="CCA" placeholder="CCA" class="form-control2" readonly />
                    </div>	
					<div class="form-group">
                      <label for="otherAllowance">Other Allowance:<span class="star">*</span></label>                    
					   <input type="text" id="otherAllowance" name="otherAllowance" value="0" required placeholder="Others Allowance" class="form-control2" onChange="total_amt()" onfocus="clearbox('otherAllowancediv')" />	
					  <div id="otherAllowancediv" class="valid" style="color:#FF6600;"></div>	  
                    </div>	
					<div class="form-group">
                      <label for="pf">Provident Fund(PF):</label>                    
					     <input type="text" id="pf" name="pf" value="<?php echo $row['pf'];?>"  title="PF" placeholder="PF" class="form-control2" readonly />
                    </div>						                   								 
                  
                    <div class="form-group">
                      <label for="sli">State Life Insurance(SLI):</label>
                     <input type="text" id="sli" name="sli" value="<?php echo $row['sli'];?>"  title="SLI" placeholder="SLI" class="form-control2" readonly />
                    </div>
					<div class="form-group">
                      <label for="gli">Group Life Insurance(GLI):</label>
                      <input type="text" id="gli" name="gli" value="<?php echo $row['gli'];?>"  title="GLI" placeholder="GLI" class="form-control2" readonly />							 
                    </div>
					 
                    <div class="form-group">
                      <label for="otherDeductions">Other Deductions:<span class="star">*</span></label>
                      <input type="text" id="otherDeductions" name="otherDeductions" value="0" required placeholder="Other Deductions" class="form-control2" onChange="total_amt()" onfocus="clearbox('otherDeductionsdiv')" />	
					  <div id="otherDeductionsdiv" class="valid" style="color:#FF6600;"></div>	  				
                    </div>
                   	<div class="form-group">
                      <label for="total">Paying amount:<span class="star">*</span></label>
                     <input type="text" id="total" name="total" value=""  title="Total Amount"  placeholder="Total Amount" class="form-control2"  readonly onfocus="clearbox('totaldiv')"/>
					 <div id="totaldiv" class="valid" style="color:#FF6600;"></div>
                    </div>
					<div class="form-group">
                      <label for="payDate">Payment Date:</label>
                     <input type="text" id="payDate" name="payDate"   value="<?php echo date("d/m/Y");?>"  title="Payment Date"  class="form-control2 datepicker" placeholder="Payment Date" readonly="readonly" />						 
                    </div>
					 <?php 							
							$res=mysql_query("select MAX(voucherNo) as vn from ".TABLE_STAFFPAYMENT."");
			  				$row=mysql_fetch_array($res);
							$a=$row['vn'];
					?>					 
                    <div class="form-group">
                      <label for="voucher">Voucher Number:</label>
                      <input type="text" id="voucher" name="voucher" value="<?php if($a==0 || $a=="" || $a==null)
																				{
																				 echo '1';
																				 } 
																				 else
																				 { 
																				 echo $a+1;
																				 }?>"  title="Voucher Number"  placeholder="Voucher Number" class="form-control2" readonly="readonly" />	
                    </div>							  	
								  	
					 <div class="form-group">
                      <label for="from_ledger">Account:</label>
					<?php  
					$from_ledger_qry= $db->query("SELECT * FROM `".TABLE_LEDGER."` WHERE `account_group`='8' OR `account_group`='15'"); 
					?>
					<select name="from_ledger" class="form-control2" id="from_ledger"  title="Select Account">
					<?php    
					while($from_ledger_row=mysql_fetch_array($from_ledger_qry))					
					{					 
					?>  
						 <option value="<?php echo $from_ledger_row['ID']; ?>" ><?php echo $from_ledger_row['name']; ?></option>
					<?php  
					}  
					?>
                  	</select>
                    </div>
	
				</div>
			</div>
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
	  </div>
      <!-- Modal1 cls --> 
     
	 
	 
	  
      </div>
  </div>
<!--<script src="../../js/jquery-1.7.2.min.js"></script>
<script src="../../js/bootstrap.js"></script>-->
<script src="../../js/base.js"></script>
<script src="../../js/guidely/guidely.min.js"></script>
 
<?php include("../adminFooter.php") ?>

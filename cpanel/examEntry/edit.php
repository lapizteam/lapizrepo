<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

function getStaffId(str)
{
document.getElementById('hiddenStaffId').value=str;
 if (str=="") {
    document.getElementById("staffId").innerHTML="";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("staffId").value=xmlhttp.responseText;
    }
  }
//  xmlhttp.open("GET","work_rate_1.php?q="+str+"&c="+client+"&df="+date_from+"&dt="+date_to,true);
   xmlhttp.open("GET","staffId.php?q="+str,true);
 
  xmlhttp.send();
}
</script>
<script>
function valid()
{
flag=false;
	jTimeFromHr=document.getElementById('timeFromHr').value;
	jTimeFromMin=document.getElementById('timeFromMin').value;
		if((isNaN(jTimeFromHr))||(isNaN(jTimeFromMin)))
		{																		
		document.getElementById('timeFromDiv').innerHTML="Time should be valid.";
		flag=true;
		}
	jTimeToHr=document.getElementById('timeToHr').value;
	jTimeToMin=document.getElementById('timeToMin').value;		
		if((isNaN(jTimeToHr))||(isNaN(jTimeToMin)))
		{																			
		document.getElementById('timeToDiv').innerHTML="Time should be valid.";
		flag=true;
		}
if(flag==true)
	{
	return false;
	}
	
																				
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	 $fid=$_REQUEST['id'];	
         $fid = mysql_real_escape_string($fid);

					 
		$editView=mysql_query("SELECT ".TABLE_EXAMENTRY.".ID,".TABLE_EXAMENTRY.".examName,".TABLE_EXAMTYPES.".examType,".TABLE_EXAMENTRY.".subject,".TABLE_SUBJECT.".subjectName,".TABLE_EXAMENTRY.".examDate,".TABLE_EXAMENTRY.".timeFrom,".TABLE_EXAMENTRY.".timeTo,".TABLE_EXAMENTRY.".discription,".TABLE_EXAMENTRY.".acYear FROM `".TABLE_EXAMENTRY."`,`".TABLE_SUBJECT."`,`".TABLE_EXAMTYPES."` WHERE ".TABLE_EXAMENTRY.".subject=`".TABLE_SUBJECT."`.ID AND ".TABLE_EXAMTYPES.".ID=".TABLE_EXAMENTRY.".examName AND ".TABLE_EXAMENTRY.".ID='$fid' ");				
						
	$editRow=mysql_fetch_array($editView);	
?>		
 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">EXAM REGISTRATION</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">
			  <input type="hidden" name="fid" id="fid" value="<?php echo $fid ?>">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
					
                      <label for="year">Academic Year:<span class="star">*</span></label>
					  	 <select name="year" id="year" class="form-control2" required>
						<?php 
						$select1="select * from ".TABLE_ACADEMICYEAR."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>"<?php if($row['ID']==$editRow['acYear']){?> selected="selected" <?php }?> ><?php echo $row['fromYear']."-".$row['toYear']?></option>
						<?php 
						}
						?>
                    </select>
                    </div>
					
                    <div class="form-group">
                      <label for="exam">Exam Type:<span class="star">*</span></label>                    
					  <select name="exam" id="exam" required  class="form-control2" title="Exam Name">                    
						<?php 
						$select1="SELECT * FROM  ".TABLE_EXAMTYPES." ORDER BY examType";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>" <?php if($row['ID']==$editRow['examName']){?> selected="selected" <?php }
?>><?php echo $row['examType']?></option>
						<?php 
						}
						?>
                    </select>							
                    </div>
                   
                    <div class="form-group">
                      <label for="subject">Subject:<span class="star">*</span></label>                    
					<select name="subject" id="subject" required title="Subject" class="form-control2" >                    	
						<?php 
						$select1="select * from ".TABLE_SUBJECT."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>" <?php if($row['ID']==$editRow['subject']){?> selected="selected" <?php }
?>><?php echo $row['subjectName']?></option>
						<?php 
						}
						?>
                    </select>
                    </div>					                   								 
                  
                    <div class="form-group">
                      <label for="examDate">Exam Date:<span class="star">*</span></label>
                      <input type="text" id="examDate" name="examDate" value="<?php echo $App->dbformat_date_db($editRow['examDate']);?>" required title="Exam Date" class="form-control2 datepicker" />
                    </div>	
<?php 
//Time spliting code
$timeFrom =$editRow['timeFrom'];
preg_match("/([0-9]{1,2}):([0-9]{1,2}):([a-zA-Z]+)/", $timeFrom, $match);
$hour = $match[1];
$min = $match[2];
$ampm = $match[3];

$timeTo =$editRow['timeTo'];
preg_match("/([0-9]{1,2}):([0-9]{1,2}):([a-zA-Z]+)/", $timeTo, $match2);
$hour2 = $match2[1];
$min2 = $match2[2];
$ampm2 = $match2[3];

?>
					<div class="form-group">
						<label >Time:<span class="star">*</span></label>
		<table>	
			<tr>
				<td colspan="3"><label for="timeFromHr">From<span class="star">*</span></label></td>																																																					
			</tr>
			<tr>				
				<td><input type="text" name="timeFromHr" id="timeFromHr" value="<?php echo $hour ?>" placeholder="Hour" style="width:60%" required onfocus="clearbox('timeFromDiv')"></td>
				<td><input type="text" name="timeFromMin" id="timeFromMin" value="<?php echo $min?>" placeholder="Minute" style="width:60%" required onfocus="clearbox('timeFromDiv')"></td>	
				<td><select name="type1" id="type1" >
                    	<option value="AM" <?php if($ampm=="AM") echo 'selected'; ?>>AM</option>
						<option value="PM" <?php if($ampm=="PM") echo 'selected'; ?>>PM</option>						
                    </select>
				</td>																																															
			</tr>
			<tr>
				<td colspan="3"><div id="timeFromDiv" class="valid" style="color:#FF6600"></div></td>
			</tr>
			<tr>
				<td colspan="3"><label for="timeToHr">To<span class="star">*</span></label></td>																																																						
			</tr>
			<tr>							
				<td><input type="text" name="timeToHr" id="timeToHr" value="<?php echo $hour2 ?>" placeholder="Hour" style="width:60%" required onfocus="clearbox('timeToDiv')"></td>
				<td><input type="text" name="timeToMin" id="timeToMin" value="<?php echo $min2 ?>" placeholder="Minute" style="width:60%" required onfocus="clearbox('timeToDiv')"></td>	
				<td><select name="type2" id="type2" >
                    	<option value="AM" <?php if($ampm2=="AM") echo 'selected'; ?>>AM</option>
						<option value="PM" <?php if($ampm2=="PM") echo 'selected'; ?>>PM</option>						
                    </select>
				</td>																																											
			</tr>
			<tr>
				<td colspan="3"><div id="timeToDiv" class="valid" style="color:#FF6600"></div></td>
			</tr>			
		</table>
		
					</div>	
					 <div class="form-group">
                      <label for="discription">Discription:</label>
                      <textarea id="discription" style="width:265px" name="discription"  class="login" ><?php echo $editRow['discription'];?></textarea>			
                    </div>							
				</div> 
                 
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['year'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$success=0;
				$year			=	$App->convert($_REQUEST['year']);
				$exam			=	$App->convert($_REQUEST['exam']);
				$subject		=	$App->convert($_REQUEST['subject']);
				
				$existId=$db->existValuesId(TABLE_EXAMENTRY," acYear='$year' AND examName='$exam' AND subject='$subject'");
				
				if($existId>0)
				{						
					$_SESSION['msg']="Exam already Exist !!";					
					header("location:new.php");
					break ;
				}
				else
				{	
				
				$timeFrom=$_REQUEST['timeFromHr'].":".$_REQUEST['timeFromMin'].":".$_REQUEST['type1'];
				$timeTo=$_REQUEST['timeToHr'].":".$_REQUEST['timeToMin'].":".$_REQUEST['type2'];
				//echo $timeFrom;die;
				
				$data['acYear']			=	$App->convert($_REQUEST['year']);
				$data['examName']		=	$App->convert($_REQUEST['exam']);
				$data['subject']		=	$App->convert($_REQUEST['subject']);
				$data['examDate']		=	$App->dbFormat_date($_REQUEST['examDate']);
				$data['timeFrom']		=	$App->convert($timeFrom);
				$data['timeTo']			=	$App->convert($timeTo);
				$data['discription']	=	$App->convert($_REQUEST['discription']);
				$data['loginId']		=	$App->convert($uid);
						
				$success=$db->query_insert(TABLE_EXAMENTRY, $data);	
				//echo TABLE_EXAMENTRY, $data;die;
				$db->close();
							
				
				if($success)
					{
					$_SESSION['msg']="Exam Details added successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}	
				}
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  			    
		if(!$_POST['year'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$success=0;
				$year			=	$App->convert($_REQUEST['year']);
				$exam			=	$App->convert($_REQUEST['exam']);
				$subject		=	$App->convert($_REQUEST['subject']);
				$existId=$db->existValuesId(TABLE_EXAMENTRY," acYear='$year' AND examName='$exam' AND subject='$subject' AND ID!='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Exam already Exist !!";					
					header("location:edit.php?id=$fid");
						break ;
				}
				else
				{	
				
				$timeFrom=$_REQUEST['timeFromHr'].":".$_REQUEST['timeFromMin'].":".$_REQUEST['type1'];
				$timeTo=$_REQUEST['timeToHr'].":".$_REQUEST['timeToMin'].":".$_REQUEST['type2'];
				//echo $timeFrom;die;
				
				$data['acYear']			=	$App->convert($_REQUEST['year']);
				$data['examName']		=	$App->convert($_REQUEST['exam']);
				$data['subject']		=	$App->convert($_REQUEST['subject']);
				$data['examDate']		=	$App->dbFormat_date($_REQUEST['examDate']);
				$data['timeFrom']		=	$App->convert($timeFrom);
				$data['timeTo']			=	$App->convert($timeTo);
				$data['discription']	=	$App->convert($_REQUEST['discription']);
				$data['loginId']		=	$App->convert($uid);
						
				
				$success=$db->query_update(TABLE_EXAMENTRY, $data," ID='{$fid}'");	
				//echo TABLE_EXAMENTRY, $data," ID='{$fid}'";die;		
     			$db->close();
		
				
				if($success)
					{
					$_SESSION['msg']="Exam Details updated successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}	
				}												
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];							
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				//$success=$db->query("DELETE FROM `".TABLE_EXAMENTRY."` WHERE ID='{$id}'");	
				
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_EXAMENTRY."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}	
											
				$db->close(); 	
				
				
				if($success)
					{
					$_SESSION['msg']="Exam Details deleted successfully";					
					header("location:new.php");		
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";
					header("location:new.php");					
					}				
		break;		
}
?>
<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['year'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
					$success=0;
				
				$acYear			=	$App->convert($_REQUEST['year']);
				$acMonth		=	$App->convert($_REQUEST['month']);
				$existId=$db->existValuesId(TABLE_WORKINGDAYS," acYear='$acYear' AND acMonth='$acMonth'");

				if($existId>0)
				{				
					$_SESSION['msg']="Working Days already Exist !!";					
					header("location:new.php");
					break ;
				}
				
				
				$data['acYear']			=	$App->convert($_REQUEST['year']);
				$data['acMonth']		=	$App->convert($_REQUEST['month']);
				$data['workingDay']		=	$App->convert($_REQUEST['workDay']);				
				$data['loginId']		=	$App->convert($uid);
						
					$success=$db->query_insert(TABLE_WORKINGDAYS, $data);								
				$db->close();
				
				
				if($success)
					{
					$_SESSION['msg']="Working Days added successfully";											
					}
					else
					{
					$_SESSION['msg']="Failed";									
					}	
					header("location:new.php");	
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		 $fid	=	$_REQUEST['fid']; 		  
					    
		if(!$_POST['year'])
			{			
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
					$success=0;
				$acYear			=	$App->convert($_REQUEST['year']);
				$acMonth		=	$App->convert($_REQUEST['month']);
				$existId=$db->existValuesId(TABLE_WORKINGDAYS," acYear='$acYear' AND acMonth='$acMonth' and ID !='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Working Days already Exist !!";					
					header("location:edit.php?id=$fid");
					break ;
				}
				
				$data['acYear']			=	$App->convert($_REQUEST['year']);
				$data['acMonth']		=	$App->convert($_REQUEST['month']);
				$data['workingDay']		=	$App->convert($_REQUEST['workDay']);				
				$data['loginId']		=	$App->convert($uid);											
						
				
					$success=$db->query_update(TABLE_WORKINGDAYS, $data, " ID='{$fid}'");					
     			$db->close();
				 				
										
				if($success)
					{
						$_SESSION['msg']="Working Days updated successfully";										
					}
					else
					{
					$_SESSION['msg']="Failed";									
					}	
					header("location:new.php");												
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id		=	$_REQUEST['id'];				
				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
				$success=0;			
				
				//$success=$db->query("DELETE FROM `".TABLE_WORKINGDAYS."` WHERE ID='{$id}'");								
				
				try
				{			
				$success= @mysql_query("DELETE FROM `".TABLE_WORKINGDAYS."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				} 	
				$db->close();				
				if($success)
					{
					$_SESSION['msg']="Working Days deleted successfully";											
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
					}	
					header("location:new.php");					
		break;
				
}
?>
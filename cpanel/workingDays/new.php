<?php include("../adminHeader.php");
$type				=	$_SESSION['LogType'];
$basicSettingEdit	=	$_SESSION['basicSettingEdit'];

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<script>
function valid()
{
flag=false;

var m = document.getElementById('month').value;
var jYear = document.getElementById('toYear').value;
var jWorkDay = document.getElementById('workDay').value;	

m = parseInt(m);
var month;
 switch(m)
  {
        case 1:
            var month = 31;
			break;
		case 2 :
            var month =  (jYear % 4 == 0 && jYear % 100) || jYear % 400 == 0 ? 29 : 28;
			break;	 
		case 3 : 
			var month = 31;
			break;
		case 4 :
			var month = 30;
			break;
		case 5 :
			var month = 31;
			break;
		case 6 :
			var month = 30;
			break;
		case 7 :
			var month = 31; 
			break;
		case 8 :
			var month = 31;
			break;
		case 9 : 
			var month = 30;
			break;
		case 10 :
			var month = 31;
			break;
		case 11 : 
			var month = 30; 
			break;
		case 12 :
			var month = 31;
			break;
    }

//alert (month);

		
		if(isNaN(jWorkDay)) 
		{																			///for age
		document.getElementById('workDayDiv').innerHTML="Enter the working days";
		flag=true;
		}
		if(Number(jWorkDay) > Number(month))
		{
		document.getElementById('workDayDiv').innerHTML="Check total working days";
		flag=true;
		}
if(flag==true)
	{
	return false;
	}
	
																				
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">WORKING DAYS REGISTRATION</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>         
        </div>

        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table view_limitter pagination_table" >
                <thead>
                  <tr >
				    <th>Sl No</th>               
					<th>AcademicYear</th>
					<th>Month</th>	
					<th>WorkingDays</th>							
                  </tr>
                </thead>
                <tbody>
				<?php 															
						$selAllQuery="SELECT ".TABLE_WORKINGDAYS.".ID,".TABLE_ACADEMICYEAR.".fromYear,".TABLE_ACADEMICYEAR.".toYear,".TABLE_MONTH.".monthName,".TABLE_WORKINGDAYS.".workingDay FROM `".TABLE_WORKINGDAYS."`,`".TABLE_ACADEMICYEAR."`,`".TABLE_MONTH."` WHERE ".TABLE_WORKINGDAYS.".acYear=`".TABLE_ACADEMICYEAR."`.ID AND ".TABLE_WORKINGDAYS.".acMonth=`".TABLE_MONTH."`.ID ORDER BY ID DESC";
												
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="4">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
					
							while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
					   <td><?php echo $i++;?>
						  <div class="adno-dtls"> <?php if($basicSettingEdit || $type=="Admin"){?> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a><?php }?> 													  						</td>
						<td><?php echo $row['fromYear']."-".$row['toYear']; ?> </td>
						<td><?php echo $row['monthName']; ?></td>
						<td><?php echo $row['workingDay']; ?></td>											                  	 	
					  </tr>
					  <?php }
				}?>                  
                </tbody>
              </table>
			  	 
            </div>
			<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">WORKING DAYS REGISTRATION</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="year">Academic Year:<span class="star">*</span></label>
                      <select name="year" id="year" class="form-control2" required>
                    	<?php 
						$select1="select * from ".TABLE_ACADEMICYEAR."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>" <?php if($row['status']=='YES'){?> selected="selected"<?php }?>><?php echo $row['fromYear']."-".$row['toYear']?></option>
						
						<?php 
						}
						?>
                    </select>	
					<input type="hidden" name="toYear" id="toYear" value=" <?php echo $row['toYear'];?>">								 
                    </div>
					
                    <div class="form-group">
                      <label for="month">Month:<span class="star">*</span></label>
                      <select name="month" id="month" title="Month" class="form-control2" required >
                    	<option value="">Select</option>
						<?php 
						$select1="select * from ".TABLE_MONTH."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>"><?php echo $row['monthName']?></option>
						<?php 
						}
						?>
                    </select>							
                    </div>   
					 <div class="form-group">
                      <label for="workDay">Total Working Days:<span class="star">*</span></label>
                      <input type="text" id="workDay" name="workDay" value="" required  title="Total Working Days" placeholder="Total Working Days" class="form-control2" onfocus="clearbox('workDayDiv')"/>
					  <div id="workDayDiv" class="valid" style="color:#FF6600"></div>									 
                    </div>					                                                                    
				</div>
			</div>
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
	  </div>
      <!-- Modal1 cls --> 
     
	 
	 
	  
      </div>
  </div>
<?php include("../adminFooter.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../core/logout.php");
}

$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['routeNo'])
			{				
				$_SESSION['msg']="Error, Invalid Route No!";					
				header("location:new.php");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$success=0;
				$routeNo=$App->convert($_REQUEST['routeNo']);
				$existId=$db->existValuesId(TABLE_VEHICLEREG,"routeNo='$routeNo'");

				if($existId>0)
				{					
					$_SESSION['msg']="Route Number already Exist!!";					
					header("location:new.php");				
				}
				else
				{	
				
					$data['routeNo']			=	$App->convert($_REQUEST['routeNo']);
					$data['routeName']			=	$App->convert($_REQUEST['routeName']);
					$data['vehicle']			=	$App->convert($_REQUEST['vehicleNo']);
					$data['stageNo']			=	$App->convert($_REQUEST['stageNo']);				
					$data['loginId']			=	$App->convert($uid);										
													
					$vehicleId = $db->query_insert(TABLE_VEHICLEREG, $data);
															
					$i=0;
					if($App->convert($_REQUEST['amount1'])>0 && $_REQUEST['stage1']!='' )
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage1']);
						$data2['amount']		=	$App->convert($_REQUEST['amount1']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount2'])>0 && $_REQUEST['stage2']!='')
					{
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage2']);
						$data2['amount']		=	$App->convert($_REQUEST['amount2']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount3'])>0 && $_REQUEST['stage3']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage3']);
						$data2['amount']		=	$App->convert($_REQUEST['amount3']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount4'])>0 && $_REQUEST['stage4']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage4']);
						$data2['amount']		=	$App->convert($_REQUEST['amount4']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount5'])>0 && $_REQUEST['stage5']!='')
					{
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage5']);
						$data2['amount']		=	$App->convert($_REQUEST['amount5']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount6'])>0 && $_REQUEST['stage6']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage6']);
						$data2['amount']		=	$App->convert($_REQUEST['amount6']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount7'])>0 && $_REQUEST['stage7']!='')
					{
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage7']);
						$data2['amount']		=	$App->convert($_REQUEST['amount7']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount8'])>0 && $_REQUEST['stage8']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage8']);
						$data2['amount']		=	$App->convert($_REQUEST['amount8']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount9'])>0 && $_REQUEST['stage9']!='')
					{
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage9']);
						$data2['amount']		=	$App->convert($_REQUEST['amount9']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount10'])>0 && $_REQUEST['stage10']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage10']);
						$data2['amount']		=	$App->convert($_REQUEST['amount10']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount11'])>0 && $_REQUEST['stage11']!='')
					{
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage11']);
						$data2['amount']		=	$App->convert($_REQUEST['amount11']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount12'])>0 && $_REQUEST['stage12']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage12']);
						$data2['amount']		=	$App->convert($_REQUEST['amount12']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount13'])>0 && $_REQUEST['stage13']!='')
					{
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage13']);
						$data2['amount']		=	$App->convert($_REQUEST['amount13']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount14'])>0 && $_REQUEST['stage14']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage14']);
						$data2['amount']		=	$App->convert($_REQUEST['amount14']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount15'])>0 && $_REQUEST['stage15']!='')
					{
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage15']);
						$data2['amount']		=	$App->convert($_REQUEST['amount15']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount16'])>0 && $_REQUEST['stage16']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage16']);
						$data2['amount']		=	$App->convert($_REQUEST['amount16']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount17'])>0 && $_REQUEST['stage17']!='')
					{
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage17']);
						$data2['amount']		=	$App->convert($_REQUEST['amount17']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount18'])>0 && $_REQUEST['stage18']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage18']);
						$data2['amount']		=	$App->convert($_REQUEST['amount18']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount19'])>0 && $_REQUEST['stage19']!='')
					{
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage19']);
						$data2['amount']		=	$App->convert($_REQUEST['amount19']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount20'])>0 && $_REQUEST['stage20']!='')
					{	
						$data2['vehicleId']		=	$App->convert($vehicleId);
						$data2['stageName']		=	$App->convert($_REQUEST['stage20']);
						$data2['amount']		=	$App->convert($_REQUEST['amount20']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					
															
					$data3['stageNo']			=	$App->convert($i);															
													
					$success=$db->query_update(TABLE_VEHICLEREG, $data3, "ID='{$vehicleId}'");				
					$db->close();
									
					if($success)
					{
					$_SESSION['msg']="Vehicle Details added successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}	
				}
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid		=	$_REQUEST['fid'];  				
		
		if(!$_POST['routeNo'])
			{				
				$_SESSION['msg']="Error, Invalid Route No!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$success=0;
				
				$routeNo=$App->convert($_REQUEST['routeNo']);
				$existId=$db->existValuesId(TABLE_VEHICLEREG,"routeNo='$routeNo' AND ID!='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Route Number already Exist!";					
					header("location:edit.php?id=$fid");				
				}
				else
				{		
									
					$data['routeNo']			=	$App->convert($_REQUEST['routeNo']);
					$data['routeName']			=	$App->convert($_REQUEST['routeName']);
					$data['vehicle']			=	$App->convert($_REQUEST['vehicleNo']);
					$data['stageNo']			=	$App->convert($_REQUEST['stageNo']);				
					$data['loginId']			=	$App->convert($uid);								
													
					$success=$db->query_update(TABLE_VEHICLEREG, $data, "ID='{$fid}'");				
					$sql = "DELETE FROM `".TABLE_STAGEDETAILS."` WHERE vehicleId='$fid'";
					$db->query($sql);
					//die;
					$i=0;
					if($App->convert($_REQUEST['amount1'])>0 && $_REQUEST['stage1']!='' )
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage1']);
						$data2['amount']		=	$App->convert($_REQUEST['amount1']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount2'])>0 && $_REQUEST['stage2']!='')
					{
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage2']);
						$data2['amount']		=	$App->convert($_REQUEST['amount2']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount3'])>0 && $_REQUEST['stage3']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage3']);
						$data2['amount']		=	$App->convert($_REQUEST['amount3']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount4'])>0 && $_REQUEST['stage4']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage4']);
						$data2['amount']		=	$App->convert($_REQUEST['amount4']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount5'])>0 && $_REQUEST['stage5']!='')
					{
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage5']);
						$data2['amount']		=	$App->convert($_REQUEST['amount5']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount6'])>0 && $_REQUEST['stage6']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage6']);
						$data2['amount']		=	$App->convert($_REQUEST['amount6']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount7'])>0 && $_REQUEST['stage7']!='')
					{
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage7']);
						$data2['amount']		=	$App->convert($_REQUEST['amount7']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount8'])>0 && $_REQUEST['stage8']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage8']);
						$data2['amount']		=	$App->convert($_REQUEST['amount8']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount9'])>0 && $_REQUEST['stage9']!='')
					{
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage9']);
						$data2['amount']		=	$App->convert($_REQUEST['amount9']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount10'])>0 && $_REQUEST['stage10']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage10']);
						$data2['amount']		=	$App->convert($_REQUEST['amount10']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount11'])>0 && $_REQUEST['stage11']!='')
					{
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage11']);
						$data2['amount']		=	$App->convert($_REQUEST['amount11']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount12'])>0 && $_REQUEST['stage12']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage12']);
						$data2['amount']		=	$App->convert($_REQUEST['amount12']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount13'])>0 && $_REQUEST['stage13']!='')
					{
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage13']);
						$data2['amount']		=	$App->convert($_REQUEST['amount13']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount14'])>0 && $_REQUEST['stage14']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage14']);
						$data2['amount']		=	$App->convert($_REQUEST['amount14']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount15'])>0 && $_REQUEST['stage15']!='')
					{
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage15']);
						$data2['amount']		=	$App->convert($_REQUEST['amount15']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount16'])>0 && $_REQUEST['stage16']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage16']);
						$data2['amount']		=	$App->convert($_REQUEST['amount16']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount17'])>0 && $_REQUEST['stage17']!='')
					{
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage17']);
						$data2['amount']		=	$App->convert($_REQUEST['amount17']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount18'])>0 && $_REQUEST['stage18']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage18']);
						$data2['amount']		=	$App->convert($_REQUEST['amount18']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount19'])>0 && $_REQUEST['stage19']!='')
					{
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage19']);
						$data2['amount']		=	$App->convert($_REQUEST['amount19']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					if($App->convert($_REQUEST['amount20'])>0 && $_REQUEST['stage20']!='')
					{	
						$data2['vehicleId']		=	$App->convert($fid);
						$data2['stageName']		=	$App->convert($_REQUEST['stage20']);
						$data2['amount']		=	$App->convert($_REQUEST['amount20']);					
						$data2['loginId']		=	$App->convert($uid);
						$db->query_insert(TABLE_STAGEDETAILS, $data2);
						$i++;
					}
					
															
					$data3['stageNo']			=	$App->convert($i);															
													
					$db->query_update(TABLE_VEHICLEREG, $data3, "ID='{$fid}'");		
					$db->close();					
					
					if($success)
					{
					$_SESSION['msg']="Stage Details Updated successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}														
				}	
			}	
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];
				$success=0;
				$success2=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();									
				
				/*$sql = "DELETE FROM `".TABLE_STAGEDETAILS."` WHERE vehicleId='{$id}'";
				$success=$db->query($sql);
				
				$sql = "DELETE FROM `".TABLE_VEHICLEREG."` WHERE ID='{$id}'";
				$success2=$db->query($sql);*/
				
				try
				{
				$success1= @mysql_query("DELETE FROM `".TABLE_STAGEDETAILS."` WHERE vehicleId='{$id}'");	
				//echo "DELETE FROM `".TABLE_STAGEDETAILS."` WHERE vehicleId='{$id}'";
				//echo "DELETE FROM `".TABLE_VEHICLEREG."` WHERE ID='{$id}'";die;
				$success2=@mysql_query("DELETE FROM `".TABLE_VEHICLEREG."` WHERE ID='{$id}'");		      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}
				
				$db->close(); 								
				
				 	if($success1 && $success2)
					{
					$_SESSION['msg']="Vehicle Details deleted successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";	
					header("location:new.php");					
					}					
		break;		
}
?>
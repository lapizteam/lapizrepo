<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
$tableView="SELECT * FROM ".TABLE_RETURN_DAYS."" ;
$table_res=$db->query($tableView);
$table_row=mysql_fetch_array($table_res);

 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>

<script>
//validation msg
function valid()
{
flag=false;

jDays=document.getElementById('days').value;	
	if(isNaN(jDays))
	{		
	document.getElementById('daysdiv').innerHTML="Enter valid days";
	flag=true;
	}
	
jnoOfBooks=document.getElementById('noOfBooks').value;	
	if(isNaN(jnoOfBooks))
	{		
	document.getElementById('noOfBooksdiv').innerHTML="Enter valid number.";
	flag=true;
	}
	
	
if(flag==true)
	{
	return false;
	}
}
//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>
      <!-- Modal1 -->
 
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="../adminDash/adminDash.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">BOOK HANDLING DAYS REGISTRATION</h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=edit" class="form1" method="post" onsubmit="return valid()">			
                <div class="row">
                  <div class="col-sm-6">
                     <div class="form-group">
                      <label for="oldUserName">Book Handling Days Number:<span class="star">*</span></label>
					  <input type="text" id="days" name="days" value="<?php echo $table_row['noOfDays']; ?>" required title="Number of Days" placeholder="Book Handling Days" class="form-control2" onfocus="clearbox('daysdiv')" />
					  <div id="daysdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
					
					<div class="form-group">
                      <label for="newUserName">Maximum Number Of Books TO Be Issue:<span class="star">*</span></label>
					  <input type="text" id="noOfBooks" name="noOfBooks" value="<?php echo $table_row['noOfBooks']; ?>"  required title="Number of Books"  placeholder="Maximum Number Of Books" class="form-control2" onfocus="clearbox('noOfBooksdiv')" />
					   <div id="noOfBooksdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
				</div>	
				
                   </div>  				             
                </div>
              
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>

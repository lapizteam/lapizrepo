<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['days'] || !$_POST['noOfBooks'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$data['noOfDays']			=	$App->convert($_REQUEST['days']);
				$data['noOfBooks']			=	$App->convert($_REQUEST['noOfBooks']);
				
				$success=$db->query_update(TABLE_RETURN_DAYS, $data);					
     			$db->close();
								
				if($success)
					{					
					$_SESSION['msg']="Details are updated successfully";							
					}
					else
					{
					$_SESSION['msg']="Failed";								
					}
					header("location:new.php");														
			}		
		break;		
			
}
?>
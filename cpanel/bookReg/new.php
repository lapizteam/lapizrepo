<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$type		=	$_SESSION['LogType'];
$libraryEdit	=	$_SESSION['libraryEdit'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

//book no check-exist or not

$(document).ready(function() {
$("#bookNo").keyup(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var bookNo = $(this).val(); //get the string typed by user
   $("#user-result").html('<img src="ajax-loader.gif" />');
   $.post('check_book.php', {'bookNo':bookNo}, function(data) { //make ajax call to check_username.php
   $("#user-result").html(data); //dump the data received from PHP page
   });
});
});

</script>
<script>
//validation msg
function valid()
{
flag=false;

jPrice=document.getElementById('price').value;	
	if(isNaN(jPrice) && jPrice!='')
	{		
	document.getElementById('pricediv').innerHTML="Check price.";
	flag=true;
	}
if(flag==true)
	{
	return false;
	}
}
//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>
<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">BOOK REGISTRATION</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4">
            <form method="post">
              <div class="input-group">
			  <input id="search_book" name="search_book" value="<?php echo @$_POST['search_book'] ?>"  type="text" class="form-control" placeholder="Book Name" />
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['search_book'])
{
	$cond=$cond." and ".TABLE_BOOK_REG.".bookName like'%".$_POST['search_book']."%'";
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table  view_limitter pagination_table" >
                <thead>
                  <tr >
				    <th>Sl No</th>               
					<th>Book No</th>
					<th>Book Name</th>
					<th>Author</th>	
					<th>Availability</th>				
										
                  </tr>
                </thead>
                <tbody>
				<?php 															
						$selAllQuery="SELECT  ".TABLE_BOOK_REG.".ID,".TABLE_BOOK_REG.".regDate,".TABLE_BOOK_REG.".bookNo,".TABLE_BOOK_REG.".bookName,".TABLE_BOOK_REG.".author,".TABLE_BOOK_REG.".publisher,".TABLE_BOOK_CATEGORY.".category,".TABLE_BOOK_REG.".price,".TABLE_RACK_REG.".rackNo,".TABLE_BOOK_REG.".remark ,".TABLE_BOOK_REG.".availability FROM ".TABLE_BOOK_REG.",".TABLE_BOOK_CATEGORY.",".TABLE_RACK_REG." WHERE ".TABLE_BOOK_CATEGORY.".ID=".TABLE_BOOK_REG.".category and ".TABLE_RACK_REG.".ID=".TABLE_BOOK_REG.".rack AND $cond ORDER BY ID DESC";
											
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="5">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{							
							$i=1;
							while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
					   <td><?php echo $i++;?>
						  <div class="adno-dtls"> <?php if($libraryEdit || $type=="Admin"){?> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  | <?php }?> <a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a> </div>
						  
							<!-- Modal3 -->
							  <div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
								<div class="modal-dialog">
								  <div class="modal-content"> 
									<!-- <div class="modal-body clearfix">
								fddhdhdhddhdh
								  </div>-->	 		
									<div role="tabpanel" class="tabarea2"> 
									  
									  <!-- Nav tabs -->
									 <ul class="nav nav-tabs" role="tablist">									  
										<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Book Details</a> </li>										
									</ul>
									  
									  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
										  <table class="table nobg">
											<tbody style="background-color:#FFFFFF">
						
											   <tr>
												<td>Book Register Date</td>
												<td> : </td>
												<td><?php echo $App->dbFormat_date_db($row['regDate']); ?></td>
											  </tr>
											  <tr>
												<td>Book No</td>
												<td> : </td>
												<td><?php echo $row['bookNo']; ?></td>
											  </tr>
											  <tr>
												<td>Book Name</td>
												<td> : </td>
												<td><?php echo $row['bookName']; ?></td>
											  </tr>
											  <tr>
												<td>Author</td>
												<td> : </td>
												<td><?php echo $row['author']; ?></td>
											  </tr>
											  <tr>
												<td>Publisher</td>
												<td> : </td>
												<td><?php echo $row['publisher']; ?></td>
											  </tr>
											  <tr>
												<td>Category</td>
												<td> : </td>
												<td><?php echo $row['category']; ?></td>
											  </tr>
											   <tr>
												<td>Price</td>
												<td> : </td>
												<td><?php echo $row['price']; ?></td>
											  </tr>	
											   <tr>
												<td>Rack Number</td>
												<td> : </td>
												<td><?php echo $row['rackNo']; ?></td>
											  </tr>
                                             <tr>
												<td>Remark</td>
												<td> : </td>
												<td><?php echo $row['remark']; ?></td>
											  </tr>
										   	  
											</tbody>
										  </table>
										</div>																	
									  </div>
									</div>
								  </div>
								</div>
							  </div>
							  <!-- Modal3 cls --> 
						  
						  
						  </td>
						<td><?php echo $row['bookNo']; ?></td>
						<td><?php echo $row['bookName']; ?></td>						
						<td><?php echo $row['author']; ?> </td>
						<td><?php echo $row['availability']; ?></td>                 	 	
					  </tr>
					  <?php }
				}?>                  
                </tbody>
              </table>			  	 
            </div>
			<!-- paging -->		
			<div style="clear:both;"></div>
			   <div class="text-center">
			   		<div class="btn-group pager_selector"></div>
			   </div>
          	</div>
			<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">BOOK REGISTRATION </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="regDate">Date:<span class="star">*</span></label>
					  <input type="text" id="regDate" name="regDate" value="<?php echo date("d/m/Y");?>"   class="form-control2 datepicker" required />                    					  
                    </div>
                    <div class="form-group">
                      <label for="bookNo">Book Number: <span class="star">*</span></label>
					   <input type="text" id="bookNo" name="bookNo" value="" required placeholder="Book Number:"  title="Book Number" class="form-control2"/>
					   <span id="user-result"></span>
                    </div>
                    <div class="form-group">
                      <label for="bookName">Book Name:<span class="star">*</span></label>   
					  <input type="text" id="bookName" name="bookName" value="" required  title="Book Name" placeholder="Book Name" class="form-control2" />                 
					  
                    </div>					                   								 
                  
                    <div class="form-group">
                      <label for="author">Author:</label>
					  <input type="text" id="author" name="author" value=""  placeholder="Author Name" class="form-control2" />
                    
                    </div>									  	
					
					<div class="form-group">
                      <label for="publisher">Publisher:</label>   
					  <input type="text" id="publisher" name="publisher" value=""  placeholder="Publisher Name" class="form-control2" />                   
						 				   
                    </div>									
					
					<div class="form-group">
                      <label for="category">Category:<span class="star">*</span></label>
					   <select name="category" id="category"  title="Category" required class="form-control2">
						<option value="">Select</option>
                   <?php 
						$select1="select * from ".TABLE_BOOK_CATEGORY."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>"><?php echo $row['category']?></option>
						<?php 
						}
						?>
                    </select>  
                    </div>
										
					<div class="form-groupy">
                      <label for="price">Price:</label>
					  <input type="text" id="price" name="price" value="0" placeholder="Price" class="form-control2 " onfocus="clearbox('pricediv')" />	
					  <div id="pricediv" class="valid" style="color:#FF6600;"></div>	  
                    </div>					
					
					<div class="form-groupy">
                      <label for="rack">Rack:<span class="star">*</span> </label>
						 <select name="rack" id="rack"  title="Rack" required class="form-control2">
						<option value="">Select</option>
                   <?php 
						$select1="select * from ".TABLE_RACK_REG."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>"><?php echo $row['rackNo']?></option>
						<?php 
						}
						?>
                    </select>		  
                    </div>								
                    <div class="form-group">
                      <label for="remark">Remark:</label>
                        <textarea id="remark" name="remark" value="" style=" width:265px"  placeholder="Remark" class="form-control2" ></textarea>
                    </div>
				</div>
				 </div>
			
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
	  </div>
      <!-- Modal1 cls --> 
     
	 
	 
	  
  
  </div>
<?php include("../adminFooter.php") ?>

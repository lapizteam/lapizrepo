<?php include("../adminHeader.php"); 
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<script>
function valid()
{
jPrice=document.getElementById('price').value;	
	if(isNaN(jPrice) && jPrice!='')
	{		
	document.getElementById('pricediv').innerHTML="Check price.";
	flag=true;
	}

	if(flag==true)
	{
	return false;
	}
																				
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}


</script>


<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$fid=$_REQUEST['id'];
        $fid = mysql_real_escape_string($fid);

	$tableEdit="SELECT  ".TABLE_BOOK_REG.".ID,".TABLE_BOOK_REG.".regDate,".TABLE_BOOK_REG.".bookNo,".TABLE_BOOK_REG.".bookName,".TABLE_BOOK_REG.".author,".TABLE_BOOK_REG.".publisher,".TABLE_BOOK_REG.".category,".TABLE_BOOK_REG.".price,".TABLE_BOOK_REG.".rack,".TABLE_BOOK_REG.".remark FROM ".TABLE_BOOK_REG.",".TABLE_BOOK_CATEGORY.",".TABLE_RACK_REG." WHERE ".TABLE_BOOK_CATEGORY.".ID=".TABLE_BOOK_REG.".category and ".TABLE_RACK_REG.".ID=".TABLE_BOOK_REG.".rack and ".TABLE_BOOK_REG.".ID='$fid'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);

?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">BOOK UPDATION </h4>
            </div>
            <div class="modal-body clearfix">
			<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
			<input type="hidden" name="fid" id="fid" value="<?php echo $fid ?>">
			             
                <div class="row">
                 
				 <div class="col-sm-6">
                    <div class="form-group">
                      <label for="regDate">Date: <span class="star">*</span></label>
					  <input type="text" id="regDate" name="regDate" value="<?php echo $App->dbFormat_date_db($editRow['regDate']);?>" required  class="form-control2 datepicker " />
                    
					  <span id="user-result"></span>
                    </div>
                    <div class="form-group">
                      <label for="bookNo">Book Number: </label>
					   <input type="text" id="bookNo" name="bookNo" value="<?php echo $editRow['bookNo'];?>" placeholder="Book Number:"  title="Book Number" class="form-control2" readonly="readonly"/>
					  
                    </div>
                    <div class="form-group">
                      <label for="bookName">Book Name:<span class="star">*</span></label>   
					  <input type="text" id="bookName" name="bookName" value="<?php echo $editRow['bookName'];?>" required title="Book Name" placeholder="Book Name" class="form-control2" />                 
					  
                    </div>					                   								 
                  
                    <div class="form-group">
                      <label for="author">Author:</label>
					  <input type="text" id="author" name="author" value="<?php echo $editRow['author'];?>"  placeholder="Author Name" class="form-control2" />
                    
                    </div>									  	
					
					<div class="form-group">
                      <label for="publisher">Publisher:</label>   
					  <input type="text" id="publisher" name="publisher" value="<?php echo $editRow['publisher'];?>"  placeholder="Publisher Name" class="form-control2" />                       </div>									
					<div class="form-group">
                      <label for="category">Category:<span class="star">*</span></label>
					   <select name="category" id="category" required title="Category" class="form-control2">
						<option value="">Select</option>
                   <?php 
						$select1="select * from ".TABLE_BOOK_CATEGORY."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>"<?php if($row['ID']==$editRow['category']){?> selected="selected"<?php }?> ><?php echo $row['category']?></option>
						<?php 
						}
						?>
                    </select>  
                    </div>
										
					<div class="form-groupy">
                      <label for="price">Price:</label>
					  <input type="text" id="price" name="price" value="<?php echo $editRow['price'];?>" placeholder="Price" class="form-control2 " onfocus="clearbox('pricediv')"/>
					   <div id="pricediv" class="valid" style="color:#FF6600;"></div>		  
                    </div>					
					
					<div class="form-groupy">
                      <label for="rack">Rack:<span class="star">*</span> </label>
						 <select name="rack" id="rack"  title="Rack" required class="form-control2">
						<option value="">Select</option>
                   <?php 
						$select1="select * from ".TABLE_RACK_REG."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>" <?php if($row['ID']==$editRow['rack']){?> selected="selected"<?php }?> ><?php   echo $row['rackNo']?></option>
						<?php 
						}
						?>
                    </select>		  
                    </div>								
                    <div class="form-group">
                      <label for="remark">Remark:</label>
                        <textarea id="remark" name="remark" value=""    placeholder="Remark" class="form-control2" ><?php echo $editRow['remark'];?></textarea>
                    </div>
				</div>					 																								
                </div>	
		</div>
	 <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>

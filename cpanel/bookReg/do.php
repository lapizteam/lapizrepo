<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$uid = $_SESSION['LogID'];


$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{

	// NEW SECTION
	//-
	case 'new':
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();		
		
		if(!$_POST['bookNo'])
			{
				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{
				$success=0;
				$bookNo=$_REQUEST['bookNo'];
			
				$existId=$db->existValuesId(TABLE_BOOK_REG," bookNo='$bookNo'");
				if($existId>0)
				{						
					$_SESSION['msg']="Book Number Is already Exist !!";					
					header("location:new.php");					
				}
				else
				{								
				$data['regDate']				=	$App->dbFormat_date($_REQUEST['regDate']);
				$data['bookNo']					=	$App->convert($_REQUEST['bookNo']);
				$data['bookName']				=	$App->convert($_REQUEST['bookName']);
				$data['author']					=	$App->convert($_REQUEST['author']);
				$data['publisher']				=	$App->convert($_REQUEST['publisher']);
				$data['category']				=	$App->convert($_REQUEST['category']);
				$data['price']					=	$App->convert($_REQUEST['price']);
				$data['rack']					=	$App->convert($_REQUEST['rack']);
				$data['remark']					=	$App->convert($_REQUEST['remark']);
				$data['loginId']				=	$App->convert($uid);
				$data['availability']			=	'available';
 				                						 				                										
				$success=$db->query_insert(TABLE_BOOK_REG,$data);			
				$db->close();
				
					if($success)
					{
					$_SESSION['msg']="Book Added Successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}		
				}
			}
		
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];		
       
		if(!$_POST['bookNo'])
			{
					
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$bookNo=$_REQUEST['bookNo'];
			
				$existId=$db->existValuesId(TABLE_BOOK_REG," bookNo='$bookNo' AND ID!='$fid'");

				if($existId>0)
				{					
					$_SESSION['msg']="Book Number Is already Exist !!";					
					header("location:edit.php?id=$fid");					
				}
				else
				{
				
				$data['regDate']				=	$App->dbFormat_date($_REQUEST['regDate']);
				$data['bookNo']					=	$App->convert($_REQUEST['bookNo']);
				$data['bookName']				=	$App->convert($_REQUEST['bookName']);
				$data['author']					=	$App->convert($_REQUEST['author']);
				$data['publisher']				=	$App->convert($_REQUEST['publisher']);
				$data['category']				=	$App->convert($_REQUEST['category']);
				$data['price']					=	$App->convert($_REQUEST['price']);
				$data['rack']					=	$App->convert($_REQUEST['rack']);
				$data['remark']					=	$App->convert($_REQUEST['remark']);
				$data['loginId']				=	$App->convert($uid);
 				                						
				$success=$db->query_update(TABLE_BOOK_REG, $data, "ID='{$fid}'");										 
				$db->close(); 
				 
				 if($success)
					{
					$_SESSION['msg']="Book updated successfully";					
				 	header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}		
				 
				 }
				
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				/*$sql = "DELETE FROM `".TABLE_BOOK_REG."` WHERE ID='{$id}'";
				$success=$db->query($sql);*/
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_BOOK_REG."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}
				$db->close(); 
								
				if($success)
				{
				$_SESSION['msg']="Book deleted successfully!";					
				header("location:new.php");
				}
				else
				{
				$_SESSION['msg']="You can't edit. Because this data is used some where else";	
				header("location:new.php");					
				}	
			
		
		break;
		

}
?>
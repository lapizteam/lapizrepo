<?php include("../adminHeader.php"); 
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function valid()
{
flag=false;
maxLength=9;
	amount=document.getElementById('amount').value;		
		if(isNaN(amount))
		{																			///for age
		document.getElementById('amountDiv').innerHTML="Amount should be valid.";
		flag=true;
		}
		if(amount<0)
		{																			///for age
		document.getElementById('amountDiv').innerHTML="Amount should be valid.";
		flag=true;
		}
		if(amount==0)
		{																			///for age
		document.getElementById('amountDiv').innerHTML="Amount should be valid.";
		flag=true;
		}
		if(amount.length>maxLength)
		{																			///for age
		document.getElementById('amountDiv').innerHTML="Amount should be valid.";
		flag=true;
		}
	from_ledger=document.getElementById('from_ledger').value;
	to_ledger=document.getElementById('to_ledger').value;		
		if(from_ledger==to_ledger)
		{																			///for age
		document.getElementById('to_ledgerDiv').innerHTML="Creditor & Debitor must be different";
		flag=true;
		}
	if(flag==true)
	{
	return false;
	}	
}
//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>
<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
 
$tid=$_REQUEST['id'];
$tid = mysql_real_escape_string($tid);
$sql ="SELECT * FROM `".TABLE_TRANSACTION."` WHERE `id`='$tid' AND `voucher_type`='Account Transfer' AND `debit`!='0'";
$record = $db->query_first($sql);

?>
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">ACCOUNT TRANSFER</h4>
            </div>
            <div class="modal-body clearfix">
			<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
			<input type="hidden" name="ledgerid" value="<?php echo $record['ID']; ?>" />
			             
                <div class="row">
                 <div class="col-sm-6">
                    <div class="form-group">
                      <label for="name">Date:<span class="star">*</span></label>
						 <input type="text" class="form-control2 datepicker" id="transaction_date" name="transaction_date" readonly="readonly" value="<?php echo  $App->dbformat_date_db($record['transaction_date']) ?>" required title="Date" /> 								
                    </div>
					
                    <div class="form-group">
                      <label for="father">Creditor / From:<span class="star">*</span></label>
                      <?php  
					  $from_ledger_qry= $db->query("SELECT * FROM `".TABLE_LEDGER."`");  
					  ?>
						  <select name="from_ledger" class="form-control2" id="from_ledger" required title="Select Creditor / From">						
							  <?php     
							  while($from_ledger_row=mysql_fetch_array($from_ledger_qry))					
							  {					 
							  ?>  
								  <option value="<?php echo $from_ledger_row['ID']; ?>" <?php if($from_ledger_row['ID']==$record['from_ledger']) { ?> selected="selected" <?php } ?>><?php echo $from_ledger_row['name']; ?></option>
							  <?php  
							  }  
							  ?>
	                  </select>				
                    </div>
                   
                    <div class="form-group">
                      <label for="dob">Debitor / To:<span class="star">*</span></label>                    
					  <?php  
					  $to_ledger_qry= $db->query("SELECT * FROM `".TABLE_LEDGER."`");
					  ?>
					  <select name="to_ledger" class="form-control2" id="to_ledger" required title="Select Debitor / To"onfocus="clearbox('to_ledgerDiv')">	                  
	                   <?php    
					   while($to_ledger_row=mysql_fetch_array($to_ledger_qry))					 
					   {					 
					   ?>  
					  	<option value="<?php echo $to_ledger_row['ID']; ?>" <?php if($to_ledger_row['ID']==$record['to_ledger']) { ?> selected="selected" <?php } ?>  ><?php echo $to_ledger_row['name']; ?></option>
					  <?php 
					  }  
					  ?>
					  
	                  </select>
					  <div id="to_ledgerDiv" class="valid" style="color:#FF6600"></div> 
                    </div>					                   								 
                  
                    <div class="form-group">
                      <label for="nationality">Paid Amount:<span class="star">*</span></label>
                      <input name="amount" type="text" class="form-control2" id="amount" required title="Paid Amount" value="<?php echo $record['debit']; ?>" onfocus="clearbox('amountDiv')" >
					   <div id="amountDiv" class="valid" style="color:#FF6600"></div> 
                    </div>									  	
					
					<div class="form-group">
                      <label for="mobile">Remark:</label>                      
						 <textarea name="remark" class="form-control2" id="remark"><?php echo $record['remark']; ?></textarea>  				   
                    </div>																			
				</div>			 																								
                </div>	
		</div>
	 <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>

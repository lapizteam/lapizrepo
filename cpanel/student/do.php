<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

//$uid = $_SESSION['vfw_cpasecpass'];
//$proid = $_SESSION['pro'];
switch($optype)
{

	// NEW SECTION
	//-
	case 'new':
		
		if(!$_POST['name'] || !$_POST['ad_no'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");	
			}
		else
			{
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$success=0;
				$adm=$App->convert($_REQUEST['ad_no']);
				$existId=$db->existValuesId(TABLE_STUDENT," adNo='$adm'");

				if($existId>0)
				{	
					$_SESSION['msg']="Admission Number already Exist !!";					
					header("location:new.php");												
				}
				else
				{				
				$data['adDate']					=	$App->dbFormat_date($_REQUEST['ad_date']);
				$data['adNo']					=	$App->convert($_REQUEST['ad_no']);
				$data['name']					=	$App->convert($_REQUEST['name']);
				$data['class']					=	$App->convert($_REQUEST['class']);
				$data['division']				=	$App->convert($_REQUEST['division']);
				$data['address']				=	$App->convert($_REQUEST['address']);
				$data['district']				=	$App->convert($_REQUEST['district']);
				$data['pin']					=	$App->convert($_REQUEST['pin']);
				$data['phone']					=	$App->convert($_REQUEST['phone']);
 				$data['mobile']					=	$App->convert($_REQUEST['mobile']);
				$data['sex']					=	$App->convert($_REQUEST['gender']);
				$data['dob']					=	$App->dbFormat_date($_REQUEST['dob']);
				$data['age']					=	$App->convert($_REQUEST['age']);
				$data['placeOfBirth']			=	$App->convert($_REQUEST['placeBirth']);
				$data['motherTongue']			=	$App->convert($_REQUEST['motherTongue']);
				$data['religion']				=	$App->convert($_REQUEST['religion']);
				$data['caste']					=	$App->convert($_REQUEST['caste']);
				$data['nationality']			=	$App->convert($_REQUEST['nation']);
				$data['bloodGroup']				=	$App->convert($_REQUEST['blood']);
				$data['idMark']					=	$App->convert($_REQUEST['idMark']);
				$data['father']					=	$App->convert($_REQUEST['father']);
				$data['fOccup']					=	$App->convert($_REQUEST['fatherOccup']);
				$data['fQuali']					=	$App->convert($_REQUEST['fatherQuali']);
				$data['fOfficeAddr']			=	$App->convert($_REQUEST['fatherOfficeAddr']);
				$data['fDistrict']				=	$App->convert($_REQUEST['fatherDistrict']);
				$data['fPin']					=	$App->convert($_REQUEST['fatherPincode']);
				$data['fPhone']					=	$App->convert($_REQUEST['fatherPhone']);
				$data['fMobile']				=	$App->convert($_REQUEST['fatherMobile']);
				$data['fEmail']					=	$App->convert($_REQUEST['fatherEmail']);
				$data['mother']					=	$App->convert($_REQUEST['mother']);
				$data['mOccup']					=	$App->convert($_REQUEST['motherOccup']);
				$data['mQuali']					=	$App->convert($_REQUEST['motherQuali']);
				$data['mOfficeAddr']			=	$App->convert($_REQUEST['motherOfficeAddr']);
 				$data['mDistrict']				=	$App->convert($_REQUEST['motherDistrict']);
				$data['mPhone']					=	$App->convert($_REQUEST['motherPhone']);
				$data['mMobile']				=	$App->convert($_REQUEST['motherMobile']);
				$data['mEmail']					=	$App->convert($_REQUEST['motherEmail']);
				$data['sibling']				=	$App->convert($_REQUEST['siblings']);
				$data['guardian']				=	$App->convert($_REQUEST['guardian']);
				$data['gAddress']				=	$App->convert($_REQUEST['guardianAddr']);
				$data['gDistrict']				=	$App->convert($_REQUEST['guardianDistrict']);
				$data['gMobile']				=	$App->convert($_REQUEST['guardianMobile']);
				$data['gEmail']					=	$App->convert($_REQUEST['guardianEmail']);
				$data['boardingPoint']			=	$App->convert($_REQUEST['boardingPoint']);				
				$data['acYear']					=	$App->convert($_REQUEST['year']);		
				$data['loginId']				=	$App->convert($uid);
						
				//echo $_FILES["photo"]["name"];die;
				$newName = $_REQUEST['ad_no']."_".$_FILES["photo"]["name"];	
				if(move_uploaded_file($_FILES["photo"]["tmp_name"],"uploads/".basename($newName)))
					{
						$img="uploads/" . $newName;		
					}
							
				$data['photo']			=	$App->convert($img);
				
				$success=$db->query_insert(TABLE_STUDENT, $data);
				//echo TABLE_STUDENT, $data;die;			
				$db->close();				
				
				if($success)
					{
					$_SESSION['msg']="Student added successfully";					
					header("location:new.php");	
					}
				else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}		
				}
			}
		
		break;
		
	// EDIT SECTION
	//-
	case 'edit':
		
		$fid	=	$_REQUEST['fid'];
		
       
		if(!$_POST['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$adm=$App->convert($_REQUEST['ad_no']);
				$existId=$db->existValuesId(TABLE_STUDENT," adNo='$adm' AND ID!='{$fid}'");

				if($existId>0)
				{
					$_SESSION['msg']="Admission Number already Exist !!";					
					header("location:new.php");					
				}
				else
				{					
				$data['adDate']					=	$App->dbFormat_date($_REQUEST['ad_date']);
				$data['adNo']					=	$App->convert($_REQUEST['ad_no']);
				$data['name']					=	$App->convert($_REQUEST['name']);
				$data['class']					=	$App->convert($_REQUEST['class']);
				$data['division']				=	$App->convert($_REQUEST['division']);
				$data['address']				=	$App->convert($_REQUEST['address']);
				$data['district']				=	$App->convert($_REQUEST['district']);
				$data['pin']					=	$App->convert($_REQUEST['pin']);
				$data['phone']					=	$App->convert($_REQUEST['phone']);
 				$data['mobile']					=	$App->convert($_REQUEST['mobile']);
				$data['sex']					=	$App->convert($_REQUEST['gender']);
				$data['dob']					=	$App->dbFormat_date($_REQUEST['dob']);
				$data['age']					=	$App->convert($_REQUEST['age']);
				$data['placeOfBirth']			=	$App->convert($_REQUEST['placeBirth']);
				$data['motherTongue']			=	$App->convert($_REQUEST['motherTongue']);
				$data['religion']				=	$App->convert($_REQUEST['religion']);
				$data['caste']					=	$App->convert($_REQUEST['caste']);
				$data['nationality']			=	$App->convert($_REQUEST['nation']);
				$data['bloodGroup']				=	$App->convert($_REQUEST['blood']);
				$data['idMark']					=	$App->convert($_REQUEST['idMark']);
				$data['father']					=	$App->convert($_REQUEST['father']);
				$data['fOccup']					=	$App->convert($_REQUEST['fatherOccup']);
				$data['fQuali']					=	$App->convert($_REQUEST['fatherQuali']);
				$data['fOfficeAddr']			=	$App->convert($_REQUEST['fatherOfficeAddr']);
				$data['fDistrict']				=	$App->convert($_REQUEST['fatherDistrict']);
				$data['fPin']					=	$App->convert($_REQUEST['fatherPincode']);
				$data['fPhone']					=	$App->convert($_REQUEST['fatherPhone']);
				$data['fMobile']				=	$App->convert($_REQUEST['fatherMobile']);
				$data['fEmail']					=	$App->convert($_REQUEST['fatherEmail']);
				$data['mother']					=	$App->convert($_REQUEST['mother']);
				$data['mOccup']					=	$App->convert($_REQUEST['motherOccup']);
				$data['mQuali']					=	$App->convert($_REQUEST['motherQuali']);
				$data['mOfficeAddr']			=	$App->convert($_REQUEST['motherOfficeAddr']);
 				$data['mDistrict']				=	$App->convert($_REQUEST['motherDistrict']);
				$data['mPhone']					=	$App->convert($_REQUEST['motherPhone']);
				$data['mMobile']				=	$App->convert($_REQUEST['motherMobile']);
				$data['mEmail']					=	$App->convert($_REQUEST['motherEmail']);
				$data['sibling']				=	$App->convert($_REQUEST['siblings']);
				$data['guardian']				=	$App->convert($_REQUEST['guardian']);
				$data['gAddress']				=	$App->convert($_REQUEST['guardianAddr']);
				$data['gDistrict']				=	$App->convert($_REQUEST['guardianDistrict']);
				$data['gMobile']				=	$App->convert($_REQUEST['guardianMobile']);
				$data['gEmail']					=	$App->convert($_REQUEST['guardianEmail']);
				$data['boardingPoint']			=	$App->convert($_REQUEST['boardingPoint']);
				$data['acYear']					=	$App->convert($_REQUEST['year']);
				$data['loginId']				=	$App->convert($uid);	
					
				$changePhoto			=	$_REQUEST['change'];
				if($changePhoto=='yes')
				{
					// upload file	
					$newName = $_REQUEST['ad_no']."_".$_FILES["photo"]["name"];		
					if(move_uploaded_file($_FILES["photo"]["tmp_name"],"uploads/".basename($newName)))
					{
						$img="uploads/" .$newName;		
					}							
				$data['photo']			=	$App->convert($img);
				}			
				
				
					$success=$db->query_update(TABLE_STUDENT, $data, "ID='{$fid}'");										 
					$db->close(); 				
					
					if($success)
					{
					$_SESSION['msg']="Student updated successfully";					
					header("location:new.php");		
					}
				else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}		
				}
									
			}
		
		break;
		
	// DELETE SECTION
	//-
	case 'delete':
		
				$id	=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				try
				{			
				$success= @mysql_query("DELETE FROM `".TABLE_STUDENT."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}	
				/*$sql = "DELETE FROM `".TABLE_STUDENT."` WHERE ID='{$id}'";
				$success=$db->query($sql);*/
				$db->close(); 		
				
				if($success)
					{
					$_SESSION['msg']="Student deleted successfully";										
					}
				else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";	
									
					}					
			header("location:new.php");	
		break;
		

}
?>
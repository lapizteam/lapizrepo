<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$type			=	$_SESSION['LogType'];
$studentEdit	=	$_SESSION['studentEdit'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
</script>
<script>
function valid()
{
flag=false;
var reguler1 =/^[a-zA-Z ]*$/;
var rphone = /^(\d+)+(\d+)$/;			
var rmobile =/^(?:(?:\+|0{0,2})91(\s\s*)?|[0]?)?[789]\d{9}$/; 
var remail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;

	 jName=document.getElementById('name').value;
	 	
	  	if(!jName.match(reguler1))
		{		 
		document.getElementById('namediv').innerHTML="Enter valid name";
		flag=true;
		}
	jPin=document.getElementById('pin').value;	
		
		if((isNaN(jPin)||jPin.length!=6) && jPin!=0)
		{		
		document.getElementById('pindiv').innerHTML="Enter valid pin number.";
		flag=true;
		}
		
	jPhone=document.getElementById('phone').value;
		if(!jPhone.match(rphone) && jPhone!='')
		{	
		document.getElementById('phonediv').innerHTML="Enter valid phone number.";
		flag=true;
		}
		
	jMobile=document.getElementById('mobile').value;
		if(!jMobile.match(rmobile) && jMobile!='')
		{	
		document.getElementById('mobilediv').innerHTML="Enter valid Mobile number.";
		flag=true;
		}
		
	jFather=document.getElementById('father').value;
	  	if(!jFather.match(reguler1) && jFather!='')
		{		
		document.getElementById('fatherdiv').innerHTML="Enter valid name";
		flag=true;
		}
		
	jmother=document.getElementById('mother').value;
	  	if(!jmother.match(reguler1)&& jmother!='' )
		{		 
		document.getElementById('motherdiv').innerHTML="Enter valid name";
		flag=true;
		}
		
	jFPin=document.getElementById('fatherPincode').value;	
		if(isNaN(jFPin) && jFPin!='')
		{		
		document.getElementById('fatherPincodediv').innerHTML="Enter valid pin number";
		flag=true;
		}
		
	jFPhone=document.getElementById('fatherPhone').value;
		if(!jFPhone.match(rphone) && jFPhone!='')
		{	
		document.getElementById('fatherPhonediv').innerHTML="Enter valid phone number.";
		flag=true;
		}
		
	jFMobile=document.getElementById('fatherMobile').value;
		 
		if(!jFMobile.match(rmobile) && jFMobile!='')
		{	
		document.getElementById('fatherMobilediv').innerHTML="Enter valid Mobile number.";
		flag=true;
		}
		
	fatherEmail=document.getElementById('fatherEmail').value;		
		
		if(!fatherEmail.match(remail) && fatherEmail!= '')
		{
		document.getElementById('fatherEmaildiv').innerHTML="Enter valid email address";
		flag=true;
		}
		
	jMPhone=document.getElementById('motherPhone').value;
		if(!jMPhone.match(rphone) && jMPhone!='')
		{	
		document.getElementById('motherPhonediv').innerHTML="Enter valid phone number.";
		flag=true;
		}
		
	jMMobile=document.getElementById('motherMobile').value;
		if(!jMMobile.match(rmobile) && jMMobile!='')
		{	
		document.getElementById('motherMobilediv').innerHTML="Enter valid Mobile number.";
		flag=true;
		}
		
	motherEmail=document.getElementById('motherEmail').value;		
		if(!motherEmail.match(remail) && motherEmail!='')
		{
		document.getElementById('motherEmaildiv').innerHTML="Enter valid Email address";
		flag=true;
		}
		
	jGuardian=document.getElementById('guardianname').value;
	  	if(!jGuardian.match(reguler1) && jGuardian!='')
		{		
		document.getElementById('guardiandiv').innerHTML="Enter valid name";
		flag=true;
		}
		
		
	jguardianMobile=document.getElementById('guardianMobile').value;
		if(!jguardianMobile.match(rmobile) && jguardianMobile!='')
		{	
		document.getElementById('guardianMobilediv').innerHTML="Enter valid Mobile number.";
		flag=true;
		}
		
	guardianEmail=document.getElementById('guardianEmail').value;		
		if(!guardianEmail.match(remail) && guardianEmail!='')
		{
		document.getElementById('guardianEmaildiv').innerHTML="Enter valid email address";
		flag=true;
		}
	
	Jdob=document.getElementById('dob').value;		
		if(Jdob=='')
		{
		document.getElementById('dobdiv').innerHTML="You can't leave this empty.";
		flag=true;
		}
			var date  = Jdob.substring(0,2);
		 	var month = Jdob.substring(3,5);
		    var year  = Jdob.substring(6,10);
		
		   var myDate= new Date(year,month-1,date);		
		   var today = new Date();	
		
		  if (myDate>today)
		  {
		  document.getElementById('dobdiv').innerHTML="Enter valid date of birth";
		  flag=true;
		  }	
		
	if(flag==true)
	{
	return false;
	}
	
																				
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>
<script>
//for searching

function getDivisionSearch(str) 
{
//alert(str);
  if (str=="") {
    document.getElementById("searchDivision").innerHTML="";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("searchDivision").innerHTML=xmlhttp.responseText;
    }
  }
//  xmlhttp.open("GET","work_rate_1.php?q="+str+"&c="+client+"&df="+date_from+"&dt="+date_to,true);
   xmlhttp.open("GET","division.php?q="+str,true);
 
  xmlhttp.send();
}

//for registration
function getDivision(str) 
{
//alert(str);
  if (str=="") {
    document.getElementById("division").innerHTML="";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("division").innerHTML=xmlhttp.responseText;
    }
  }
//  xmlhttp.open("GET","work_rate_1.php?q="+str+"&c="+client+"&df="+date_from+"&dt="+date_to,true);
   xmlhttp.open("GET","division.php?q="+str,true);
 
  xmlhttp.send();
}
//guardian details show
function guardianDeatils(chk)
{

	if(chk=='yes')
	{
	document.getElementById('guardian').style.display='block';	
	document.getElementById('guardianname').setAttribute("required","required"); 
	/*document.getElementById('guardianAddr').setAttribute("required","required");
	document.getElementById('guardianMobile').setAttribute("required","required");*/
	 
	}
	else
	{
	document.getElementById('guardian').style.display='none';
	}
}

//ad no check-exist or not

$(document).ready(function() {
$("#ad_no").keyup(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var adNo = $(this).val(); //get the string typed by user
   $("#user-result").html('<img src="ajax-loader.gif" />');
   $.post('check_adm.php', {'adno':adNo}, function(data) { //make ajax call to check_username.php
   $("#user-result").html(data); //dump the data received from PHP page
   });
});
});

//calc age
function getAge()
{
var dob=document.getElementById('dob').value;

//today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = dd+'-'+mm+'-'+yyyy;
//today end
var dobSplit= dob.split('/');
var todaySplit= today.split('-');

var dobLast = new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
var todayLast = new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
var dateDiff=(todayLast.getTime() - dobLast.getTime()) / (1000*60*60*24);
var age=Math.round(dateDiff/365);

document.getElementById('age').value=age;
}
</script>	
<script>
	/*$(document).ready(function(){
		$('#guardianCheck1').click(function(){
			alert('hi');
			if ($(this).is(':checked'){
				$('#guardianname, #guardianAddr, guardianMobile').attr('required', 'required');
			} else{
				$('#guardianname, #guardianAddr, guardianMobile').removeAttr('required');
			}
		});
	});*/
</script>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
  <style>
 	.modal-backdrop.in{
		position:fixed;
	}
 </style>
      <div class="col-md-10 col-sm-8 rightarea">
      
        <div class="row rightareatop">
           <div class="col-sm-6"> 
          		<div class="clearfix">
					<h2 class="q-title">STUDENT REGISTRATION</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          
          <div class="col-sm-6 text-right">
            <form method="post" class="form-inline form3">
            <div class="form-group">
				
                  <select name="searchClass" id="searchClass" onChange="getDivisionSearch(this.value)" class="form-control .namebox" onfocus="clearbox('searchdiv')">
                          <option value="">Class</option>
                          <?php 
								$select3="select * from ".TABLE_CLASS."";
								$res=mysql_query($select3);
								while($row=mysql_fetch_array($res))
								{
							?>
                          <option value="<?php echo $row['ID']?>" <?php if($row['ID']==@$_POST['searchClass']){echo 'selected';}?>><?php echo $row['class']?></option>
                          <?php 
								}
							?>
                  </select>
                </div>
                <div class="form-group">
				
                 <select name="searchDivision" id="searchDivision" class="form-control .namebox" onfocus="clearbox('searchdiv')">
                 				<option value="">Division</option>
								<option value=""><?php if(isset($_POST['submit']))
												{
												@$m=@$_POST['searchDivision'];
													if($m=='')
													{
														echo "Select";
													}
													else
													{
														@$res3=mysql_query("SELECT * FROM ".TABLE_DIVISION." WHERE ID='$m'");
														@$row3=mysql_fetch_array(@$res3);
														echo @$row3['division'];
													}
												} 
												?></option>						
							</select>	
                </div>
                <div class="form-group">
                <input type="text" class="form-control"  name="sname" placeholder="Student Name"  value="<?php echo @$_REQUEST['sname'] ?>">  
                </div>
              <div class="input-group">
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
        
      
        </div>
        </div>
        
 <?php	
$cond="1";
if(@$_REQUEST['sname'])
{
	$cond = $cond." and ".TABLE_STUDENT.".name like'%".$_POST['sname']."%'";
}
if(@$_REQUEST['searchClass'])
{
	$cond = $cond." and ".TABLE_STUDENT.".class ='".$_POST['searchClass']."'";
}
if(@$_REQUEST['searchDivision'])
{
	$cond = $cond." and ".TABLE_STUDENT.".division ='".$_POST['searchDivision']."'";
}


?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table view_limitter pagination_table" >
                <thead>
                  <tr >
				    <th>Sl No</th>               
					<th>Admission No</th>
					<th>Name</th>
					<th>Class</th>
					<th>Division</th>
					<th>Mobile</th>	
										
                  </tr>
                </thead>
                <tbody>
                <?php
				$studNum = mysql_query("select count(ID) from ".TABLE_STUDENT." limit 1");
				if($studNum==0){
				?>
                 <tr>
                    <td align="center" colspan="8">
                        There is no data in list.
                    </td>
                </tr>
				<?php 															
						
				}
				else{
						
						$classQuery 	= "SELECT classId,ID FROM ".TABLE_DIVISION." ORDER BY classId asc";
						$classRes 	  = $db->query($classQuery);
						$classNum	  = mysql_num_rows($classRes);
						
						
						$num=0;
						while($classRow=mysql_fetch_array($classRes))
						{
							$data[$num][0] = $classRow['classId'];
							$data[$num][1] = $classRow['ID'];
							$num++;
						}
						
						$num1=0;
						
						$districtQry = "SELECT ID,districtName FROM ".TABLE_DISTRICT."";
						$districtRes = mysql_query($districtQry);
						while($districtRow=mysql_fetch_array($districtRes))
						{
							$district[$num1][0] = $districtRow['ID'];
							$district[$num1][1] = $districtRow['districtName'];
							$num1++;
						}
						
						$ar = 0;
						$k = 1;
						for($ar=0;$ar<$num;$ar++)
						{
							
							$class    = $data[$ar][0];
							$division = $data[$ar][1];
							
							$selAllQuery="SELECT ".TABLE_STUDENT.".ID,".TABLE_STUDENT.".adDate,".TABLE_STUDENT.".adNo,".TABLE_STUDENT.".name,".TABLE_STUDENT.".address,".TABLE_STUDENT.".district,".TABLE_STUDENT.".pin,".TABLE_STUDENT.".phone,".TABLE_STUDENT.".mobile,".TABLE_STUDENT.".sex,".TABLE_STUDENT.".dob,".TABLE_STUDENT.".age,".TABLE_STUDENT.".placeOfBirth,".TABLE_STUDENT.".motherTongue,".TABLE_STUDENT.".nationality,".TABLE_STUDENT.".religion,".TABLE_STUDENT.".caste,".TABLE_STUDENT.".bloodGroup,".TABLE_STUDENT.".idMark,".TABLE_ACADEMICYEAR.".fromYear,".TABLE_ACADEMICYEAR.".toYear,".TABLE_STUDENT.".father,".TABLE_STUDENT.".fOccup,".TABLE_STUDENT.".fQuali,".TABLE_STUDENT.".fOfficeAddr,".TABLE_STUDENT.".fDistrict,".TABLE_STUDENT.".fPin,".TABLE_STUDENT.".fPhone,".TABLE_STUDENT.".fMobile,".TABLE_STUDENT.".fEmail,".TABLE_STUDENT.".mother,".TABLE_STUDENT.".mOccup,".TABLE_STUDENT.".mQuali,".TABLE_STUDENT.".mOfficeAddr,".TABLE_STUDENT.".mDistrict,".TABLE_STUDENT.".mPhone,".TABLE_STUDENT.".mMobile,".TABLE_STUDENT.".mEmail,".TABLE_STUDENT.".sibling,".TABLE_STUDENT.".guardian,".TABLE_STUDENT.".gAddress,".TABLE_STUDENT.".gDistrict,".TABLE_STUDENT.".boardingPoint,".TABLE_STUDENT.".gMobile,".TABLE_STUDENT.".gEmail,".TABLE_STUDENT.".photo,".TABLE_CLASS.".class,".TABLE_DIVISION.".division  
										FROM `".TABLE_STUDENT."`,`".TABLE_ACADEMICYEAR."`,`".TABLE_CLASS."`,`".TABLE_DIVISION."` 
										WHERE studentTcStatus='no' and ".TABLE_STUDENT.".acYear=".TABLE_ACADEMICYEAR.".ID  and $cond and ".TABLE_STUDENT.".class=".TABLE_CLASS.".ID and ".TABLE_DIVISION.".ID = ".TABLE_STUDENT.".division and ".TABLE_STUDENT.".division='$division' and ".TABLE_STUDENT.".class ='$class'
										ORDER BY ".TABLE_STUDENT.".name asc";
										
							$selectAll= $db->query($selAllQuery);
							$number=mysql_num_rows($selectAll);
							$i=$k;
							while($row=mysql_fetch_array($selectAll))
							{
							 $adNo = $row['adNo'];	
							 $tableId=$row['ID'];
								 
							 $clubQry = "SELECT ".TABLE_CLUB.".clubName FROM ".TABLE_CLUB_STUDENTS.",".TABLE_CLUB.",".TABLE_ACADEMICYEAR." WHERE ".TABLE_CLUB.".ID=".TABLE_CLUB_STUDENTS.".clubId and ".TABLE_CLUB_STUDENTS.".adNo='$adNo' and ".TABLE_CLUB_STUDENTS.".acYear=".TABLE_ACADEMICYEAR.".ID and ".TABLE_ACADEMICYEAR.".status='yes'";
							 $clubRes = $db->query($clubQry);
							 
							?>
					  <tr>
					   <td><?php echo $i++;?>
						  <div class="adno-dtls"> <?php if($studentEdit || $type=="Admin"){?> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> | <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  | <?php }?><a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a> </div>
						  
							<!-- Modal3 -->
							  <div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
								<div class="modal-dialog">
								  <div class="modal-content"> 
									<!-- <div class="modal-body clearfix">
								fddhdhdhddhdh
								  </div>-->	 		
									<div role="tabpanel" class="tabarea2"> 
									  
									  <!-- Nav tabs -->
									 <ul class="nav nav-tabs" role="tablist">
									  <li role="presentation" class="active"> <a href="#academic<?php echo $tableId; ?>" aria-controls="academic" role="tab" data-toggle="tab">Academic</a> </li>
										<li role="presentation"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Personal</a> </li>
										<li role="presentation"> <a href="#parent<?php echo $tableId; ?>" aria-controls="parent" role="tab" data-toggle="tab">Parent</a> </li>
										<li role="presentation"> <a href="#guardian<?php echo $tableId; ?>" aria-controls="guardian" role="tab" data-toggle="tab">Guardian</a> </li>
									</ul>
									  
									  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="academic<?php echo $tableId; ?>">
										  <table class="table nobg">
											<tbody style="background-color:#FFFFFF">
						
											   <tr>
												<td>Admission Number</td>
												<td> : </td>
												<td><?php echo $row['adNo']; ?></td>
											  </tr>
											  <tr>
												<td>Admission Date</td>
												<td> : </td>
												<td><?php echo  $App->dbformat_date_db($row['adDate']); ?></td>
											  </tr>
											  <tr>
												<td>Name</td>
												<td> : </td>
												<td><?php echo $row['name']; ?></td>
											  </tr>
											  <tr>
												<td>Class</td>
												<td> : </td>
												<td><?php echo $row['class']; ?></td>
											  </tr>
											  <tr>
												<td>Division</td>
												<td> : </td>
												<td><?php echo $row['division']; ?></td>
											  </tr>
											   <tr>
												<td>Academic Year </td>
												<td> : </td>
												<td><?php echo $row['fromYear']."-".$row['toYear']; ?></td>
											  </tr>	
											   <tr>
												<td>Boarding Point </td>
												<td> : </td>
												<td><?php echo $row['boardingPoint']; ?></td>
											  </tr>
                                             
                                               <tr>
												<td>Club </td>
												<td> : </td>
												<td><?php
												 if(mysql_num_rows($clubRes)>0)
												 {
													while($clubRow = mysql_fetch_array($clubRes))
													{
													 echo nl2br($clubRow['clubName']."\n");
													} 
												 }
												 else
												 {
													 echo "Not Registered";
												 }
												 ?></td>
											  </tr>
                                             											  
											</tbody>
										  </table>
										</div>
										<div role="tabpanel" class="tab-pane" id="personal<?php echo $tableId; ?>">
										  <table class="table nobg" >
											<tbody style="background-color:#FFFFFF">
                                           
											  <tr>
												<td>Address</td>
												<td> : </td>
												<td><?php echo $row['address']; ?></td>
											  </tr>
											  <tr>
												<td>District</td>
												<td> : </td>
												<td><?php 
												for($d=0;$d<$num1;$d++)
												 {
													 if($row['district']==$district[$d][0])
													 {
														 echo $district[$d][1];
														
													 }
												 }
													?></td>
											  </tr>
											  <tr>
												<td>Pin</td>
												<td> : </td>
												<td><?php echo $row['pin']; ?></td>
											  </tr>
											  <tr>
												<td>Phone</td>
												<td> : </td>
												<td><?php echo $row['phone']; ?></td>
											  </tr>
											  <tr>
												<td>Mobile</td>
												<td> : </td>
												<td><?php echo $row['mobile']; ?></td>
											  </tr>
                                              <tr>
												<td>Gender</td>
												<td> : </td>
												<td><?php echo $row['sex']; ?></td>
											  </tr>
											  <tr>
												<td>Date of Birth</td>
												<td> : </td>											
												<td><?php echo  $App->dbformat_date_db($row['dob']); ?></td>
											  </tr>
											  <tr>
												<td>Age</td>
												<td> : </td>
												<td><?php echo $row['age']; ?></td>
											  </tr>
											  <tr>
												<td>Place Of Birth</td>
												<td> : </td>
												<td><?php echo $row['placeOfBirth']; ?></td>
											  </tr>
											  <tr>
												<td>Mother Tongue </td>
												<td> : </td>
												<td><?php echo $row['motherTongue']; ?></td>
											  </tr>
											  <tr>
												<td>Nationality </td>
												<td> : </td>
												<td><?php echo $row['nationality']; ?></td>
											  </tr>
											  <tr>
												<td>Religion </td>
												<td> : </td>
												<td><?php echo $row['religion']; ?></td>
											  </tr>
											  <tr>
												<td>Caste</td>
												<td> : </td>
												<td><?php echo $row['caste']; ?></td>
											  </tr>
                                              <tr>
												<td>Blood Group </td>
												<td> : </td>
												<td><?php echo $row['bloodGroup']; ?></td>
											  </tr>
											  <tr>
												<td>IdMark </td>
												<td> : </td>
												<td><?php echo $row['idMark']; ?></td>
											  </tr>
                                              <tr>
												<td>Photo </td>
												<td> : </td>
												<td>
                                                <?php if($row['photo']){?>
                                                <img src="<?php echo $row['photo']; ?>" alt="" width="177" height="146" class="img-responsive" />
                                                <?php
                                                }
                                                else
												{
													echo "Not available";
												}
                                                ?>
                                                </td>
											  </tr>
                                              	
											</tbody>
										  </table>
										</div>
										<div role="tabpanel" class="tab-pane" id="parent<?php echo $tableId; ?>">
										  <table class="table nobg">
											<tbody style="background-color:#FFFFFF">
                                            <tr>
												<td>Father </td>
												<td> : </td>
												<td><?php echo $row['father']; ?></td>
											  </tr>
											  <tr>
												<td>Father's Occupasion </td>
												<td> : </td>
												<td><?php echo $row['fOccup']; ?></td>
											  </tr>
											  <tr>
												<td>Father's Qualification </td>
												<td> : </td>
												<td><?php echo $row['fQuali']; ?></td>
											  </tr>
											  <tr>
												<td>Father's Office Address </td>
												<td> : </td>
												<td><?php echo $row['fOfficeAddr']; ?></td>
											  </tr>
											  <tr>
												<td>Father's District </td>
												<td> : </td>
												<td><?php  for($d=0;$d<$num1;$d++)
													 {
														 
														 if($row['fDistrict']==$district[$d][0])
														 {
															echo $district[$d][1];
														 }
														
													 } ?>
												 </td>
											  </tr>
											  <tr>
												<td>Father's Pin </td>
												<td> : </td>
												<td><?php echo $row['fPin']; ?></td>
											  </tr>
											  <tr>
												<td>Father's Phone </td>
												<td> : </td>
												<td><?php echo $row['fPhone']; ?></td>
											  </tr>
											  <tr>
												<td>Father's Mobile</td>
												<td> : </td>
												<td><?php echo $row['fMobile']; ?></td>
											  </tr>	
                                              <tr>
												<td>Father's Email</td>
												<td> : </td>
												<td style="text-transform: none;"><?php echo $row['fEmail']; ?></td>
											  </tr>		
                                                <tr>
												<td>Mother </td>
												<td> : </td>
												<td><?php echo $row['mother']; ?></td>
											  </tr>
											  <tr>
												<td>Mother's Occupasion </td>
												<td> : </td>
												<td><?php echo $row['mOccup']; ?></td>
											  </tr>
											  <tr>
												<td>Mother's Qualification </td>
												<td> : </td>
												<td><?php echo $row['mQuali']; ?></td>
											  </tr>
											  <tr>
												<td>Mother's Office Address </td>
												<td> : </td>
												<td><?php echo $row['mOfficeAddr']; ?></td>
											  </tr>
											  <tr>
												<td>Mother's District </td>
												<td> : </td>
												<td><?php 
													 for($d=0;$d<$num1;$d++)
													 {
														 if($row['mDistrict']==$district[$d][0])
														 {
															 echo $district[$d][1];
														 }
													 }
													?>
                                                </td>
											  </tr>
											  
											  <tr>
												<td>Mother's Phone </td>
												<td> : </td>
												<td><?php echo $row['mPhone']; ?></td>
											  </tr>
											  <tr>
												<td>Mother's Mobile</td>
												<td> : </td>
												<td><?php echo $row['mMobile']; ?></td>
											  </tr>	
                                              <tr>
												<td>Mother's Email </td>
												<td> : </td>
												<td style="text-transform: none;"><?php echo $row['mEmail']; ?></td>
											  </tr>
                                              <tr>
												<td>Siblings </td>
												<td> : </td>
												<td><?php echo $row['sibling']; ?></td>
											  </tr>	
                                             							 
											</tbody>
										  </table>
										</div>
										<div role="tabpanel" class="tab-pane" id="guardian<?php echo $tableId; ?>">
										  <table class="table nobg">
											<tbody style="background-color:#FFFFFF">
						
											  <tr>
												<td>Guardian </td>
												<td> : </td>
												<td><?php echo $row['guardian']; ?></td>
											  </tr>
											  <tr>
												<td>Guardian's Address </td>
												<td> : </td>
												<td><?php echo $row['gAddress']; ?></td>
											  </tr>
											  <tr>
												<td>Guardian's District </td>
												<td> : </td>
												<td><?php echo $row['gDistrict']; ?></td>
											  </tr>
											  <tr>
												<td>Guardian's Mobile</td>
												<td> : </td>
												<td><?php echo $row['gMobile']; ?></td>
											  </tr>
                                              <tr>
												<td>Guardian's Email</td>
												<td> : </td>
												<td style="text-transform: none;"><?php echo $row['gEmail']; ?></td>
											  </tr>
											  
											</tbody>
										  </table>
										</div>
									  </div>
									</div>
								  </div>
								</div>
							  </div>
							  <!-- Modal3 cls --> 
						  
						  
						  </td>
						<td><?php echo $row['adNo']; ?></td>
						<td><?php echo $row['name']; ?></td>						
						<td><?php echo $row['class']; ?> </td>
                   	 	<td><?php echo $row['division']; ?> </td>
						<td><?php echo $row['mobile']; ?></td>
					  </tr>
					  <?php }
					  $k=$i;
				}
				}
				?>                  
                </tbody>
              </table>
			  	 
            </div>
			<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">STUDENT REGISTRATION </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="ad_date">Admission Date:</label>
                      <input type="text" id="ad_date" name="ad_date" class="form-control2"  readonly value="<?php echo date("d/m/Y");?>"  >					
                    </div>
					
                    <div class="form-group">
                      <label for="ad_no">Admission No:<span class="star">*</span> </label>
                      <input type="text" id="ad_no"  name="ad_no" class="form-control2" required  title="Admission No" placeholder="Admission No" >
					  <span id="user-result"></span>
                    </div>
                   
                     <div class="form-group">
                      <label for="name">Name:<span class="star">*</span></label>                    
					  <input type="text" id="name" name="name" value="" class="form-control2" required placeholder="Name"  title="Name" onfocus="clearbox('namediv')"/>
					  <div id="namediv" class="valid" style="color:#FF6600;"></div>
                    </div>					                   								 
                  
                   <div class="form-group">
                      <label for="class">Class:<span class="star">*</span></label>
                     <select name="class" id="class" onChange="getDivision(this.value)" class="form-control2" required>
                    	<option value="">Select</option>
						<?php 
						$select1="select * from ".TABLE_CLASS."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>"><?php echo $row['class']?></option>
						<?php 
						}
						?>
                    </select>
                    </div>									  	
					
					<div class="form-group">
                      <label for="division">Division:<span class="star">*</span></label>                      
						<select name="division" id="division" class="form-control2" required>
                    		<option value="">Select</option>						
                   		 </select>					   
                    </div>									
					
					<div class="form-groupy">
                      <label for="address">Residential Address:<span class="star">*</span></label>
					  <textarea id="address"  name="address"  class="form-control2" required ></textarea>	  
                    </div>
										
					<div class="form-groupy">
                      <label for="district">District: </label>
                      <select name="district" id="district" class="form-control2" placeholder="District">
                      <option value=""> District </option>
                      <?php 
					  $desQry = "SELECT * FROM ".TABLE_DISTRICT;
					  $desRes = mysql_query($desQry);
					  while($desRow = mysql_fetch_array($desRes))
					  {
					  ?>
                      <option value="<?php echo $desRow['ID'];?>"><?php echo $desRow['districtName'];?></option>
                      <?php } ?>
                      </select>			  
                    </div>
										
					<div class="form-groupy">
                      <label for="pin">Pin: </label>
                      <input type="text" id="pin" name="pin" class="form-control2" alt="number" value="0"  placeholder="Pin Code" title="Pin" onfocus="clearbox('pindiv')" />	
					  <div id="pindiv" class="valid" style="color:#FF6600;"></div>		  
                    </div>
					
					<div class="form-groupy">
                      <label for="phone">Phone Number: </label>
                      <input type="text" id="phone" name="phone" class="form-control2"  placeholder="Phone No" onfocus="clearbox('phonediv')" />	
					  <div id="phonediv" class="valid" style="color:#FF6600;"></div>	  
                    </div>										
					
					<div class="form-groupy">
                      <label for="mobile">Mobile: </label>
                      <input type="text" id="mobile" name="mobile" class="form-control2"  placeholder="Mobile" onfocus="clearbox('mobilediv')" />	
					  <div id="mobilediv" class="valid" style="color:#FF6600;"></div>				  
                    </div>					
					
					<div class="form-groupy">
                      <label for="gender">Gender:<span class="star">*</span></label>
						 <select name="gender" id="gender" class="form-control2" title="Select Type" required >
							<option value="Male"> Male </option>
							<option value="Female"> Female </option>
						</select>		  
                    </div>							
                    <div class="form-group">
                      <label for="dob">Date Of Birth:<span class="star">*</span></label>
                        <input type="text" id="dob" name="dob"  class="form-control2 datepicker"  readonly="readonly" value="" onchange="getAge()" placeholder="Date Of Birth" onfocus="clearbox('dobdiv')" />
						<div id="dobdiv" class="valid" style="color:#FF6600;"></div>
						
                    </div>
					 <div class="form-group">
                      <label for="age">Age:<span class="star">*</span></label>
                      <input type="text" id="age" name="age" class="form-control2" alt="number|required" placeholder="Age"  title="Age" onclick="getAge()" readonly />
                    </div>
					 <div class="form-group">
                      <label for="placeBirth">Place of Birth:</label><BR />
                      <input type="text" id="placeBirth" name="placeBirth" class="form-control2" placeholder="Place of Birth"/>
                    </div>			  
                    <div class="form-groupy">
                      <label for="motherTongue">Mother Tongue:</label>
                       <input type="text" id="motherTongue" name="motherTongue" class="form-control2" placeholder="Mother Tongue"  />					  
                    </div>
					<div class="form-group">
                      <label for="religion">Religion:</label>
                      <input type="text" id="religion" name="religion" class="form-control2"  placeholder="Religion" /> 				  
                    </div>
					<div class="form-group">
                      <label for="caste">Caste: </label>
                      <input type="text" id="caste" name="caste" class="form-control2" placeholder="Caste"  /> 						  		  
                    </div>
					<div class="form-group">
                      <label for="nation">Nationality:</label>
                      <input type="text" id="nation" name="nation" class="form-control2" placeholder="Nationality" value="Indian" />				  
                    </div>					
					<div class="form-group">
                      <label for="blood">Blood Group: </label>
                      <input type="text" id="blood" name="blood" class="form-control2" placeholder="Blood Group"  />			  
                    </div>
					<div class="form-group">
                      <label for="idMark">Identification Mark:</label>
                      <input type="text" id="idMark" name="idMark" class="form-control2"   placeholder="Identification Mark"  />  	  
                    </div>
					<div class="form-group">
                      <label for="year">Academic Year:<span class="star">*</span> </label>
						 <select name="year" id="year" class="form-control2" title="Academic Year" required>                    	
							<?php 							
						$select1="select * from ".TABLE_ACADEMICYEAR."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>" <?php if($row['status']=='YES'){?> selected="selected"<?php }?>><?php echo $row['fromYear']."-".$row['toYear']?></option>
						
						<?php 
						}
						?>
						</select>	  
                    </div>						
				
                	<div class="form-group">	
					<span>FAMILY DETAILS</span>	</br> 
                 	<label for="father">Father's Name:</label>
                      <input type="text" id="father" name="father" class="form-control2" placeholder="Father's Name"  onfocus="clearbox('fatherdiv')" />
					  <div id="fatherdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
					 <div class="form-group">
                      <label for="fatherQuali">Father's Occupation:</label>
                       <input type="text" id="fatherOccup" name="fatherOccup" class="form-control2" placeholder="Father's Occupation"  />
                    </div>
				</div>
				<div class="col-sm-6">			  
                    <div class="form-group">
                      <label for="fatherQuali">Father's Educational Qualification: </label>
                       <input type="text" id="fatherQuali" name="fatherQuali" class="form-control2" placeholder="Educational Qualification" />					  
                    </div>
					<div class="form-group">
                      <label for="fatherOfficeAddr">Father's Office Address:</label>
                     <textarea id="fatherOfficeAddr" name="fatherOfficeAddr" class="form-control2" placeholder="Office Address"></textarea> 				  
                    </div>
					<div class="form-group">
                      <label for="fatherDistrict">Father's District: </label>
                      <select  id="fatherDistrict" name="fatherDistrict" class="form-control2" placeholder="District">
                      <option value=""> District </option>
                      <?php 
					  $desQry = "SELECT * FROM ".TABLE_DISTRICT;
					  $desRes = mysql_query($desQry);
					  while($desRow = mysql_fetch_array($desRes))
					  {
					  ?>
                      <option value="<?php echo $desRow['ID'];?>"><?php echo $desRow['districtName'];?></option>
                      <?php } ?>
                      </select>				  		
                    </div>
					
					
					<div class="form-group">
                      <label for="fatherPincode">Father's Pincode: </label>
                       <input type="text" id="fatherPincode" name="fatherPincode" class="form-control2" placeholder="Pincode" value="0" alt="number" title="PinCode" onfocus="clearbox('fatherPincodediv')"/>				  						 
					   <div id="fatherPincodediv" class="valid" style="color:#FF6600;"></div>	
                    </div>					
					<div class="form-group">
                      <label for="fatherPhone">Father's Phone Number: </label>
                      <input type="text" id="fatherPhone" name="fatherPhone" class="form-control2"  placeholder="Phone No" onfocus="clearbox('fatherPhonediv')"/>
					   <div id="fatherPhonediv" class="valid" style="color:#FF6600;"></div>				  
                    </div>
					<div class="form-group">
                      <label for="fatherMobile">Father's Mobile Number: </label>
                      <input type="text" id="fatherMobile" name="fatherMobile" class="form-control2"  placeholder="Mobile"  onfocus="clearbox('fatherMobilediv')"/>	
					  <div id="fatherMobilediv" class="valid" style="color:#FF6600;"></div>		  
                    </div>
					<div class="form-group">
                      <label for="fatherEmail">Father's E-mail ID: </label>
						 <input type="text" id="fatherEmail" name="fatherEmail" class="form-control2" placeholder="E-Mail ID"  alt="email" title="Father Email" onfocus="clearbox('fatherEmaildiv')" />	
						 <div id="fatherEmaildiv" class="valid" style="color:#FF6600;"></div>  
                    </div>	
					<div class="form-group">
					<label for="mother">Mother's Name:</label>
                      <input type="text" id="mother" name="mother" class="form-control2" placeholder="Mother's Name"  onfocus="clearbox('motherdiv')" />
					  <div id="motherdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
					 <div class="form-group">
                      <label for="motherOccup">Mother's Occupation:</label>
                       <input type="text" id="motherOccup" name="motherOccup" class="form-control2"  placeholder="Occupation" />
                    </div>			  
                    <div class="form-group">
                      <label for="motherQuali">Mother's Educational Qualification :</label>
                      <input type="text" id="motherQuali" name="motherQuali" class="form-control2" placeholder="Educational Qualification" />					
                    </div>
					<div class="form-group">
                      <label for="motherOfficeAddr">Mother's Office Address:</label>
                     <textarea id="motherOfficeAddr" style="width:265px" name="motherOfficeAddr" class="form-control2" placeholder="Office Address" ></textarea> 				  
                    </div>
					<div class="form-group">
                      <label for="motherDistrict">Mother's District:</label>
                      <select id="motherDistrict" name="motherDistrict" class="form-control2" placeholder="District" >
                      <option value=""> District </option>
                      <?php 
					  $desQry = "SELECT * FROM ".TABLE_DISTRICT;
					  $desRes = mysql_query($desQry);
					  while($desRow = mysql_fetch_array($desRes))
					  {
					  ?>
                      <option value="<?php echo $desRow['ID'];?>"><?php echo $desRow['districtName'];?></option>
                      <?php } ?>
                      </select> 				  		
                    </div>
					<div class="form-group">
                      <label for="motherPhone">Mother's Phone Number:</label>
                       <input type="text" id="motherPhone" name="motherPhone" class="form-control2"  placeholder="Phone No" onfocus="clearbox('motherPhonediv')" />	
					   <div id="motherPhonediv" class="valid" style="color:#FF6600;"></div>			  
                    </div>					
					<div class="form-group">
                      <label for="motherMobile">Mother's Mobile: </label>
                      <input type="text" id="motherMobile" name="motherMobile" class="form-control2"  placeholder="Mobile" onfocus="clearbox('motherMobilediv')"  />	
					  <div id="motherMobilediv" class="valid" style="color:#FF6600;"></div>		  
                    </div>
					<div class="form-group">
                      <label for="motherEmail">Mother's E-mail ID:</label>
                      <input type="text" id="motherEmail" name="motherEmail" class="form-control2"  placeholder="E-Mail ID" onfocus="clearbox('motherEmaildiv')"  />	
					  <div id="motherEmaildiv" class="valid" style="color:#FF6600;"></div>		  
                    </div>
					<div class="form-group">
                      <label for="siblings">Siblings if any in Peace:</label>
						 <input type="text" id="siblings" name="siblings" class="form-control2"  placeholder="Siblings if any"/>	  
                    </div>	
					<div class="form-group">
						  <label for="boardingPoint">Boarding Point:</label>
						    <input type="text" id="boardingPoint" name="boardingPoint" class="form-control2" placeholder="Boarding Point" />			
					</div>
                    <div class="form-group">
                      <label for="photo">Photo: </label>
                      <input type="file" name="photo" id="photo" >		  
					</div>
					<div id="guardianStatus">
						<div class="form-group">
						<label for="guardianCheck">Guardian Details:</label>
						  <input type="radio" id="guardianCheck1" name="guardianCheck" value="yes"  onclick="guardianDeatils(this.value)">Yes
						  <input type="radio" id="guardianCheck2" name="guardianCheck" value="no" checked="checked" onclick="guardianDeatils(this.value)">No
						</div>
					</div>
					<div id="guardian" style="display:none">
						<div class="form-group">
						 <label for="guardianname">Local Guardian:<span class="star">*</span></label><br/>
						 <input type="text" id="guardianname" name="guardian" class="form-control2" placeholder="Local Guardian" onfocus="clearbox('guardiandiv')"  />
						 <div id="guardiandiv" class="valid" style="color:#FF6600;"></div>
						</div>			  
						<div class="form-group">
						  <label for="guardianAddr">Guardian's Address:</label>
						  <textarea id="guardianAddr" style="width:265px" name="guardianAddr" class="form-control2" placeholder="Address" ></textarea>					
						</div>
						<div class="form-group">
						  <label for="guardianDistrict">Guardian's District:</label>
						 <input type="text" id="guardianDistrict" name="guardianDistrict" class="form-control2" placeholder="District" />		  
						</div>
						<div class="form-group">  
						  <label for="guardianMobile">Guardian's Mobile number:</label>
						 <input type="text" id="guardianMobile" name="guardianMobile" class="form-control2" placeholder="Mobile" onfocus="clearbox('guardianMobilediv')"  /> 	
						 <div id="guardianMobilediv" class="valid" style="color:#FF6600;"></div>			  		
						</div>
						<div class="form-group">
						  <label for="guardianEmail">Guardian's E-mail ID:</label>
						   <input type="text" id="guardianEmail" name="guardianEmail" class="form-control2"  placeholder="E-Mail ID" onfocus="clearbox('guardianEmaildiv')" />	
						   <div id="guardianEmaildiv" class="valid" style="color:#FF6600;"></div>			
						</div>
																														
                	</div>
				</div>
			</div>
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
	  </div>
      <!-- Modal1 cls --> 
     
	 
	 
	  
      </div>
  </div>
<?php include("../adminFooter.php") ?>

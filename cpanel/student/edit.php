<?php include("../adminHeader.php"); 
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}


$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function getAge()
{

var dob=document.getElementById('dob').value;

//today
var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

if(dd<10) {
    dd='0'+dd
} 

if(mm<10) {
    mm='0'+mm
} 

today = dd+'-'+mm+'-'+yyyy;
//today end
var dobSplit= dob.split('/');
var todaySplit= today.split('-');

var dobLast = new Date(dobSplit[2], +dobSplit[1]-1, dobSplit[0]);
var todayLast = new Date(todaySplit[2], +todaySplit[1]-1, todaySplit[0]);
var dateDiff=(todayLast.getTime() - dobLast.getTime()) / (1000*60*60*24);
var age=Math.round(dateDiff/365);

document.getElementById('age').value=age;
}
//staff Id check-exist or not

$(document).ready(function() {
$("#staffId").keyup(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var staffId = $(this).val(); //get the string typed by user
   $("#user-result").html('<img src="ajax-loader.gif" />');
   $.post('check_staffId.php', {'staff':staffId}, function(data) { //make ajax call to check_username.php
   $("#user-result").html(data); //dump the data received from PHP page
   });
});
});




</script>
<script>
function valid()
{
flag=false;
var reguler1 =/^[a-zA-Z ]*$/;
var rphone = /^(\d+)+(\d+)$/;	
var rmobile =/^(?:(?:\+|0{0,2})91(\s\s*)?|[0]?)?[789]\d{9}$/;  
var remail = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;
	
	 
	 jName=document.getElementById('name').value;
	 	
	  	if(!jName.match(reguler1))
		{		 
		document.getElementById('namediv').innerHTML="Enter valid name";
		flag=true;
		}
	jPin=document.getElementById('pin').value;	
		
		if((isNaN(jPin)||jPin.length!=6) && jPin!=0)
		{		
		document.getElementById('pindiv').innerHTML="Enter valid pin number.";
		flag=true;
		}
		
	jPhone=document.getElementById('phone').value;
		if(!jPhone.match(rphone) && jPhone!='')
		{	
		document.getElementById('phonediv').innerHTML="Enter valid phone number.";
		flag=true;
		}
		
	jMobile=document.getElementById('mobile').value;
		if(!jMobile.match(rmobile) && jMobile!='')
		{	
		document.getElementById('mobilediv').innerHTML="Enter valid Mobile number.";
		flag=true;
		}
		
	jFather=document.getElementById('father').value;
	  	if(!jFather.match(reguler1) && jFather!='')
		{		
		document.getElementById('fatherdiv').innerHTML="Enter valid name";
		flag=true;
		}
		
	jmother=document.getElementById('mother').value;
	  	if(!jmother.match(reguler1) && jmother!='')
		{		 
		document.getElementById('motherdiv').innerHTML="Enter valid name";
		flag=true;
		}
		
	jFPin=document.getElementById('fatherPincode').value;	
		if(isNaN(jFPin) && jFPin!='')
		{		
		document.getElementById('fatherPincodediv').innerHTML="Enter valid pin number";
		flag=true;
		}
		
	jFPhone=document.getElementById('fatherPhone').value;
		if(!jFPhone.match(rphone) && jFPhone!='')
		{	
		document.getElementById('fatherPhonediv').innerHTML="Enter valid phone number.";
		flag=true;
		}
		
	jFMobile=document.getElementById('fatherMobile').value;
		if(!jFMobile.match(rmobile) && jFMobile!='')
		{	
		document.getElementById('fatherMobilediv').innerHTML="Enter valid Mobile number.";
		flag=true;
		}
		
	fatherEmail=document.getElementById('fatherEmail').value;		
		
		if(!fatherEmail.match(remail) && fatherEmail!= '')
		{
		document.getElementById('fatherEmaildiv').innerHTML="Enter valid email address";
		flag=true;
		}
		
	jMPhone=document.getElementById('motherPhone').value;
		if(!jMPhone.match(rphone) && jMPhone!='')
		{	
		document.getElementById('motherPhonediv').innerHTML="Enter valid phone number.";
		flag=true;
		}
		
	jMMobile=document.getElementById('motherMobile').value;
		if(!jMMobile.match(rmobile) && jMMobile!='')
		{	
		document.getElementById('motherMobilediv').innerHTML="Enter valid Mobile number.";
		flag=true;
		}
		
	motherEmail=document.getElementById('motherEmail').value;		
		if(!motherEmail.match(remail) && motherEmail!='')
		{
		document.getElementById('motherEmaildiv').innerHTML="Enter valid Email address";
		flag=true;
		}
		
	jGuardian=document.getElementById('guardianname').value;
	  	if(!jGuardian.match(reguler1) && jGuardian!='')
		{		
		document.getElementById('guardiandiv').innerHTML="Enter valid name";
		flag=true;
		}
		
		
	jguardianMobile=document.getElementById('guardianMobile').value;
		if(!jguardianMobile.match(rmobile) && jguardianMobile!='')
		{	
		document.getElementById('guardianMobilediv').innerHTML="Enter valid Mobile number.";
		flag=true;
		}
		
	guardianEmail=document.getElementById('guardianEmail').value;		
		if(!guardianEmail.match(remail) && guardianEmail!='')
		{
		document.getElementById('guardianEmaildiv').innerHTML="Enter valid email address";
		flag=true;
		}
	
	Jdob=document.getElementById('dob').value;		
		if(Jdob=='')
		{
		document.getElementById('dobdiv').innerHTML="You can't leave this empty.";
		flag=true;
		}
			var date  = Jdob.substring(0,2);
		 	var month = Jdob.substring(3,5);
		    var year  = Jdob.substring(6,10);
		
		   var myDate= new Date(year,month-1,date);		
		   var today = new Date();	
		
		  if (myDate>today)
		  {
		  document.getElementById('dobdiv').innerHTML="Enter valid date of birth";
		  flag=true;
		  }	
		
		
	if(flag==true)
	{
	return false;
	}
	
																				
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}


function getDivision(str) 
{
//alert(str);
  if (str=="") {
    document.getElementById("division").innerHTML="";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("division").innerHTML=xmlhttp.responseText;
    }
  }
//  xmlhttp.open("GET","work_rate_1.php?q="+str+"&c="+client+"&df="+date_from+"&dt="+date_to,true);
   xmlhttp.open("GET","division.php?q="+str,true);
 
  xmlhttp.send();
}

//guardian details show
function guardianDeatils(chk)
{

	if(chk=='yes')
	{
	document.getElementById('guardian').style.display='block';
	document.getElementById('guardianname').setAttribute("required","required"); 
	/*document.getElementById('guardianAddr').setAttribute("required","required");
	document.getElementById('guardianMobile').setAttribute("required","required");*/
	 
	}
	else
	{
	document.getElementById('guardian').style.display='none';
	}
}

</script>
<script>
function getPhoto()
{
	document.getElementById('photoDiv').style.display='block';
}
function getPhotoNone()
{
	document.getElementById('photoDiv').style.display='none';
}

</script>


<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$editId=$_REQUEST['id'];

$editId = mysql_real_escape_string($editId);

 $tableEdit="SELECT ".TABLE_STUDENT.".ID,".TABLE_STUDENT.".adDate,".TABLE_STUDENT.".adNo,".TABLE_STUDENT.".name,".TABLE_STUDENT.".address,".TABLE_STUDENT.".district,".TABLE_STUDENT.".pin,".TABLE_STUDENT.".phone,".TABLE_STUDENT.".mobile,".TABLE_STUDENT.".sex,".TABLE_STUDENT.".dob,".TABLE_STUDENT.".age,".TABLE_STUDENT.".placeOfBirth,".TABLE_STUDENT.".motherTongue,".TABLE_STUDENT.".nationality,".TABLE_STUDENT.".religion,".TABLE_STUDENT.".caste,".TABLE_STUDENT.".bloodGroup,".TABLE_STUDENT.".idMark,".TABLE_ACADEMICYEAR.".fromYear,".TABLE_ACADEMICYEAR.".toYear,".TABLE_STUDENT.".acYear,".TABLE_STUDENT.".father,".TABLE_STUDENT.".fOccup,".TABLE_STUDENT.".fQuali,".TABLE_STUDENT.".fOfficeAddr,".TABLE_STUDENT.".fDistrict,".TABLE_STUDENT.".fPin,".TABLE_STUDENT.".fPhone,".TABLE_STUDENT.".fMobile,".TABLE_STUDENT.".fEmail,".TABLE_STUDENT.".mother,".TABLE_STUDENT.".mOccup,".TABLE_STUDENT.".mQuali,".TABLE_STUDENT.".mOfficeAddr,".TABLE_STUDENT.".mDistrict,".TABLE_STUDENT.".mPhone,".TABLE_STUDENT.".mMobile,".TABLE_STUDENT.".mEmail,".TABLE_STUDENT.".sibling,".TABLE_STUDENT.".guardian,".TABLE_STUDENT.".gAddress,".TABLE_STUDENT.".gDistrict,".TABLE_STUDENT.".boardingPoint,".TABLE_STUDENT.".gMobile,".TABLE_STUDENT.".gEmail,".TABLE_STUDENT.".division,".TABLE_STUDENT.".class ,".TABLE_STUDENT.".photo  FROM `".TABLE_STUDENT."`,`".TABLE_ACADEMICYEAR."` WHERE  ".TABLE_STUDENT.".ID='$editId' AND ".TABLE_STUDENT.".acYear=".TABLE_ACADEMICYEAR.".ID AND ".TABLE_STUDENT.".studentTcStatus='no'";
						  
$editRes=mysql_query($tableEdit);
$editRow=mysql_fetch_array($editRes);			

$sClass= $editRow['class']; 

	 //for student district
	 $sDistrict = $editRow['district'];
	 $disQry = "SELECT districtName FROM ".TABLE_DISTRICT." WHERE ID= '$sDistrict'";
	 $disRes = $db->query($disQry);
	 $disRow = mysql_fetch_array($disRes);
	 
	 //for father district
	 $fDistrict = $editRow['fDistrict'];
	 $fdisQry = "SELECT districtName FROM ".TABLE_DISTRICT." WHERE ID= '$fDistrict'";
	 $fdisRes = $db->query($fdisQry);
	 $fdisRow = mysql_fetch_array($fdisRes);
	 
	 //for mother district
	 $mDistrict = $editRow['mDistrict'];
	 $mdisQry = "SELECT districtName FROM ".TABLE_DISTRICT." WHERE ID= '$mDistrict'";
	 $mdisRes = $db->query($mdisQry);
	 $mdisRow = mysql_fetch_array($mdisRes);							 
?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">STUDENT REGISTRATION </h4>
            </div>
            <div class="modal-body clearfix">
			<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
			<input type="hidden" name="fid" id="fid" value="<?php echo $editId ?>">
			             
                <div class="row">
                 <div class="col-sm-6">
                    <div class="form-group">
                      <label for="ad_date">Admission Date:</label>
                      <input type="text" id="ad_date" name="ad_date"  class="form-control2 datepicker"  readonly value="<?php echo $App->dbformat_date_db($editRow['adDate']);?>"  >					
                    </div>
					
                    <div class="form-group">
                      <label for="ad_no">Admission No: </label>
                      <input type="text" id="ad_no"  name="ad_no" class="form-control2" readonly="readonly"  title="Admission No" value="<?php echo $editRow['adNo']?>" >
                    </div>
                   
                    <div class="form-group">
                      <label for="name">Name:<span class="star">*</span></label>                    
					  <input type="text" id="name" name="name" value="<?php echo $editRow['name']?>" class="form-control2"  placeholder="Name"  title="Name" onfocus="clearbox('namediv')" required/>
					  <div id="namediv" class="valid" style="color:#FF6600;"></div>
                    </div>					                   								 
                  
                   <div class="form-group">
                      <label for="class">Class:<span class="star">*</span></label>
                     <select name="class" id="class" onChange="getDivision(this.value)" class="form-control2" required>
                    	<option value="">Select</option>
						<?php 
						$select1="select * from ".TABLE_CLASS."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>"  <?php if($row['ID']==$editRow['class']){?> selected="selected"<?php } ?>><?php echo $row['class']?></option>
						<?php 
						}
						?>
                    </select>
                    </div>									  	
					
					<div class="form-group">
                      <label for="division">Division:<span class="star">*</span></label>                      
						<select name="division" id="division" class="form-control2" required>
                    		<option value="">Select</option>
							<?php 
						$select1="select * from ".TABLE_DIVISION." where classId='$sClass'";
						$res2=mysql_query($select1);
						while($row2=mysql_fetch_array($res2))
						{
						?>
                    	<option value="<?php echo $row2['ID']?>" <?php if($editRow['division']==$row2['ID']){?> selected="selected"<?php } ?>><?php echo $row2['division']?></option>
						<?php 
						}
						?>					
                   		 </select>					   
                    </div>									
					
					<div class="form-groupy">
                      <label for="address">Residential Address:<span class="star">*</span></label>
					  <textarea id="address"  name="address"  class="form-control2" required ><?php echo $editRow['address']?></textarea>	  
                    </div>
										
					<div class="form-groupy">
                      <label for="district">District:</label>
                      <select name="district" id="district" class="form-control2" placeholder="District">
                      <option value=""> District </option>
                      <?php 
					  $desQry = "SELECT * FROM ".TABLE_DISTRICT;
					  $desRes = mysql_query($desQry);
					  while($desRow = mysql_fetch_array($desRes))
					  {
					  ?>
                      <option value="<?php echo $desRow['ID'];?>" <?php if($desRow['ID']==$editRow['district']){?> selected="selected"<?php }?>><?php echo $desRow['districtName'];?></option>
                      <?php } ?>
                      </select>
                    </div>
										
					<div class="form-groupy">
                      <label for="pin">Pin: </label>
                      <input type="text" id="pin" name="pin" class="form-control2" alt="number"  placeholder="Pin Code" title="Pin" value="<?php echo $editRow['pin']?>" onfocus="clearbox('pindiv')" />		
					<div id="pindiv" class="valid" style="color:#FF6600;"></div>
                    </div>
					
					<div class="form-groupy">
                      <label for="phone">Phone Number: </label>
                      <input type="text" id="phone" name="phone" class="form-control2"  placeholder="Phone No" value="<?php echo $editRow['phone']?>" onfocus="clearbox('phonediv')" />	
					  <div id="phonediv" class="valid" style="color:#FF6600;"></div>		  
                    </div>										
					
					<div class="form-groupy">
                     <label for="mobile">Mobile: </label>
                      <input type="text" id="mobile" name="mobile" class="form-control2"  placeholder="Mobile"  value="<?php echo $editRow['mobile']?>" onfocus="clearbox('mobilediv')" />	
					  <div id="mobilediv" class="valid" style="color:#FF6600;"></div>				  
                    </div>					
					
					<div class="form-groupy">
                      <label for="gender">Gender: <span class="star">*</span></label>
						 <select name="gender" id="gender" class="form-control2" title="Select Type" required>
							<option value="Male" <?php if($editRow['sex']=='Male'){?> selected="selected"<?php }?>> Male </option>
                    		<option value="Female" <?php if($editRow['sex']=='Female'){?> selected="selected"<?php }?>> Female </option>
						</select>		  
                    </div>								
                     <div class="form-group">
                      <label for="dob">Date Of Birth:<span class="star">*</span></label>
                        <input type="text" id="dob" name="dob" required class="form-control2 datepicker" readonly="readonly" onchange="getAge()" value="<?php echo $App->dbformat_date_db($editRow['dob']);?>"onfocus="clearbox('dobdiv')"  />
						<div id="dobdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
					 <div class="form-group">
                      <label for="age">Age:<span class="star">*</span></label>
                      <input type="text" id="age" name="age" class="form-control2" alt="number|required" placeholder="Age"  title="Age" onclick="getAge()" readonly value="<?php echo $editRow['age']?>" />
                    </div>
					 <div class="form-group">
                      <label for="placeBirth">Place of Birth:</label><BR />
                      <input type="text" id="placeBirth" name="placeBirth" class="form-control2" placeholder="Place of Birth" value="<?php echo $editRow['placeOfBirth']?>"/>
                    </div>			  
                    <div class="form-groupy">
                      <label for="motherTongue">Mother Tongue: </label>
                       <input type="text" id="motherTongue" name="motherTongue" class="form-control2" placeholder="Mother Tongue" value="<?php echo $editRow['motherTongue']?>"  />					  
                    </div>
					<div class="form-group">
                      <label for="religion">Religion:</label>
                      <input type="text" id="religion" name="religion" class="form-control2"  placeholder="Religion" value="<?php echo $editRow['religion']?>" /> 				  
                    </div>
					<div class="form-group">
                      <label for="caste">Caste:</label>
                      <input type="text" id="caste" name="caste" class="form-control2" placeholder="Caste" value="<?php echo $editRow['caste']?>"  /> 						  		  
                    </div>
					<div class="form-group">
                      <label for="nation">Nationality:</label>
                      <input type="text" id="nation" name="nation" class="form-control2" placeholder="Nationality" value="<?php if($editRow['nationality']==''){echo "Indian";}{ echo $editRow['nationality'];}?>"  />				  
                    </div>					
					<div class="form-group">
                      <label for="blood">Blood Group: </label>
                      <input type="text" id="blood" name="blood" class="form-control2" placeholder="Blood Group" value="<?php echo $editRow['bloodGroup']?>" />			  
                    </div>
					<div class="form-group">
                      <label for="idMark">Identification Mark: </label>
                      <input type="text" id="idMark" name="idMark" class="form-control2"   placeholder="Identification Mark" value="<?php echo $editRow['idMark']?>" /> 			  
                    </div>
					<div class="form-group">
                      <label for="year">Academic Year:<span class="star">*</span> </label>
						 <select name="year" id="year" class="form-control2" title="Academic Year" required>                    	
							<?php 
							$select1="select * from ".TABLE_ACADEMICYEAR."";
							$res=mysql_query($select1);
							while($row=mysql_fetch_array($res))
							{
							?>
							<option value="<?php echo $row['ID']?>" <?php if($row['ID']==$editRow['acYear']){?> selected="selected"<?php }?>><?php echo $row['fromYear']."-".$row['toYear']?></option>
							<?php 
							}
							?>
						</select>	  
                    </div>					
				
                	<div class="form-group">	
					<span>FAMILY DETAILS</span>	</br> 
                 	<label for="father">Father's Name:</label>
                      <input type="text" id="father" name="father" class="form-control2" placeholder="Father's Name" value="<?php echo $editRow['father']?>" onfocus="clearbox('fatherdiv')" />
					  <div id="fatherdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
					 <div class="form-group">
                      <label for="fatherOccup">Father's Occupation:</label>
                       <input type="text" id="fatherOccup" name="fatherOccup" class="form-control2" value="<?php echo $editRow['fOccup']?>"  /> 
                    </div>
				</div>
				<div class="col-sm-6">			  
                    <div class="form-group">
                      <label for="fatherQuali">Father's Educational Qualification: </label>
                       <input type="text" id="fatherQuali" name="fatherQuali" class="form-control2" placeholder="Educational Qualification" value="<?php echo $editRow['fQuali']?>" />					  
                    </div>
					<div class="form-group">
                      <label for="fatherOfficeAddr">Father's Office Address:</label>
                     <textarea id="fatherOfficeAddr" name="fatherOfficeAddr" class="form-control2" placeholder="Office Address"><?php echo $editRow['fOfficeAddr']?></textarea> 				  
                    </div>
					<div class="form-group">
                      <label for="fatherDistrict">Father's District: </label>
                      <select name="fatherDistrict" id="fatherDistrict" class="form-control2" placeholder="District">
                      <option value=""> District </option>
                      <?php 
					  $desQry = "SELECT * FROM ".TABLE_DISTRICT;
					  $desRes = mysql_query($desQry);
					  while($desRow = mysql_fetch_array($desRes))
					  {
					  ?>
                      <option value="<?php echo $desRow['ID'];?>" <?php if($desRow['ID']==$editRow['fDistrict']){?> selected="selected"<?php }?> ><?php echo $desRow['districtName'];?></option>
                      <?php } ?>
                      </select>			  		
                    </div>
					<div class="form-group">
                      <label for="fatherPincode">Father's Pincode: </label>
                       <input type="text" id="fatherPincode" name="fatherPincode" class="form-control2" placeholder="Pincode"  alt="number" title="PinCode" value="<?php echo $editRow['fPin']?>" onfocus="clearbox('fatherPincodediv')"/>				  						 
					   <div id="fatherPincodediv" class="valid" style="color:#FF6600;"></div>					  
                    </div>					
					<div class="form-group">
                      <label for="fatherPhone">Father's Phone Number: </label>
                      <input type="text" id="fatherPhone" name="fatherPhone" class="form-control2"  placeholder="Phone No" value="<?php echo $editRow['fPhone']?>" onfocus="clearbox('fatherPhonediv')" />
					   <div id="fatherPhonediv" class="valid" style="color:#FF6600;"></div>				  			  
                    </div>
					<div class="form-group">
                      <label for="fatherMobile">Father's Mobile Number:</label>
                      <input type="text" id="fatherMobile" name="fatherMobile" class="form-control2"  placeholder="Mobile" value="<?php echo $editRow['fMobile']?>"  onfocus="clearbox('fatherMobilediv')" />	
					  <div id="fatherMobilediv" class="valid" style="color:#FF6600;"></div>		  			  
                    </div>
					<div class="form-group">
                      <label for="fatherEmail">Father's E-mail ID: </label>
						 <input type="text" id="fatherEmail" name="fatherEmail" class="form-control2" placeholder="E-Mail ID"  alt="email" title="Father Email" value="<?php echo $editRow['fEmail']?>"  onfocus="clearbox('fatherEmaildiv')" />	
						 <div id="fatherEmaildiv" class="valid" style="color:#FF6600;"></div>  	  
                    </div>	
					<div class="form-group">
					<label for="mother">Mother's Name:</label>
                      <input type="text" id="mother" name="mother" class="form-control2" placeholder="Mother's Name" value="<?php echo $editRow['mother']?>"  onfocus="clearbox('motherdiv')" />
					  <div id="motherdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
					 <div class="form-group">
                      <label for="motherOccup">Mother's Occupation:</label>
                       <input type="text" id="motherOccup" name="motherOccup" class="form-control2"  placeholder="Occupation" value="<?php echo $editRow['mOccup']?>" />
                    </div>			  
                    <div class="form-group">
                      <label for="motherQuali">Mother's Educational Qualification :</label>
                      <input type="text" id="motherQuali" name="motherQuali" class="form-control2" placeholder="Educational Qualification" value="<?php echo $editRow['mQuali']?>"/>					
                    </div>
					<div class="form-group">
                      <label for="motherOfficeAddr">Mother's Office Address:</label>
                     <textarea id="motherOfficeAddr" style="width:265px" name="motherOfficeAddr" class="form-control2" placeholder="Office Address" ><?php echo $editRow['mOfficeAddr']?></textarea> 				  
                    </div>
					<div class="form-group">
                      <label for="motherDistrict">Mother's District:</label>
                      <select name="motherDistrict" id="motherDistrict" class="form-control2" placeholder="District">
                      <option value=""> District </option>
                      <?php 
					  $desQry = "SELECT * FROM ".TABLE_DISTRICT;
					  $desRes = mysql_query($desQry);
					  while($desRow = mysql_fetch_array($desRes))
					  {
					  ?>
                      <option value="<?php echo $desRow['ID'];?>" <?php if($desRow['ID']==$editRow['mDistrict']){?> selected="selected"<?php }?>><?php echo $desRow['districtName'];?></option>
                      <?php } ?>
                      </select>			  		
                    </div>
					<div class="form-group">
                      <label for="motherPhone">Mother's Phone Number:</label>
                       <input type="text" id="motherPhone" name="motherPhone" class="form-control2"  placeholder="Phone No" value="<?php echo $editRow['mPhone']?>" onfocus="clearbox('motherPhonediv')" />	
					   <div id="motherPhonediv" class="valid" style="color:#FF6600;"></div>			  				  
                    </div>					
					<div class="form-group">
                      <label for="motherMobile">Mother's Mobile: </label>
                      <input type="text" id="motherMobile" name="motherMobile" class="form-control2"  placeholder="Mobile" value="<?php echo $editRow['mMobile']?>"  onfocus="clearbox('motherMobilediv')"  />	
					  <div id="motherMobilediv" class="valid" style="color:#FF6600;"></div>		  			  
                    </div>
					<div class="form-group">
                      <label for="motherEmail">Mother's E-mail ID:</label>
                      <input type="text" id="motherEmail" name="motherEmail" class="form-control2"  placeholder="E-Mail ID" value="<?php echo $editRow['mEmail']?>"  onfocus="clearbox('motherEmaildiv')"  />	
					  <div id="motherEmaildiv" class="valid" style="color:#FF6600;"></div>		  			  
                    </div>
					<div class="form-group">
                      <label for="siblings">Siblings if any in Peace:</label>
						 <input type="text" id="siblings" name="siblings" class="form-control2"  placeholder="Siblings if any" value="<?php echo $editRow['sibling']?>"/>	  
                    </div>
                    <div class="form-group">
                    <label for="change">Photo: </label>
					  <a href="<?php echo $editRow['photo'];  ?>" target="_blank">Existing Photo</a></br>
                      Change..??  <input type="radio" name="change" id="change1"  value="yes" onClick="getPhoto()" class="form-control1"> YES
				<input type="radio" name="change" id="change2"  value="no" onClick="getPhotoNone()" checked="checked" class="form-contro1">NO</br>
                    </div>
                    
                    <div id="photoDiv" style="display:none">
					 <div class="form-group">
					  <input type="file" name="photo" id="photo">
					 </div>
					</div>
                    
					<div class="form-group">
					<label for="boardingPoint">Boarding Point:</label>					
				    <input type="text" id="boardingPoint" name="boardingPoint" value="<?php echo $editRow['boardingPoint']?>" placeholder="Boarding Point" class="form-control2" />
            		</div>	
					<div id="guardianStatus">
						<div class="form-group">
						<label for="guardianCheck">Guardian Details:</label>
						  <input type="radio" id="guardianCheck" name="guardianCheck" value="yes"  onclick="guardianDeatils(this.value)" <?php if($editRow['guardian']!=''){?> checked="checked" <?php }?>>Yes
						  <input type="radio" id="guardianCheck" name="guardianCheck" value="no"  onclick="guardianDeatils(this.value)" <?php if($editRow['guardian']==''){?> checked="checked" <?php }?>>No
						</div>
					</div>
					<div id="guardian" <?php if($editRow['guardian']==""){?> style="display:none" <?php }?>>
						<div class="form-group">
						 <label for="guardianname">Local Guardian:<span class="star">*</span></label><br/>
						 <input type="text" id="guardianname" name="guardian" class="form-control2" placeholder="Local Guardian" value="<?php echo $editRow['guardian']?>"  onfocus="clearbox('guardiandiv')"  />
						 <div id="guardiandiv" class="valid" style="color:#FF6600;"></div>
						</div>			  
						<div class="form-group">
						  <label for="guardianAddr">Guardian's Address:</label>
						  <textarea id="guardianAddr" style="width:265px" name="guardianAddr" class="form-control2" placeholder="Address" ><?php echo $editRow['gAddress']?></textarea>					
						</div>
						<div class="form-group">
						  <label for="guardianDistrict">Guardian's District:</label>
						 <input type="text" id="guardianDistrict" name="guardianDistrict" class="form-control2" placeholder="District"  value="<?php echo $editRow['gDistrict']?>" />		  
						</div>
						<div class="form-group">
						  <label for="guardianMobile">Guardian's Mobile number:</label>
						 <input type="text" id="guardianMobile" name="guardianMobile" class="form-control2"  placeholder="Mobile" value="<?php echo $editRow['gMobile']?>"  onfocus="clearbox('guardianMobilediv')"  /> 	
						 <div id="guardianMobilediv" class="valid" style="color:#FF6600;"></div>			  		 				  		
						</div>
						<div class="form-group">
						  <label for="guardianEmail">Guardian's E-mail ID:</label>
						   <input type="text" id="guardianEmail" name="guardianEmail" class="form-control2"  placeholder="E-Mail ID" value="<?php echo $editRow['gEmail']?>"  onfocus="clearbox('guardianEmaildiv')" />	
						   <div id="guardianEmaildiv" class="valid" style="color:#FF6600;"></div>			
						</div>																								
                	</div>						 																								
                </div>
			</div>
		</div>
	 <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>

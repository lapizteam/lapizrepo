<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../core/logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['class'])
			{
				$_SESSION['msg']="Error,Invalid Details!";					
				header("location:new.php");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$success2=0;
				$adNo=$_REQUEST['adNo'];	
				$name=$_REQUEST['name'];				
				$existId=$db->existValuesId(TABLE_TCISSUE,"adNo='$adNo' AND name='$name' AND (tcIssueStatus='TcIssued' OR tcIssueStatus='Pending')");			
				if($existId>0)
				{						
					$_SESSION['msg']="TC Is Already Issued";					
					header("location:new.php");							
				}
				else
				{			
					/*$existId2=$db->existValuesId(TABLE_TCISSUE,"adNo='$adNo' AND name='$name' AND tcIssueStatus='Pending'");			
					if($existId>0)
					{						
						$sql = "DELETE FROM `".TABLE_TCISSUE."` WHERE ID='{$existId2}'";
						$db->query($sql);
									
					}*/
					
								
				$data['tcNo']					=	$App->convert($_REQUEST['tcNo']);
				$data['dateOfTc']				=	$App->dbFormat_date($_REQUEST['dateOfTc']);
				$data['adNo']					=	$App->convert($_REQUEST['adNo']);
				$data['adDate']					=	$App->dbFormat_date($_REQUEST['adDate']);										
				$data['name']					=	$App->convert($_REQUEST['name']);
				$data['class']					=	$App->convert($_REQUEST['class']);
				$data['division']				=	$App->convert($_REQUEST['division']);
				$data['dob']					=	$App->dbFormat_date($_REQUEST['dob']);	
				$data['qualiStatus']			=	$App->convert($_REQUEST['qualiStatus']);	
				$data['reason']					=	$App->convert($_REQUEST['reason']);
				$data['placeToGo']				=	$App->convert($_REQUEST['placeToGo']);	
				$data['tcIssueStatus']			=	$App->convert('Pending');	
				$data['loginId']				=	$App->convert($uid);
																	
												
				$success=$db->query_insert(TABLE_TCISSUE, $data);			
				
				$sample['studentTcStatus']		=	'yes';				
				$success2=$db->query_update(TABLE_STUDENT, $sample,"adNo='{$adNo}'");			
								
				$db->close();
				
				
				if($success && $success2)
					{
					$_SESSION['msg']="TC Issued Successfully";					
					header("location:new.php");	
					}
				else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}
				}
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid		=	$_REQUEST['fid'];   
		 
		if(!$_POST['class'])
			{				
				$_SESSION['msg']="Error,Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$success2=0;
								
				$adNo=$_REQUEST['adNo'];
				
				//$existId=$db->existValuesId(TABLE_TCISSUE,"adNo='$adNo'");						
				//if($existId>0)
				//{						
					//$_SESSION['msg']="You have already took the copy";					
					//header("location:new.php");							
				/*}
				
				else
				{*/	
				//changing old adno as not issued		
				$qry = "select adNo from ".TABLE_TCISSUE." where ID='{$fid}'";	
				$existadno = mysql_query($qry);
				$existadnorows = mysql_fetch_array($existadno);
				$newAdno = $existadnorows['adNo'];
				
				if($newAdno != $adNo)			
				{
				
				$sample['studentTcStatus'] = 'no';
				$db->query_update(TABLE_STUDENT, $sample,"adNo='{$newAdno}'");
				
				}
				
				$data['tcNo']					=	$App->convert($_REQUEST['tcNo']);
				$data['dateOfTc']				=	$App->dbFormat_date($_REQUEST['dateOfTc']);
				$data['adNo']					=	$App->convert($_REQUEST['adNo']);
				$data['adDate']					=	$App->dbFormat_date($_REQUEST['adDate']);										
				$data['name']					=	$App->convert($_REQUEST['name']);
				$data['class']					=	$App->convert($_REQUEST['class']);
				$data['division']				=	$App->convert($_REQUEST['division']);
				$data['dob']					=	$App->dbFormat_date($_REQUEST['dob']);	
				$data['qualiStatus']			=	$App->convert($_REQUEST['qualiStatus']);	
				$data['reason']					=	$App->convert($_REQUEST['reason']);
				$data['placeToGo']				=	$App->convert($_REQUEST['placeToGo']);	
				$data['tcIssueStatus']			=	$App->convert('TcIssued');	
				$data['loginId']				=	$App->convert($uid);
				
				$success=$db->query_update(TABLE_TCISSUE, $data,"ID='{$fid}'");
				//echo TABLE_TCISSUE, $data,"ID='{$fid}'";die;
				
				$sample['studentTcStatus']		=	'yes';				
				$success2=$db->query_update(TABLE_STUDENT, $sample,"adNo='{$adNo}'");
									
				$db->close();
				
				
				if($success && $success2)
					{
					$_SESSION['msg']="TC Details updated successfully";					
					header("location:new.php");	
					}
				else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}												
				//}
			//}
		}		
		break;	
		
		//TC ISSUE STATUS CHANGE
		//-
		
		
		case 'tcIssueChange':
	
				$id	=	$_REQUEST['id'];
				$adNo=	$_REQUEST['adNo1'];
				
				//$page	=	$_REQUEST['page'];
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
				$success=0;
				$success2=0;								
				
					$data['tcIssueStatus']			=	'Pending';
					$data['loginId']				=	$App->convert($uid);											
									
				
				$success=$db->query_update(TABLE_TCISSUE, $data,"ID='{$id}'");
				
				$sample['studentTcStatus']		=	'no';
											
				$success2=$db->query_update(TABLE_STUDENT, $sample,"adNo='{$adNo}'");
									
				$db->close();
				
				
				if($success && $success2)
					{
					$_SESSION['msg']="TC Issued Status updated successfully";					
					header("location:new.php");	
					}
				else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}		
		break;
		
					
	// DELETE SECTION
	//-
	case 'delete':	
	
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
				$success=0;
				$success2=0;	
					
				$id				=	$_REQUEST['id'];
				$page			=	$_REQUEST['page'];
				$adNo			=	$_REQUEST['adNo1'];	
				$tcIssueStatus	=	$_REQUEST['tcIssueStatus1'];
				//echo $id.$tcIssueStatus;die;							
				
				$existId=$db->existValuesId(TABLE_TCISSUE,"adNo='$adNo'");			
				if($existId>0 && $tcIssueStatus=='TcIssued')
				{					
					$_SESSION['msg']="Deletion Is Not Possible TC Is Already Issued";					
					header("location:new.php");													
				}
				
				else
				{																				
				
				/*$sample['studentTcStatus']		=	'no';				
				$success=$db->query_update(TABLE_STUDENT, $sample,"adNo='{$adNo}'");
				*/
					try
					{	
						$success= @mysql_query("UPDATE `".TABLE_STUDENT."` set studentTcStatus='no' WHERE adNo='{$adNo}'");		
						$success2= @mysql_query("DELETE FROM `".TABLE_TCISSUE."` WHERE ID='{$id}'");				      
					}
					catch (Exception $e) 
					{
						 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
					}
				
				$db->close(); 				
			
				
				if($success && $success2)
					{
					$_SESSION['msg']="TC Details deleted successfully";					
					header("location:new.php");	
					}
				else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}
				}				
		break;	
		// Printing  ========================================================================
	case 'print':
				
				$fid	=	$_REQUEST['id'];								
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();			
				
				//$id = $_REQUEST['id'];
				//echo $id;die;
				/*$qry = "select ".TABLE_TCISSUE.".printcount from ".TABLE_TCISSUE." where ".TABLE_TCISSUE.".ID = '{$fid}'";
				$tcidExist = mysql_query($qry);
				$printcount = mysql_fetch_array($tcidExist);
				
				//Updating princount in tcissue while taking print.
				
				$newPrintcount = $printcount['printcount']+1;
				$sample['printcount'] =	$newPrintcount;				
				$db->query_update(TABLE_TCISSUE, $sample,"ID='$fid'");
				//end
				
				//updating as tc issued
				
				$newstatus = 'TcIssued';
				$sample['tcIssueStatus'] =	$newstatus;				
				$db->query_update(TABLE_TCISSUE, $sample,"ID='$fid'");*/
				
				
				//end
				
				
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				
				// set default header data
				$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
															
				$sql2 ="SELECT ".TABLE_TCISSUE.".ID,".TABLE_TCISSUE.".tcNo,".TABLE_TCISSUE.".dateOfTc,".TABLE_TCISSUE.".adNo,".TABLE_TCISSUE.".adDate,".TABLE_TCISSUE.".name,".TABLE_TCISSUE.".class,".TABLE_TCISSUE.".division,".TABLE_TCISSUE.".dob,".TABLE_TCISSUE.".qualiStatus,".TABLE_TCISSUE.".reason,".TABLE_TCISSUE.".placeToGo,".TABLE_TCISSUE.".tcIssueStatus FROM ".TABLE_TCISSUE." WHERE ".TABLE_TCISSUE.".ID = '$fid' ORDER BY ".TABLE_TCISSUE.".ID DESC ";

				$vou_record = 	$db->query_first($sql2);
				$tcNo 			= 	$vou_record['tcNo'];
				$dateOfTc 		= 	$vou_record['dateOfTc'];
				$adNo 			= 	$vou_record['adNo'];
				$adDate 		= 	$vou_record['adDate'];
				$name 			= 	$vou_record['name'];
				$class 			= 	$vou_record['class'];
				//$division 		= 	$vou_record['division'];
				$dob 			= 	$vou_record['dob'];
				
								
				
				$html = 'TC NO            :'.$tcNo;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				$html = 'Date of TC       :'.$App->dbFormat_date_db($dateOfTc);		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
							
				$html = 'Adm No           :'.$adNo;						
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				$html = 'Adm Date         :'.$App->dbFormat_date_db($adDate);		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
													
				$html = 'Name             :'.$name;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
								
				$html = 'Class            :'.$class;	
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
				
				$html = 'Date Of Birth    :'.$App->dbFormat_date_db($dob);	
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
				
				
				
				//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'tcreport'.$adNo.pdf", 'I');	
				
				
		break;
		
				
}
?>	

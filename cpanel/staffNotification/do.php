<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid=$_SESSION['LogID'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['event'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['acYear']			=	$App->convert($_REQUEST['year']);
				$data['notification']	=	$App->convert($_REQUEST['event']);
				$data['discription']	=	$App->convert($_REQUEST['discription']);
				$data['addDate']		=	date("Y-m-d");
				$data['loginId']		=	$App->convert($uid);
						
				$eventId=$db->query_insert(TABLE_STAFFNOTIFICATION, $data);
				//echo TABLE_STAFFNOTIFICATION, $data;die;
				$db->close();
				
				$_SESSION['msg']="Notification added successfully";					
				header("location:new.php");
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['event'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['acYear']			=	$App->convert($_REQUEST['year']);
				$data['notification']	=	$App->convert($_REQUEST['event']);
				$data['discription']	=	$App->convert($_REQUEST['discription']);
				$data['addDate']		=	date("Y-m-d");
				$data['loginId']		=	$App->convert($uid);
						
				$db->query_update(TABLE_STAFFNOTIFICATION, $data,"ID='{$fid}'");
				//echo TABLE_STAFFNOTIFICATION, $data,"ID='{$fid}'";die;
				$db->close();				
				$_SESSION['msg']="Notification updated successfully";					
				header("location:new.php");							
			}
		break;	
		
	// DELETE SECTION
	//-
	case 'delete':		
				$id		=	$_REQUEST['id'];
				$page	=	$_REQUEST['page'];
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				$db->query("DELETE FROM `".TABLE_STAFFNOTIFICATION."` WHERE ID='{$id}'");								
				$db->close();	
											
				$_SESSION['msg']="Notification deleted successfully";					
				header("location:new.php");				
		break;		
}
?>
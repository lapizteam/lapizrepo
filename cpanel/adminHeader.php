<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$uid 				=	$_SESSION['LogID'];
$type				=	$_SESSION['LogType'];
$staffId			=	$_SESSION['LogStaffId'];
				
$basicSetting		=	$_SESSION['basicSetting'];
$basicSettingEdit	=	$_SESSION['basicSettingEdit'];
$studAttendance		=	$_SESSION['studAttendance'];
$feePayment			=	$_SESSION['feePayment'];
$feePaymentEdit		=	$_SESSION['feePaymentEdit'];
$busSettings		=	$_SESSION['busSettings'];
$busSettingEdit		=	$_SESSION['busSettingEdit'];	
$messFee			=	$_SESSION['messFee'];
$messFeeEdit		=	$_SESSION['messFeeEdit'];
$accountReport		=	$_SESSION['accountReport'];
$accountTransfer	=	$_SESSION['accountTransfer'];
$accountTransferEdit=	$_SESSION['accountTransferEdit'];
$examType			=	$_SESSION['examType'];
$examEntry			=	$_SESSION['examEntry'];
$examEntryEdit		=	$_SESSION['examEntryEdit'];
$markEntry			=	$_SESSION['markEntry'];
$notification		=	$_SESSION['notification'];
$notificationEdit	=	$_SESSION['notificationEdit'];
$printSetting		=	$_SESSION['printSetting'];
$promotion			=	$_SESSION['promotion'];
$reportView			=	$_SESSION['reportView'];
$studentReg			=	$_SESSION['studentReg'];
$studentEdit		=	$_SESSION['studentEdit'];
$staffReg			=	$_SESSION['staffReg'];
$staffEdit			=	$_SESSION['staffEdit'];
$library			=	$_SESSION['library'];
$libraryEdit		=	$_SESSION['libraryEdit'];
$mail				=	$_SESSION['mail'];
$club				=	$_SESSION['club'];
$clubEdit			=	$_SESSION['clubEdit'];
$calendar 			=	$_SESSION['calendar'];
$diary			   =	$_SESSION['diary'];
$classTeacher		=	$_SESSION['classTeacher'];
$classTeacherEdit	=	$_SESSION['classTeacherEdit'];
$chat				=	$_SESSION['chat'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Lapiz</title>
<script type="text/javascript" src="../../js/modernizr-1.5.min.js"></script>
<script type="text/javascript" src="../../js/jquery.js"></script>
<script type="text/javascript" src="../../js/jquery_pagination.js"></script>


<!-- Bootstrap -->
<link href="../../css/bootstrap.min.css" rel="stylesheet">
<link href="../../css/style.css" rel="stylesheet">
<link href="../../css/newstyle.css" rel="stylesheet">
<link href="../../css/bootstrap-datepicker.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,900' rel='stylesheet' type='text/css'>
<link href="../../css/owl.carousel.css" rel="stylesheet" />
<link rel="stylesheet" type="text/css" href="../../font-awesome/css/font-awesome.min.css"/>
<!--<link rel="stylesheet" type="text/css" href="../../css/bootstrap-theme.css">-->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="bgmain">
<div id="header">
  <div class="container"> <a href="#" class="logo"></a>
    <div class="navigation">
      <div class="dropdown"> <a id="dLabel" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false"> Settings <span class="caret"></span> </a>
        <ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
          <li><a href="../changePassword/new.php">Change Password</a></li>
          <li><a href="../../logout.php">Logout</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="clearer"></div>
</div>
<div id="contentarea">
  <div class="container">
    <div class="row">
      <div class="col-md-2 col-sm-4 leftnav">
        <div class="leftmenu">
        <div class="mobmenu"></div>
		 <ul>
            <li> <a href="../adminDash/adminDash.php" class="dropdown-toggle" > <span class="menuicons icn2"></span> Home </a></li>
		<?php 
			if($type=='Admin')
			{					
		?>	
            <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn1"></span>Settings</a>
              <ul class="dropdown-menu dropdown2" role="menu">
                <li><a href="../about/new.php">About School</a></li>
              	<li><a href="../academicYear/new.php">Academic Year Registration</a></li>
				<li><a href="../class/new.php">Class Registration</a></li>
				<li><a href="../division/new.php">Division Registration</a></li>
				<li><a href="../workingDays/new.php">Working Days Registration</a></li>
                <li><a href="../club/new.php">Club Registration</a></li>
				
				<li><a href="../subject/new.php">Subject Registration</a></li>                
				<li><a href="../examType/new.php">Exam Type Registration</a></li>
				<li><a href="../examEntry/new.php">Exam Registration</a></li>
			
				<li><a href="../setFee/new.php">Fee Setting</a></li>
				<li><a href="../messFeeSet/new.php">Mess Fee Setting</a></li>
				<li><a href="../bus/new.php">Route Setting</a></li> 
				
				<li><a href="../printSetting/new.php">Print Setting</a></li>
                <li><a href="../dataResetting/new.php">Reset Data</a></li>
              </ul>
            </li>
            <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn6"></span>Front Office</a>
              <ul class="dropdown-menu dropdown2" role="menu">
                <li><a href="../student/new.php">Student Registration</a></li>
                <li><a href="../staff/new.php">Staff Registration</a></li>
				<li><a href="../setPassword/new.php">Set User</a></li>
				<li><a href="../userPermission/new.php">Set User Permission</a></li>
				<li><a href="../promotion/new.php">Promotion</a></li>
				<li><a href="../tcIssue/new.php">TC Issue</a></li>                              
              </ul>
            </li>	
            <li> <a href="../calendar/new.php" class="dropdown-toggle"> <span class="menuicons icn6"></span>Calendar</a></li>
            <li> <a href="../diary/new.php" class="dropdown-toggle"> <span class="menuicons icn8"></span>Student Diary</a></li>
            <!--<li> <a href="../chatHistory/view.php" class="dropdown-toggle" > <span class="menuicons icn2"></span> Chat History </a></li>-->
            <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn4"></span>Allocation</a>
              <ul class="dropdown-menu dropdown2" role="menu">
                <li><a href="../busAllocation/new.php">Bus Allocation</a></li>
                <li><a href="../classMessFee/new.php">Mess Allocation</a></li>
                <li><a href="../markEntry/new.php">Mark Entry</a></li>
				<li><a href="../attendance/new.php">Attendance Entry</a></li>
                <li><a href="../clubAllocation/new.php">Club Allocation</a></li>
                <li><a href="../teacherAllocation/new.php">Teacher Allocation</a></li>
              </ul>
            </li>
            <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn5"></span>Payments</a>
              <ul class="dropdown-menu dropdown2" role="menu">
                <li><a href="../feePayment/new.php">Fee Payment</a></li>
                <li><a href="../messFeePayment/new.php">Mess Fee Payment</a></li> 
				<li><a href="../busFeePayment/new.php">Bus Fee Payment</a></li>
                <li><a href="../staffPayment/new.php">Staff Payment</a></li>               
              </ul>
            </li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn10"></span>Reports</a>
              <ul class="dropdown-menu dropdown2" role="menu">                
                <li><a href="../reports/studentReport.php">Student Report</a></li> 								              
				<li><a href="../reports/markReport.php">Marks Report</a></li>	
				<li><a href="../reports/attendanceReport.php">Attendance Report</a></li>	
				<li><a href="../reports/staffReport.php">Staff Report</a></li>					                             
              </ul>
            </li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn10"></span>Payment Reports</a>
              <ul class="dropdown-menu dropdown2" role="menu">               								
                <li><a href="../reports/feePaymentReport.php">Fee Payment Report</a></li>
				<li><a href="../reports/busFeePaymentReport.php">Bus Fee Payment Report</a></li>				               
				<li><a href="../reports/messFeePaymentReport.php">Mess Fee Payment Report</a></li> 								 	
				<li><a href="../reports/staffPaymentReport.php">Staff Payment Report</a></li> 
				<li></li>
				<li><a href="../reports/DuefeePayment.php">Due fee Payment Report</a></li> 
				<li><a href="../reports/DueBusfeePayment.php">Due Bus Fee Payment</a></li>	
				<li><a href="../reports/DueMessfeePayment.php">Due Messfee Payment</a></li> 
				<li><a href="../reports/feeReport.php">Fee Details</a></li>	 			 				 			              
              </ul>
            </li>			
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn7"></span>Accounts</a>
              <ul class="dropdown-menu dropdown2" role="menu">
                <li><a href="../ledger/new.php">Ledger</a></li>																		
				<li><a href="../otherPayments/new.php">Cash Payment</a></li>						
				<li><a href="../otherReceipt/new.php">Cash Receipt</a></li>
				<li><a href="../accounttransfer/new.php">Account Transefer</a></li>	              
              </ul>
            </li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn10"></span>Account Reports</a>
              <ul class="dropdown-menu dropdown2" role="menu">
                <li><a href="../AccountReport/ledgerReport.php">Ledger Report</a></li>																		
				<li><a href="../AccountReport/PaymentReport.php">Payment Report</a></li>						
				<li><a href="../AccountReport/receiptReport.php">Receipt Report</a></li>
				<li><a href="../AccountReport/TransferReport.php">Transfer Report</a></li>						
				<li><a href="../AccountReport/cashBook.php">Cash Book</a></li>
				<li><a href="../AccountReport/dayBook.php">Day Book</a></li>         
              </ul>
            </li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn9"></span>Notification</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			  	<li><a href="../event/new.php">Event</a></li>
                <li><a href="../notification/new.php">Notification</a></li>																						
				<li><a href="../staffNotification/new.php">Staff Notification</a></li>
				<li><a href="../mail/new.php">Send Mail</a></li>
				<li><a href="../sms/new.php">Send SMS</a></li>										             
              </ul>
            </li>
            
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn8"></span>Library</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			 		<li><a href="../bookReg/new.php">Book Registration</a></li>					
					<li><a href="../bookIssue/new.php">Book Issue</a></li>
					<li><a href="../bookFinePayment/new.php">Fine Payment</a></li>						                        	
             </ul>
         	</li>
		 	<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn3"></span>Library Settings</a>
              <ul class="dropdown-menu dropdown2" role="menu">			 		
					<li><a href="../categoryReg/new.php">Book Category Registration</a></li>
                    <li><a href="../rackReg/new.php">Rack Registration</a></li>
					<li><a href="../returnDate/new.php">Book Handling Days Registration</a></li>  															                        	
             </ul>
         	</li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn10"></span>Library Reports</a>
              <ul class="dropdown-menu dropdown2" role="menu">
                <li><a href="../libraryReport/bookReport.php">Book Report</a></li>																		
				<li><a href="../libraryReport/issuedBookReport.php">Book Issue Report</a></li>						
				<li><a href="../libraryReport/bookDeliveryReport.php">Book Delivery Report</a></li>				
              </ul>
            </li>
	
		<?php 
		}
		
		if($basicSetting==1 || $examType==1 || $examEntry==1 || $messFee==1 || $busSettings==1 || $printSetting==1 || $club==1 )
		{				
		?>
		 <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn1"></span>Settings</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			  	<?php if($basicSetting==1){?>	
                <li><a href="../class/new.php">Class Registration</a></li>
				<li><a href="../division/new.php">Division Registration</a></li>
				<li><a href="../workingDays/new.php">Working Days Registration</a></li>
				<li><a href="../subject/new.php">Subject Registration</a></li>                
			 	<?php }if($examType==1){?>
				<li><a href="../examType/new.php">Exam Type Registration</a></li>						
				<?php }if($examEntry==1){?>
				<li><a href="../examEntry/new.php">Exam Registration</a></li>						
				<?php } if($messFee==1){?>
				<li><a href="../messFeeSet/new.php">Mess Fee Setting</a></li>
				<?php } if($busSettings==1){?>
                <li><a href="../bus/new.php">Route Setting</a></li> 
				<?php } if($printSetting==1){?>
				<li><a href="../printSetting/new.php">Print Setting</a></li>
				<?php }if($club==1){?>
				<li><a href="../club/new.php">Club Registration</a></li>
				<?php }?>			
             </ul>
         </li>	
		<?php 
		}			
		if($studentReg==1 || $staffReg==1 || $basicSetting==1 || $promotion==1 )
		{				
		?>
		<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn6"></span>Front Office</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			  <?php if($studentReg==1){?>	
                <li><a href="../student/new.php">Student Registration</a></li>	
			  <?php }if($staffReg==1){?>																	
				<li><a href="../staff/new.php">Staff Registration</a></li>
				<li><a href="../setPassword/new.php">Set User</a></li>
				<li><a href="../userPermission/new.php">Set User Permission</a></li>
			  <?php } if($promotion==1){?>
				<li><a href="../promotion/new.php">Promotion</a></li>
				<?php } if($basicSetting==1){?>
				<li><a href="../tcIssue/new.php">TC Issue</a></li>			
			<?php } ?>				
             </ul>
         </li>	
		<?php 
		}
		if($calendar==1 || $diary==1 || $chat==1)
		{
		?>
        <li> <a href="../calendar/new.php" class="dropdown-toggle"> <span class="menuicons icn6"></span>Calendar</a></li>
        <li> <a href="../diary/new.php" class="dropdown-toggle"> <span class="menuicons icn6"></span>Student Diary</a></li>
		<?php
		}		
		if($type=='Teacher')
		{
		?>
        <li> <a href="../chatWithParent/new.php" class="dropdown-toggle"> <span class="menuicons icn6"></span>Chat With Parent</a></li>
        <?php
		}       
		if($busSettings==1 || $messFee==1 || $markEntry==1 || $studAttendance==1 || $club==1 || $classTeacher==1)
		{
		?>		
		<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn4"></span>Allocation</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			  <?php if($busSettings==1){?>
                    	<li><a href="../busAllocation/new.php">Bus Allocation</a></li> 
				<?php } 
					if($messFee==1){?>						                         
						<li><a href="../classMessFee/new.php">Mess Allocation</a></li>
				<?php } 
					if($markEntry==1){?>
						<li><a href="../markEntry/new.php">Mark Entry</a></li> 
				<?php } 
					if($studAttendance==1){?> 
						<li><a href="../attendance/new.php">Attendance</a></li>   
				<?php } if($club==1){?>
                 		<li><a href="../clubAllocation/new.php">Club</a></li>
                <?php }if($classTeacher==1){?>
                		<li><a href="../teacherAllocation/new.php">Teacher Allocation</a></li>
                <?php } ?>
                   	
             </ul>
         </li>	
		<?php 
		}
		if($feePayment==1 || $messFee==1 || $busSettings==1 || $staffReg==1 )
		{	
		?>    	
		<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn5"></span>Payments</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			 <?php if($feePayment==1){?>
					<li><a href="../feePayment/new.php">Fee Payment</a></li>
			 <?php } 
				 if($messFee==1){?>													
					<li><a href="../messFeePayment/new.php">Mess Fee Payment</a></li>
			<?php } 
				if($busSettings==1){?>	
					<li><a href="../busFeePayment/new.php">Bus Fee Payment</a></li>
			<?php } 
				if($staffReg==1){?>	
					<li><a href="../staffPayment/new.php">Staff Payment</a></li>	
			<?php } ?>	  	
             </ul>
         </li>	
		<?php 
		}
		if($reportView==1 || $studentReg==1 || $staffReg==1 || $markEntry==1 || $studAttendance==1)
		{	
		?> 		
		<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn10"></span>Reports</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			<?php
				if($reportView==1){?>	
					<li><a href="../reports/studentReport.php">Student Report</a></li>
					<li><a href="../reports/staffReport.php">Staff Report</a></li>
			<?php }
				else if(($reportView==0)&&($studentReg==1)){?>	
                    <li><a href="../reports/studentReport.php">Student Report</a></li>	
			<?php } 
				else if(($reportView==0)&&($staffReg==1)){?>	
					<li><a href="../reports/staffReport.php">Staff Report</a></li>					
			<?php } 										
				if($markEntry==1){?>							
					<li><a href="../reports/markReport.php">Marks Report</a></li>
			<?php } 
				if($studAttendance==1){?> 
					<li><a href="../reports/attendanceReport.php">Attendance Report</a></li>  
				<?php }    														
				?>
             </ul>
         </li>	
		<?php 
		}
		if($feePayment==1 || $busSettings==1 || $messFee==1 || $staffReg==1)
		{				
		?>
		<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn10"></span>Payment Report</a>
              <ul class="dropdown-menu dropdown2" role="menu">
				  <?php if($feePayment==1){?>	
					<li><a href="../reports/feePaymentReport.php">Fee Payment Report</a></li>	
				  <?php }if($busSettings==1){?>																	
					<li><a href="../reports/busFeePaymentReport.php">Bus Fee Payment</a></li>
				  <?php }if($messFee==1){?>							
					<li><a href="../reports/messFeePaymentReport.php">Mess Fee Payment Report</a></li>				
				  <?php }if($staffReg==1){?>						
					<li><a href="../reports/staffPaymentReport.php">Staff Payment Report</a></li>	
					<li></li>
				<?php } if($feePayment==1){?>	
					 <li><a href="../reports/DuefeePayment.php">Due fee Payment Report</a></li> 
				  <?php }if($busSettings==1){?>																	
					<li><a href="../reports/DueBusfeePayment.php">Due Bus Fee Payment</a></li>
				  <?php }if($messFee==1){?>							
					<li><a href="../reports/DueMessfeePayment.php">Due Mess fee Payment</a></li>								  	
				<?php } ?>	
				<li><a href="../reports/feeReport.php">Fee Details</a></li>	 				
             </ul>
         </li>	
		<?php 
		}
		
		if($accountTransfer==1){
		?>    
		<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn7"></span>Accounts</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			 	<li><a href="../ledger/new.php">Ledger</a></li>																		
				<li><a href="../otherPayments/new.php">Cash Payment</a></li>						
				<li><a href="../otherReceipt/new.php">Cash Receipt</a></li>
				<li><a href="../accounttransfer/new.php">Account Transefer</a></li>				  	
             </ul>
         </li>	
		<?php 
		}
		if($accountReport==1){
		?> 	
		<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn10"></span>Accounts Report</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			 	<li><a href="../AccountReport/ledgerReport.php">Ledger Report</a></li>																		
				<li><a href="../AccountReport/PaymentReport.php">Payment Report</a></li>						
				<li><a href="../AccountReport/receiptReport.php">Receipt Report</a></li>
				<li><a href="../AccountReport/TransferReport.php">Transfer Report</a></li>						
				<li><a href="../AccountReport/cashBook.php">Cash Book</a></li>
				<li><a href="../AccountReport/dayBook.php">Day Book</a></li>			  	
             </ul>
         </li>	
		<?php 
		}
		 if($notification==1||$mail==1){
		?>		
		<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn9"></span><?php if($notification==1){ echo "Notification"; } else {echo "Mail";} ?></a>
              <ul class="dropdown-menu dropdown2" role="menu">
			 	<?php if($notification==1){ ?>                	
					<li><a href="../event/new.php">Event</a></li>
					<li><a href="../notification/new.php">Notification</a></li>																						
					<li><a href="../staffNotification/new.php">Staff Notification</a></li>							
				<?php }
				if($mail==1){ ?>
					<li><a href="../mail/new.php">Send Mail</a></li>
					<li><a href="../sms/new.php">Send SMS</a></li>
				<?php }?> 	
             </ul>
         </li>	
		<?php 
		}
		 if($library==1){
		?>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn8"></span>Library</a>
              <ul class="dropdown-menu dropdown2" role="menu">
			 		<li><a href="../bookReg/new.php">Book Registration</a></li>					
					<li><a href="../bookIssue/new.php">Book Issue</a></li>
					<li><a href="../bookFinePayment/new.php">Fine Payment</a></li>						                        	
             </ul>
         	</li>
		 	<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn3"></span>Library Settings</a>
              <ul class="dropdown-menu dropdown2" role="menu">			 		
					<li><a href="../categoryReg/new.php">Book Category Registration</a></li>
                    <li><a href="../rackReg/new.php">Rack Registration</a></li>
					<li><a href="../returnDate/new.php">Book Handling Days Registration</a></li>  															                        	
             </ul>
         	</li>
			<li> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="menuicons icn10"></span>Library Reports</a>
              <ul class="dropdown-menu dropdown2" role="menu">
                <li><a href="../libraryReport/bookReport.php">Book Report</a></li>																		
				<li><a href="../libraryReport/issuedBookReport.php">Book Issue Report</a></li>						
				<li><a href="../libraryReport/bookDeliveryReport.php">Book Delivery Report</a></li>				
              </ul>
            </li>	
		<?php 
		}
		?>
	</li>
</ul>
        </div>
      </div>
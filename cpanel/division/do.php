<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../core/logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['division'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$divisionUpper			=	strtoupper($_REQUEST['division']);
				$classId				=	$_REQUEST['class'];	
				$success=0;
				
				$existId=$db->existValuesId(TABLE_DIVISION,"classId='$classId' AND division='$divisionUpper'");
				if($existId>0)
				{			
				$_SESSION['msg']="Division Is Already Exist!!";					
				header("location:new.php");
				}
				else
				{
				
				$data['classId']		=	$App->convert($_REQUEST['class']);
				$data['division']		=	$App->convert($divisionUpper);
						
				$success=$db->query_insert(TABLE_DIVISION, $data);								
				$db->close();
												
				if($success)
					{
					$_SESSION['msg']="Division Details added successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}	
				}
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['division'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$divisionUpper			=	strtoupper($_REQUEST['division']);
				$classId				=	$_REQUEST['class'];	
				
				$existId=$db->existValuesId(TABLE_DIVISION,"classId='$classId' AND division='$divisionUpper' AND ID!='{$fid}'");
				if($existId>0)
				{				
				$_SESSION['msg']="Division Is Already Exist!!";					
				header("location:edit.php?id=$fid");
				}
				else
				{
				$data['classId']		=	$App->convert($_REQUEST['class']);
				$data['division']		=	$App->convert($divisionUpper);
				
				$success=$db->query_update(TABLE_DIVISION, $data, " ID='{$fid}'");					
     			$db->close();
				 				
				
				if($success)
					{
					$_SESSION['msg']="Division Details updated successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}	
				}												
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];			
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				//$success=$db->query("DELETE FROM `".TABLE_DIVISION."` WHERE ID='{$id}'");	
				
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_DIVISION."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}								
				$db->close(); 	
			
				
				if($success)
					{
					$_SESSION['msg']="Division Details deleted successfully";										
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
					}	
					header("location:new.php");						
		break;		
}
?>
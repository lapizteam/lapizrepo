<?php include("../adminHeader.php");
$type				=	$_SESSION['LogType'];
$basicSettingEdit	=	$_SESSION['basicSettingEdit'];

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>

 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-6"> 
          		<div class="clearfix">
					<h2 class="q-title">DIVISION REGISTRATION</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div> 
		 <div class="col-sm-6 text-right">
              <form class="form-inline form3" method="post" >
                
                <div class="form-group">				
                  <select name="searchClass" id="searchClass"  class="form-control .namebox" onfocus="clearbox('searchdiv')">
                          <option value="">Class</option>
                          <?php 
								$select3="select * from ".TABLE_CLASS."";
								$res=mysql_query($select3);
								while($row=mysql_fetch_array($res))
								{
							?>
                          <option value="<?php echo $row['ID']?>" <?php if($row['ID']==@$_POST['searchClass']){echo 'selected';}?>><?php echo $row['class']?></option>
                          <?php 
								}
							?>
                  </select>
                </div>
                <div class="form-group">			
                 <input type="text" name="searchDivision" id="searchDivision" class="form-control .namebox" placeholder="Division" value="<?php echo @$_POST['searchDivision']; ?>">
							
                </div>
               
              <div class="form-group">			
                   <input type="submit" name="submit" value=""class="btn btn-default lens" title="Search"/>
				   
                </div>
              </form>
            </div>
        </div>
<?php	
$cond="1";
if(@$_REQUEST['searchClass'])
{
	$cond=$cond." and ".TABLE_CLASS.".ID like'%".$_POST['searchClass']."%'";
}
if(@$_REQUEST['searchDivision'])
{
	$cond=$cond." and ".TABLE_DIVISION.".division like'%".$_POST['searchDivision']."%'";
}
?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table view_limitter pagination_table" >
                <thead>
                  <tr >
				    <th>Sl No</th>               
					<th>Class</th>
					<th>Division</th>								
                  </tr>
                </thead>
                <tbody>
				<?php 															
						$selAllQuery="SELECT ".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_DIVISION.".ID FROM `".TABLE_CLASS."`,`".TABLE_DIVISION."` WHERE ".TABLE_DIVISION.".classId=".TABLE_CLASS.".ID and $cond  ORDER BY ".TABLE_DIVISION.".ID DESC";
												
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="3">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
							while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
					   <td><?php echo $i++;?>
						  <div class="adno-dtls"> <?php if($basicSettingEdit || $type=="Admin"){?> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> | <a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a><?php }?>													  						</td>
						<td><?php echo $row['class']; ?></td>
						<td><?php echo $row['division']; ?></td>												                  	 	
					  </tr>
					  <?php }
				}?>                  
                </tbody>
              </table>			  	 
            </div>
				<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">DIVISION REGISTRATION </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="class">Class:<span class="star">*</span></label>
                      <select name="class" id="class" onChange="getDivision(this.value)" required title="Class" class="form-control2">
                    	<option value="">Select</option>
						<?php 
						$select1="select * from ".TABLE_CLASS."";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>"><?php echo $row['class']?></option>
						<?php 
						}
						?>
                    </select>									 
                    </div>
					
                    <div class="form-group">
                      <label for="division">Division:<span class="star">*</span></label>
                      <input type="text" id="division" name="division" value="" required title="Division" placeholder="Division" class="form-control2"/>				
                    </div>                                       
				</div>
			</div>
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
	  </div>
      <!-- Modal1 cls --> 
     
	 
	 
	  
      </div>
  </div>
<?php include("../adminFooter.php") ?>

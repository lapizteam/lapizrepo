<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid	=	$_SESSION['LogID'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
	
		if(!$_REQUEST['staffTableId'])
			{	
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{		
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;	
				$staffTableId		=	$_POST['staffTableId'];

				$selectRes3=mysql_query("SELECT type FROM ".TABLE_ADMIN." WHERE staffId='$staffTableId'");
				$row3=mysql_fetch_array($selectRes3);
				$type=$row3['type'];
				
				$basic1				=	$_POST['basic1'];
				$basic2				=	$_POST['basic2'];
				$dataResetting		=	$_POST['dataResetting'];
				$attendance			=	$_POST['attendance'];
				$feePayment1		=	$_POST['feePayment1'];
				$feePayment2		=	$_POST['feePayment2'];
				$busFee1			=	$_POST['busFee1'];
				$busFee2			=	$_POST['busFee2'];
				$messFee1			=	$_POST['messFee1'];
				$messFee2			=	$_POST['messFee2'];
				$accountReport		=	$_POST['accountReport'];
				$accountTransfer1	=	$_POST['accountTransfer1'];
				$accountTransfer2	=	$_POST['accountTransfer2'];
				$examType1			=	$_POST['examType1'];
				$examType2			=	$_POST['examType2'];
				$examEntry1			=	$_POST['examEntry1'];
				$examEntry2			=	$_POST['examEntry2'];
				$markEntry			=	$_POST['markEntry'];
				$notification1		=	$_POST['notification1'];
				$notification2		=	$_POST['notification2'];
				$printSetting1		=	$_POST['printSetting1'];
				$promotion			=	$_POST['promotion'];
				$reportView			=	$_POST['reportView'];
				$student1			=	$_POST['student1'];
				$student2			=	$_POST['student2'];
				$staff1				=	$_POST['staff1'];
				$staff2				=	$_POST['staff2'];
				$library1			=	$_POST['library1'];
				$library2			=	$_POST['library2'];
				$club1				=	$_POST['club1'];
				$club2				=	$_POST['club2'];
				$mail				=	$_POST['mail'];
				$classTeacher		=	$_POST['classTeacher1'];
				$classTeacherEdit	=	$_POST['classTeacher2'];

	
			
				$data['StaffId']				=	$App->convert($staffTableId);
				
				if($basic1=="")				
				$data['basicSetting']			=	0;
				else
				$data['basicSetting']			=	1;
					if($basic2=="")				
					$data['basicSettingEdit']		=	0;
					else
					$data['basicSettingEdit']		=	1;
				/*if($dataResetting=="")				
				$data['dataResetting']			=	0;
				else
				$data['dataResetting']			=	1;*/
					if($attendance=="")				
					$data['studAttendance']			=	0;
					else
					$data['studAttendance']			=	1;
				if($feePayment1=="")				
				$data['feePayment']				=	0;
				else
				$data['feePayment']				=	1;
					if($feePayment2=="")				
					$data['feePaymentEdit']			=	0;
					else
					$data['feePaymentEdit']			=	1;
				if($busFee1=="")				
				$data['busSettings']			=	0;
				else
				$data['busSettings']			=	1;
					if($busFee2=="")				
					$data['busSettingEdit']			=	0;
					else
					$data['busSettingEdit']			=	1;
				
				if($messFee1=="")				
				$data['messFee']				=	0;
				else
				$data['messFee']				=	1;
					if($messFee2=="")				
					$data['messFeeEdit']			=	0;
					else
					$data['messFeeEdit']			=	1;
				if($accountReport=="")				
				$data['accountReport']			=	0;
				else
				$data['accountReport']			=	1;
					if($accountTransfer1=="")				
					$data['accountTransfer']		=	0;
					else
					$data['accountTransfer']		=	1;
				if($accountTransfer2=="")				
				$data['accountTransferEdit']	=	0;
				else
				$data['accountTransferEdit']	=	1;
					if($examType1=="")				
					$data['examType']				=	0;
					else
					$data['examType']				=	1;
				if($examType2=="")				
				$data['examTypeEdit']			=	0;
				else
				$data['examTypeEdit']			=	1;
				
					if($examEntry1=="")				
					$data['examEntry']				=	0;
					else
					$data['examEntry']				=	1;
				if($examEntry2=="")				
				$data['examEntryEdit']			=	0;
				else
				$data['examEntryEdit']			=	1;
					if($markEntry=="")				
					$data['markEntry']				=	0;
					else
					$data['markEntry']				=	1;
				if($notification1=="")				
				$data['notification']			=	0;
				else
				$data['notification']			=	1;
					if($notification2=="")				
					$data['notificationEdit']		=	0;
					else
					$data['notificationEdit']		=	1;
				if($printSetting1=="")				
				$data['printSetting']			=	0;
				else
				$data['printSetting']			=	1;
					if($promotion=="")				
					$data['promotion']				=	0;
					else
					$data['promotion']				=	1;
								
				if($reportView=="")				
				$data['reportView']				=	0;
				else
				$data['reportView']				=	1;
					
					if($student1=="")				
					$data['studentReg']				=	0;
					else
					$data['studentReg']				=	1;
				if($student2=="")				
				$data['studentEdit']			=	0;
				else
				$data['studentEdit']			=	1;
					if($staff1=="")				
					$data['staffReg']				=	0;
					else
					$data['staffReg']				=	1;
				if($staff2=="")				
				$data['staffEdit']				=	0;
				else
				$data['staffEdit']				=	1;
					if($library1=="")				
					$data['library']				=	0;
					else
					$data['library']				=	1;
				if($library2=="")
				$data['libraryEdit']			=	0;
				else
				$data['libraryEdit']			=	1;
					if($mail=="")
					$data['mail']			=	0;
					else
					$data['mail']			=	1;
				if($club1=="")
				$data['club']			=	0;
				else
				$data['club']			=	1;
					if($club2=="")
					$data['clubEdit']			=	0;
					else
					$data['clubEdit']			=	1;
				if($classTeacher=="")
				$data['classTeacher']			=	0;
				else
				$data['classTeacher']			=	1;
					if($classTeacherEdit=="")
					$data['classTeacherEdit']		=	0;
					else
					$data['classTeacherEdit']		=	1;
				
				$data['loginId']				=	$App->convert($uid);														
												
				$success=$db->query_update(TABLE_USERPERMISSION,$data,"staffId='{$staffTableId}'");	
				
				if($success)
					{
					$_SESSION['msg']="User Permission Updated Successfully";					
					header("location:new.php");	
					}
				else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}								
				}				

}
?>
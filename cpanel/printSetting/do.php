<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid	=	$_SESSION['LogID'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['all']&&!$_REQUEST['feePayment']&&!$_REQUEST['messFeePayment']&&!$_REQUEST['busFeePayment']&&!$_REQUEST['staffPayment'])
			{				
				$_SESSION['msg']="Error, Invalid Details";	
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				if($_REQUEST['all']!='')
					$data['all']					=	$App->convert($_REQUEST['all']);
				else
					$data['all']					=	'off';
					
				if($_REQUEST['feePayment']!='')
					$data['feePayment']				=	$App->convert($_REQUEST['feePayment']);
				else
					$data['feePayment']				=	'off';
					
				if($_REQUEST['messFeePayment']!='')
					$data['messFeePayment']			=	$App->convert($_REQUEST['messFeePayment']);
				else
					$data['messFeePayment']			=	'off';
					
				if($_REQUEST['busFeePayment']!='')
					$data['busFeePayment']			=	$App->convert($_REQUEST['busFeePayment']);
				else
					$data['busFeePayment']			=	'off';
					
				if($_REQUEST['staffPayment']!='')
					$data['staffPayment']			=	$App->convert($_REQUEST['staffPayment']);
				else
					$data['staffPayment']			=	'off';
															
										
					$data['loginId']				=	$App->convert($uid);
							
					$success=$db->query_update(TABLE_PRINTSETTINGS,$data,"ID=1");								
					$db->close();
															
					if($success)
					{										
					$_SESSION['msg']="Print Settings Updated Successfully";																											
					}
					else
					{
					$_SESSION['msg']="Failed";								
					}
					header("location:new.php");				
				}				
		break;			
}
?>
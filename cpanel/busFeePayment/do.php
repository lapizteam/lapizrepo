<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../core/logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['adNo'])
			{
				$_SESSION['msg']="Error,Invalid Details!";					
				header("location:new.php");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				$success=0;	
				$year=$_REQUEST['year'];	
				$month=$_REQUEST['month'];
				$adNo=$_REQUEST['adNo'];	
							
				$select="select ".TABLE_BUSALLOCATION.".ID from ".TABLE_BUSALLOCATION.",".TABLE_STUDENT." where ".TABLE_STUDENT.".adNo='$adNo' and ".TABLE_BUSALLOCATION.".adNo=".TABLE_STUDENT.".adNo and ".TABLE_BUSALLOCATION.".acYear='$year'";
				$res=mysql_query($select);
				$row=mysql_fetch_array($res);
				$extId=$row['ID'];
				if($extId>0)
				{																						
					$existId=$db->existValuesId(TABLE_BUSFEEPAYMENT,"acYear='$year' AND month='$month' AND adNo='$adNo'");				
					if($existId>0)
					{					
						$_SESSION['msg']="Vehicle Fee Is Already Paid";					
						header("location:new.php");							
					}
					else
					{				
									
					$data['acYear']					=	$App->convert($_REQUEST['year']);
					$data['month']					=	$App->convert($_REQUEST['month']);
					$data['adNo']					=	$App->convert($_REQUEST['adNo']);
					$data['payDate']				=	$App->dbFormat_date($_REQUEST['payDate']);											
					$data['voucherNo']				=	$App->convert($_REQUEST['voucherNo']);
					$data['paidAmount']				=	$App->convert($_REQUEST['amount']);
					
					$data['loginId']				=	$App->convert($uid);													
													
					$success=$db->query_insert(TABLE_BUSFEEPAYMENT, $data);
					
					
					
					$vou_sql2 ="SELECT ID FROM `".TABLE_LEDGER."` WHERE `name`='Fee Payment' ";
					$vou_record2 = $db->query_first($vou_sql2);
					$Fee=$vou_record2['ID'];
					
	//				$vou_sql ="SELECT MAX(voucher_no) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Fee Payments' ";
					$vou_record = $App->convert($_REQUEST['voucherNo']);
					/* ------------------------------------------ */																
					/* ------------------------------------------ */
					$tsdata['from_ledger'] 		=  	$App->convert($_POST['from_ledger']);
					$tsdata['to_ledger'] 		=  	$Fee;
					$tsdata['voucher_no']		= 	$vou_record;
					$tsdata['voucher_type']		=  	'Bus Fee Payments';
					$tsdata['credit']			= 	$App->convert($_POST['amount']);
					$tsdata['debit']			=   0;
					$tsdata['cdate']			=	"NOW()";
					$tsdata['transaction_date']	=	$App->dbformat_date($_POST['payDate']);
					$tsdata['remark']			=  	'Bus - Admission No: '.$App->convert($_REQUEST['adNo']);
					$tsdata['uid']				=	$_SESSION['LogID'];
					$tsdata['loginId']			=	$_SESSION['LogID'];
	
					$db->query_insert(TABLE_TRANSACTION, $tsdata);
					/* ------------------------------------------ */
					/* ------------------------------------------ */
					$tfdata['from_ledger'] 		=  	$Fee;
					$tfdata['to_ledger'] 		=  	$App->convert($_POST['from_ledger']);
					$tfdata['voucher_no']		= 	 $vou_record;
					$tfdata['voucher_type']		=  	'Bus Fee Payments';
					$tfdata['credit']			=   0;
					$tfdata['debit']			= 	$App->convert($_POST['amount']);
					$tfdata['cdate']			=	"NOW()";
					$tfdata['transaction_date']	=	$App->dbformat_date($_POST['payDate']);
					$tfdata['remark']			=  	'Bus - Admission No: '.$App->convert($_REQUEST['adNo']);
					$tfdata['uid']				=	$_SESSION['LogID'];
					$tfdata['loginId']			=	$_SESSION['LogID'];
				
					$db->query_insert(TABLE_TRANSACTION, $tfdata);				
					$db->close();
													
					if($success)
						{
						$_SESSION['msg']="Vehicle Fee Paid successfully";					
						header("location:new.php");	
						}
						else
						{
						$_SESSION['msg']="Failed";	
						header("location:new.php");					
						}	
					}
			}
			else
			{
				$_SESSION['msg']="Bus is not allocated to this student";
				header("location:new.php");
			}
		
				
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid		=	$_REQUEST['fid']; 
		$year=$_REQUEST['year'];	
		$month=$_REQUEST['month'];
		$adNo=$_REQUEST['adNo'];  
		 
		if(!$_POST['adNo'])
			{			
				$_SESSION['msg']="Error,Invalid Details!";					
				header("location:edit.php?id=$fid");			
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$success=0;
				$select="select ".TABLE_BUSALLOCATION.".ID from ".TABLE_BUSALLOCATION.",".TABLE_STUDENT." where ".TABLE_STUDENT.".adNo='$adNo' and ".TABLE_BUSALLOCATION.".adNo=".TABLE_STUDENT.".adNo";
				$res=mysql_query($select);
				$row=mysql_fetch_array($res);
				$extId=$row['ID'];
				if($extId>0)
				{			
												
				
				
				/*$existId=$db->existValuesId(TABLE_BUSFEEPAYMENT,"acYear=$year AND month=$month AND adNo=$adNo");				
				if($existId>0)
				{					
					$_SESSION['msg']="Vehicle Fee Is Already Paid";					
					header("location:edit.php?id=$fid");									
				}
				else
				{	*/				
				
				$data['acYear']					=	$App->convert($_REQUEST['year']);
				$data['month']					=	$App->convert($_REQUEST['month']);
				$data['adNo']					=	$App->convert($_REQUEST['adNo']);
				$data['payDate']				=	$App->dbFormat_date($_REQUEST['payDate']);											
				$data['voucherNo']				=	$App->convert($_REQUEST['voucherNo']);
				$data['paidAmount']				=	$App->convert($_REQUEST['amount']);
				
				$data['loginId']				=	$App->convert($uid);													
												
				
				$success=$db->query_update(TABLE_BUSFEEPAYMENT, $data,"ID='{$fid}'");	
				
				
				
				$sql2 = "SELECT voucherNo FROM `".TABLE_BUSFEEPAYMENT."` WHERE ID='$fid'";
				$vou_record = 	$db->query_first($sql2);
				$voucher_no = 	$vou_record['voucherNo'];
				
				$vou_sql2 ="SELECT ID FROM `".TABLE_LEDGER."` WHERE `name`='Fee Payment' ";
				$vou_record2 = $db->query_first($vou_sql2);
				$Fee=$vou_record2['ID'];
				
				$vou_sql	="DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_no`='$voucher_no' AND `voucher_type`='Bus Fee Payments'";
				$db->query($vou_sql);

//				$vou_sql ="SELECT MAX(voucher_no) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Fee Payments' ";
				//$vou_record = $App->convert($_REQUEST['voucherNo']);
				/* ------------------------------------------ */																
				/* ------------------------------------------ */
				$tsdata['from_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tsdata['to_ledger'] 		=  	$Fee;
				$tsdata['voucher_no']		= 	$voucher_no;
				$tsdata['voucher_type']		=  	'Bus Fee Payments';
				$tsdata['credit']			= 	$App->convert($_POST['amount']);
				$tsdata['debit']			=   0;
				$tsdata['cdate']			=	"NOW()";
				$tsdata['transaction_date']	=	$App->dbformat_date($_POST['payDate']);
				$tsdata['remark']			=  	'Bus - Admission No: '.$App->convert($_REQUEST['adNo']);
				$tsdata['uid']				=	$_SESSION['LogID'];
				$tsdata['loginId']			=	$_SESSION['LogID'];

				$db->query_insert(TABLE_TRANSACTION, $tsdata);
				/* ------------------------------------------ */
				/* ------------------------------------------ */
				$tfdata['from_ledger'] 		=  	$Fee;
				$tfdata['to_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tfdata['voucher_no']		= 	 $voucher_no;
				$tfdata['voucher_type']		=  	'Bus Fee Payments';
				$tfdata['credit']			=   0;
				$tfdata['debit']			= 	$App->convert($_POST['amount']);
				$tfdata['cdate']			=	"NOW()";
				$tfdata['transaction_date']	=	$App->dbformat_date($_POST['payDate']);
				$tfdata['remark']			=  	'Bus - Admission No: '.$App->convert($_REQUEST['adNo']);
				$tfdata['uid']				=	$_SESSION['LogID'];
				$tfdata['loginId']			=	$_SESSION['LogID'];
			
				$db->query_insert(TABLE_TRANSACTION, $tfdata);
												
				$db->close();			
				
				if($success)
					{
					$_SESSION['msg']="Vehicle Fee updated successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}		
				//}	
				}
				else
				{
					$_SESSION['msg']="Bus is not allocated to this student";
					header("location:new.php");
				}
			
															
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id		=	$_REQUEST['id'];				
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();								
				
				$sql2 = "SELECT voucherNo FROM `".TABLE_BUSFEEPAYMENT."` WHERE ID='$id'";
				$vou_record = 	$db->query_first($sql2);
				$voucher_no = 	$vou_record['voucherNo'];
				
				/*$vou_sql	="DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_no`='$voucher_no' AND `voucher_type`='Bus Fee Payments'";
				$db->query($vou_sql);
				
				$sql = "DELETE FROM `".TABLE_BUSFEEPAYMENT."` WHERE ID='$id'";
				$success=$db->query($sql);*/
				
				try
				{
				$success1 = @mysql_query("DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_no`='$voucher_no' AND `voucher_type`='Bus Fee Payments'");
				$success2 = @mysql_query("DELETE FROM `".TABLE_BUSFEEPAYMENT."` WHERE ID='$id'");	      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}
				
				$db->close(); 				
								
				if($success1 && success2)
					{
					$_SESSION['msg']=" Vehicle Fee deleted successfully";					
					header("location:new.php");		
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";	
					header("location:new.php");					
					}						
		break;	
		
		
		// Printing  ========================================================================
	case 'print':
				require_once('../../printing/tcpdf_include.php');
				require_once('../../printing/tcpdf_config_alt.php');
				require_once('../../printing/tcpdf.php');

				$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
				// set document information
				$pdf->SetCreator(PDF_CREATOR);
				
				
				// set default header data
				$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,0,0), array(0,0,0));

				// set margins
				$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
				
				// set default font subsetting mode
				$pdf->setFontSubsetting(true);
				
				// Set font
				// dejavusans is a UTF-8 Unicode font, if you only need to
				// print standard ASCII chars, you can use core fonts like
				// helvetica or times to reduce file size.
				$pdf->SetFont('courier', '', 11, '', true);
				
				// Add a page
				$pdf->AddPage();
				
				// Set some content to print
				
				$fid	=	$_REQUEST['id'];								
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();												
				$sql2 = "SELECT ".TABLE_BUSFEEPAYMENT.".ID,".TABLE_ACADEMICYEAR.".fromYear,".TABLE_ACADEMICYEAR.".toYear,".TABLE_MONTH.".monthName,".TABLE_BUSFEEPAYMENT.".adNo,".TABLE_STUDENT.".name,".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_BUSFEEPAYMENT.".payDate,".TABLE_BUSFEEPAYMENT.".voucherNo,".TABLE_BUSFEEPAYMENT.".paidAmount FROM ".TABLE_BUSFEEPAYMENT.",".TABLE_ACADEMICYEAR.",".TABLE_MONTH.",".TABLE_STUDENT.",".TABLE_CLASS.",".TABLE_DIVISION." WHERE ".TABLE_BUSFEEPAYMENT.".adNo=".TABLE_STUDENT.".adNo AND ".TABLE_STUDENT.".class=".TABLE_CLASS.".ID AND ".TABLE_STUDENT.".division=".TABLE_DIVISION.".ID AND ".TABLE_BUSFEEPAYMENT.".month=".TABLE_MONTH.".ID AND ".TABLE_BUSFEEPAYMENT.".acYear=".TABLE_ACADEMICYEAR.".ID AND ".TABLE_STUDENT.".studentTcStatus='no' AND ".TABLE_BUSFEEPAYMENT.".ID='$fid'";

				$vou_record = 	$db->query_first($sql2);
				
				$adNo 			= 	$vou_record['adNo'];
				$name 			= 	$vou_record['name'];
				$class 			= 	$vou_record['class'];
				$division 		= 	$vou_record['division'];
				$monthName 		= 	$vou_record['monthName'];
				$payDate 		= 	$vou_record['payDate'];
				$voucherNo 		= 	$vou_record['voucherNo'];
				$paidAmount 	= 	$vou_record['paidAmount'];
								
				
				$html = 'Date        :'.$App->dbFormat_date_db($payDate);		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				
				$html = 'Voucher No  :'.$voucherNo;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
							
				$html = 'Name        :'.$name.'     Adm No  :'.$adNo;						
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
													
				$html = 'Class       :'.$class.' - '.$division;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
								
				$html = 'Month       :'.$monthName;	
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);	
				$pdf->Write(0, '', '', 0, '', true, 0, false, false, 0);	
				
				if($paidAmount!=0)
				{
				$html = 'Paid Amount :'.$paidAmount;		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				}
				else
				{
				$html = 'No Fee Paid';		
				$pdf->Write(0, $html, '', 0, '', true, 0, false, false, 0);
				}
				
				//$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
				
				// ---------------------------------------------------------
				
				// Close and output PDF document
				$pdf->Output("'BusFeePayment'.$adNo.pdf", 'I');	
		break;
		
			
}
?>
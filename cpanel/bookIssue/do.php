<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../core/logout.php");
}

$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{		
	
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['bookNo'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
								
				$success=0;
				$bookNo=$_REQUEST['bookNo'];
				$bookName=$_REQUEST['bookName'];
				$selectRes=mysql_query("SELECT ID FROM ".TABLE_BOOK_REG." WHERE bookNo='$bookNo' and bookName='$bookName'");
				$selectRow=mysql_fetch_array($selectRes);
				$ID=$selectRow['ID'];
				
				$existId=$db->existValuesId(TABLE_BOOK_ISSUE,"bookId=$ID AND status='pending'");
				if($existId==0)
				{													
				$data['adNo']					=	$App->convert($_REQUEST['adNo']);				
				$data['bookId']					=	$ID;
				$data['issueDate']				=	$App->dbFormat_date($_REQUEST['issuedDate']);											
				$data['returnDate']				=	$App->dbFormat_date($_REQUEST['returnDate']);
				$data['status']					=	'pending';
				$data['loginId']				=	$App->convert($uid);
				
				$success=$db->query_insert(TABLE_BOOK_ISSUE, $data);	
				//echo TABLE_BOOK_ISSUE, $data;die;			
				$sample['availability']		=	'issued';
				$db->query_update(TABLE_BOOK_REG, $sample,"ID='{$ID}'");							
				$db->close();
				
					if($success)
					{
					$_SESSION['msg']="Book Issued successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}															
				}
				else
				{
				$_SESSION['msg']="This Book Is Already Issued To Another Student";					
				header("location:new.php");	
				}												
				
			}		
		break;
		
		// RETURN SECTION	
	case 'bookReturn':
	
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
				
				$success=0;
				$id	=	$_REQUEST['id'];			
				$qry = "SELECT bookId FROM ".TABLE_BOOK_ISSUE." WHERE ID = '$id' ";
				$bookId = mysql_query($qry);
				$bookIdRows = mysql_fetch_array($bookId);
				$bookIdIs = $bookIdRows['bookId'];
								
					$data['status']					=	'delivered';
					$data['loginId']				=	$App->convert($uid);
					$data['returnedDate']			=	date("Y-m-d");				
									
				$success=$db->query_update(TABLE_BOOK_ISSUE, $data,"ID='{$id}'");			
				
				$sample['availability']		=	'available';
				$db->query_update(TABLE_BOOK_REG, $sample,"ID='{$bookIdIs}'");													
				$db->close();
					if($success)
					{
					$_SESSION['msg']="Book Issued Details updated successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}				
					
		break;		
				
				
	// EDIT SECTION
	//-
	case 'edit':		
		$fid		=	$_REQUEST['fid'];   
		// echo $fid;die;
		if(!$_POST['bookNo'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$success=0;
				$bookNo=$_REQUEST['bookNo'];
				$bookName=$_REQUEST['bookName'];
				$selectRes=mysql_query("SELECT ID FROM ".TABLE_BOOK_REG." WHERE bookNo='$bookNo' and bookName='$bookName'");
				$selectRow=mysql_fetch_array($selectRes);
				$ID=$selectRow['ID'];
			
								
				$data['adNo']					=	$App->convert($_REQUEST['adNo']);				
				$data['bookId']					=	$ID;
				$data['issueDate']				=	$App->dbFormat_date($_REQUEST['issuedDate']);											
				$data['returnDate']				=	$App->dbFormat_date($_REQUEST['returnDate']);
				$data['loginId']				=	$App->convert($uid);
																	
																	
				$success=$db->query_update(TABLE_BOOK_ISSUE, $data,"ID='{$fid}'");	
				
				$sample['availability']		=	'issued';
				$db->query_update(TABLE_BOOK_REG, $sample,"ID='{$ID}'");
				//echo TABLE_BOOK_REG, $sample,"ID='{$ID}'";die;	
								
				$db->close();
					if($success)
					{
					$_SESSION['msg']="Book Issued Details updated successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}																
			}		
		break;		
	// DELETE SECTION
	
	case 'delete':		
				$id		=	$_REQUEST['id'];
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();								
				
				/*$sql = "DELETE FROM `".TABLE_BOOK_ISSUE."` WHERE ID='$id'";
				$success=$db->query($sql);*/
				
				try
				{
					$success= @mysql_query( "DELETE FROM `".TABLE_BOOK_ISSUE."` WHERE ID='$id'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}
								
				$db->close(); 				
				
				if($success)
					{
					$_SESSION['msg']="Book Issued Details deleted successfully";					
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";	
					header("location:new.php");					
					}										
		break;		
}
?>
<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$type		=	$_SESSION['LogType'];
$libraryEdit	=	$_SESSION['libraryEdit'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();


?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

//book returning
return_type

function return_type()
{
var ret=confirm("Do you Want to Return ?");
	if(ret==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}
//book no check-exist or not

$(document).ready(function() {
$("#bookNo").keyup(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var bookNo = $(this).val(); //get the string typed by user
   $("#user-result1").html('<img src="ajax-loader.gif" />');
   $.post('check_book.php', {'bookNo':bookNo}, function(data) { //make ajax call to check_username.php
   $("#user-result1").html(data); //dump the data received from PHP page
   });
});
});

//ad no check-exist or not

$(document).ready(function() {
$("#adNo").keyup(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var adNo = $(this).val(); //get the string typed by user
   $("#user-result").html('<img src="ajax-loader.gif" />');
   $.post('check_admno.php', {'adno':adNo}, function(data) { //make ajax call to check_username.php
   $("#user-result").html(data); //dump the data received from PHP page
   });
});
});

//calculating return date


//for getting retun date onchange
function bookReturn()
	 {
	 var issue=document.getElementById('issuedDate').value; 
	var numDay=document.getElementById('numDay').value;	
	var issueDate = moment(issue,"DD-MM-YYYY");	
	issueDate.add(parseInt(numDay), 'days');
	var result = issueDate.format("DD/MM/YYYY");	
	document.getElementById('returnDate').value=result;
	}
</script>
<script type="text/javascript">
$(document).ready(function() {
	
$("#adNo").change(function (e) { //user types username on inputfiled
	//$(this).val($(this).val().replace(/\s/g, ''));
	
   var value = $('#adNo').val();  //get the string typed by user

  
   //$("#user-result").html('<img src="ajax-loader.gif" />');
  // $.post('check_adm.php', {'adno':adNo}, function(data) { //make ajax call to check_username.php
   
   
   //to return the number of books pending to return
   
   $.ajax({    //create an ajax request to paidamt.php	  
        type: "post",
        url: "check_bookno.php", 	
		data:{ value: value },   		 
     //expect html to be returned                
        success: function(response){ 		      
            //$data1=response; 
          
		   if(response!=0)
		   {
			   var data1=response.trim();
			   alert(data1+" books pending to return");
		   }
		   
        }   
   });
   
   //to know if the number of book limit exceed or not
   
   $.ajax({    //create an ajax request to paidamt.php	  
        type: "post",
        url: "check_adm.php", 	
		data:{ value: value },   		 
     //expect html to be returned                
        success: function(response){ 		      
            //$data=response; 
            //alert(response);
		   if(response==0)
		   {
		   alert ("User already Exceed the Book limit");
		   }
		   
        }   
   });
});



 $("#adNo").change(function() {    
	var value=$('#adNo').val();
	var issue=$('#issuedDate').val(); 
	var numDay=$('#numDay').val(); 	 
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "name.php", 
	
		data:{ value: value},   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#name").val(response); 
            //alert(response);
        }

    });
	         
      $.ajax({    //create an ajax request to load_page.php
	  
        type: "post",
        url: "class.php", 
		
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#class").val(response); 
            //alert(response);
        }

    });
	
  $.ajax({    //create an ajax request to load_page.php
	  
        type: "post",
        url: "division.php", 
	
		data:{ value: value},   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#division").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to retdate.php - return date
	  
        type: "post",
        url: "retdate.php", 
		
		data:{ value1: issue,value2:numDay },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#returnDate").val(response); 
            //alert(response);
        }

    });
	
	 });
	 
	 
 $("#bookNo").change(function() {    
	var value=$('#bookNo').val();   
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "bookName.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#bookName").val(response); 
            //alert(response);
        }

    });
	});
	 
});
</script>
<script src="../JS/moment.js"></script>
<script>
	
	
	
	//validation for search
	function searchValid()
	{
	flag=false;
	jSearchAdNo = document.getElementById('searchAdNo').value;
	jSearchBook = document.getElementById('searchBookName').value;
	
	if((jSearchAdNo == "") && (jSearchBook == ""))
	{		
	document.getElementById('searchdiv').innerHTML="At least one field must be filled.";
	flag=true;
	}
	
		if(flag==true)
		{
		return false;
		}
	}
	
	//form validation
	function valid()
	{
	flag=false;
	var retDate = document.getElementById('returnDate').value;
	//alert (retDate);
	if(retDate == "")
		{
		document.getElementById('returnDatediv').innerHTML="You can't leave this empty.";
		flag = true;
		}
	var numDay = document.getElementById('numDay').value;
		if(isNaN(numDay))
		{
		document.getElementById('numDaydiv').innerHTML="Check Days.";
		flag=true;
		}
	var name = document.getElementById('name').value;
	if(name == "")
		{
		document.getElementById('adNoDiv').innerHTML="Student is not exist";
		flag = true;
		}
	var bookName = document.getElementById('bookName').value;	
	if(bookName == "")
		{
		document.getElementById('bookNoDiv').innerHTML="Book is not available";
		flag = true;
		}
	if(flag==true)
		{
		return false;
		}
	
	}
	
	//clear the validation msg
	function clearbox(Element_id)
	{
	document.getElementById(Element_id).innerHTML="";
	}
</script>


<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-12 rightarea">
        <div class="row">
           <div class="col-sm-6"> 
          		<div class="clearfix">
					<h2 class="q-title">BOOK ISSUE</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
			</div>
			<div class="col-sm-6">
			<!--onsubmit="return searchValid()"-->
            <form method="post" >
              <div class="input-group" style="float: left; margin-right: 10px;margin-left:10%">
			  <input id="searchAdNo" name="searchAdNo" value="<?php echo @$_POST['searchAdNo'] ?>" type="text" placeholder="Admission Number" class="form-control" onfocus="return clearbox('searchdiv')"/>
			  </div>
			  <div class="input-group" style="float: left; margin-right: 10px;">
			  <input id="searchBookName" name="searchBookName" value="<?php echo @$_POST['searchBookName'] ?>" type="text" placeholder="Book Name" class="form-control" onfocus="return clearbox('searchdiv')" />
			   </div>
			  <div class="form-group">              
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> 
				<div id="searchdiv" class="valid" style="color:#FF6600;margin-left:45px;"></div>
				</div>
					
            </form>
         
          </div>
          
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['searchAdNo'])
{
	//$cond=" name like '%$st%'  ";
$cond=$cond." and ".TABLE_BOOK_ISSUE.".adNo like'%".$_POST['searchAdNo']."%'";
}
if(@$_REQUEST['searchBookName'])
{
//$bname=$_POST['bookName'];
$cond=$cond." and ".TABLE_BOOK_REG.".bookName like'%".$_POST['searchBookName']."%'";
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table view_limitter pagination_table" >
                <thead>
                  <tr >
				    <th>Sl No</th>               
					<th>AdNo</th>
					<th>Student Name</th>
					<th>Class</th>
					<th>Division</th>
					<th>Book Name</th>
					<th>Return Date</th>
					<th>Delay</th>					
										
                  </tr>
                </thead>
                <tbody>
				<?php 															
						$selAllQuery="SELECT ".TABLE_BOOK_ISSUE.".adNo,".TABLE_STUDENT.".name,".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_BOOK_ISSUE.".ID,".TABLE_BOOK_ISSUE.".bookId,".TABLE_BOOK_REG.".bookNo,".TABLE_BOOK_REG.".bookName,".TABLE_BOOK_ISSUE.".issueDate,".TABLE_BOOK_ISSUE.".returnDate FROM ".TABLE_STUDENT.",".TABLE_CLASS.",".TABLE_DIVISION.",".TABLE_BOOK_ISSUE.",".TABLE_BOOK_REG.",".TABLE_RETURN_DAYS." WHERE ".TABLE_BOOK_ISSUE.".adNo=".TABLE_STUDENT.".adNo and ".TABLE_STUDENT.".class=".TABLE_CLASS.".ID and ".TABLE_STUDENT.".division=".TABLE_DIVISION.".ID and ".TABLE_BOOK_ISSUE.".bookId=".TABLE_BOOK_REG.".ID and ".TABLE_BOOK_ISSUE.".status='pending' AND $cond ORDER BY ID DESC";
						//echo $selAllQuery;
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="8">
									There is no data in list.
								</td>
							</tr>
                             <?php
						}
						else
						{							
						$i=1;
					
					while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];
						    $day=date("Y-m-d");	
												 		
						    $returnDate=$row['returnDate'];					
							$today1=date_create($day);
							//echo $today1;
							$retday1=date_create($returnDate);	
												
							$diff=date_diff($retday1,$today1);									
							$delay=$diff->format("%R%a");	
							$delayDays=$diff->format("%a days");
							?>
					  <tr>
					   <td><?php echo $i++;?>
						  <div class="adno-dtls"> <?php if($libraryEdit || $type=="Admin"){?> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  |<?php }?>  <a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a>  | <a href="do.php?id=<?php echo $tableId; ?>&op=bookReturn" onClick="return return_type()">RETURN</a> </div>
						  
							<!-- Modal3 -->
							  <div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
								<div class="modal-dialog">
								  <div class="modal-content"> 
									<!-- <div class="modal-body clearfix">
								fddhdhdhddhdh
								  </div>-->	 		
									<div role="tabpanel" class="tabarea2"> 
									  
									  <!-- Nav tabs -->
									 <ul class="nav nav-tabs" role="tablist">									  
										<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Book Issued Details</a> </li>										
									</ul>
									  
									  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
										  <table class="table nobg">
											<tbody style="background-color:#FFFFFF">
						
											   <tr>
												<td>Admission Number</td>
												<td> : </td>
												<td><?php  echo $row['adNo']; ?></td>
											  </tr>
											  <tr>
												<td>Name</td>
												<td> : </td>
												<td><?php  echo $row['name']; ?></td>
											  </tr>
											  <tr>
												<td>Class</td>
												<td> : </td>
												<td><?php  echo $row['class']; ?></td>
											  </tr>
											  <tr>
												<td>Division</td>
												<td> : </td>
												<td><?php  echo $row['division']; ?></td>
											  </tr>
											  <tr>
												<td>Book Number</td>
												<td> : </td>
												<td><?php echo $row['bookNo']; ?></td>
											  </tr>
											  <tr>
												<td>Book Name</td>
												<td> : </td>
												<td> <?php  echo $row['bookName']; ?></td>
											  </tr>
											   <tr>
												<td>Issued Date</td>
												<td> : </td>
												<td><?php echo $App->dbformat_date_db($row['issueDate']); ?></td>
											  </tr>	
											   <tr>
												<td>Return Date</td>
												<td> : </td>
												<td><?php echo $App->dbformat_date_db($row['returnDate']); ?></td>
											  </tr>
                                             <tr>
												<td>Delay Days</td>
												<td> : </td>
												<td><?php if($delay>0){ echo $delayDays;} else echo '0'." days"; ?> </td>
											  </tr>
											</tbody>
										  </table>
										</div>
																				
									  </div>
									</div>
								  </div>
								</div>
							  </div>
							  <!-- Modal3 cls --> 
						  
						  
						  </td>
						<td><?php echo $row['adNo']; ?></td>
						<td><?php echo $row['name']; ?></td>						
						<td><?php echo $row['class']; ?> </td> 
						<td><?php echo $row['division']; ?> </td>
						<td><?php echo $row['bookName']; ?> </td>
						<td><?php echo $App->dbformat_date_db($row['returnDate']); ?> </td>
						<td><?php  if($delay>0){ echo $delayDays;} else {echo '0'." days";} ?> </td>                	 	
					  </tr>
					  <?php }
				}?>                  
                </tbody>
              </table>			  	
            </div>
			<!-- paging -->		
			<div style="clear:both;"></div>
			   <div class="text-center">
			   		<div class="btn-group pager_selector"></div>
			   </div>
          	</div>
			<!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">BOOK ISSUE </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="adNo">Admission Number:<span class="star">*</span></label>
					  <input type="text" id="adNo" name="adNo" value="" required title="Admission Number" placeholder="Admission Number" class="form-control2" onfocus="return clearbox('adNoDiv')" />						
					  <span id="user-result"></span>
					  <div id="adNoDiv" class="valid" style="color:#FF9900;"></div>
                    </div>
                    <div class="form-group">
                      <label for="name">Name:</label>
					    <input type="text" id="name" name="name" value=""  title="Name" placeholder="Name" class="form-control2 " readonly />
                    </div>
                    <div class="form-group">
                      <label for="class">Class:</label>  
					  <input type="text" id="class" name="class" value=""  title="Class" placeholder="Class" class="form-control2 " readonly /> 
                    </div>					                   								 
                    <input type="hidden" name="classHidden"  id="classHidden" value="">
                    <div class="form-group">
                      <label for="division">Division:</label>
                    <input type="text" id="division" name="division" value=""  title="Division" placeholder="Division" class="form-control2" readonly />
                    </div>									  	
					<input type="hidden" id="divisionHidden" name="divisionHidden" >
					<div class="form-group">
                      <label for="bookNo">Book Number:<span class="star">*</span></label>   
					                    
						<input type="text" id="bookNo" name="bookNo" value="" placeholder="Book Number:" required title="Book Number" class="form-control2" onfocus="return clearbox('bookNoDiv')" />						
						<span id="user-result1"></span>		
						<div id="bookNoDiv" class="valid" style="color:#FF9900;"></div> 		   
                    </div>									
					
					<div class="form-group">
                      <label for="bookName">Book Name:</label>
					   <input type="text" id="bookName" name="bookName" value=""  title="Book Name" placeholder="Book Name" class="form-control2" readonly />
                    </div>
										
					<div class="form-groupy">
                      <label for="issuedDate">Issued Date:<span class="star">*</span></label>
					  
					  <input type="text" id="issuedDate" name="issuedDate" value="<?php echo date("d/m/Y");?>" required title="Issue Date" class="form-control2 datepicker"  onchange="bookReturn()"  />		  
                    </div>					
					<?php			 
					$retdate=mysql_query("SELECT noOfDays FROM ".TABLE_RETURN_DAYS."");
					$retRow=mysql_fetch_array($retdate);	
					$days=$retRow['noOfDays'];			
		  			?>
					<div class="form-groupy">
                      <label for="numDay">Number Of Days To Handling:<span class="star">*</span></label>
						 <input type="text" name="numDay" id="numDay" value="<?php echo $retRow['noOfDays']; ?>" required class="form-control2" onfocus="return clearbox('numDaydiv')" onchange="bookReturn()" >
						 <div id="numDaydiv" class="valid" style="color:#FF9900;"></div>	  
                    </div>								
                    <div class="form-group">
                      <label for="returnDate">Return Date:<span class="star">*</span></label>
					  	<input type="text" id="returnDate" name="returnDate" value=""  title="Return Date"  readonly class="form-control2" onfocus="return clearbox('returnDatediv')" />
						<div id="returnDatediv" class="valid" style="color:#FF9900;"></div>
                    </div>
				</div>
				 </div>
			
			  <div>
            </div>
            <div class="modal-footer">

              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
	  </div>
      <!-- Modal1 cls --> 
     
  </div>
<?php include("../adminFooter.php") ?>

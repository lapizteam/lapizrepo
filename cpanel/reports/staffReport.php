<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function fnExcelReport()
    {
	
	
             var tab_text="<table border='2px'><tr> <td colspan='17' style='text-align:center; font-size:11px;'><h3>STAFF REPORT</h3></td> </tr>";
             var textRange; var j=0;
          tab = document.getElementById('headerTable'); // id of table1	
		   // id of table1	
		  
try
{
	tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>SL No</td><td bgcolor='#87AFC6'>Staff Id</td><td bgcolor='#87AFC6'>Name</td><td bgcolor='#87AFC6'>Designation</td><td bgcolor='#87AFC6'>Phone</td><td bgcolor='#87AFC6'>Date of Birth</td><td bgcolor='#87AFC6'>Address</td><td bgcolor='#87AFC6'>Date of Join</td><td bgcolor='#87AFC6'>Gender</td><td bgcolor='#87AFC6'>Type</td><td bgcolor='#87AFC6'>Basic Pay</td><td bgcolor='#87AFC6'>HRA</td><td bgcolor='#87AFC6'>CCA</td><td bgcolor='#87AFC6'>PF</td><td bgcolor='#87AFC6'>SLI</td><td bgcolor='#87AFC6'>GLI</td><td bgcolor='#87AFC6'>Account Number</td></tr>";

x=0;
          for(j = 1 ; j < tab.rows.length ; j=j+1) 
          {     
			
			  tab_text = tab_text + "<tr>";
			 dd=0;
		  	 for(k = 0; k < tab.rows[j].cells.length -1; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab.rows[j].cells[k].innerHTML+"</td>";
						dd=1;
						
			 }
			 
			 if(dd==1)
			 {
			 	x++;
		 		tabID="headerTable2"+(x+1);			 		
			 	tab2 = document.getElementById(tabID);
				tab_text = tab_text+"<td>"+tab2.rows[2].cells[2].innerHTML+"</td><td>"+tab2.rows[3].cells[2].innerHTML+"</td><td>"+tab2.rows[5].cells[2].innerHTML+"</td><td>"+tab2.rows[6].cells[2].innerHTML+"</td><td>"+tab2.rows[7].cells[2].innerHTML+"</td>";
				
				tabID3="headerTable3"+(x+1);	
				tab3 = document.getElementById(tabID3);	
				tab_text = tab_text+"<td>"+tab3.rows[0].cells[2].innerHTML+"</td><td>"+tab3.rows[1].cells[2].innerHTML+"</td><td>"+tab3.rows[2].cells[2].innerHTML+"</td><td>"+tab3.rows[3].cells[2].innerHTML+"</td><td>"+tab3.rows[4].cells[2].innerHTML+"</td><td>"+tab3.rows[5].cells[2].innerHTML+"</td><td>"+tab3.rows[6].cells[2].innerHTML+"</td>";
								
				
			}
			tab_text = tab_text + "</tr>";
			
          }

	  
}
catch(err)
{
 alert(err);
}
          tab_text=tab_text+"</table>";
		  tab_text= tab_text.replace("<tr></tr>", "");
		  tab_text= tab_text.replace("<tr>  </tr>", "");
	
          tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
          tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
          tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

               var ua = window.navigator.userAgent;
              var msie = ua.indexOf("MSIE "); 

                 if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                    {
                           txtArea1.document.open("txt/html","replace");
                           txtArea1.document.write(tab_text);
                           txtArea1.document.close();
                           txtArea1.focus(); 
                           sa=txtArea1.document.execCommand("SaveAs",true,"");
                     }  
                  else                 //other browser not tested on IE 11
                      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  


                      return (sa);

	
	}

</script>

<script>
//validation msg
function valid()
{
flag=false;

jSearchName = document.getElementById('searchName').value;
jSearchDesig = document.getElementById('searchDesig').value;

	if(jSearchDesig == "" && jSearchName == "")
	{		
	document.getElementById('searchdiv').innerHTML="At least one field must be filled..";
	flag=true;
	}
		
if(flag==true)
	{
	return false;
	}
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>


      <div class="col-md-10 col-sm-8 rightarea">
        <div class="student-report">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>STAFF REPORT</h2>
            </div>
            <div class="col-sm-8 text-right">
              <form class="form-inline form3" method="post" >
                
               <div class="form-group">
				Staff Name</br> 
                 <input type="text" class="form-control .namebox" value="<?php echo @$_REQUEST['searchName'] ?>" id="searchName" name="searchName" placeholder="Name" onfocus="return clearbox('searchdiv')">
                </div>
                <div class="form-group">
				Staff Designation</br> 
                 <input type="text" class="form-control .namebox" value="<?php echo @$_REQUEST['searchDesig'] ?>" id="searchDesig" name="searchDesig" placeholder="Designation" onfocus="return clearbox('searchdiv')">
                </div>
               <div class="form-group">
					</br>
                   <input type="submit" name="submit" value=""class="btn btn-default lens" title="Search" onsubmit="return valid()"/>
				   <button onclick="fnExcelReport();"  class="btn btn-default export" title="Export To Excel"></button>
				   
                </div>	
				<br />
				<div id="searchdiv" class="valid" style="color:#FF6600;margin-left:-196px;"></div>				 
              </form>
            </div>
          </div>
<?php	
$cond="1";
if(@$_REQUEST['searchName'])
{
	$cond=$cond." and ".TABLE_STAFF.".name like'%".$_POST['searchName']."%'";
}
if(@$_REQUEST['searchDesig'])
{
$cond=$cond." and ".TABLE_STAFF.".designation like'%".$_POST['searchDesig']."%'";
}

?>
          <div class="row">
            <div class="col-sm-12">
              <div class="tablearea3 table-responsive">
                <table class="table view_limitter pagination_table" id="headerTable" >
                  <thead>
                    <tr>
                      <td>SLNO</td>
						<td>Staff ID</td>
						<td>Name</td>
						<td>Designation</td>
						<td>Phone</td>						
						<td>View</td>
                    </tr>
                  </thead>
				  
                  <tbody>
				  <?php					
					$selAllQuery="SELECT * FROM ".TABLE_STAFF." WHERE $cond ORDER BY ID desc";					
					$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="6">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
						
					$i=1;
					while($row=mysql_fetch_array($selectAll))
					{
					$tableId=$row['ID'];
					?>
					<tr>
						<td><?php echo $i;$i++; ?></td>
						<td><?php echo $row['staffId']; ?></td>
						<td><?php echo $row['name']; ?></td>						
						<td><?php echo $row['designation']; ?> </td>
                   	 	<td><?php echo $row['phone']; ?> </td>
						
						<td><a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>" class="viewbtn">View</a>
						<!-- Modal3 -->
						  <div class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
							<div class="modal-dialog">
							  <div class="modal-content"> 										
								<div role="tabpanel" class="tabarea2"> 
								  
								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">									 
										<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">Personal</a> </li>	
										 <li role="presentation" > <a href="#salary<?php echo $tableId; ?>" aria-controls="salary" role="tab" data-toggle="tab">Salary</a> </li>									
									</ul>
								  
								  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
										  <table class="table nobg" id="headerTable2<?php echo $i; ?>">
											<tbody style="background-color:#FFFFFF">
											  <tr> 
												  <td >Staff ID</td>
												  <td>:</td>
												  <td><?php  echo $row['staffId']; ?></td>                    	  
											  </tr>
											  
											  <tr> 
												  <td >Staff Name</td>
												  <td>:</td>
												  <td><?php  echo $row['name']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Date of Birth</td>
												  <td>:</td>
												  <td><?php  echo $App->dbFormat_date_db($row['dob']);  ?></td>                    	  
											  </tr>
											  
											  <tr> 
												  <td >Address</td>
												  <td>:</td>
												  <td><?php  echo $row['address']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Phone</td>
												  <td>:</td>
												  <td><?php  echo $row['phone']; ?></td>                    	  
											  </tr>
											  
											  <tr> 
												  <td >Date Of Join</td>
												  <td>:</td>
												  <td><?php  echo $App->dbFormat_date_db($row['dateOfJoin']);  ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Gender</td>
												  <td>:</td>
												  <td><?php  echo $row['gender']; ?></td>                    	  
											  </tr>
											  <tr> 
												  <td >Type</td>
												  <td>:</td>
												  <td><?php  echo $row['type']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >Designation</td>
												  <td>:</td>
												  <td><?php  echo $row['designation']; ?></td>                    	  
											  </tr>	  
											</tbody>
										  </table>
										</div>
										<div role="tabpanel" class="tab-pane" id="salary<?php echo $tableId; ?>">
										  <table class="table nobg" id="headerTable3<?php echo $i; ?>">
											<tbody style="background-color:#FFFFFF">
                                           
											  <tr> 
												  <td >Basic Pay</td>
												  <td>:</td>
												  <td><?php  echo $row['basicPay']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >HRA</td>
												  <td>:</td>
												  <td><?php  echo $row['hra']; ?></td>                    	  
											  </tr>
											  <tr> 
												  <td >CCA</td>
												  <td>:</td>
												  <td><?php  echo $row['cca']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >PF</td>
												  <td>:</td>
												  <td><?php  echo $row['pf']; ?></td>                    	  
											  </tr>
											  <tr> 
												  <td >SLI</td>
												  <td>:</td>
												  <td><?php  echo $row['sli']; ?></td>                    	  
											  </tr>
											  
											 <tr> 
												  <td >GLI</td>
												  <td>:</td>
												  <td><?php  echo $row['gli']; ?></td>                    	  
											  </tr>
											  <tr> 
												  <td >Account Number</td>
												  <td>:</td>
												  <td><?php  echo $row['accountNo']; ?></td>                    	  
											  </tr>
                                              	
											</tbody>
										  </table>
										</div>
									  </div>
								</div>
							  </div>
							</div>
						  </div>
						  <!-- Modal3 cls --> 
						
						
						
						
						</td>
					 </tr>
					<?php
					}
			}
					?>
                  </tbody>
                </table>				 
				
              </div>
			  <!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
            </div>
          </div>
        </div>
      </div>
	 
     
      
      
      
  <?php include("../adminFooter.php") ?>
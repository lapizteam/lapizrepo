<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();


?>
<script>
function fnExcelReport()
    {
	
             var tab_text="<table border='2px'><tr> <td colspan='10' style='text-align:center; font-size:11px;'><h3>MESS FEE PAYMENT REPORT</h3></td> </tr>";
             var textRange; var j=0;
          tab = document.getElementById('headerTable'); // id of table1	
		  //alert (tab.rows.length);
		  
try
{
	tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>SL NO</td><td bgcolor='#87AFC6'>Admission No</td><td bgcolor='#87AFC6'>Name</td><td bgcolor='#87AFC6'>Month</td><td bgcolor='#87AFC6'>Paid Date</td><td bgcolor='#87AFC6'>Voucher No</td><td bgcolor='#87AFC6'>Paid Amount</td><td bgcolor='#87AFC6'>Class</td><td bgcolor='#87AFC6'>Division</td><td bgcolor='#87AFC6'>Academic Year</td></tr>";

x=0;
          for(j = 1 ; j < tab.rows.length ; j++) 
          {     
			
			  tab_text = tab_text + "<tr>";
			 dd=0;
		  	 for(k = 0; k < tab.rows[j].cells.length -1; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab.rows[j].cells[k].innerHTML+"</td>";
						dd=1;
						
			 }
			 
			 
			 
			 
			 
			 
			 if(dd==1)
			 {
			 	x++;
		 		tabID="headerTable2"+(x+1);			 		
			 	tab2 = document.getElementById(tabID);
				tab_text=tab_text+"<td>"+tab2.rows[2].cells[2].innerHTML+"</td><td>"+tab2.rows[3].cells[2].innerHTML+"</td><td>"+tab2.rows[4].cells[2].innerHTML+"</td>";
				
			}
			
			tab_text = tab_text + "</tr>";
			
          }

	  
}
catch(err)
{
 alert(err);
}
          tab_text=tab_text+"</table>";
		  tab_text= tab_text.replace("<tr></tr>", "");
		  tab_text= tab_text.replace("<tr>  </tr>", "");
	
          tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
          tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
                      tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

               var ua = window.navigator.userAgent;
              var msie = ua.indexOf("MSIE "); 

                 if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                    {
                           txtArea1.document.open("txt/html","replace");
                           txtArea1.document.write(tab_text);
                           txtArea1.document.close();
                           txtArea1.focus(); 
                           sa=txtArea1.document.execCommand("SaveAs",true,"");
                     }  
                  else                 //other browser not tested on IE 11
                      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  


                      return (sa);

	}

</script>
<script>
function getDivision(str) 
{

  if (str=="") {
  str=0;
   // document.getElementById("searchDivision").innerHTML="";
    //return;
  }
 
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("searchDivision").innerHTML=xmlhttp.responseText;
    }
  }
//  xmlhttp.open("GET","work_rate_1.php?q="+str+"&c="+client+"&df="+date_from+"&dt="+date_to,true);
   xmlhttp.open("GET","division.php?q="+str,true);
 
  xmlhttp.send();
}
</script>

<script>
//validation msg
function valid()
{
flag=false;

jSearchAdNo = document.getElementById('searchAdNo').value;
jSearchName = document.getElementById('searchName').value;
jSearchDiv = document.getElementById('searchDivision').value;
jSearchClass = document.getElementById('searchClass').value;
	
	if((jSearchAdNo == "") && (jSearchName == "") && (jSearchDiv == "") && (jSearchClass == ""))
	{		
	document.getElementById('searchdiv').innerHTML="At least one field must be filled.";
	flag=true;
	}
		
if(flag==true)
	{
	return false;
	}
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}
</script>

      <div class="col-md-10 col-sm-8 rightarea">
        <div class="student-report">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>MESS FEE PAYMENT REPORT</h2>
            </div>
            <div class="col-sm-8 text-right">
             <form class="form-inline form3" method="post">
              
				<div class="form-group">
				Year:</br> 
               <select name="searchYear" id="searchYear" class="form-control .namebox"  required>                    
					<?php 
					$select1="select * from ".TABLE_ACADEMICYEAR."";
					$res=mysql_query($select1);
					while($row=mysql_fetch_array($res))
					{
					?>					
                          <option value="<?php echo $row['ID']?>" <?php if($row['ID']==@$_POST['searchYear']){echo 'selected';}?>><?php echo $row['fromYear']."-".$row['toYear']?></option>
                          <?php 
								}
							?>
                  </select>
                </div>
                 <div class="form-group">
				Month:</br>                 
				<select name="searchMonth" id="searchMonth" class="form-control .namebox"  required>
					<option value="">Select</option>
					<?php 
					$select1="select * from ".TABLE_MONTH."";
					$res=mysql_query($select1);
					while($row=mysql_fetch_array($res))
					{
					?>					
					<option value="<?php echo $row['ID']?>"  <?php if($row['ID']==@$_POST['searchMonth']){echo 'selected';}?>><?php echo $row['monthName']?></option>
					<?php 
					}
					?>
				</select>	
                </div>
                
                <div class="form-group">
				Class</br> 
                  <select name="searchClass" id="searchClass" onChange="getDivision(this.value)" class="form-control .namebox" onfocus="clearbox('searchdiv')">
                          <option value="">Class</option>
                          <?php 
								$select3="select * from ".TABLE_CLASS."";
								$res=mysql_query($select3);
								while($row=mysql_fetch_array($res))
								{
							?>
                          <option value="<?php echo $row['ID']?>" <?php if($row['ID']==@$_POST['searchClass']){echo 'selected';}?>><?php echo $row['class']?></option>
                          <?php 
								}
							?>
                  </select>
                </div>
                <div class="form-group">
				Division</br> 
                 <select name="searchDivision" id="searchDivision" class="form-control .namebox" onfocus="clearbox('searchdiv')">
								<option value=""><?php if(isset($_POST['submit']))
												{
												@$m=@$_POST['searchDivision'];
													if($m=='')
													{
														echo "Select";
													}
													else
													{
														@$res3=mysql_query("SELECT * FROM ".TABLE_DIVISION." WHERE ID='$m'");
														@$row3=mysql_fetch_array(@$res3);
														echo @$row3['division'];
													}
												} 
												else
												{ 
												echo "Select";
												}?></option>						
							</select>	
                </div>
                
				 <div class="form-group">
				Name</br> 
                  <input type="text" class="form-control .namebox" value="<?php echo @$_REQUEST['searchName'] ?>" id="searchName" name="searchName" placeholder="Name" onfocus="clearbox('searchdiv')">
                </div>
				<div class="form-group">
					</br>
                   <input type="submit" name="submit" value=""class="btn btn-default lens" title="Search"/>
				   <button onclick="fnExcelReport();"  class="btn btn-default export" title="Export To Excel"></button>

                </div>
                
              </form>
            </div>
          </div>
<?php	
$cond="1";
$cond2="1";
$cond3="1";

if(@$_REQUEST['searchYear'])
{	
	$cond2=$cond2." and ".TABLE_MESSFEESETTING.".year like'%".$_POST['searchYear']."%'";
	$cond3=$cond3." and ".TABLE_MESSFEEPAYMENT.".acYear like'%".$_POST['searchYear']."%'";
}
if(@$_REQUEST['searchMonth'])
{	
	$cond2=$cond2." and ".TABLE_MESSFEESETTING.".month like'%".$_POST['searchMonth']."%'";
	$cond3=$cond3." and ".TABLE_MESSFEEPAYMENT.".month like'%".$_POST['searchMonth']."%'";
}
if(@$_REQUEST['searchClass'])
{	
	$cond=$cond." and ".TABLE_STUDENT.".class like'%".$_POST['searchClass']."%'";
	$cond2=$cond2." and ".TABLE_MESSFEESETTING.".class like'%".$_POST['searchClass']."%'";
}
if(@$_REQUEST['searchDivision'])
{	
	$cond=$cond." and ".TABLE_STUDENT.".division like'%".$_POST['searchDivision']."%'";
}
if(@$_REQUEST['searchName'])
{	
	$cond=$cond." and ".TABLE_STUDENT.".name like'%".$_POST['searchName']."%'";
}
?>
          <div class="row">
            <div class="col-sm-12">
              <div class="tablearea3 table-responsive">
                <table class="table view_limitter pagination_table" id="headerTable" >
                  <thead>
                    <tr>
                      <td>SLNO</td>
					  	<td>Admission No</td>
						<td>Name</td>
						<td>Month</td>
						<td>Paid Date</td>
						<td>Voucher No</td>
						<td>Paid Amount</td>
                        <td>View</td>
                    </tr>
                  </thead>
				  
                  <tbody>
				  <?php					
					$selAllQuery="SELECT ".TABLE_STUDENT.".adNo,".TABLE_STUDENT.".name,".TABLE_CLASS.".class,".TABLE_CLASS.".ID as cId,".TABLE_DIVISION.".division from ".TABLE_STUDENT.",".TABLE_CLASS.",".TABLE_DIVISION.",".TABLE_CLASSMESSFEE."   where ".TABLE_STUDENT.".class=".TABLE_CLASS.".ID and ".TABLE_DIVISION.".ID=".TABLE_STUDENT.".division and ".TABLE_CLASSMESSFEE.".adNo=".TABLE_STUDENT.".adNo and studentTcStatus='no' and $cond GROUP BY ". TABLE_STUDENT.".adNo";
					//echo $selAllQuery;
					$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="8">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
					while($row=mysql_fetch_array($selectAll))
					{
					
					$adNo=$row['adNo'];
					$cId=$row['cId'];				
					
					$totalQry="SELECT fee from `".TABLE_MESSFEESETTING."` WHERE $cond2";					
					$TotalToBePaid = $db->query_first($totalQry);
					$AmountToBePaid=$TotalToBePaid['fee']; 
																			  
					$tableView="SELECT ".TABLE_MESSFEEPAYMENT.".ID,".TABLE_MESSFEEPAYMENT.".month,".TABLE_MONTH.".monthName,".TABLE_MESSFEEPAYMENT.".payDate ,".TABLE_MESSFEEPAYMENT.".voucherNo,".TABLE_MESSFEEPAYMENT.".paidAmount,".TABLE_ACADEMICYEAR.".fromYear,".TABLE_ACADEMICYEAR.".toYear FROM `".TABLE_MESSFEEPAYMENT."`,`".TABLE_ACADEMICYEAR."`,".TABLE_MONTH." WHERE ".TABLE_MESSFEEPAYMENT.".adNo='$adNo' AND ".TABLE_MESSFEEPAYMENT.".acYear = ".TABLE_ACADEMICYEAR.".ID and ".TABLE_MESSFEEPAYMENT.".month=".TABLE_MONTH.".ID AND $cond3";
					
					//echo $tableView;
					$tableView_Qry = mysql_query($tableView);
					$tableView_Res = mysql_fetch_array($tableView_Qry);
					$tableViewRows = mysql_num_rows($tableView_Qry );
					//$AmountPaid=$tableView_Res['paidAmount']; 	
					
					$tableId=$tableView_Res['ID'];
					
					//$balance = $AmountToBePaid-$AmountPaid;
					
					if($tableViewRows >0)
					{
					?>
					<tr>
						<td><?php echo $i;$i++; ?></td>
						<td><?php echo $row['adNo']; ?> </td>
						<td><?php echo $row['name']; ?> </td>
						<td><?php echo $tableView_Res['monthName']; ?> </td>
						<td><?php echo $App->dbformat_date_db($tableView_Res['payDate']); ?> </td>
						<td><?php echo $tableView_Res['voucherNo']; ?> </td>
						<td><?php echo $tableView_Res['paidAmount']?></td>
										
						<td><a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>" class="viewbtn">View</a>
						<!-- Modal3 -->
						  <div class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
							<div class="modal-dialog">
							  <div class="modal-content"> 										
								<div role="tabpanel" class="tabarea2"> 
								  
								  <!-- Nav tabs -->
								  <ul class="nav nav-tabs" role="tablist">
									  <li role="presentation" class="active"> <a href="#academic<?php echo $tableId; ?>" aria-controls="academic" role="tab" data-toggle="tab">Fee Payment Details</a> </li>
										
									</ul>
								  
								  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="academic<?php echo $tableId; ?>">
										  <table class="table nobg" id="headerTable2<?php echo $i; ?>">
											<tbody style="background-color:#FFFFFF">
						
											   <tr>
												  <td >Name</td>
												  <td> : </td>
												  <td><?php  echo $row['name']; ?></td>
											  </tr>
											  
											  <tr>
												  <td >Admission No</td>
												  <td> : </td>
												  <td><?php  echo $row['adNo']; ?></td>
											  </tr>
											  
											  <tr>
												<td >Class</td>
												<td> : </td>
												 <td><?php  echo $row['class']; ?></td>  
											  </tr>
																	
											  <tr>
												<td >Division</td>
												<td> : </td>
												<td><?php  echo $row['division']; ?></td>  
											  </tr>
											  
											  <tr>
												<td >Academic Year</td>
												<td> : </td>
												<td><?php  echo $tableView_Res['fromYear']."-".$tableView_Res['toYear']; ?></td>  
											  </tr>
																	
											  <tr>
												<td >Month</td>
												<td> : </td>
												<td><?php  echo $tableView_Res['monthName']; ?></td>  
											  </tr>
											  <tr>
												  <td >Amount paid</td>
												  <td> : </td>
												  <td><?php echo $tableView_Res['paidAmount']; ?>	</td>			                                           
											  </tr>
												
											  <tr>
												  <td >Voucher Number</td>
												  <td> : </td>
												  <td><?php  echo $tableView_Res['voucherNo']; ?></td>
											  </tr>                                                               			 		
											
											<tr>
											  <td>Paid Date</td>
											  <td> : </td>
											  <td><?php echo $App->dbformat_date_db($tableView_Res['payDate']); ?></td>
											</tr>                                             											  
											</tbody>
										  </table>
										</div>
										
									  </div>
								</div>
							  </div>
							</div>
						  </div>
						  <!-- Modal3 cls --> 
						
						
						
						
						</td>
					 </tr>
					<?php
					}
					}
			}
					?>
                  </tbody>
                </table>
				
              </div>
			  <!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
            </div>
          </div>
        </div>
      </div>
      
     
      
      
      
  <?php include("../adminFooter.php") ?>
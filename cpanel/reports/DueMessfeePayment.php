<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>
<script>
function fnExcelReport()
    {
	
             var tab_text="<table border='2px'><tr> <td colspan='8' style='text-align:center; font-size:11px;'><h3>DUE MESS FEE PAYMENT REPORT</h3></td> </tr>";
             var textRange; var j=0;
          tab = document.getElementById('headerTable'); // id of table1	
		   // id of table1	
		  
try
{
	tab_text=tab_text+"<tr><td bgcolor='#87AFC6'>SL No</td><td bgcolor='#87AFC6'>Admission No</td><td bgcolor='#87AFC6'>Name</td><td bgcolor='#87AFC6'>Class</td><td bgcolor='#87AFC6'>Division</td><td bgcolor='#87AFC6'>Amount to be Paid</td><td bgcolor='#87AFC6'>Paid Amount</td><td bgcolor='#87AFC6'>Balance</td></tr>";

x=0;
          for(j = 1 ; j < tab.rows.length ; j=j+1) 
          {     
			
			  tab_text = tab_text + "<tr>";
			 dd=0;
		  	 for(k = 0; k < tab.rows[j].cells.length; k++ ) 
			 {			 	
                tab_text=tab_text+"<td>"+tab.rows[j].cells[k].innerHTML+"</td>";
						dd=1;
						
			 }
			 
			
			
			tab_text = tab_text + "</tr>";
			
          }

	  
}
catch(err)
{
 alert(err);
}
          tab_text=tab_text+"</table>";
		  tab_text= tab_text.replace("<tr></tr>", "");
		  tab_text= tab_text.replace("<tr>  </tr>", "");
	
          tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
          tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
                      tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

               var ua = window.navigator.userAgent;
              var msie = ua.indexOf("MSIE "); 

                 if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                    {
                           txtArea1.document.open("txt/html","replace");
                           txtArea1.document.write(tab_text);
                           txtArea1.document.close();
                           txtArea1.focus(); 
                           sa=txtArea1.document.execCommand("SaveAs",true,"");
                     }  
                  else                 //other browser not tested on IE 11
                      sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  


                      return (sa);

	}

</script>
<script>
function getDivision(str) 
{

  if (str=="") {
  str=0;
   // document.getElementById("searchDivision").innerHTML="";
    //return;
  }
 
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("searchDivision").innerHTML=xmlhttp.responseText;
    }
  }
//  xmlhttp.open("GET","work_rate_1.php?q="+str+"&c="+client+"&df="+date_from+"&dt="+date_to,true);
   xmlhttp.open("GET","division.php?q="+str,true);
 
  xmlhttp.send();
}
</script>
<?php
if(!@$_REQUEST['frdate'])
{
	$cDate = date('d/m/Y');	
}
else
{
	$cDate =$_REQUEST['frdate'];
}

?>
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="student-report">
          <div class="row rightareatop">
            <div class="col-sm-4">
              <h2>MESS FEE PAYMENT DUES</h2>
            </div>
            <div class="col-sm-8 text-right">
             <form class="form-inline form3" method="post">
                
                <div class="form-group">
				Year:</br> 
               <select name="searchYear" id="searchYear" class="form-control .namebox"  required>                    
					<?php 
					$select1="select * from ".TABLE_ACADEMICYEAR."";
					$res=mysql_query($select1);
					while($row=mysql_fetch_array($res))
					{
					?>
					<option value="<?php echo $row['ID']?>" <?php if($row['ID']==@$_POST['searchYear']){ ?> selected="selected"<?php }?>><?php echo $row['fromYear']."-".$row['toYear']?></option>
					<?php 
					}
					?>
				</select>         
                </div>
                 <div class="form-group">
				Month:</br>                 
				<select name="searchMonth" id="searchMonth" class="form-control .namebox"  required>
					<option value="">Select</option>
					<?php 
					$select1="select * from ".TABLE_MONTH."";
					$res=mysql_query($select1);
					while($row=mysql_fetch_array($res))
					{
					?>
					<option value="<?php echo $row['ID']?>"  <?php if($row['ID']==@$_POST['searchMonth']){?> selected="selected" <?php }?> ><?php echo $row['monthName']?></option>
					<?php 
					}
					?>
				</select>	
                </div>
                
                <div class="form-group">
				Class:</br> 
                <select name="searchClass" id="searchClass" onChange="getDivision(this.value)" class="form-control .namebox"  required>
					<option value="">Class</option>
					<?php 
					$select3="select * from ".TABLE_CLASS."";
					$res=mysql_query($select3);
					while($row=mysql_fetch_array($res))
					{
					?>
					<option value="<?php echo $row['ID']?>" <?php if($row['ID']==@$_REQUEST['searchClass']){?> selected="selected"<?php }?>><?php echo $row['class']?></option>
					<?php 
					}
					?>						
				</select>
                </div>
                
				<div class="form-group">
				Admission Number:</br> 
                  <input type="text" name="searchAdNo" id="searchAdNo" value="<?php echo @$_POST['searchAdNo'];?>" class="form-control .namebox"  placeholder="Admission Number">
                </div>
				<div class="form-group">
					</br>
                   <input type="submit" name="submit" value=""class="btn btn-default lens" title="Search"/>
				   <button onclick="fnExcelReport();"  class="btn btn-default export" title="Export To Excel"></button>

                </div>
                
              </form>
            </div>
          </div>
<?php	
$cond="1";
$cond2="1";
$cond3="1";


if(@$_REQUEST['searchYear'])
{		
	$cond2=$cond2." and ".TABLE_MESSFEESETTING.".year like'%".$_REQUEST['searchYear']."%'";
	$cond3=$cond3." and ".TABLE_MESSFEEPAYMENT.".acYear like'%".$_REQUEST['searchYear']."%'";
}
if(@$_REQUEST['searchMonth'])
{	
	$cond2=$cond2." and ".TABLE_MESSFEESETTING.".month like'%".$_REQUEST['searchMonth']."%'";
	$cond3=$cond3." and ".TABLE_MESSFEEPAYMENT.".month like'%".$_REQUEST['searchMonth']."%'";
}
if(@$_REQUEST['searchClass'])
{		
	//$cond=$cond." and ".TABLE_STUDENT.".class like'%".$_REQUEST['searchClass']."%'";
	$cond2=$cond2." and ".TABLE_MESSFEESETTING.".class like'%".$_REQUEST['searchClass']."%'";
}
if(@$_REQUEST['searchAdNo'])
{	
	$cond2=$cond2." and ".TABLE_CLASSMESSFEE.".adNo ='".$_REQUEST['searchAdNo']."'";
	$cond3=$cond3." and ".TABLE_MESSFEEPAYMENT.".adNo ='".$_REQUEST['searchAdNo']."'";
}

?>
<div class="row">
            <div class="col-sm-12">
              <div class="tablearea3 table-responsive">
 <table class="table view_limitter pagination_table" id="headerTable" >
                  <thead>
                    <tr>
                      	<td>SLNO</td>
					  	<td>Admission No</td>
						<td>Name</td>
						<td>Class</td>
						<td>Division</td>
						<td>Amount To be Paid</td>
						<td>Paid Amount</td>
                        <td>Balance</td>
					
                    </tr>
                  </thead>
				  
                  <tbody>
<?php
		
					$selAllQuery = "SELECT ".TABLE_STUDENT.".name,".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_MESSFEESETTING.".year,".TABLE_MESSFEESETTING.".month,".TABLE_MESSFEESETTING.".class,".TABLE_MESSFEESETTING.".fee,".TABLE_CLASSMESSFEE.".adNo FROM ".TABLE_MESSFEESETTING.",".TABLE_CLASSMESSFEE.",".TABLE_CLASS.",".TABLE_STUDENT.",".TABLE_DIVISION." WHERE ".TABLE_CLASSMESSFEE.".messFeeSetId=".TABLE_MESSFEESETTING.".ID AND ".TABLE_CLASSMESSFEE.".adNo = ".TABLE_STUDENT.".adNo AND ".TABLE_MESSFEESETTING.".class=".TABLE_CLASS.".ID AND ".TABLE_STUDENT.".division=".TABLE_DIVISION.".ID  AND $cond2";						
					//echo $selAllQuery;
					$selectAll= $db->query($selAllQuery);
					$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="8">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{
							$i=1;
					
							while($row=mysql_fetch_array($selectAll))
							{	
											
							$adNo=$row['adNo'];
							$AmountToBePaid = $row['fee'];		
																					  
							$tableView="SELECT paidAmount FROM `".TABLE_MESSFEEPAYMENT."` WHERE adNo='$adNo' AND $cond3";
							$tableView_Res = $db->query_first($tableView);
							$AmountPaid=$tableView_Res['paidAmount']; 	
							//echo $tableView;
							$balance = $AmountToBePaid-$AmountPaid;												
								if($balance>0)
								{									
								?>
								<tr>
									<td><?php echo $i;$i++;  ?> </td>
									<td><?php echo $row['adNo']; ?> </td>
									<td><?php echo $row['name']; ?> </td>
									<td><?php echo $row['class']; ?> </td>
									<td><?php echo $row['division']; ?> </td>
									<td><?php echo $AmountToBePaid ?> </td>
									<?php
									if($AmountPaid!='')
									{
									?>
										<td><?php echo $AmountPaid ?></td>
									<?php
									}
									else
									{
									?>
										<td>0</td>
									<?php
									}
									?>   						
									<td><?php echo $balance ?> </td>                                          
								</tr>
								<?php					
								}//if($balance>0)
							}//while
						}	//else								
					?>
                  </tbody>
                </table>
				
				</div>
				<!-- paging -->		
				<div style="clear:both;"></div>
				<div class="text-center">
					<div class="btn-group pager_selector"></div>
				</div>        
				<!-- paging end-->
              </div>
            </div>
          </div>
        </div>
      </div>
      
     
      
      
      
  <?php include("../adminFooter.php") ?>         
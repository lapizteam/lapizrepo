<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../core/logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['class'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$class				=	ucfirst($App->convert($_REQUEST['class']));					
				
				$existId=$db->existValuesId(TABLE_CLASS,"class='$class'");
				if($existId>0)
				{			
				$_SESSION['msg']="Class Is Already Exist!!";					
				header("location:new.php");
				}
				else
				{
				
				$data['class']		=	$class;
						
				$success=$db->query_insert(TABLE_CLASS, $data);								
				$db->close();
												
				if($success)
					{
					$_SESSION['msg']="Class Details added successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}	
				}
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['class'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
								
				$class				=	ucfirst($App->convert($_REQUEST['class']));		
				
				$existId=$db->existValuesId(TABLE_CLASS,"class='$class' AND ID!='{$fid}'");
				if($existId>0)
				{				
				$_SESSION['msg']="Class Is Already Exist!!";					
				header("location:edit.php?id=$fid");
				}
				else
				{
				$data['class']		=	$class;
								
				$success=$db->query_update(TABLE_CLASS, $data, "ID='{$fid}'");					
     			$db->close();
				 				
				
				if($success)
					{
					$_SESSION['msg']="Class Details updated successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}	
				}												
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];			
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				//$success=$db->query("DELETE FROM `".TABLE_DIVISION."` WHERE ID='{$id}'");	
				
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_CLASS."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}								
				$db->close(); 	
			
				
				if($success)
					{
					$_SESSION['msg']="Class Details deleted successfully";										
					}
					else
					{
					$_SESSION['msg']="You can't delete. Because this data is used some where else";									
					}	
					header("location:new.php");						
		break;		
}
?>
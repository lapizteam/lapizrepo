<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];
$uid=$_SESSION['LogID'];
switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['class'])
			{				
				$_SESSION['msg']="Error,Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				$year=$_REQUEST['year'];	
				$month=$_REQUEST['month'];
				$class=$_REQUEST['class'];								
				
				$existId=$db->existValuesId(TABLE_MESSFEESETTING,"year=$year AND month=$month AND class=$class");
				if($existId>0)
				{
					
					//$data['fee']		=	$App->convert($_REQUEST['fee']);
					//$data['loginId']	=	$App->convert($uid);
							
					//$db->query_update(TABLE_MESSFEESETTING, $data,"ID='{$existId}'");						
					$_SESSION['msg']="Mess Fee is already existing for this item";					
					header("location:new.php");							
				}
				else
				{					
					
					$data['year']		=	$App->convert($_REQUEST['year']);
					$data['month']		=	$App->convert($_REQUEST['month']);
					$data['class']		=	$App->convert($_REQUEST['class']);
					$data['fee']		=	$App->convert($_REQUEST['fee']);
					$data['loginId']	=	$App->convert($uid);
							
					$success=$db->query_insert(TABLE_MESSFEESETTING, $data);								
					$db->close();
										
					
					if($success)
					{					
					$_SESSION['msg']="Mess Fee Added Successfully";															
					}
					else
					{
					$_SESSION['msg']="Failed";								
					}
					header("location:new.php");		
				}
				
			}		
		break;		
		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				try
				{			
				$success= @mysql_query("DELETE FROM `".TABLE_MESSFEESETTING."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}					
				
				//$success=$db->query("DELETE FROM `".TABLE_MESSFEESETTING."` WHERE ID='{$id}'");								
				$db->close(); 					
				
				if($success)
					{					
					$_SESSION['msg']="Mess Fee Details deleted successfully";														
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";								
					}
					header("location:new.php");					
		break;		
}
?>
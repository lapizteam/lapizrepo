<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

//$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
//$db->connect();
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
		
	case 'new':
		
		if(!$_POST['from_ledger'] || !$_POST['to_ledger'] || !$_POST['amount'] || !$_POST['transaction_date'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");	
			}
		else
			{
			
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$s=0;
				$s2=0;
				$uid = $_SESSION['LogID'];	
				/* ------------------------------------------ */
				$vou_sql ="SELECT MAX(voucher_no) AS vn FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Other Payments' ";
				$vou_record = $db->query_first($vou_sql);
				/* ------------------------------------------ */																
				/* ------------------------------------------ */
				$tsdata['from_ledger'] 		=  	$App->convert($_POST['to_ledger']);
				$tsdata['to_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tsdata['voucher_no']		= 	 $vou_record['vn']+1;
				$tsdata['voucher_type']		=  	'Other Payments';
				$tsdata['credit']			= 	 0;
				$tsdata['debit']			=  $App->convert($_POST['amount']);
				$tsdata['cdate']			=	"NOW()";
				$tsdata['transaction_date']	=	$App->dbformat_date($_POST['transaction_date']);
				$tsdata['remark']			=  	$App->convert($_POST['remark']);
				$tsdata['uid']				=	$_SESSION['LogID'];
				$tsdata['loginId']			=	$_SESSION['LogID'];

				$s=$db->query_insert(TABLE_TRANSACTION, $tsdata);
				
				/* ------------------------------------------ */
				/* ------------------------------------------ */
				$tfdata['from_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tfdata['to_ledger'] 		=  	$App->convert($_POST['to_ledger']);
				$tfdata['voucher_no']		= 	 $vou_record['vn']+1;
				$tfdata['voucher_type']		=  	'Other Payments';
				$tfdata['credit']			=   $App->convert($_POST['amount']);
				$tfdata['debit']			= 	0;
				$tfdata['cdate']			=	"NOW()";
				$tfdata['transaction_date']	=	$App->dbformat_date($_POST['transaction_date']);
				$tfdata['remark']			=  	$App->convert($_POST['remark']);
				$tfdata['uid']				=	$_SESSION['LogID'];
				$tfdata['loginId']			=	$_SESSION['LogID'];
			
				$s2=$db->query_insert(TABLE_TRANSACTION, $tfdata);
				//echo TABLE_TRANSACTION, $tfdata;die;
				/* ------------------------------------------ */
			
				if($s && $s2)
					{
					$_SESSION['msg']="Payment stored successfully";																				
					}
					else
					{
					$_SESSION['msg']="Failed";									
					}	
					header("location:new.php");	
					
			}
		
		break;
	
	// EDIT SECTION
	//-
	case 'edit':
		
		
		$tid			=	$_REQUEST['ledgerid'];
		$uid 			= 	$_SESSION['LogID'];
		if(!$_POST['from_ledger'] || !$_POST['to_ledger'] || !$_POST['amount'] || !$_POST['transaction_date'])
			{			
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$tid");	
			}
		else
			{
				/* ------------------------------------------ */
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$s=0;
				$s2=0;
				
				/* ------------------------------------------ */
				$vou_sql	=	"SELECT * FROM `".TABLE_TRANSACTION."` WHERE `id`='$tid' AND `voucher_type`='Other Payments' ";
				$vou_record = 	$db->query_first($vou_sql);
				$voucher_no = 	$vou_record['voucher_no'];
				
				$del_vou_sql = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Other Payments' AND `voucher_no`='$voucher_no'";
				$db->query($del_vou_sql);
				/* ------------------------------------------ */																
				/* ------------------------------------------ */
				$tsdata['from_ledger'] 		=  	$App->convert($_POST['to_ledger']);
				$tsdata['to_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tsdata['voucher_no']		= 	$voucher_no;
				$tsdata['voucher_type']		=  	'Other Payments';
				$tsdata['credit']			= 	0;
				$tsdata['debit']			=   $App->convert($_POST['amount']);
				$tsdata['cdate']			=	"NOW()";
				$tsdata['transaction_date']	=	$App->dbformat_date($_POST['transaction_date']);
				$tsdata['remark']			=  	$App->convert($_POST['remark']);
				$tsdata['uid']				=	$uid;
				$tsdata['loginId']			=	$uid;

				$s=$db->query_insert(TABLE_TRANSACTION, $tsdata);
				/* ------------------------------------------ */
				/* ------------------------------------------ */
				$tfdata['from_ledger'] 		=  	$App->convert($_POST['from_ledger']);
				$tfdata['to_ledger'] 		=  	$App->convert($_POST['to_ledger']);
				$tfdata['voucher_no']		= 	$voucher_no;
				$tfdata['voucher_type']		=  	'Other Payments';
				$tfdata['credit']			=   $App->convert($_POST['amount']);
				$tfdata['debit']			= 	0;
				$tfdata['cdate']			=	"NOW()";
				$tfdata['transaction_date']	=	$App->dbformat_date($_POST['transaction_date']);
				$tfdata['remark']			=  	$App->convert($_POST['remark']);
				$tfdata['uid']				=	$uid;
				$tfdata['loginId']			=	$uid;

				$s2 = $db->query_insert(TABLE_TRANSACTION, $tfdata);
				//echo TABLE_TRANSACTION, $tfdata;
				/* ------------------------------------------ */
				$db->close();
				
				if($s && $s2)
					{
					$_SESSION['msg']="Payment updated successfully";																			
					}
					else
					{
					$_SESSION['msg']="Failed";									
					}	
					header("location:new.php");	
			}
		
		break;
	
	// DELETE SECTION
	//-
	case 'delete':

		
				$tid	=	$_REQUEST['id'];
				$uid 	= 	$_SESSION['LogID'];
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$vou_sql	=	"SELECT * FROM `".TABLE_TRANSACTION."` WHERE `id`='$tid' AND `voucher_type`='Other Payments' ";
				$vou_record = 	$db->query_first($vou_sql);
				$voucher_no = 	$vou_record['voucher_no'];
		
				$del_vou_sql = "DELETE FROM `".TABLE_TRANSACTION."` WHERE `voucher_type`='Other Payments' AND `voucher_no`='$voucher_no'";
				$success=$db->query($del_vou_sql);	
				$db->close(); 
			
				if($success)
					{
					$_SESSION['msg']="Payment deleted successfully";													
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";									
					}	
					header("location:new.php");	

		break;

}
?>
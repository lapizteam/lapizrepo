<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['rack'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$data['rackNo']				=	$App->convert($_REQUEST['rack']);
				$data['discription']		=	$App->convert($_REQUEST['discription']);
				
						
				$success=$db->query_insert(TABLE_RACK_REG, $data);								
				$db->close();
								
				if($success)
					{										
					$_SESSION['msg']="Rack Details added successfully";																										
					}
					else
					{
					$_SESSION['msg']="Failed";								
					}
					header("location:new.php");		
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['rack'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$data['rackNo']				=	$App->convert($_REQUEST['rack']);
				$data['discription']		=	$App->convert($_REQUEST['discription']);
				
				$success=$db->query_update(TABLE_RACK_REG, $data, " ID='{$fid}'");					
     			$db->close();				 
				
				if($success)
					{										
					$_SESSION['msg']="Rack Details updated successfully";																										
					}
					else
					{
					$_SESSION['msg']="Failed";								
					}
					header("location:new.php");													
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				//$success=$db->query("DELETE FROM `".TABLE_RACK_REG."` WHERE ID='{$id}'");
				try
				{			
				$success= @mysql_query("DELETE FROM `".TABLE_RACK_REG."` WHERE ID='{$id}'");				      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}									
				$db->close(); 	
				
				if($success)
					{										
					$_SESSION['msg']="Rack Details deleted successfully";																					
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";								
					}
					header("location:new.php");					
		break;		
}
?>
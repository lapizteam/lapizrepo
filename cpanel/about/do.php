<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");


if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$uid	=	$_SESSION['LogID'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['name'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{	
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
					$data['schoolName']		=	strtoupper($App->convert($_REQUEST['name']));
					$data['location']		=	ucwords ($App->convert($_REQUEST['location']));
					$data['address']		=	ucwords ($App->convert($_REQUEST['address']));
					$data['phone']			=	$App->convert($_REQUEST['phone']);
					$data['mobile']			=	$App->convert($_REQUEST['mobile']);
					$data['eMail']			=	$App->convert($_REQUEST['email']);
					$data['website']		=	$App->convert($_REQUEST['website']);					
					$data['loginId']		=	$App->convert($uid);
							
					$success = $db->query_update(TABLE_ABOUT, $data,"ID=1");								
					$db->close();
					if($success)
					{
					$_SESSION['msg']="School Details Updated Successfully";	
					header("location:new.php");
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");
					}
			}
		break;			
}
?>
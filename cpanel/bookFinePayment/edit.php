<?php include("../adminHeader.php"); 
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}


$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();
?>

<script>
function valid()
{
flag=false;
maxLength=9;
jName = document.getElementById('bookName').value;
	if(jName=='')
	{
	document.getElementById('bookNamediv').innerHTML="Incorrect Book Number.";
	flag=true;
	}
jfineAmount=document.getElementById('fineAmount').value;	
	if(isNaN(jfineAmount)||jfineAmount==0)
	{		
	document.getElementById('fineAmountdiv').innerHTML="Amount should be valid";
	flag=true;
	}
		if(jfineAmount.length>maxLength)
		{																			
		document.getElementById('fineAmountdiv').innerHTML="Amount should be valid";
		flag=true;
		}	
	
	var numDay = document.getElementById('delayDays').value;
		if(isNaN(numDay))
		{
		document.getElementById('delayDaysdiv').innerHTML="Check Delaydays.";
		flag=true;
		}
			if(numDay.length>maxLength)
			{																			
			document.getElementById('delayDaysdiv').innerHTML="Check Delaydays.";
			flag=true;
			}
		
	if(flag==true)
	{
	return false;
	}
}

//clear the validation msg
function clearbox(Element_id)
{
document.getElementById(Element_id).innerHTML="";
}

//book no check-exist or not

$(document).ready(function() {
$("#bookNo").keyup(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var bookNo = $(this).val(); //get the string typed by user
   $("#user-result1").html('<img src="ajax-loader.gif" />');
   $.post('check_book.php', {'bookNo':bookNo}, function(data) { //make ajax call to check_username.php
   $("#user-result1").html(data); //dump the data received from PHP page
   });
});
});

</script>

<script type="text/javascript">
$(document).ready(function() {
 $("#bookNo").change(function() {    
	var value=$('#bookNo').val();   
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "bookName.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#bookName").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to load_page.php
	  
        type: "post",
        url: "bookIssueId.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#bookIssueId").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "adNo.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#adNo").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "name.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#name").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to load_page.php
	  
        type: "post",
        url: "delayDays.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#delayDays").val(response); 
            //alert(response);
        }

    });
	
	});
	 
});
</script>


<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$fid=$_REQUEST['id'];
        $fid = mysql_real_escape_string($fid);
	$tableEdit="SELECT ".TABLE_BOOKFINEPAYMENT.".ID,".TABLE_BOOKFINEPAYMENT.".bookIssueId,".TABLE_BOOK_REG.".bookNo,".TABLE_BOOK_REG.".bookName,".TABLE_BOOKFINEPAYMENT.".adNo,".TABLE_STUDENT.".name,".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_BOOK_ISSUE.".issueDate,".TABLE_BOOK_ISSUE.".returnDate,".TABLE_BOOKFINEPAYMENT.".fineAmount,".TABLE_BOOKFINEPAYMENT.".delayDays,".TABLE_BOOKFINEPAYMENT.".paymentDate,".TABLE_BOOKFINEPAYMENT.".remark FROM ".TABLE_STUDENT.",".TABLE_CLASS.",".TABLE_DIVISION.",".TABLE_BOOK_ISSUE.",".TABLE_BOOK_REG.",".TABLE_BOOKFINEPAYMENT." WHERE ".TABLE_BOOKFINEPAYMENT.".adNo=".TABLE_STUDENT.".adNo and ".TABLE_STUDENT.".class=".TABLE_CLASS.".ID and ".TABLE_STUDENT.".division=".TABLE_DIVISION.".ID AND ".TABLE_BOOKFINEPAYMENT.".bookIssueId=".TABLE_BOOK_ISSUE.".ID AND ".TABLE_BOOK_REG.".ID=".TABLE_BOOK_ISSUE.".bookId AND ".TABLE_BOOKFINEPAYMENT.".ID='$fid'";
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);

?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">BOOK FINE PAYMENT</h4>
            </div>
            <div class="modal-body clearfix">
			<form method="post" action="do.php?op=edit" class="form1" enctype="multipart/form-data" onsubmit="return valid()">
			<input type="hidden" name="fid" id="fid" value="<?php echo $fid ?>">
			             
                <div class="row">
                 
				 <div class="col-sm-6">
                    <div class="form-group">
                      <label for="bookNo">Book Number:<span class="star">*</span></label>
					  <input type="text" id="bookNo" name="bookNo" value="<?php echo $editRow['bookNo'];?>" placeholder="Book Number" required title="Book Number" class="form-control2"/>
					  <span id="user-result1"></span>
                    </div>
                    <div class="form-group">
                      <label for="bookName">Book Name: </label>
					  <input type="text" id="bookName" name="bookName" value="<?php echo $editRow['bookName'];?>" placeholder="Book Name" class="form-control2" readonly/>
					   
                    </div>
					<input type="hidden" name="bookIssueId" id="bookIssueId" value="<?php echo $editRow['bookIssueId'];?>">
                    <div class="form-group">
                      <label for="adNo">Admission Number:</label>
					  <input type="text" id="adNo" name="adNo" value="<?php echo $editRow['adNo'];?>"  placeholder="Admission Number" class="form-control2" readonly/>   
                    </div>					                   								 
                  
                    <div class="form-group">
                      <label for="name">Name:</label>
					  <input type="text" id="name" name="name" value="<?php echo $editRow['name'];?>"  placeholder="Name" class="form-control2 " readonly />
					  
                    </div>									  	
					
					<div class="form-group">
                      <label for="delayDays">Delay Days:</label>   
					  <input type="text" name="delayDays" id="delayDays" placeholder="Delaydays" value="<?php echo $editRow['delayDays'];?>" required class="form-control2" onfocus="return clearbox('delayDaysdiv')" >
					 <div id="delayDaysdiv" class="valid" style="color:#FF6600;"></div>
                    </div>									
					
					<div class="form-group">
                      <label for="fineAmount">Fine Amount:<span class="star">*</span></label>
					   <input type="text" id="fineAmount" name="fineAmount" value="<?php echo $editRow['fineAmount'];?>" required title="Fine Amount" placeholder="Fine Amount" class="form-control2" onfocus="return clearbox('fineAmountdiv')"> 
					   <div id="fineAmountdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
										
					<div class="form-groupy">
                      <label for="payDate">Payment Date:<span class="star">*</span></label>
					  <input type="text" id="payDate" name="payDate"   value="<?php echo date("d/m/Y");?>" required title="Payment Date"  class="form-control2 datepicker" />		  
                    </div>					
					
					<div class="form-groupy">
                      <label for="remark">Remark: <span class="star">*</span></label>
						 <textarea id="remark" name="remark" placeholder="Remark" class="form-control2"><?php echo $editRow['remark'];?></textarea>		  
                    </div>								
                    
				</div>					 																								
                </div>	
		</div>
	 <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="UPDATE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
<?php include("../adminFooter.php") ?>

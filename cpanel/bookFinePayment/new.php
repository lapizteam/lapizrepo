<?php include("../adminHeader.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$type		=	$_SESSION['LogType'];
$libraryEdit	=	$_SESSION['libraryEdit'];

$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

?>
<script>
function delete_type()
{
var del=confirm("Do you Want to Delete ?");
	if(del==true)
	{
	window.submit();
	}
	else
	{
	return false;
	}
}

//book no check-exist or not

$(document).ready(function() {
$("#bookNo").keyup(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var bookNo = $(this).val(); //get the string typed by user
   $("#user-result1").html('<img src="ajax-loader.gif" />');
   $.post('check_book.php', {'bookNo':bookNo}, function(data) { //make ajax call to check_username.php
   $("#user-result1").html(data); //dump the data received from PHP page
   });
});
});
</script>


<script type="text/javascript">
$(document).ready(function() {
	 
 $("#bookNo").change(function() {    
	var value=$('#bookNo').val();
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "bookName.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#bookName").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to load_page.php
	  
        type: "post",
        url: "bookIssueId.php", 
	
		data:{ value: value },   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#bookIssueId").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "adNo.php", 
	
		data:{ value: value},   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#adNo").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to paidamt.php
	  
        type: "post",
        url: "name.php", 
	
		data:{ value: value},   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#name").val(response); 
            //alert(response);
        }

    });
	
	$.ajax({    //create an ajax request to load_page.php
	  
        type: "post",
        url: "delayDays.php", 
	
		data:{ value: value},   
		 
     //expect html to be returned                
        success: function(response){ 
		      
            $("#delayDays").val(response); 
            //alert(response);
        }

    });
	
	});
	 
});
</script>
<script>
// validation
function valid()
{
flag=false;
maxLength=9;
jName = document.getElementById('bookName').value;
	if(jName=='')
	{
	document.getElementById('bookNamediv').innerHTML="Incorrect Book Number.";
	flag=true;
	}

jfineAmount=document.getElementById('fineAmount').value;	
	if(isNaN(jfineAmount)||jfineAmount==0)
	{		
	document.getElementById('fineAmountdiv').innerHTML="Amount should be valid";
	flag=true;
	}
		if(jfineAmount.length>maxLength)
		{																			
		document.getElementById('fineAmountdiv').innerHTML="Amount should be valid";
		flag=true;
		}
	
var numDay = document.getElementById('delayDays').value;
	if(isNaN(numDay))
	{
	document.getElementById('delayDaysdiv').innerHTML="Check Delaydays.";
	flag=true;
	}
		if(numDay.length>maxLength)
		{																			
		document.getElementById('delayDaysdiv').innerHTML="Check Delaydays";
		flag=true;
		}
	
	if(flag==true)
	{
	return false;
	}
}

//clear msg

function clearbox(Element_id)
{

document.getElementById(Element_id).innerHTML="";
}
</script>
<?php
 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';
?>
 
      <div class="col-md-10 col-sm-8 rightarea">
        <div class="row">
           <div class="col-sm-8"> 
          		<div class="clearfix">
					<h2 class="q-title">BOOK FINE PAYMENT</h2> 
					<a href="#" class="addnew"  data-toggle="modal" data-target="#myModal"> <span class="plus">+</span> ADD New</a> 
				</div>
          </div>
          <div class="col-sm-4">
            <form method="post">
              <div class="input-group">
			  <input id="searchadNo" name="searchadNo" value="<?php echo @$_POST['searchadNo'] ?>"  type="text" class="form-control" placeholder="Admission Number"/>
                <span class="input-group-btn">
                <button class="btn btn-default lens" type="submit"></button>
                </span> </div>
            </form>
          </div>
        </div>
 <?php	
$cond="1";
if(@$_REQUEST['searchadNo'])
{
	$cond=$cond." and ".TABLE_BOOK_ISSUE.".adNo like'%".$_POST['searchadNo']."%'";
}

?>
        <div class="row">
          <div class="col-sm-12">
            <div class="tablearea table-responsive">
              <table class="table view_limitter pagination_table" >
                <thead>
                  <tr >
				    <th>Sl No</th>               
					<th>BookNumber</th>
					<th>AdNo</th>
					<th>StudentName</th>
					<th>PaymentDate</th>
					<th>FineAmount</th>
										
                  </tr>
                </thead>
                <tbody>
				<?php 															
						$selAllQuery="SELECT ".TABLE_BOOKFINEPAYMENT.".ID,".TABLE_BOOK_REG.".bookNo,".TABLE_BOOK_REG.".bookName,".TABLE_BOOKFINEPAYMENT.".adNo,".TABLE_STUDENT.".name,".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_BOOK_ISSUE.".issueDate,".TABLE_BOOK_ISSUE.".returnDate,".TABLE_BOOKFINEPAYMENT.".fineAmount,".TABLE_BOOKFINEPAYMENT.".delayDays,".TABLE_BOOKFINEPAYMENT.".paymentDate,".TABLE_BOOKFINEPAYMENT.".remark FROM ".TABLE_STUDENT.",".TABLE_CLASS.",".TABLE_DIVISION.",".TABLE_BOOK_ISSUE.",".TABLE_BOOK_REG.",".TABLE_BOOKFINEPAYMENT." WHERE ".TABLE_BOOKFINEPAYMENT.".adNo=".TABLE_STUDENT.".adNo and ".TABLE_STUDENT.".class=".TABLE_CLASS.".ID and ".TABLE_STUDENT.".division=".TABLE_DIVISION.".ID AND ".TABLE_BOOKFINEPAYMENT.".bookIssueId=".TABLE_BOOK_ISSUE.".ID AND ".TABLE_BOOK_REG.".ID=".TABLE_BOOK_ISSUE.".bookId AND $cond ORDER BY ID DESC ";
						//echo $selAllQuery;
						$selectAll= $db->query($selAllQuery);
						$number=mysql_num_rows($selectAll);
						if($number==0)
						{
						?>
							 <tr>
								<td align="center" colspan="6">
									There is no data in list.
								</td>
							</tr>
						<?php
						}
						else
						{							
							$i=1;
							while($row=mysql_fetch_array($selectAll))
							{	
							$tableId=$row['ID'];
							?>
					  <tr>
					   <td><?php echo $i++;?>
						  <div class="adno-dtls"> <?php if($libraryEdit || $type=="Admin"){?> <a href="edit.php?id=<?php echo $tableId?>">EDIT</a> |<a href="do.php?id=<?php echo $tableId; ?>&op=delete" class="delete" onclick="return delete_type();">DELETE</a>  |<?php }?>  <a href="#" data-toggle="modal" data-target="#myModal3<?php echo $tableId; ?>"> VIEW</a> </div>
						  
							<!-- Modal3 -->
							  <div  class="modal fade" id="myModal3<?php echo $tableId; ?>" tabindex="-1" role="dialog">
								<div class="modal-dialog">
								  <div class="modal-content"> 
									<!-- <div class="modal-body clearfix">
								fddhdhdhddhdh
								  </div>-->	 		
									<div role="tabpanel" class="tabarea2"> 
									  
									  <!-- Nav tabs -->
									 <ul class="nav nav-tabs" role="tablist">									  
										<li role="presentation" class="active"> <a href="#personal<?php echo $tableId; ?>" aria-controls="personal" role="tab" data-toggle="tab">BOOK FINE PAYMENT</a> </li>										
									</ul>
									  
									  <!-- Tab panes -->
									  <div class="tab-content">
									  <div role="tabpanel" class="tab-pane active" id="personal<?php echo $tableId; ?>">
										  <table class="table nobg">
											<tbody style="background-color:#FFFFFF">
						
											   <tr>
												<td>Book Number</td>
												<td> : </td>
												<td><?php  echo $row['bookNo']; ?></td>
											  </tr>
											  <tr>
												<td>Book Name</td>
												<td> : </td>
												<td><?php echo $row['bookName']; ?></td>
											  </tr>
											  <tr>
												<td>Admission Number</td>
												<td> : </td>
												<td><?php  echo $row['adNo']; ?></td>
											  </tr>
											  <tr>
												<td>Name</td>
												<td> : </td>
												<td><?php  echo $row['name']; ?></td>
											  </tr>
											  <tr>
												<td>Class</td>
												<td> : </td>
												<td><?php echo $row['class']; ?>	</td>
											  </tr>
											  <tr>
												<td>Division</td>
												<td> : </td>
												<td><?php  echo $row['division']; ?></td>
											  </tr>
											   <tr>
												<td>Issued Date</td>
												<td> : </td>
												<td><?php echo $App->dbFormat_date_db($row['issueDate']); ?></td>
											  </tr>	
											   <tr>
												<td>Return Date</td>
												<td> : </td>
												<td><?php echo $App->dbFormat_date_db($row['returnDate']); ?></td>
											  </tr>
											   <tr>
												<td>Delay Days</td>
												<td> : </td>
												<td><?php echo $row['delayDays']; ?></td>
											  </tr>
											   <tr>
												<td>Fine Amount</td>
												<td> : </td>
												<td><?php echo $row['fineAmount']; ?></td>
											  </tr>
											   <tr>
												<td>Payment Date</td>
												<td> : </td>
												<td><?php echo $row['paymentDate']; ?></td>
											  </tr>
                                             <tr>
												<td>Remark</td>
												<td> : </td>
												<td><?php echo $row['remark']; ?></td>
											  </tr>
											</tbody>
										  </table>
										</div>
																				
									  </div>
									</div>
								  </div>
								</div>
							  </div>
							  <!-- Modal3 cls --> 
						  
						  
						  </td>
						<td><?php echo $row['bookNo']; ?></td>
						<td><?php echo $row['adNo']; ?></td>						
						<td><?php echo $row['name']; ?> </td>
						<td><?php echo $App->dbformat_date_db($row['paymentDate']); ?> </td>
						<td><?php echo $row['fineAmount']; ?> </td>                 	 	
					  </tr>
					  <?php }
				}?>                  
                </tbody>
              </table>
			  	 
            </div>	
			  <!-- paging -->		
			<div style="clear:both;"></div>
			   <div class="text-center">
			   		<div class="btn-group pager_selector"></div>
			   </div>
          	</div>
			  <!-- paging end-->
          </div>
        </div>
      </div>
      
      <!-- Modal1 -->
      <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">BOOK FINE PAYMENT </h4>
            </div>
            <div class="modal-body clearfix">
              <form action="do.php?op=new" class="form1" method="post" onsubmit="return valid()">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label for="bookNo">Book Number:<span class="star">*</span></label>
					  <input type="text" id="bookNo" name="bookNo" value="" placeholder="Book Number" required title="Book Number" class="form-control2" onfocus="return clearbox('bookNamediv')"/>
					  <span id="user-result1"></span><div id="bookNamediv" class="valid" style="color:#FF6600;"></div>
                    </div>
                    <div class="form-group">
                      <label for="bookName">Book Name: </label>
					  <input type="text" id="bookName" name="bookName" value="" placeholder="Book Name" class="form-control2" readonly/>
                    </div>
					<input type="hidden" name="bookIssueId" id="bookIssueId" value="">
                    <div class="form-group">
                      <label for="adNo">Admission Number:</label>
					  <input type="text" id="adNo" name="adNo" value=""  placeholder="Admission Number" class="form-control2" readonly/>   
                    </div>					                   								 
                  
                    <div class="form-group">
                      <label for="name">Name:</label>
					  <input type="text" id="name" name="name" value="" placeholder="Name" class="form-control2 " readonly />
					  
                    </div>									  	
					
					<div class="form-group">
                      <label for="delayDays">Delay Days:<span class="star">*</span></label>   
					  <input type="text" name="delayDays" id="delayDays" placeholder="Delay days" required class="form-control2" onfocus="return clearbox('delayDaysdiv')">
					 <div id="delayDaysdiv" class="valid" style="color:#FF6600;"></div>
                    </div>									
					
					<div class="form-group">
                      <label for="fineAmount">Fine Amount:<span class="star">*</span></label>
					   <input type="text" id="fineAmount" name="fineAmount" value="" required title="Fine Amount" placeholder="Fine Amount" class="form-control2" onfocus="return clearbox('fineAmountdiv')"> 
					   <div id="fineAmountdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
										
					<div class="form-groupy">
                      <label for="payDate">Payment Date:<span class="star">*</span></label>
					  <input type="text" id="payDate" name="payDate"   value="<?php echo date("d/m/Y");?>" required title="Payment Date"  class="form-control2 datepicker" />		  
                    </div>					
					
					<div class="form-groupy">
                      <label for="remark">Remark:</label>
						 <textarea id="remark" name="remark" value=""  placeholder="Remark" class="form-control2"></textarea>		  
                    </div>								
                    
				</div>
				 </div>
			  <div>
            </div>
            <div class="modal-footer">

              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			</form>
          </div>
        </div>
      </div>
	  </div>
      <!-- Modal1 cls --> 
     
	 
	 
	  
     
  </div>
<?php include("../adminFooter.php") ?>

<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{		
	
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['bookNo'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				/*$bookNo=$_REQUEST['bookNo'];
				$bookName=$_REQUEST['bookName'];
				$selectRes=mysql_query("SELECT ID FROM ".TABLE_BOOK_REG." WHERE bookNo='$bookNo' and bookName='$bookName'");
				$selectRow=mysql_fetch_array($selectRes);
				$ID=$selectRow['ID'];*/
				$success=0;
													
				$data['bookIssueId']			=	$App->convert($_REQUEST['bookIssueId']);
				$data['adNo']					=	$App->convert($_REQUEST['adNo']);
				$data['delayDays']				=	$App->convert($_REQUEST['delayDays']);											
				$data['fineAmount']				=	$App->convert($_REQUEST['fineAmount']);
				$data['paymentDate']			=	$App->dbformat_date($_REQUEST['payDate']);
				$data['remark']					=	$App->convert($_REQUEST['remark']);
				$data['loginId']				=	$App->convert($uid);
								
																	
												
				$success=$db->query_insert(TABLE_BOOKFINEPAYMENT, $data);								
				$db->close();
											
				if($success)
				{
				$_SESSION['msg']="Book Fine Paid successfully";					
				header("location:new.php");
				}
				else
				{
				$_SESSION['msg']="Failed";	
				header("location:new.php");					
				}
			}		
		break;
		
				
	// EDIT SECTION
	//-
	case 'edit':		
		$fid		=	$_REQUEST['fid'];   
		 
		if(!$_POST['bookNo'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				/*$bookNo=$_REQUEST['bookNo'];
				$bookName=$_REQUEST['bookName'];
				$selectRes=mysql_query("SELECT ID FROM ".TABLE_BOOK_REG." WHERE bookNo='$bookNo' and bookName='$bookName'");
				$selectRow=mysql_fetch_array($selectRes);
				$ID=$selectRow['ID'];*/
																			
				$data['bookIssueId']			=	$App->convert($_REQUEST['bookIssueId']);
				$data['adNo']					=	$App->convert($_REQUEST['adNo']);
				$data['delayDays']				=	$App->convert($_REQUEST['delayDays']);											
				$data['fineAmount']				=	$App->convert($_REQUEST['fineAmount']);
				$data['paymentDate']			=	$App->dbformat_date($_REQUEST['payDate']);
				$data['remark']					=	$App->convert($_REQUEST['remark']);
				$data['loginId']				=	$App->convert($uid);
								
																	
				$success=$db->query_update(TABLE_BOOKFINEPAYMENT, $data,"ID='{$fid}'");					
				$db->close();
				
				if($success)
				{
				$_SESSION['msg']="Book Fine Details updated successfully";					
				header("location:new.php");	
				}
				else
				{
				$_SESSION['msg']="Failed";	
				header("location:new.php");					
				}												
			}		
		break;		
	// DELETE SECTION
	
	case 'delete':		
				$id		=	$_REQUEST['id'];
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();								
				
				//$success=$sql = "DELETE FROM `".TABLE_BOOKFINEPAYMENT."` WHERE ID='$id'";
				//$db->query($sql);
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_BOOKFINEPAYMENT."` WHERE ID='$id'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}
				
				$db->close(); 				
				
				if($success)
				{
				$_SESSION['msg']="Book Fine Details deleted successfully";					
				header("location:new.php");	
				}
				else
				{
				$_SESSION['msg']="Failed";	
				header("location:new.php");					
				}			
		break;		
}
?>
<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid	=	$_SESSION['LogID'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	case 'new':
		
		if(!$_REQUEST['class'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");		
			}
		else
			{	
			$success =0;
			$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
			$db->connect();
			$totNumber=$_POST['totNumber'];
				if($totNumber==0)
				{
				$total_mark=$_POST['totalMark2'];
				$min_mark=$_POST['minMak2'];
				$examDate=$_POST['examDate2'];				
				}
				else
				{
				$total_mark=$_POST['totalMark1'];
				$min_mark=$_POST['minMak1'];
				$examDate=$_POST['examDate1'];
				}
	
			$id=0;
			$id=$_REQUEST['fid'];
			if($id>0)
			{		
			$db->query("DELETE FROM `".TABLE_MARKENTRY."` WHERE markBasicId='{$id}'");			
			$db->query("DELETE FROM `".TABLE_MARKBASIC."` WHERE ID='{$id}'");	
			}		
			
				$data['year']				=	$App->convert($_POST['acYear']);
				$data['examName']			=	$App->convert($_POST['examName']);
				$data['subject']			=	$App->convert($_POST['subject']);				
				$data['class']				=	$App->convert($_POST['class']);
				$data['division']			=	$App->convert($_POST['division']);
				$data['totalMark']			=	$App->convert($total_mark);
				$data['minMark']			=	$App->convert($min_mark);
				$data['markDate']		   =	$App->dbformat_date($examDate);
				$data['examentryId']		=	$App->convert($_POST['examentryId']);
				$data['loginId']			=	$App->convert($uid);
			
				$basicMarkId	=	$db->query_insert(TABLE_MARKBASIC, $data);
		
				
					$k=1;
					for($k=1;$k<=$_POST['leng'];$k++)
					{
					$a="adNo".$k;
					$m="mark".$k;
					$r="remark".$k;
					
					$mark=$_REQUEST[$m];
					 if($mark == 'A')
					 {
					 $mark=0;
					 }
					 else
					 {
					 $mark=$_REQUEST[$m];
					 }
					$percentage=$mark/$total_mark*100;
						if($percentage>=90)
						{
						$grade="A+";
						}
						else if($percentage>=80 && $percentage<90)
						{
						$grade="A";
						}
						else if($percentage>=70 && $percentage<80)
						{
						$grade="B+";
						}
						else if($percentage>=60 && $percentage<70)
						{
						$grade="B";
						}
						else if($percentage>=50 && $percentage<60)
						{
						$grade="C+";
						}
						else if($percentage>=40 && $percentage<50)
						{
						$grade="C";
						}
						else if($percentage>=30 && $percentage<40)
						{
						$grade="D+";
						}
						else if($percentage>=20 && $percentage<30)
						{
						$grade="D";
						}
						else 
						{
						$grade="FAIL";
						}
					
										
					  $sample['markBasicId']		=	$App->convert($basicMarkId);
					  $sample['adNo']				=	$App->convert($_REQUEST[$a]);
					  if($mark == 'A')
					  {
					  $sample['mark']				=	-1;
					  }
					  else
					  {
					  $sample['mark']				=	$App->convert($_REQUEST[$m]);
					  }
					  $sample['percentage']			=	$App->convert($percentage);
					  $sample['grade']				=	$App->convert($grade);
					  $sample['remark']				=	$App->convert($_REQUEST[$r]);
					  $sample['loginId']			=	$App->convert($uid);
					  
						$success	=	$db->query_insert(TABLE_MARKENTRY, $sample);								
				
					}	
						$db->close();
				if($success)	
				{
					$_SESSION['msg']="Mark Details added successfully";					
					header("location:new.php");
				}
				else
				{
					$_SESSION['msg']="Failed";					
					header("location:new.php");
				}
			}
		break;			
}
?>
<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['examType'])
			{					
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;
				
				$examType=ucfirst($_REQUEST['examType']);
				$flag=0;
				
				$res=mysql_query("SELECT * FROM ".TABLE_EXAMTYPES."");
				while($row=mysql_fetch_array($res))
				{
					$examTypeInTable=$row['examType'];
					if(strcasecmp($examType,$examTypeInTable)==0)
					{
					$flag=1;
					break;
					}
				}
				
				if($flag==1)
				{				
				$_SESSION['msg']="Exam Type Is Already Exist!";					
				header("location:new.php");	
				}
				
				else
				{
				$data['examType']		=	$App->convert($examType);			
							
				$success=$db->query_insert(TABLE_EXAMTYPES, $data);								
				$db->close();
				
				
				if($success)
					{
					$_SESSION['msg']="Exam type added successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}	
				}
				
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['examType'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$examType=ucfirst($_REQUEST['examType']);
				$flag=0;
				$success=0;
				
				$res=mysql_query("SELECT * FROM ".TABLE_EXAMTYPES." WHERE ID!='$fid'");
				while($row=mysql_fetch_array($res))
				{
					$examTypeInTable=$row['examType'];
					if(strcasecmp($examType,$examTypeInTable)==0)
					{
					$flag=1;
					break;
					}
				}
				
				if($flag==1)
				{				
				$_SESSION['msg']="Exam Type Is Already Exist!";					
				header("location:edit.php?id=$fid");
				}
				
				else
				{				
				$data['examType']		=	$App->convert($examType);			
							
				$success=$db->query_update(TABLE_EXAMTYPES, $data,"ID='{$fid}'");								
				$db->close();
				
				
				if($success)
					{
					$_SESSION['msg']="Exam Type Updated successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";
					header("location:new.php");					
					}	
				}
				
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				
				//$success=$db->query("DELETE FROM `".TABLE_EXAMTYPES."` WHERE ID='{$id}'");		
				
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_EXAMTYPES."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}						
				$db->close(); 	
				
				
				if($success)
					{
					$_SESSION['msg']="Exam Type deleted successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";
					header("location:new.php");					
					}				
		break;		
}
?>
<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid = $_SESSION['LogID'];

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['class'])
			{				
				$_SESSION['msg']="Error,Invalid Details!";					
				header("location:new.php");	
			}
		else
		{	
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success  = 0;
				$class 	= $_REQUEST['class'];
				$division = $_REQUEST['division'];
				$teacher  = $_REQUEST['teacher'];	
				$acYear   = $_REQUEST['year'];								
				$ExistStatus=$db->checkValueExist(TABLE_TEACHERALLOCATION,"class='$class' and division='$division' and acYear = '$acYear' ");				
				
			if($ExistStatus==1)
			{		
			$_SESSION['msg']="Already Allocated Teacher To This Class";					
			header("location:new.php");	
			}
			
			else
			{	
				$data['acYear']				 =	$App->convert($acYear);																				
				$data['class']				  =	$App->convert($class);
				$data['division']			   =	$App->convert($division);	
				$data['teacher']				=	$App->convert($teacher);							
				$data['loginId']				=	$App->convert($uid);													
												
				$success=$db->query_insert(TABLE_TEACHERALLOCATION, $data);								
				$db->close();
								
				if($success)
					{
					$_SESSION['msg']="Allocated Teacher to this Class Successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}		
			}
		}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid		=	$_REQUEST['fid'];   
		 
		if(!$_POST['class'])
			{			
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['acYear']				 =	$App->convert($_REQUEST['year']);																				
				$data['class']				  =	$App->convert($_REQUEST['class']);
				$data['division']			   =	$App->convert($_REQUEST['division']);	
				$data['teacher']				=	$App->convert($_REQUEST['teacher']);							
				$data['loginId']				=	$App->convert($uid);	
				
				$success=$db->query_update(TABLE_TEACHERALLOCATION, $data,"ID='{$fid}'");
				$db->close();				
				
				if($success)
					{
					$_SESSION['msg']="Class Teacher Allocation Details updated successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}														
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];			
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();								
				
				/*$sql = "DELETE FROM `".TABLE_BUSALLOCATION."` WHERE ID='$id'";
				$success=$db->query($sql);*/	
				
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_TEACHERALLOCATION."` WHERE ID='$id'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't delete. Because this data is used some where else";				            
				}
							
				$db->close(); 								
				
				if($success)
					{
					$_SESSION['msg']="Class Teacher Allocation Details deleted successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";	
					header("location:new.php");					
					}						
		break;		
}
?>
<?php include("../adminHeader.php"); 
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
?>
<script>
//for getting division
function getDivision(str) 
{
//alert(str);
  if (str=="") {
    document.getElementById("division").innerHTML="";
    return;
  }
  if (window.XMLHttpRequest) {
    // code for IE7+, Firefox, Chrome, Opera, Safari
    xmlhttp=new XMLHttpRequest();
  } else { // code for IE6, IE5
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
  xmlhttp.onreadystatechange=function() {
    if (xmlhttp.readyState==4 && xmlhttp.status==200) {
      document.getElementById("division").innerHTML=xmlhttp.responseText;
    }
  }
//  xmlhttp.open("GET","work_rate_1.php?q="+str+"&c="+client+"&df="+date_from+"&dt="+date_to,true);
   xmlhttp.open("GET","division.php?q="+str,true);
 
  xmlhttp.send();
}

//for checking teacher already allocated or not


$(document).ready(function() {
$("#teacher").change(function (e) { //user types username on inputfiled
	$(this).val($(this).val().replace(/\s/g, ''));
   var teacher = $(this).val(); //get the string typed by user
   var acYear = $('#year').val(); 
   $("#teacherdiv").html('<img src="ajax-loader.gif" />');
   $.post('check_teacher.php', {'teacher':teacher ,'acYear':acYear}, function(data) { //make ajax call to check_username.php
   $("#teacherdiv").html(data); //dump the data received from PHP page
   });
});
});


</script>
<?php
$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect();

 if(isset($_SESSION['msg'])){?><font color="red"><?php echo $_SESSION['msg']; ?></font><?php }	
 $_SESSION['msg']='';

	$fid = $_REQUEST['id'];
         
        $fid = mysql_real_escape_string($fid);

	$tableEdit = "SELECT ".TABLE_TEACHERALLOCATION.".ID,".TABLE_TEACHERALLOCATION.".acYear,".TABLE_TEACHERALLOCATION.".class tclass,".TABLE_TEACHERALLOCATION.".division tdivision,".TABLE_TEACHERALLOCATION.".teacher,".TABLE_ACADEMICYEAR.".fromYear,".TABLE_ACADEMICYEAR.".toYear,".TABLE_CLASS.".class,".TABLE_DIVISION.".division,".TABLE_STAFF.".name FROM ".TABLE_TEACHERALLOCATION.",".TABLE_ACADEMICYEAR.",".TABLE_CLASS.",".TABLE_DIVISION.",".TABLE_STAFF." WHERE ".TABLE_TEACHERALLOCATION.".acYear=".TABLE_ACADEMICYEAR.".ID AND ".TABLE_TEACHERALLOCATION.".class=".TABLE_CLASS.".ID AND ".TABLE_TEACHERALLOCATION.".division=".TABLE_DIVISION.".ID AND ".TABLE_TEACHERALLOCATION.".teacher = ".TABLE_STAFF.".ID AND ".TABLE_TEACHERALLOCATION.".ID='$fid'";
	
	$editField=mysql_query($tableEdit);
	$editRow=mysql_fetch_array($editField);
	$sclass = $editRow['tclass'];

?>

 
      <!-- Modal1 -->
      <div >
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <a class="close" href="new.php" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></a>
              <h4 class="modal-title">CLASS TEACHER ALLOCATION</h4>
            </div>
            <div class="modal-body clearfix">
			<form method="post" action="do.php?op=edit" class="form1"  onsubmit="return valid()">
            <input type="hidden" name="fid" id="fid" value="<?php echo $fid ?>">
                <div class="row">
                  <div class="col-sm-6">
				   <div class="form-group">
                      <label for="year">Academic Year:<span class="star">*</span></label>
                      <select name="year" id="year" required title="Academic Year" class="form-control2">                    	
						<?php 
						$select1="select * from ".TABLE_ACADEMICYEAR." ";
						$res=mysql_query($select1);
						while($row=mysql_fetch_array($res))
						{
						?>
                    	<option value="<?php echo $row['ID']?>" <?php if($editRow['acYear']==$row['ID']){?> selected="selected" <?php }?>><?php echo $row['fromYear']."-".$row['toYear']?></option>
						<?php 
						}
						?>
                    </select>								 
                    </div>
                    <div class="form-group">
                      <label for="class">Class:<span class="star">*</span></label>
                    	<select name="class" id="class" onChange="getDivision(this.value)" class="form-control2" required>
                    	<option value="">Select</option>
						<?php 
						$select2="select * from ".TABLE_CLASS."";
						$res2=mysql_query($select2);
						while($row2=mysql_fetch_array($res2))
						{
						?>
                    	<option value="<?php echo $row2['ID'];?>" <?php if($row2['ID']==$editRow['tclass']){?> selected="selected" <?php }?>><?php echo $row2['class']?></option>
						<?php 
						}
						?>
                    </select>
                    </div>	
					 <div class="form-group">
                      <label for="division">Division:<span class="star">*</span></label>                      
						<select name="division" id="division" class="form-control2" required>
                    		<option value="">Select</option>
                            <?php 
							$select3="select * from ".TABLE_DIVISION." where classId='$sclass'";
							$res3=mysql_query($select3);
							while($row3=mysql_fetch_array($res3))
							{
							?>
							<option value="<?php echo $row3['ID'];?>" <?php if($row3['ID']==$editRow['tdivision']){?> selected="selected" <?php }?>><?php echo $row3['division']?></option>
							<?php 
							}
							?>						
                   		 </select>					   
                    </div>	
                    					
				  	<div class="form-group">
                      <label for="teacher">Teacher:<span class="star">*</span></label>
                    	<select name="teacher" id="teacher"  class="form-control2" onChange="getTeacher(this.value)" onfocus="clearbox('teacherdiv')" required>
                    	<option value="">Select</option>
						<?php 
						$selectStaff="select * from ".TABLE_STAFF." where type='teaching'";
						$resStaff=mysql_query($selectStaff);
						while($rowStaff=mysql_fetch_array($resStaff))
						{
						?>
                    	<option value="<?php echo $rowStaff['ID']?>"  <?php if($rowStaff['ID']==$editRow['teacher']){?> selected="selected" <?php }?> ><?php echo $rowStaff['name']?></option>
						<?php 
						}
						?>
                    	</select>
                    <div id="teacherdiv" class="valid" style="color:#FF6600;"></div>
                    </div>
				</div>
			</div>
			  <div>
            </div>
            <div class="modal-footer">
              <input type="submit" name="save" id="save" value="SAVE" class="btn btn-primary continuebtn" />
            </div>
			
			</form>
          </div>
        </div>
      </div>
      <!-- Modal1 cls --> 
     
      
  </div>
  <script src="../../js/jquery-1.7.2.min.js"></script>
<script src="../../js/bootstrap.js"></script>
<script src="../../js/base.js"></script>
<script src="../../js/guidely/guidely.min.js"></script>
<?php include("../adminFooter.php") ?>

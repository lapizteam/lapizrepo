<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");
if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}
$uid=$_SESSION['LogID'];
$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['event'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");
			}
		else
			{		
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$eventId=0;
				
				$data['acYear']			=	$App->convert($_REQUEST['year']);
				$data['event']			=	$App->convert($_REQUEST['event']);
				$data['discription']	=	$App->convert($_REQUEST['discription']);
				$data['showStatus']		=	$App->convert($_REQUEST['showStatus']);
				$data['adNo']			=	$App->convert($_REQUEST['adNo']);
				$data['addDate']		=	date("Y-m-d");
				$data['loginId']		=	$App->convert($uid);
						
				$eventId=$db->query_insert(TABLE_NOTIFICATION, $data);
				
				if($_REQUEST['showStatus']=='ClassWise')	
				{
						
						for($k=1;$k<=$_POST['leng'];$k++)
						{
						$class="class".$k;
						$classId=$_POST[$class];
						$status="status".$k;
						$status=$_POST[$status];
						
							if($status=='yes')
							{
						
							 $sample['eventId']			=	$App->convert($eventId);						
							 $sample['classId']			=	$App->convert($classId);										  
							 $sample['loginId']			=	$App->convert($uid);
						  
					 $db->query_insert(TABLE_NOTIFICATIONCLASS, $sample); 
							}											
						}																	
			
				}							
				$db->close();
								
					if($eventId)
					{					
					$_SESSION['msg']="Notification added successfully";																							
					}
					else
					{
					$_SESSION['msg']="Failed";								
					}
					header("location:new.php");		
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['event'])
			{				
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				$success=0;				
				
				$data['acYear']			=	$App->convert($_REQUEST['year']);
				$data['event']			=	$App->convert($_REQUEST['event']);
				$data['discription']	=	$App->convert($_REQUEST['discription']);
				$data['showStatus']		=	$App->convert($_REQUEST['showStatus']);
				$data['adNo']			=	$App->convert($_REQUEST['adNo']);
				$data['addDate']		=	date("Y-m-d");
				$data['loginId']		=	$App->convert($uid);
						
				$success=$db->query_update(TABLE_NOTIFICATION, $data,"ID='{$fid}'");
				
				$sql = "DELETE FROM `".TABLE_NOTIFICATIONCLASS."` WHERE eventId='$fid'";
				$db->query($sql);
				
				if($_REQUEST['showStatus']=='ClassWise')	
				{
						
						for($k=1;$k<=$_POST['leng'];$k++)
						{
						$class="class".$k;
						$classId=$_POST[$class];
						$status="status".$k;
						$status=$_POST[$status];
						
							if($status=='yes')
							{
						
							 $sample['eventId']			=	$App->convert($fid);						
							 $sample['classId']			=	$App->convert($classId);										  
							 $sample['loginId']			=	$App->convert($uid);
						
					 $db->query_insert(TABLE_NOTIFICATIONCLASS, $sample); 
							}											
						}																				
				}	
				$db->close();								
				if($success)
					{					
					$_SESSION['msg']="Notification updated successfully";																							
					}
					else
					{
					$_SESSION['msg']="Failed";								
					}
					header("location:new.php");					
			}
		break;	
		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];
				$success=0;
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();	
				try
				{			
				$success= @mysql_query("DELETE FROM `".TABLE_NOTIFICATIONCLASS."` WHERE eventId='{$id}'");	
				mysql_query("DELETE FROM `".TABLE_NOTIFICATION."` WHERE ID='{$id}'");			      
			  	}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}							
				
				//$db->query("DELETE FROM `".TABLE_NOTIFICATIONCLASS."` WHERE eventId='{$id}'");																
				//$success=$db->query("DELETE FROM `".TABLE_NOTIFICATION."` WHERE ID='{$id}'");								
				$db->close();	
															
				if($success)
					{										
					$_SESSION['msg']="Notification deleted successfully";																				
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";								
					}
					header("location:new.php");				
		break;		
}
?>
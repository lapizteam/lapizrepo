<?php
require("../../config/config.inc.php"); 
require("../../config/Database.class.php");
require("../../config/Application.class.php");

if($_SESSION['LogID']=="")
{
header("location:../../logout.php");
}

$optype=(strtolower(empty($_POST['op']))) ? ((strtolower(empty($_GET['op']))) ? $_REQUEST['op'] : $_GET['op']) : $_POST['op'];

switch($optype)
{
	// NEW SECTION
	//-
	case 'new':
		
		if(!$_REQUEST['category'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:new.php");	
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['category']		=	$App->convert($_REQUEST['category']);
				$success=0;
						
				$success=$db->query_insert(TABLE_BOOK_CATEGORY, $data);								
				$db->close();
				
				if($success)
					{
					$_SESSION['msg']="Book Category Details added successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}					
			}		
		break;		
	// EDIT SECTION
	//-
	case 'edit':		
		$fid	=	$_REQUEST['fid'];  		    
		if(!$_POST['category'])
			{
				$_SESSION['msg']="Error, Invalid Details!";					
				header("location:edit.php?id=$fid");		
			}
		else
			{				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();
				
				$data['category']		=	$App->convert($_REQUEST['category']);
				$success=0;
				
				$success=$db->query_update(TABLE_BOOK_CATEGORY, $data, " ID='{$fid}'");					
     			$db->close();
				
				if($success)
					{
					$_SESSION['msg']="Book Category Details updated successfully";					
					header("location:new.php");		
					}
					else
					{
					$_SESSION['msg']="Failed";	
					header("location:new.php");					
					}					 															
			}		
		break;		
	// DELETE SECTION
	//-
	case 'delete':		
				$id	=	$_REQUEST['id'];				
				
				$db = new Database(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
				$db->connect();				
				$success=0;
				
				/*$success=$db->query("DELETE FROM `".TABLE_BOOK_CATEGORY."` WHERE ID='{$id}'");								
				$db->close(); */
				try
				{
				$success= @mysql_query("DELETE FROM `".TABLE_BOOK_CATEGORY."` WHERE ID='{$id}'");				      
				}
				catch (Exception $e) 
				{
					 $_SESSION['msg']="You can't edit. Because this data is used some where else";				            
				}	
				
				if($success)
					{
					$_SESSION['msg']="Book Category Details deleted successfully";					
					header("location:new.php");	
					}
					else
					{
					$_SESSION['msg']="You can't edit. Because this data is used some where else";	
					header("location:new.php");					
					}											
		break;		
}
?>
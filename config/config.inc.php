<?php
//database server
define('DB_SERVER', "localhost");
define('DB_USER', "bodhiinf_android");
define('DB_PASS', "07210721");
//database name
define('DB_DATABASE', "bodhiinf_dyfi");

//smart to define your table names also
define('TABLE_ADMIN', "login");

define('TABLE_CLASS', "class");
define('TABLE_DIVISION', "division");
define('TABLE_SUBJECT', "subject");
define('TABLE_STUDENT', "student");
define('TABLE_DISTRICT', "district");

define('TABLE_STAFF', "staff");
define('TABLE_SALARYSETTING', "salarysetting");
define('TABLE_STAFFPAYMENT', "staffpayment");
define('TABLE_TEACHERALLOCATION', "teacherallocation");
define('TABLE_TRANSACTION', "transaction");

define('TABLE_FEESETTING', "feesetting");
define('TABLE_SETINSTALMENT', "setinstalment");
define('TABLE_FEEPAYMENT', "feepayment");
define('TABLE_MESSFEESETTING', "messfeesetting");
define('TABLE_CLASSMESSFEE', "classmessfee");
define('TABLE_MESSFEEPAYMENT', "messfeepayment");

define('TABLE_LEDGER', "ledger");
define('TABLE_ACADEMICYEAR', "academicyear");
define('TABLE_MONTH', "acmonth");
define('TABLE_WORKINGDAYS', "workingdays");
define('TABLE_ATTENDANCE', "attendance");

define('TABLE_EXAMTYPES', "examtypes");
define('TABLE_EXAMENTRY', "examentry");
define('TABLE_MARKBASIC', "markbasic");
define('TABLE_MARKENTRY', "markentry");

define('TABLE_BOOK_REG', "bookreg");
define('TABLE_BOOK_CATEGORY', "book_category");
define('TABLE_BOOK_ISSUE', "book_issue");
define('TABLE_RETURN_DAYS', "book_return_days");
define('TABLE_RACK_REG', "rack_reg");
define('TABLE_BOOKFINEPAYMENT', "bookfinepayment");

define('TABLE_VEHICLEREG', "vehiclereg");
define('TABLE_STAGEDETAILS', "stagedetails");
define('TABLE_BUSALLOCATION', "busallocation");
define('TABLE_BUSFEEPAYMENT', "busfeepayment");
define('TABLE_ACCOUNT_GROUP', "account_group");
define('TABLE_ABOUT', "about");
define('TABLE_TCISSUE', "tcissue");
define('TABLE_NOTIFICATION', "notification");
define('TABLE_NOTIFICATIONCLASS', "notificationclass");
define('TABLE_PRINTSETTINGS', "printsettings");
define('TABLE_PROMOTION', "promotion");
define('TABLE_USERPERMISSION', "userpermission");

define('TABLE_EVENT', "event");
define('TABLE_STAFFNOTIFICATION', "staffnotification");
define('TABLE_TEACHERCHAT',"teacherchat");
define('TABLE_REPLY',"reply");
define('TABLE_CHATCLASS',"chatclass");

define('TABLE_CLUB', "club");
define('TABLE_CLUB_STUDENTS', "club_students");
?>